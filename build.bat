@echo off

call SemanticVersion.bat

set MSBUILD_PREVIEW="%ProgramFiles%\Microsoft Visual Studio\2022\Preview\MSBuild\Current\Bin\msbuild.exe"
set MSBUILD_COMMUNITY="%ProgramFiles%\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\msbuild.exe"
set MSBUILD_PROFESSIONAL="%ProgramFiles%\Microsoft Visual Studio\2022\Professional\MSBuild\Current\Bin\msbuild.exe"
set MSBUILD_ENTERPRISE="%ProgramFiles%\Microsoft Visual Studio\2022\Enterprise\MSBuild\Current\Bin\msbuild.exe"

if exist %MSBUILD_PREVIEW% (set MSBUILD=%MSBUILD_PREVIEW%) else (
if exist %MSBUILD_COMMUNITY% (set MSBUILD=%MSBUILD_COMMUNITY%) else (
if exist %MSBUILD_PROFESSIONAL% (set MSBUILD=%MSBUILD_PROFESSIONAL%) else (
if exist %MSBUILD_ENTERPRISE% (set MSBUILD=%MSBUILD_ENTERPRISE%) else (
	echo msbuild not found
	pause
	exit /b
))))

%MSBUILD% Builds\VisualStudio2022\Shrimp.sln /property:Configuration=Release_SSE2
%MSBUILD% Builds\VisualStudio2022\Shrimp.sln /property:Configuration=Release_AVX
%MSBUILD% Builds\VisualStudio2022\Shrimp.sln /property:Configuration=Release_AVX512
%MSBUILD% Tools\ShrimpStarter\Builds\VisualStudio2022\ShrimpStarter.sln /property:Configuration=Release
%MSBUILD% Tools\Benchmark\Builds\VisualStudio2022\Benchmark.sln /property:Configuration=Release_Starter
%MSBUILD% Tools\Benchmark\Builds\VisualStudio2022\Benchmark.sln /property:Configuration=Release_SSE2
%MSBUILD% Tools\Benchmark\Builds\VisualStudio2022\Benchmark.sln /property:Configuration=Release_AVX
%MSBUILD% Tools\Benchmark\Builds\VisualStudio2022\Benchmark.sln /property:Configuration=Release_AVX512

xcopy /y /d "Dependencies\libsofa\win64\dlls\*" "Deploy\bin\"
xcopy /y /d "Builds\VisualStudio2022\x64\Release_SSE2\App\*.exe" "Deploy\bin\"
xcopy /y /d "Builds\VisualStudio2022\x64\Release_AVX\App\*.exe" "Deploy\bin\"
xcopy /y /d "Builds\VisualStudio2022\x64\Release_AVX512\App\*.exe" "Deploy\bin\"
xcopy /y /d "Tools\ShrimpStarter\Builds\VisualStudio2022\x64\Release\App\*.exe" "Deploy\"
xcopy /y /d "Tools\Benchmark\Builds\VisualStudio2022\x64\Release_Starter\ConsoleApp\*.exe" "Deploy\"
xcopy /y /d "Tools\Benchmark\Builds\VisualStudio2022\x64\Release_SSE2\ConsoleApp\*.exe" "Deploy\bin\"
xcopy /y /d "Tools\Benchmark\Builds\VisualStudio2022\x64\Release_AVX\ConsoleApp\*.exe" "Deploy\bin\"
xcopy /y /d "Tools\Benchmark\Builds\VisualStudio2022\x64\Release_AVX512\ConsoleApp\*.exe" "Deploy\bin\"

pause
