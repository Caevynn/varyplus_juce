#pragma once

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

class FFTCoreFinal
{
    using Real = float;
    using Complex = RTC::Complex<Real>;
    using Simd = RTC::Simd<Real, RTC_SIMD_SIZE>;
    using SimdComplex = RTC::SimdComplex<Real, RTC_SIMD_SIZE>;
	using Data = RTC::Range<Complex, RTC::Regular>;

    using SwapIndex = uint16_t;
    using SwapPair = std::pair<SwapIndex, SwapIndex>;
public:
	FFTCoreFinal(size_t dataSize) : nFFT(dataSize)
	{
		nStages = 1;
		for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

		twiddle.allocate(nFFT);
		for (size_t iStage = 0; iStage < nStages; ++iStage)
		{
			size_t nButterflies = 1ull << iStage;

			auto rt = twiddle.real().range(nButterflies, nButterflies);
			auto it = twiddle.imag().range(nButterflies, nButterflies);

			for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
			{
				double angle = M_PI * iButterfly / nButterflies;
				rt[iButterfly] = static_cast<Real>(std::cos(angle));
				it[iButterfly] = static_cast<Real>(-std::sin(angle));
			}
		}
	}

	void run(Data& data)
	{
		butterflies(data);
	}

private:
	RTC::Array<Complex> twiddle;
	size_t nFFT, nStages;

	void butterflies(Data& dataIn)
	{
        auto data = dataIn.data();

		if (nStages > 2)
		{
            // Stages 1 - 3

            RTC::SimdComplex<float, 4> ct(&twiddle[4]);

            for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
            {
                size_t i = 8 * iGroup;

                RTC::Complex<float> ci0(data[i + 0]);
                RTC::Complex<float> ci1(data[i + 1]);
                RTC::Complex<float> ci2(data[i + 2]);
                RTC::Complex<float> ci3(data[i + 3]);
                RTC::Complex<float> ci4(data[i + 4]);
                RTC::Complex<float> ci5(data[i + 5]);
                RTC::Complex<float> ci6(data[i + 6]);
                RTC::Complex<float> ci7(data[i + 7]);

                RTC::SimdComplex<float, 4> c0(
                    (ci0 + ci1) + (ci2 + ci3),
                    (ci0 - ci1) + (ci2 - ci3).rotateCW(),
                    (ci0 + ci1) - (ci2 + ci3),
                    (ci0 - ci1) - (ci2 - ci3).rotateCW()
                );

                RTC::SimdComplex<float, 4> c4(
                    (ci4 + ci5) + (ci6 + ci7),
                    (ci4 - ci5) + (ci6 - ci7).rotateCW(),
                    (ci4 + ci5) - (ci6 + ci7),
                    (ci4 - ci5) - (ci6 - ci7).rotateCW()
                );

                RTC::SimdComplex<float, 4> c4tmp = c4 * ct;

                (c0 + c4tmp).store(&data[i]);
                (c0 - c4tmp).store(&data[i + 4]);
            }
		}

        size_t startStage = 3;

#if RTC_SIMD_SIZE == 4
        if(nStages % 2 == 0)
        {
            // Stage 4

            SimdComplex ct0(&twiddle[8]);
            SimdComplex ct1(&twiddle[12]);

            for(size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
            {
                size_t i0 = 16 * iGroup;
                size_t i1 = i0 + 4;
                size_t i2 = i0 + 8;
                size_t i3 = i0 + 12;

                SimdComplex c0(&data[i0]);
                SimdComplex c1(&data[i1]);
                SimdComplex c2(&data[i2]);
                SimdComplex c3(&data[i3]);

                SimdComplex c2tmp = c2 * ct0;
                SimdComplex c3tmp = c3 * ct1;

                (c0 + c2tmp).store(&data[i0]);
                (c1 + c3tmp).store(&data[i1]);
                (c0 - c2tmp).store(&data[i2]);
                (c1 - c3tmp).store(&data[i3]);
            }

            ++startStage;
        }
#elif RTC_SIMD_SIZE == 8
        if (nStages == 4)
        {
            // Stage 4

            SimdComplex ct(&twiddle[8]);

            for(size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
            {
                size_t i0 = 16 * iGroup;
                size_t i1 = i0 + 8;

                SimdComplex c0(&data[i0]);
                SimdComplex c1(&data[i1]);

                SimdComplex c1tmp = c1 * ct;

                (c0 + c1tmp).store(&data[i0]);
                (c0 - c1tmp).store(&data[i1]);
            }

            return;
        }

		if (nStages > 4)
		{
            // Stages 4 - 5

			SimdComplex ct0(&twiddle[8]);
			SimdComplex ct10(&twiddle[16]);
			SimdComplex ct11(&twiddle[24]);

			for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
			{
				size_t i0 = 32 * iGroup;
				size_t i1 = i0 + 8;
				size_t i2 = i0 + 16;
				size_t i3 = i0 + 24;

				SimdComplex c0(&data[i0]);
				SimdComplex c1(&data[i1]);
				SimdComplex c2(&data[i2]);
				SimdComplex c3(&data[i3]);

				SimdComplex c1tmp0 = c1 * ct0;
				SimdComplex c3tmp0 = c3 * ct0;
				SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
				SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;
				
			    (c0 + c1tmp0 + c2tmp1).store(&data[i0]);
				(c0 - c1tmp0 + c3tmp1).store(&data[i1]);
				(c0 + c1tmp0 - c2tmp1).store(&data[i2]);
				(c0 - c1tmp0 - c3tmp1).store(&data[i3]);
            }

            startStage += 2;
		}

        if(nStages % 2 == 0)
        {
            // Stage 6

            for(size_t iGroup = 0; iGroup < nFFT / 64; ++iGroup)
            {
                for (size_t iButterfly = 0; iButterfly < 32; iButterfly += Simd::Size)
                {
                    size_t i0 = 64 * iGroup + iButterfly;
                    size_t i1 = i0 + 32;

                    SimdComplex ct(&twiddle[32 + iButterfly]);

                    SimdComplex c0(&data[i0]);
                    SimdComplex c1(&data[i1]);

                    SimdComplex c1tmp = c1 * ct;

                    (c0 + c1tmp).store(&data[i0]);
                    (c0 - c1tmp).store(&data[i1]);
                }
            }

            ++startStage;
        }
#elif RTC_SIMD_SIZE == 16
        if (nStages == 4)
        {
            // Stage 4

            RTC::SimdComplex<float, 8> ct(&twiddle[8]);

            for(size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
            {
                size_t i0 = 16 * iGroup;
                size_t i1 = i0 + 8;

                RTC::SimdComplex<float, 8> c0(&data[i0]);
                RTC::SimdComplex<float, 8> c1(&data[i1]);

                RTC::SimdComplex<float, 8> c1tmp = c1 * ct;

                (c0 + c1tmp).store(&data[i0]);
                (c0 - c1tmp).store(&data[i1]);
            }

            return;
        }

		if (nStages > 4)
		{
            // Stages 4 - 5

			RTC::SimdComplex<float, 8> ct0(&twiddle[8]);
			RTC::SimdComplex<float, 16> ct1(&twiddle[16]);

			for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
			{
				size_t i0 = 32 * iGroup;
				size_t i1 = i0 + 8;
				size_t i2 = i0 + 16;
				size_t i3 = i0 + 24;

				RTC::SimdComplex<float, 8> c0(&data[i0]);
				RTC::SimdComplex<float, 8> c1(&data[i1]);
				RTC::SimdComplex<float, 8> c2(&data[i2]);
				RTC::SimdComplex<float, 8> c3(&data[i3]);

				RTC::SimdComplex<float, 8> c1tmp0 = c1 * ct0;
				RTC::SimdComplex<float, 8> c3tmp0 = c3 * ct0;

                SimdComplex c01(c0 + c1tmp0, c0 - c1tmp0);
                SimdComplex c23(c2 + c3tmp0, c2 - c3tmp0);

				SimdComplex c23tmp1 = c23 * ct1;
				
				(c01 + c23tmp1).store(&data[i0]);
				(c01 - c23tmp1).store(&data[i2]);
            }

            startStage += 2;
		}

        if(nStages % 2 == 0)
        {
            // Stage 6

            SimdComplex ct0(&twiddle[32]);
            SimdComplex ct1(&twiddle[48]);

            for(size_t iGroup = 0; iGroup < nFFT / 64; ++iGroup)
            {
                size_t i0 = 64 * iGroup;
                size_t i1 = i0 + 16;
                size_t i2 = i0 + 32;
                size_t i3 = i0 + 48;

                SimdComplex c0(&data[i0]);
                SimdComplex c1(&data[i1]);
                SimdComplex c2(&data[i2]);
                SimdComplex c3(&data[i3]);

                SimdComplex c2tmp = c2 * ct0;
                SimdComplex c3tmp = c3 * ct1;

                (c0 + c2tmp).store(&data[i0]);
                (c1 + c3tmp).store(&data[i1]);
                (c0 - c2tmp).store(&data[i2]);
                (c1 - c3tmp).store(&data[i3]);
            }

            ++startStage;
        }
#endif

        // Remaining stages

        for (size_t iStage = startStage; iStage < nStages; iStage += 2)
		{
            size_t nGroups = 1ull << (nStages - iStage - 1);
            size_t nButterflies = 1ull << iStage;

            assert(nGroups * nButterflies == nFFT / 2);
            assert(twiddle.size() >= 4 * nButterflies);

            for(size_t iGroup = 0; iGroup < nGroups / 2; ++iGroup)
            {
                assert(nButterflies % Simd::Size == 0);

                for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
                {
                    size_t i0 = 4 * nButterflies * iGroup + iButterfly;
                    size_t i1 = i0 + nButterflies;
                    size_t i2 = i0 + 2 * nButterflies;
                    size_t i3 = i0 + 3 * nButterflies;

                    SimdComplex ct0(&twiddle[nButterflies + iButterfly]);
                    SimdComplex ct10(&twiddle[2 * nButterflies + iButterfly]);
                    SimdComplex ct11(&twiddle[3 * nButterflies + iButterfly]);
                    
                    SimdComplex c0(&data[i0]);
                    SimdComplex c1(&data[i1]);
                    SimdComplex c2(&data[i2]);
                    SimdComplex c3(&data[i3]);
                    
                    SimdComplex c1tmp0 = c1 * ct0;
                    SimdComplex c3tmp0 = c3 * ct0;
                    SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
                    SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;
                    
                    (c0 + c1tmp0 + c2tmp1).store(&data[i0]);
                    (c0 - c1tmp0 + c3tmp1).store(&data[i1]);
                    (c0 + c1tmp0 - c2tmp1).store(&data[i2]);
                    (c0 - c1tmp0 - c3tmp1).store(&data[i3]);
                }
            }
		}
	}
};
