#pragma once

#include "Test.hpp"

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCVector.hpp"

#include <utility>

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(name) name(swaps, name ## Result)

class FFTBitReversal
{
	using Container = RTC::Array<RTC::Complex<float>>;
	using PairType = unsigned;
	using PairContainer = RTC::Vector<std::pair<PairType, PairType>>;
public:
	static void run(size_t dataSize, size_t nRepetitions, size_t nMeasurements)
	{
		std::cout << "FFTBitReversal::run(" << dataSize << ", " << nMeasurements << ")" << std::endl;

		TIME_AND_RESULT(loop);
		TIME_AND_RESULT(pre);
		TIME_AND_RESULT(fixed);

		Container input;
		input.allocate(dataSize);

		PairContainer swaps;
		swaps.allocate(dataSize / 2);

		for (size_t i = 0, j = 0; i < dataSize - 1; ++i)
		{
			if (i < j)
			{
				swaps.push_back({ static_cast<PairType>(i), static_cast<PairType>(j) });
				/*if (dataSize == 256)
				{
					std::cout << "swap(" << i << ", " << j << ");" << std::endl;
				}*/
			}

			size_t k = dataSize / 2;
			while (k <= j) { j -= k; k /= 2; }
			j += k;
		}

		Random random;

		for (size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
		{
			random.fill(input);

			loopResult.copy(input);
			preResult.copy(input);
			fixedResult.copy(input);

			RUN_TEST(loop);
			RUN_TEST(pre);
			RUN_TEST(fixed);

			compare("pre", loopResult, preResult);
			compare("fixed", loopResult, fixedResult);
		}

		print("loop", loopTime);
		print("pre", preTime);
		print("fixed", fixedTime);
	}

private:
	static void loop(const PairContainer&, Container& data)
	{
		size_t nFFT = data.size();

		auto real = data.real();
		auto imag = data.imag();

		for (size_t i = 0, j = 0; i < nFFT - 1; ++i)
		{
			if (i < j)
			{
				std::swap(real[i], real[j]);
				std::swap(imag[i], imag[j]);
			}

			// Funnily enough, this while loop ssems to be the fastest way for bit inversion
			size_t k = nFFT / 2;
			while (k <= j) { j -= k; k /= 2; }
			j += k;
		}
	}

	static void pre(const PairContainer& swaps, Container& data)
	{
		auto real = data.real();
		auto imag = data.imag();

		auto swap = [&real, &imag](unsigned first, unsigned second)
			{
				std::swap(real[first], real[second]);
				std::swap(imag[first], imag[second]);
			};

		for (const auto& [first, second] : swaps)
		{
			swap(first, second);
		}
	}

	static void fixed(const PairContainer& swaps, Container& data)
	{
		auto real = data.real();
		auto imag = data.imag();

		auto swap = [&real, &imag](unsigned first, unsigned second)
			{
				std::swap(real[first], real[second]);
				std::swap(imag[first], imag[second]);
			};

		if (data.size() == 8)
		{
			swap(1, 4);
			swap(3, 6);
		}
		else if (data.size() == 16)
		{
			swap(1, 8);
			swap(2, 4);
			swap(3, 12);
			swap(5, 10);
			swap(7, 14);
			swap(11, 13);
		}
		else if (data.size() == 32)
		{
			swap(1, 16);
			swap(2, 8);
			swap(3, 24);
			swap(5, 20);
			swap(6, 12);
			swap(7, 28);
			swap(9, 18);
			swap(11, 26);
			swap(13, 22);
			swap(15, 30);
			swap(19, 25);
			swap(23, 29);
		}
		else if (data.size() == 64)
		{
			swap(1, 32);
			swap(2, 16);
			swap(3, 48);
			swap(4, 8);
			swap(5, 40);
			swap(6, 24);
			swap(7, 56);
			swap(9, 36);
			swap(10, 20);
			swap(11, 52);
			swap(13, 44);
			swap(14, 28);
			swap(15, 60);
			swap(17, 34);
			swap(19, 50);
			swap(21, 42);
			swap(22, 26);
			swap(23, 58);
			swap(25, 38);
			swap(27, 54);
			swap(29, 46);
			swap(31, 62);
			swap(35, 49);
			swap(37, 41);
			swap(39, 57);
			swap(43, 53);
			swap(47, 61);
			swap(55, 59);
		}
		else if (data.size() == 128)
		{
			swap(1, 64);
			swap(2, 32);
			swap(3, 96);
			swap(4, 16);
			swap(5, 80);
			swap(6, 48);
			swap(7, 112);
			swap(9, 72);
			swap(10, 40);
			swap(11, 104);
			swap(12, 24);
			swap(13, 88);
			swap(14, 56);
			swap(15, 120);
			swap(17, 68);
			swap(18, 36);
			swap(19, 100);
			swap(21, 84);
			swap(22, 52);
			swap(23, 116);
			swap(25, 76);
			swap(26, 44);
			swap(27, 108);
			swap(29, 92);
			swap(30, 60);
			swap(31, 124);
			swap(33, 66);
			swap(35, 98);
			swap(37, 82);
			swap(38, 50);
			swap(39, 114);
			swap(41, 74);
			swap(43, 106);
			swap(45, 90);
			swap(46, 58);
			swap(47, 122);
			swap(49, 70);
			swap(51, 102);
			swap(53, 86);
			swap(55, 118);
			swap(57, 78);
			swap(59, 110);
			swap(61, 94);
			swap(63, 126);
			swap(67, 97);
			swap(69, 81);
			swap(71, 113);
			swap(75, 105);
			swap(77, 89);
			swap(79, 121);
			swap(83, 101);
			swap(87, 117);
			swap(91, 109);
			swap(95, 125);
			swap(103, 115);
			swap(111, 123);
		}
		else if (data.size() == 256)
		{
			swap(1, 128);
			swap(2, 64);
			swap(3, 192);
			swap(4, 32);
			swap(5, 160);
			swap(6, 96);
			swap(7, 224);
			swap(8, 16);
			swap(9, 144);
			swap(10, 80);
			swap(11, 208);
			swap(12, 48);
			swap(13, 176);
			swap(14, 112);
			swap(15, 240);
			swap(17, 136);
			swap(18, 72);
			swap(19, 200);
			swap(20, 40);
			swap(21, 168);
			swap(22, 104);
			swap(23, 232);
			swap(25, 152);
			swap(26, 88);
			swap(27, 216);
			swap(28, 56);
			swap(29, 184);
			swap(30, 120);
			swap(31, 248);
			swap(33, 132);
			swap(34, 68);
			swap(35, 196);
			swap(37, 164);
			swap(38, 100);
			swap(39, 228);
			swap(41, 148);
			swap(42, 84);
			swap(43, 212);
			swap(44, 52);
			swap(45, 180);
			swap(46, 116);
			swap(47, 244);
			swap(49, 140);
			swap(50, 76);
			swap(51, 204);
			swap(53, 172);
			swap(54, 108);
			swap(55, 236);
			swap(57, 156);
			swap(58, 92);
			swap(59, 220);
			swap(61, 188);
			swap(62, 124);
			swap(63, 252);
			swap(65, 130);
			swap(67, 194);
			swap(69, 162);
			swap(70, 98);
			swap(71, 226);
			swap(73, 146);
			swap(74, 82);
			swap(75, 210);
			swap(77, 178);
			swap(78, 114);
			swap(79, 242);
			swap(81, 138);
			swap(83, 202);
			swap(85, 170);
			swap(86, 106);
			swap(87, 234);
			swap(89, 154);
			swap(91, 218);
			swap(93, 186);
			swap(94, 122);
			swap(95, 250);
			swap(97, 134);
			swap(99, 198);
			swap(101, 166);
			swap(103, 230);
			swap(105, 150);
			swap(107, 214);
			swap(109, 182);
			swap(110, 118);
			swap(111, 246);
			swap(113, 142);
			swap(115, 206);
			swap(117, 174);
			swap(119, 238);
			swap(121, 158);
			swap(123, 222);
			swap(125, 190);
			swap(127, 254);
			swap(131, 193);
			swap(133, 161);
			swap(135, 225);
			swap(137, 145);
			swap(139, 209);
			swap(141, 177);
			swap(143, 241);
			swap(147, 201);
			swap(149, 169);
			swap(151, 233);
			swap(155, 217);
			swap(157, 185);
			swap(159, 249);
			swap(163, 197);
			swap(167, 229);
			swap(171, 213);
			swap(173, 181);
			swap(175, 245);
			swap(179, 205);
			swap(183, 237);
			swap(187, 221);
			swap(191, 253);
			swap(199, 227);
			swap(203, 211);
			swap(207, 243);
			swap(215, 235);
			swap(223, 251);
			swap(239, 247);
		}
		else
		{
			pre(swaps, data);
		}
	}
};
