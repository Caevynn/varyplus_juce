#pragma once

#include <complex>
#include <vector>

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"
#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCVector.hpp"

class RealFFT_v2
{
    using Real = float;
    using Complex = RTC::Complex<Real>;
    using Simd = RTC::Simd<Real, RTC_SIMD_SIZE>;
    using SimdComplex = RTC::SimdComplex<Real, RTC_SIMD_SIZE>;

    using Signal = RTC::Range<Real, RTC::Regular>;
    using ConstSignal = RTC::Range<const Real, RTC::Regular>;
    using Spectrum = RTC::Range<Complex, RTC::Regular>;
    using ConstSpectrum = RTC::Range<const Complex, RTC::Regular>;

    using SwapIndex = uint16_t;
    using SwapPair = std::pair<SwapIndex, SwapIndex>;

public:
    RealFFT_v2() = default;

    void allocate(size_t signalLength);
    void deallocate();

    template<typename T1, template<typename...> typename I1, typename T2, template<typename...> typename I2>
    void forward(RTC::Range<T1, I1> input, RTC::Range<T2, I2> output)
    {
        forwardConverted(ConstSignal::reinterpret(input), Spectrum::reinterpret(output));
    }

    template<typename T1, template<typename...> typename I1, typename T2, template<typename...> typename I2>
    void inverse(RTC::Range<T1, I1> input, RTC::Range<T2, I2> output)
    {
        inverseConverted(ConstSpectrum::reinterpret(input), Signal::reinterpret(output));
    }

    size_t signalLength() const { return 2 * nFFT; }
    size_t spectrumLength() const { return nFFT + RTC_SIMD_SIZE; }

private:
    struct Table
    {
        RTC::Array<Complex> twiddleFactor;
        RTC::Array<Complex> cos;
        RTC::Vector<SwapPair> swaps;
    } table;

    size_t nFFT{ 0 }, nStages{ 0 };
    RTC::Array<Complex> fftData;

    void forwardConverted(ConstSignal input, Spectrum output);
    void inverseConverted(ConstSpectrum input, Signal output);

    void signalToFFTData(ConstSignal& signal);
    void spectrumFromFFTData(Spectrum& spectrum);
    void spectrumToFFTData(ConstSpectrum& spectrum);
    void signalFromFFTData(Signal& signal);

    void run();
};
