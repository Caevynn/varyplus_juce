#pragma once

#include <chrono>

class Time
{
private:
    using ClockT = std::chrono::steady_clock;
    using TimeT = std::chrono::nanoseconds;

public:
    using TimePointT = std::chrono::time_point<ClockT>;
    using DurationT = double;

    static TimePointT now() { return ClockT::now(); }
    static DurationT duration_s(const TimePointT& start, const TimePointT& end) { return static_cast<DurationT>(std::chrono::duration_cast<TimeT>(end - start).count()) / 1000000000; }
    static DurationT duration_ms(const TimePointT& start, const TimePointT& end) { return static_cast<DurationT>(std::chrono::duration_cast<TimeT>(end - start).count()) / 1000000; }
    static DurationT duration_us(const TimePointT& start, const TimePointT& end) { return static_cast<DurationT>(std::chrono::duration_cast<TimeT>(end - start).count()) / 1000; }
    static DurationT duration_ns(const TimePointT& start, const TimePointT& end) { return static_cast<DurationT>(std::chrono::duration_cast<TimeT>(end - start).count()); }

    class Measurement
    {
    public:
        void setStart() { startPoint = Time::now(); }
        void setEnd() { endPoint = Time::now(); }

        TimePointT getStart() const { return startPoint; }
        TimePointT getEnd() const { return endPoint; }

        DurationT getDuration() const { return duration_us(startPoint, endPoint); }
        DurationT getDistance(const Measurement& other) const { return duration_us(other.getStart(), startPoint); }

    private:
        TimePointT startPoint, endPoint;
    };

    class AccumulatedMeasurement
    {
    public:
        void start() { currentMeasurement.setStart(); }
        void stop() { currentMeasurement.setEnd(); addMeasurement(); }

        DurationT getMinimum() const { return min; }
        DurationT getMaximum() const { return max; }
        DurationT getAverage() const { return sum / nMeasurements; }

    private:
        void addMeasurement()
        {
            DurationT duration = currentMeasurement.getDuration();

            if (nMeasurements == 0)
            {
                min = duration;
                max = duration;
                sum = duration;
            }
            else
            {
                min = std::min(min, duration);
                max = std::max(max, duration);
                sum += duration;
            }

            ++nMeasurements;
        }

        Measurement currentMeasurement;

        size_t nMeasurements{ 0 };
        DurationT min{ 0 }, max{ 0 }, sum{ 0 };
    };
};
