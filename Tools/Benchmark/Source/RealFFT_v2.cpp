#include "RealFFT_v2.hpp"

#include <cassert>

void RealFFT_v2::allocate(size_t signalLength)
{
    nFFT = signalLength / 2;

    fftData.allocate(nFFT);

    nStages = 1;
    for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

    table.twiddleFactor.allocate(nFFT);
    for (size_t iStage = 0; iStage < nStages; ++iStage)
    {
        size_t nButterflies = 1ull << iStage;

        auto twiddleReal = table.twiddleFactor.real().range(nButterflies, nButterflies);
        auto twiddleImag = table.twiddleFactor.imag().range(nButterflies, nButterflies);

        for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
        {
            double angle = M_PI * iButterfly / nButterflies;
            twiddleReal[iButterfly] = static_cast<Real>(std::cos(angle));
            twiddleImag[iButterfly] = static_cast<Real>(-std::sin(angle));
        }
    }

    table.cos.allocate(nFFT / 2);
    for (size_t i = 0; i < nFFT / 2; ++i)
    {
        size_t posReal = i + 1;
        size_t posImag = nFFT / 2 - 1 - i;

        table.cos.real()[i] = static_cast<Real>(std::cos(M_PI * posReal / nFFT));
        table.cos.imag()[i] = static_cast<Real>(std::cos(M_PI * posImag / nFFT));
    }

    table.swaps.allocate(nFFT / 2);
    for (size_t i = 0, j = 0; i < nFFT - 1; ++i)
    {
        if (i < j)
        {
            table.swaps.push_back({ static_cast<SwapIndex>(i), static_cast<SwapIndex>(j) });
        }

        size_t k = nFFT / 2;
        while (k <= j) { j -= k; k /= 2; }
        j += k;
    }
}

void RealFFT_v2::deallocate()
{
    fftData.deallocate();
    table.twiddleFactor.deallocate();
    table.cos.deallocate();
    table.swaps.deallocate();
}

void RealFFT_v2::forwardConverted(ConstSignal signal, Spectrum spectrum)
{
    signalToFFTData(signal);
    run();
    spectrumFromFFTData(spectrum);
}

void RealFFT_v2::inverseConverted(ConstSpectrum spectrum, Signal signal)
{
    spectrumToFFTData(spectrum);
    run();
    signalFromFFTData(signal);
}

void RealFFT_v2::signalToFFTData(ConstSignal& signal)
{
    size_t nValues = signal.size();

    assert(nValues <= 2 * nFFT);

    auto real = fftData.real();
    auto imag = fftData.imag();

    size_t i = 0;

    size_t nValuesBySimd = nValues - (nValues % Simd::Size);
    for (; i < nValuesBySimd / 2; i += Simd::Size)
    {
        // Don't assume input to be aligned

        Simd first, second;

        first.loadUnaligned(&signal[2 * i]);
        second.loadUnaligned(&signal[2 * i + Simd::Size]);

        auto [even, odd] = Simd::deinterleave(first, second);

        even.store(&real[i]);
        odd.store(&imag[i]);
    }

    size_t nValuesByPair = nValues - (nValues % 2);
    for (; i < nValuesByPair / 2; ++i)
    {
        real[i] = signal[2 * i];
        imag[i] = signal[2 * i + 1];
    }

    if (nValuesByPair != nValues)
    {
        real[i] = signal[2 * i];
        imag[i] = 0;
    }

    size_t nValuesCopied = nValues + (nValues % 2);
    real.range(nValuesCopied / 2, nFFT - nValuesCopied / 2).setToZero();
    imag.range(nValuesCopied / 2, nFFT - nValuesCopied / 2).setToZero();
}

void RealFFT_v2::spectrumFromFFTData(Spectrum& spectrum)
{
    const auto cosReal = table.cos.real();
    const auto cosImag = table.cos.imag();
    const auto inReal = fftData.real();
    const auto inImag = fftData.imag();
    auto outReal = spectrum.real();
    auto outImag = spectrum.imag();

    const Simd point25(0.25);

    for (size_t i = 0; i < nFFT / 2; i += Simd::Size)
    {
        size_t j = nFFT - Simd::Size - i;

        SimdComplex ci, cj, cc;

        ci.loadUnaligned(&inReal[i + 1], &inImag[i + 1]);
        cj.loadReversed(&inReal[j], &inImag[j]);
        cc.load(&cosImag[i], &cosReal[i]);

        SimdComplex cs = (ci + cj.conjugate()) * point25;
        SimdComplex cd = (cj - ci.conjugate()) * point25;
        SimdComplex cp = cc * cd.conjugate();

        (cs + cp).storeUnaligned(&outReal[i + 1], &outImag[i + 1]);
        (cs.conjugate() - cp.conjugate()).storeReversed(&outReal[j], &outImag[j]);
    }

    outReal[0] = (inReal[0] + inImag[0]) / 2;
    outImag[0] = 0;
    outReal[nFFT] = (inReal[0] - inImag[0]) / 2;
    outImag[nFFT] = 0;
    outReal[nFFT / 2] = inReal[nFFT / 2] / 2;
    outImag[nFFT / 2] = -inImag[nFFT / 2] / 2;
}

void RealFFT_v2::spectrumToFFTData(ConstSpectrum& spectrum)
{
    const auto cosReal = table.cos.real();
    const auto cosImag = table.cos.imag();
    const auto inReal = spectrum.real();
    const auto inImag = spectrum.imag();
    auto outReal = fftData.real();
    auto outImag = fftData.imag();

    for (size_t i = 0; i < nFFT / 2; i += Simd::Size)
    {
        size_t j = nFFT - Simd::Size - i;

        SimdComplex ci, cj, cc;

        ci.loadUnaligned(&inReal[i + 1], &inImag[i + 1]);
        cj.loadReversed(&inReal[j], &inImag[j]);
        cc.load(&cosImag[i], &cosReal[i]);

        SimdComplex cs = ci + cj.conjugate();
        SimdComplex cd = ci - cj.conjugate();
        SimdComplex cp = cc * cd.conjugate();

        (cp + cs.conjugate()).storeUnaligned(&outReal[i + 1], &outImag[i + 1]);
        (cs - cp.conjugate()).storeReversed(&outReal[j], &outImag[j]);
    }

    outReal[0] = inReal[0] + inReal[nFFT];
    outImag[0] = inReal[0] - inReal[nFFT];
    outReal[nFFT / 2] = inReal[nFFT / 2] * 2;
    outImag[nFFT / 2] = -inImag[nFFT / 2] * 2;
}

void RealFFT_v2::signalFromFFTData(Signal& signal)
{
    assert(signal.size() == 2 * nFFT);

    const auto real = fftData.real();
    const auto imag = fftData.imag();

    const Simd norm(1 / static_cast<Real>(nFFT));

    for (size_t i = 0; i < nFFT; i += Simd::Size)
    {
        Simd even(&real[i]);
        Simd odd(&imag[i]);

        auto [first, second] = Simd::interleave(even, odd);

        first *= norm;
        second *= norm;

        first.store(&signal[2 * i]);
        second.store(&signal[2 * i + Simd::Size]);
    }
}

void RealFFT_v2::run()
{
    auto real = fftData.data().realPointer();
    auto imag = fftData.data().imagPointer();
    auto twiddleReal = table.twiddleFactor.data().realPointer();
    auto twiddleImag = table.twiddleFactor.data().imagPointer();

    assert(fftData.size() == nFFT);

    // Reorder values according to bit reversal

    for (const auto& [a, b] : table.swaps)
    {
        std::swap(real[a], real[b]);
        std::swap(imag[a], imag[b]);
    }

    if (nStages > 2)
    {
        // Stages 1 - 3

        auto rt = twiddleReal + 4;
        auto it = twiddleImag + 4;

        RTC::SimdComplex<float, 4> ct(&rt[0], &it[0]);

        for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
        {
            size_t i = 8 * iGroup;

            float r0 = real[i + 0];
            float i0 = imag[i + 0];
            float r1 = real[i + 1];
            float i1 = imag[i + 1];
            float r2 = real[i + 2];
            float i2 = imag[i + 2];
            float r3 = real[i + 3];
            float i3 = imag[i + 3];
            float r4 = real[i + 4];
            float i4 = imag[i + 4];
            float r5 = real[i + 5];
            float i5 = imag[i + 5];
            float r6 = real[i + 6];
            float i6 = imag[i + 6];
            float r7 = real[i + 7];
            float i7 = imag[i + 7];

            RTC::SimdComplex<float, 4> c0(
                RTC::Simd<float, 4>(
                    r0 + r1 + r2 + r3,
                    r0 - r1 + i2 - i3,
                    r0 + r1 - r2 - r3,
                    r0 - r1 - i2 + i3
                ), 
                RTC::Simd<float, 4>(
                    i0 + i1 + i2 + i3,
                    i0 - i1 - r2 + r3,
                    i0 + i1 - i2 - i3,
                    i0 - i1 + r2 - r3
                )
            );

            RTC::SimdComplex<float, 4> c4(
                RTC::Simd<float, 4>(
                    r4 + r5 + r6 + r7,
                    r4 - r5 + i6 - i7,
                    r4 + r5 - r6 - r7,
                    r4 - r5 - i6 + i7
                ), 
                RTC::Simd<float, 4>(
                    i4 + i5 + i6 + i7,
                    i4 - i5 - r6 + r7,
                    i4 + i5 - i6 - i7,
                    i4 - i5 + r6 - r7
                )
            );

            RTC::SimdComplex<float, 4> c4tmp = c4 * ct;

            (c0 + c4tmp).store(&real[i + 0], &imag[i + 0]);
            (c0 - c4tmp).store(&real[i + 4], &imag[i + 4]);
        }
    }

    size_t startStage = 3;

#if RTC_SIMD_SIZE == 4
    if(nStages % 2 == 0)
    {
        // Stage 4

        auto rt = twiddleReal + 8;
        auto it = twiddleImag + 8;

        SimdComplex ct0(&rt[0], &it[0]);
        SimdComplex ct1(&rt[4], &it[4]);

        for(size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
        {
            size_t i0 = 16 * iGroup;
            size_t i1 = i0 + 4;
            size_t i2 = i0 + 8;
            size_t i3 = i0 + 12;

            SimdComplex c0(&real[i0], &imag[i0]);
            SimdComplex c1(&real[i1], &imag[i1]);
            SimdComplex c2(&real[i2], &imag[i2]);
            SimdComplex c3(&real[i3], &imag[i3]);

            SimdComplex c2tmp = c2 * ct0;
            SimdComplex c3tmp = c3 * ct1;

            (c0 + c2tmp).store(&real[i0], &imag[i0]);
            (c1 + c3tmp).store(&real[i1], &imag[i1]);
            (c0 - c2tmp).store(&real[i2], &imag[i2]);
            (c1 - c3tmp).store(&real[i3], &imag[i3]);
        }

        ++startStage;
    }
#elif RTC_SIMD_SIZE == 8
    if (nStages == 4)
    {
        // Stage 4

        auto rt = twiddleReal + 8;
        auto it = twiddleImag + 8;

        SimdComplex ct(&rt[0], &it[0]);

        for(size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
        {
            size_t i0 = 16 * iGroup;
            size_t i1 = i0 + 8;

            SimdComplex c0(&real[i0], &imag[i0]);
            SimdComplex c1(&real[i1], &imag[i1]);

            SimdComplex c1tmp = c1 * ct;

            (c0 + c1tmp).store(&real[i0], &imag[i0]);
            (c0 - c1tmp).store(&real[i1], &imag[i1]);
        }

        return;
    }

    if (nStages > 4)
    {
        // Stages 4 - 5

        auto rt0 = twiddleReal + 8;
        auto it0 = twiddleImag + 8;
        auto rt1 = twiddleReal + 16;
        auto it1 = twiddleImag + 16;

        SimdComplex ct0(&rt0[0], &it0[0]);
        SimdComplex ct10(&rt1[0], &it1[0]);
        SimdComplex ct11(&rt1[8], &it1[8]);

        for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
        {
            size_t i0 = 32 * iGroup;
            size_t i1 = i0 + 8;
            size_t i2 = i0 + 16;
            size_t i3 = i0 + 24;

            SimdComplex c0(&real[i0], &imag[i0]);
            SimdComplex c1(&real[i1], &imag[i1]);
            SimdComplex c2(&real[i2], &imag[i2]);
            SimdComplex c3(&real[i3], &imag[i3]);

            SimdComplex c1tmp0 = c1 * ct0;
            SimdComplex c3tmp0 = c3 * ct0;
            SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
            SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;
            
            (c0 + c1tmp0 + c2tmp1).store(&real[i0], &imag[i0]);
            (c0 - c1tmp0 + c3tmp1).store(&real[i1], &imag[i1]);
            (c0 + c1tmp0 - c2tmp1).store(&real[i2], &imag[i2]);
            (c0 - c1tmp0 - c3tmp1).store(&real[i3], &imag[i3]);
        }

        startStage += 2;
    }

    if(nStages % 2 == 0)
    {
        // Stage 6

        auto rt = twiddleReal + 32;
        auto it = twiddleImag + 32;
        
        for(size_t iGroup = 0; iGroup < nFFT / 64; ++iGroup)
        {
            for (size_t iButterfly = 0; iButterfly < 32; iButterfly += Simd::Size)
            {
                size_t i0 = 64 * iGroup + iButterfly;
                size_t i1 = i0 + 32;

                SimdComplex ct(&rt[iButterfly], &it[iButterfly]);

                SimdComplex c0(&real[i0], &imag[i0]);
                SimdComplex c1(&real[i1], &imag[i1]);

                SimdComplex c1tmp = c1 * ct;

                (c0 + c1tmp).store(&real[i0], &imag[i0]);
                (c0 - c1tmp).store(&real[i1], &imag[i1]);
            }
        }

        ++startStage;
    }
#elif RTC_SIMD_SIZE == 16
    if (nStages == 4)
    {
        // Stage 4

        auto rt = twiddleReal + 8;
        auto it = twiddleImag + 8;

        RTC::SimdComplex<float, 8> ct(&rt[0], &it[0]);

        for(size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
        {
            size_t i0 = 16 * iGroup;
            size_t i1 = i0 + 8;

            RTC::SimdComplex<float, 8> c0(&real[i0], &imag[i0]);
            RTC::SimdComplex<float, 8> c1(&real[i1], &imag[i1]);

            RTC::SimdComplex<float, 8> c1tmp = c1 * ct;

            (c0 + c1tmp).store(&real[i0], &imag[i0]);
            (c0 - c1tmp).store(&real[i1], &imag[i1]);
        }

        return;
    }

    if (nStages > 4)
    {
        // Stages 4 - 5

        auto rt0 = twiddleReal + 8;
        auto it0 = twiddleImag + 8;
        auto rt1 = twiddleReal + 16;
        auto it1 = twiddleImag + 16;

        RTC::SimdComplex<float, 8> ct0(&rt0[0], &it0[0]);
        RTC::SimdComplex<float, 16> ct1(&rt1[0], &it1[0]);

        for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
        {
            size_t i0 = 32 * iGroup;
            size_t i1 = i0 + 8;
            size_t i2 = i0 + 16;
            size_t i3 = i0 + 24;

            RTC::SimdComplex<float, 8> c0(&real[i0], &imag[i0]);
            RTC::SimdComplex<float, 8> c1(&real[i1], &imag[i1]);
            RTC::SimdComplex<float, 8> c2(&real[i2], &imag[i2]);
            RTC::SimdComplex<float, 8> c3(&real[i3], &imag[i3]);

            RTC::SimdComplex<float, 8> c1tmp0 = c1 * ct0;
            RTC::SimdComplex<float, 8> c3tmp0 = c3 * ct0;

            SimdComplex c01(c0 + c1tmp0, c0 - c1tmp0);
            SimdComplex c23(c2 + c3tmp0, c2 - c3tmp0);

            SimdComplex c23tmp1 = c23 * ct1;
            
            (c01 + c23tmp1).store(&real[i0], &imag[i0]);
            (c01 - c23tmp1).store(&real[i2], &imag[i2]);
        }

        startStage += 2;
    }

    if(nStages % 2 == 0)
    {
        // Stage 6

        auto rt = twiddleReal + 32;
        auto it = twiddleImag + 32;

        SimdComplex ct0(&rt[0], &it[0]);
        SimdComplex ct1(&rt[16], &it[16]);

        for(size_t iGroup = 0; iGroup < nFFT / 64; ++iGroup)
        {
            size_t i0 = 64 * iGroup;
            size_t i1 = i0 + 16;
            size_t i2 = i0 + 32;
            size_t i3 = i0 + 48;

            SimdComplex c0(&real[i0], &imag[i0]);
            SimdComplex c1(&real[i1], &imag[i1]);
            SimdComplex c2(&real[i2], &imag[i2]);
            SimdComplex c3(&real[i3], &imag[i3]);

            SimdComplex c2tmp = c2 * ct0;
            SimdComplex c3tmp = c3 * ct1;

            (c0 + c2tmp).store(&real[i0], &imag[i0]);
            (c1 + c3tmp).store(&real[i1], &imag[i1]);
            (c0 - c2tmp).store(&real[i2], &imag[i2]);
            (c1 - c3tmp).store(&real[i3], &imag[i3]);
        }

        ++startStage;
    }
#endif

    // Remaining stages

    for (size_t iStage = startStage; iStage < nStages; iStage += 2)
    {
        size_t nGroups = 1ull << (nStages - iStage - 1);
        size_t nButterflies = 1ull << iStage;

        assert(nGroups * nButterflies == nFFT / 2);
        assert(table.twiddleFactor.size() >= 2 * nButterflies);

        auto rt0 = twiddleReal + nButterflies;
        auto it0 = twiddleImag + nButterflies;
        auto rt1 = twiddleReal + 2 * nButterflies;
        auto it1 = twiddleImag + 2 * nButterflies;

        for(size_t iGroup = 0; iGroup < nGroups / 2; ++iGroup)
        {
            assert(nButterflies % Simd::Size == 0);

            for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
            {
                size_t i0 = 4 * nButterflies * iGroup + iButterfly;
                size_t i1 = i0 + nButterflies;
                size_t i2 = i0 + 2 * nButterflies;
                size_t i3 = i0 + 3 * nButterflies;

                SimdComplex ct0(&rt0[iButterfly], &it0[iButterfly]);
                SimdComplex ct10(&rt1[iButterfly], &it1[iButterfly]);
                SimdComplex ct11(&rt1[iButterfly + nButterflies], &it1[iButterfly + nButterflies]);
                
                SimdComplex c0(&real[i0], &imag[i0]);
                SimdComplex c1(&real[i1], &imag[i1]);
                SimdComplex c2(&real[i2], &imag[i2]);
                SimdComplex c3(&real[i3], &imag[i3]);
                
                SimdComplex c1tmp0 = c1 * ct0;
                SimdComplex c3tmp0 = c3 * ct0;
                SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
                SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;
                
                (c0 + c1tmp0 + c2tmp1).store(&real[i0], &imag[i0]);
                (c0 - c1tmp0 + c3tmp1).store(&real[i1], &imag[i1]);
                (c0 + c1tmp0 - c2tmp1).store(&real[i2], &imag[i2]);
                (c0 - c1tmp0 - c3tmp1).store(&real[i3], &imag[i3]);
            }
        }
    }
}
