#include "RealFFT_v1.hpp"

#include <cassert>

void RealFFT_v1::allocate(size_t signalLength)
{
    nFFT = signalLength / 2;

    fftData.allocate(nFFT);

    nStages = 1;
    for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

    table.twiddleFactor.allocate(nFFT);
    for (size_t iStage = 0; iStage < nStages; ++iStage)
    {
        size_t nButterflies = 1ull << iStage;

        auto twiddleReal = table.twiddleFactor.real().range(nButterflies, nButterflies);
        auto twiddleImag = table.twiddleFactor.imag().range(nButterflies, nButterflies);

        for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
        {
            double angle = M_PI * iButterfly / nButterflies;
            twiddleReal[iButterfly] = static_cast<Real>(std::cos(angle));
            twiddleImag[iButterfly] = static_cast<Real>(-std::sin(angle));
        }
    }

    table.cos.allocate(nFFT / 2);
    for (size_t i = 0; i < nFFT / 2; ++i)
    {
        size_t posReal = i + 1;
        size_t posImag = nFFT / 2 - 1 - i;

        table.cos.real()[i] = static_cast<Real>(std::cos(M_PI * posReal / nFFT));
        table.cos.imag()[i] = static_cast<Real>(std::cos(M_PI * posImag / nFFT));
    }

    table.swaps.allocate(nFFT / 2);
    for (size_t i = 0, j = 0; i < nFFT - 1; ++i)
    {
        if (i < j)
        {
            table.swaps.push_back({ static_cast<SwapIndex>(i), static_cast<SwapIndex>(j) });
        }

        size_t k = nFFT / 2;
        while (k <= j) { j -= k; k /= 2; }
        j += k;
    }
}

void RealFFT_v1::deallocate()
{
    fftData.deallocate();
    table.twiddleFactor.deallocate();
    table.cos.deallocate();
    table.swaps.deallocate();
}

void RealFFT_v1::forwardConverted(ConstSignal& signal, Spectrum& spectrum)
{
    signalToFFTData(signal);
    run();
    spectrumFromFFTData(spectrum);
}

void RealFFT_v1::inverseConverted(ConstSpectrum& spectrum, Signal& signal)
{
    spectrumToFFTData(spectrum);
    run();
    signalFromFFTData(signal);
}

void RealFFT_v1::signalToFFTData(ConstSignal& signal)
{
    size_t nValues = signal.size();

    assert(nValues <= 2 * nFFT);

    auto real = fftData.real();
    auto imag = fftData.imag();

    size_t i = 0;

    size_t nValuesBySimd = nValues - (nValues % Simd::Size);
    for (; i < nValuesBySimd / 2; i += Simd::Size)
    {
        // Don't assume input to be aligned

        Simd first, second;

        first.loadUnaligned(&signal[2 * i]);
        second.loadUnaligned(&signal[2 * i + Simd::Size]);

        auto [even, odd] = Simd::deinterleave(first, second);

        even.store(&real[i]);
        odd.store(&imag[i]);
    }

    size_t nValuesByPair = nValues - (nValues % 2);
    for (; i < nValuesByPair / 2; ++i)
    {
        real[i] = signal[2 * i];
        imag[i] = signal[2 * i + 1];
    }

    if (nValuesByPair != nValues)
    {
        real[i] = signal[2 * i];
        imag[i] = 0;
    }

    size_t nValuesCopied = nValues + (nValues % 2);
    real.range(nValuesCopied / 2, nFFT - nValuesCopied / 2).setToZero();
    imag.range(nValuesCopied / 2, nFFT - nValuesCopied / 2).setToZero();
}

void RealFFT_v1::spectrumFromFFTData(Spectrum& spectrum)
{
    auto in = fftData.data();
    auto out = spectrum.data();
    auto cos = table.cos.data();

    const Simd point5(0.5);

    for (size_t i = 0; i < nFFT / 2; i += Simd::Size)
    {
        size_t j = nFFT - Simd::Size - i;

        SimdComplex ci, cj, cc;

        ci.load(&in[i + 1]);
        cj.load(&in[j]).reverse();
        cc.load(&cos[i]);

        SimdComplex cs = (ci + cj.conjugate()) * point5;
        SimdComplex cd = (cj - ci.conjugate()) * point5;
        SimdComplex cp = cc * cd.conjugate();

        (cs + cp).store(&out[i + 1]);
        (cs.conjugate() - cp.conjugate()).reverse().store(&out[j]);
    }

    out[0].real() = in[0].real() + in[0].imag();
    out[0].imag() = 0;
    out[nFFT].real() = in[0].real() - in[0].imag();
    out[nFFT].imag() = 0;
    out[nFFT / 2].real() = in[nFFT / 2].real();
    out[nFFT / 2].imag() = -in[nFFT / 2].imag();
}

void RealFFT_v1::spectrumToFFTData(ConstSpectrum& spectrum)
{
    auto in = spectrum.data();
    auto out = fftData.data();
    auto cos = table.cos.data();

    const Simd point5(0.5);

    for (size_t i = 0; i < nFFT / 2; i += Simd::Size)
    {
        size_t j = nFFT - Simd::Size - i;

        SimdComplex ci, cj, cc;

        ci.load(&in[i + 1]);
        cj.load(&in[j]).reverse();
        cc.load(&cos[i]);

        SimdComplex cs = (ci + cj.conjugate()) * point5;
        SimdComplex cd = (ci - cj.conjugate()) * point5;
        SimdComplex cp = cc * cd.conjugate();

        (cp + cs.conjugate()).store(&out[i + 1]);
        (cs - cp.conjugate()).reverse().store(&out[j]);
    }

    out[0].real() = (in[0].real() + in[nFFT].real()) / 2;
    out[0].imag() = (in[0].real() - in[nFFT].real()) / 2;
    out[nFFT / 2].real() = in[nFFT / 2].real();
    out[nFFT / 2].imag() = -in[nFFT / 2].imag();
}

void RealFFT_v1::signalFromFFTData(Signal& signal)
{
    assert(signal.size() == 2 * nFFT);

    const auto real = fftData.real();
    const auto imag = fftData.imag();

    const Simd norm(1 / static_cast<Real>(nFFT));

    for (size_t i = 0; i < nFFT; i += Simd::Size)
    {
        Simd even(&real[i]);
        Simd odd(&imag[i]);

        auto [first, second] = Simd::interleave(even, odd);

        first *= norm;
        second *= norm;

        first.store(&signal[2 * i]);
        second.store(&signal[2 * i + Simd::Size]);
    }
}

void RealFFT_v1::run()
{
    auto data = fftData.data();
    auto twiddle = table.twiddleFactor.data();

    assert(fftData.size() == nFFT);

    // Reorder values according to bit reversal

    for (const auto& [a, b] : table.swaps)
    {
        std::swap(data[a].real(), data[b].real());
        std::swap(data[a].imag(), data[b].imag());
    }

    // First stage (iStage = 0, nGroups = nFFT / 2, nButterflies = 1)

    if (nStages > 0)
    {
        for (size_t iGroup = 0; iGroup < nFFT / 2; ++iGroup)
        {
            size_t i = 2 * iGroup;
            size_t j = i + 1;

            // Twiddle = (1, 0)
            Complex tmp(data[j]);
            data[j] = data[i] - tmp;
            data[i] = data[i] + tmp;
        }
    }

    // Second stage (iStage = 1, nGroups = nFFT / 4, nButterflies = 2)

    if (nStages > 1)
    {
        for (size_t iGroup = 0; iGroup < nFFT / 4; ++iGroup)
        {
            size_t i = 2 * 2 * iGroup;
            size_t j = i + 2;

            // Twiddle = (1, 0)
            Complex tmp0(data[j + 0]);
            data[j + 0] = data[i + 0] - tmp0;
            data[i + 0] = data[i + 0] + tmp0;

            // Twiddle = (0, -1)
            Complex tmp1 = data[j + 1].rotateCW();
            data[j + 1] = data[i + 1] - tmp1;
            data[i + 1] = data[i + 1] + tmp1;
        }
    }
    
#if RTC_SIMD_SIZE > 4
    // Third stage (iStage = 2, nGroups = nFFT / 8, nButterflies = 4)

    if (nStages > 2)
    {
        RTC::SimdComplex<float, 4> ct(&twiddle[4]);

        for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
        {
            size_t i = 2 * 4 * iGroup;
            size_t j = i + 4;

            RTC::SimdComplex<float, 4> ci(&data[i]);
            RTC::SimdComplex<float, 4> cj(&data[j]);

            RTC::SimdComplex<float, 4> ctmp = cj * ct;

            cj = ci - ctmp;
            ci = ci + ctmp;

            ci.store(&data[i]);
            cj.store(&data[j]);
        }
    }
#endif

#if RTC_SIMD_SIZE > 8
    // Fourth stage (iStage = 3, nGroups = nFFT / 16, nButterflies = 8)

    if (nStages > 3)
    {
        RTC::SimdComplex<float, 8> ct(&twiddle[8]);

        for (size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
        {
            size_t i = 2 * 8 * iGroup;
            size_t j = i + 8;

            RTC::SimdComplex<float, 8> ci(&data[i]);
            RTC::SimdComplex<float, 8> cj(&data[j]);

            RTC::SimdComplex<float, 8> ctmp = cj * ct;

            cj = ci - ctmp;
            ci = ci + ctmp;

            ci.store(&data[i]);
            cj.store(&data[j]);
        }
    }
#endif

    // Butterfly

    for (size_t iStage = Simd::Size > 4 ? (Simd::Size > 8 ? 4 : 3) : 2; iStage < nStages; ++iStage)
    {
        size_t nGroups = 1ull << (nStages - iStage - 1);
        size_t nButterflies = 1ull << iStage;

        assert(nGroups * nButterflies == nFFT / 2);
        assert(twiddle.size() >= 2 * nButterflies);

        auto twiddleRange = twiddle + nButterflies;
        
        for(size_t iGroup = 0; iGroup < nGroups; ++iGroup)
        {
            assert(nButterflies % Simd::Size == 0);

            for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
            {
                size_t i = 2 * nButterflies * iGroup + iButterfly;
                size_t j = i + nButterflies;

                SimdComplex ci(&data[i]);
                SimdComplex cj(&data[j]);
                SimdComplex ct(&twiddleRange[iButterfly]);

                SimdComplex ctmp = cj * ct;

                cj = ci - ctmp;
                ci = ci + ctmp;

                ci.store(&data[i]);
                cj.store(&data[j]);
            }
        }
    }
}
