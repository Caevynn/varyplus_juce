#pragma once

#include "fft.h"

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

class ReferenceFFT
{
public:
    ~ReferenceFFT()
    {
        deallocate();
    }

    void allocate(size_t signalLength)
    {
        signalSize = signalLength;
        spectrumSize = signalLength / 2 + 1;

        set_twiddle_table(static_cast<int>(signalLength));
        complexData = reinterpret_cast<complex_float32*>(std::malloc(sizeof(complex_float32) * spectrumSize));
        realData = reinterpret_cast<float*>(std::malloc(sizeof(float) * signalSize));
    }

    void deallocate()
    {
        if (complexData != nullptr) std::free(complexData);
        if (realData != nullptr) std::free(realData);
    }

    template<typename ScalarT, template<typename...> typename I1, typename ComplexT, template<typename...> typename I2>
    void forward(RTC::Range<ScalarT, I1> input, RTC::Range<ComplexT, I2> output)
    {
        for (size_t i = 0; i < signalSize; ++i)
        {
            realData[i] = input[i];
        }

        rfft(realData, complexData, static_cast<int>(signalSize));

        for (size_t i = 0; i < spectrumSize; ++i)
        {
            output[i].real() = complexData[i].re;
            output[i].imag() = complexData[i].im;
        }
    }

    template<typename ComplexT, template<typename...> typename I1, typename ScalarT, template<typename...> typename I2>
    void inverse(RTC::Range<ComplexT, I1> input, RTC::Range<ScalarT, I2> output)
    {
        for (size_t i = 0; i < spectrumSize; ++i)
        {
            complexData[i].re = input[i].real();
            complexData[i].im = input[i].imag();
        }

        irfft(complexData, realData, static_cast<int>(signalSize));

        for (size_t i = 0; i < signalSize; ++i)
        {
            output[i] = realData[i];
        }
    }

    size_t signalLength() const { return signalSize; }
    size_t spectrumLength() const { return spectrumSize; }

private:
    size_t signalSize{ 0 }, spectrumSize{ 0 };
    complex_float32* complexData{ nullptr };
    float* realData{ nullptr };
};
