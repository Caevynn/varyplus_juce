#pragma once

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

class FFTCoreMerged45
{
    using Real = float;
    using Complex = RTC::Complex<Real>;
    using Simd = RTC::Simd<Real, RTC_SIMD_SIZE>;
    using SimdComplex = RTC::SimdComplex<Real, RTC_SIMD_SIZE>;
	using Data = RTC::Range<Complex, RTC::Regular>;

    using SwapIndex = uint16_t;
    using SwapPair = std::pair<SwapIndex, SwapIndex>;
public:
	FFTCoreMerged45(size_t dataSize) : nFFT(dataSize)
	{
		nStages = 1;
		for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

		twiddle.allocate(nFFT);
		for (size_t iStage = 0; iStage < nStages; ++iStage)
		{
			size_t nButterflies = 1ull << iStage;

			auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
			auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

			for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
			{
				double angle = M_PI * iButterfly / nButterflies;
				twiddleReal[iButterfly] = static_cast<Real>(std::cos(angle));
				twiddleImag[iButterfly] = static_cast<Real>(-std::sin(angle));
			}
		}
	}

	void run(Data& data)
	{
		butterflies(data);
	}

private:
	RTC::Array<Complex> twiddle;
	size_t nFFT, nStages;

	void butterflies(Data& data)
	{
		float* real = data.data().realPointer();
		float* imag = data.data().imagPointer();

		// Third stage (iStage = 2, nGroups = nFFT / 8, nButterflies = 4)

		if (nStages > 2)
		{
			auto twiddleReal = twiddle.real().range(4, 4);
			auto twiddleImag = twiddle.imag().range(4, 4);

			RTC::SimdComplex<float, 4> ct(&twiddleReal[0], &twiddleImag[0]);

			for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
			{
				size_t i = 2 * 4 * iGroup;
				size_t j = i + 4;

				float ri0 = real[i + 0];
				float ii0 = imag[i + 0];
				float ri1 = real[i + 1];
				float ii1 = imag[i + 1];
				float ri2 = real[i + 2];
				float ii2 = imag[i + 2];
				float ri3 = real[i + 3];
				float ii3 = imag[i + 3];

				RTC::SimdComplex<float, 4> ci(
					RTC::Simd<float, 4>(
						(ri0 + ri1) + (ri2 + ri3),
						(ri0 - ri1) + (ii2 - ii3),
						(ri0 + ri1) - (ri2 + ri3),
						(ri0 - ri1) - (ii2 - ii3)
					),
					RTC::Simd<float, 4>(
						(ii0 + ii1) + (ii2 + ii3),
						(ii0 - ii1) - (ri2 - ri3),
						(ii0 + ii1) - (ii2 + ii3),
						(ii0 - ii1) + (ri2 - ri3)
					)
				);

				float rj0 = real[j + 0];
				float ij0 = imag[j + 0];
				float rj1 = real[j + 1];
				float ij1 = imag[j + 1];
				float rj2 = real[j + 2];
				float ij2 = imag[j + 2];
				float rj3 = real[j + 3];
				float ij3 = imag[j + 3];

				RTC::SimdComplex<float, 4> cj(
					RTC::Simd<float, 4>(
						(rj0 + rj1) + (rj2 + rj3),
						(rj0 - rj1) + (ij2 - ij3),
						(rj0 + rj1) - (rj2 + rj3),
						(rj0 - rj1) - (ij2 - ij3)
					),
					RTC::Simd<float, 4>(
						(ij0 + ij1) + (ij2 + ij3),
						(ij0 - ij1) - (rj2 - rj3),
						(ij0 + ij1) - (ij2 + ij3),
						(ij0 - ij1) + (rj2 - rj3)
					)
				);

				RTC::SimdComplex<float, 4> ctmp = cj * ct;

				cj = ci - ctmp;
				ci = ci + ctmp;

				ci.store(&real[i], &imag[i]);
				cj.store(&real[j], &imag[j]);
			}
		}

#if RTC_SIMD_SIZE == 8
		// Fourth and fifth stage (nGroups = nFFT / 16)

		if (nStages > 4)
		{
			size_t nGroups = 1ull << (nStages - 3 - 1); // nFFT / 16
			size_t nButterflies = 1ull << 3; // 8

			auto twiddleReal0 = twiddle.real().range(nButterflies, nButterflies);
			auto twiddleImag0 = twiddle.imag().range(nButterflies, nButterflies);
			auto twiddleReal1 = twiddle.real().range(nButterflies * 2, nButterflies * 2);
			auto twiddleImag1 = twiddle.imag().range(nButterflies * 2, nButterflies * 2);

			RTC::SimdComplex<float, 8> ct0(&twiddleReal0[0], &twiddleImag0[0]);
			RTC::SimdComplex<float, 8> ct10(&twiddleReal1[0], &twiddleImag1[0]);
			RTC::SimdComplex<float, 8> ct11(&twiddleReal1[nButterflies], &twiddleImag1[nButterflies]);

			for (size_t iGroup = 0; iGroup < nGroups / 2; ++iGroup)
			{
				size_t i0 = 4 * nButterflies * iGroup;
				size_t i1 = i0 + nButterflies;
				size_t i2 = i0 + 2 * nButterflies;
				size_t i3 = i0 + 3 * nButterflies;

				RTC::SimdComplex<float, 8> c0(&real[i0], &imag[i0]);
				RTC::SimdComplex<float, 8> c1(&real[i1], &imag[i1]);
				RTC::SimdComplex<float, 8> c2(&real[i2], &imag[i2]);
				RTC::SimdComplex<float, 8> c3(&real[i3], &imag[i3]);

				RTC::SimdComplex<float, 8> c1tmp4 = c1 * ct0;
				RTC::SimdComplex<float, 8> c3tmp4 = c3 * ct0;

				c1 = c0 - c1tmp4;
				c0 = c0 + c1tmp4;
				c3 = c2 - c3tmp4;
				c2 = c2 + c3tmp4;

				RTC::SimdComplex<float, 8> c2tmp5 = c2 * ct10;
				RTC::SimdComplex<float, 8> c3tmp5 = c3 * ct11;
				
                c2 = c0 - c2tmp5;
				c0 = c0 + c2tmp5;
				c3 = c1 - c3tmp5;
				c1 = c1 + c3tmp5;

				c0.store(&real[i0], &imag[i0]);
				c1.store(&real[i1], &imag[i1]);
				c2.store(&real[i2], &imag[i2]);
				c3.store(&real[i3], &imag[i3]);
			}
		}
#endif

#if RTC_SIMD_SIZE == 16
		// Fourth and fifth stage (nGroups = nFFT / 16)

		if (nStages > 4)
		{
			auto twiddleReal0 = twiddle.real().range(8, 8);
			auto twiddleImag0 = twiddle.imag().range(8, 8);
			RTC::SimdComplex<float, 8> ct0(&twiddleReal0[0], &twiddleImag0[0]);
            
			auto twiddleReal1 = twiddle.real().range(16, 16);
			auto twiddleImag1 = twiddle.imag().range(16, 16);
			RTC::SimdComplex<float, 16> ct5(&twiddleReal1[0], &twiddleImag1[0]);
            
			for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
			{
				size_t i0 = 32 * iGroup;
				size_t i1 = i0 + 8;
				size_t i2 = i0 + 16;
				size_t i3 = i0 + 24;

				RTC::SimdComplex<float, 8> c0(&real[i0], &imag[i0]);
				RTC::SimdComplex<float, 8> c1(&real[i1], &imag[i1]);

				RTC::SimdComplex<float, 8> c1tmp4 = c1 * ct0;

				c1 = c0 - c1tmp4;
				c0 = c0 + c1tmp4;

				RTC::SimdComplex<float, 8> c2(&real[i2], &imag[i2]);
				RTC::SimdComplex<float, 8> c3(&real[i3], &imag[i3]);

				RTC::SimdComplex<float, 8> c3tmp4 = c3 * ct0;

				c3 = c2 - c3tmp4;
				c2 = c2 + c3tmp4;

                RTC::SimdComplex<float, 16> c01(c0, c1);
                RTC::SimdComplex<float, 16> c23(c2, c3);

				RTC::SimdComplex<float, 16> c23tmp5 = c23 * ct5;
				
                c23 = c01 - c23tmp5;
				c01 = c01 + c23tmp5;

				c01.store(&real[i0], &imag[i0]);
				c23.store(&real[i2], &imag[i2]);
			}
		}
#endif

		// Butterfly

		for (size_t iStage = Simd::Size > 4 ? 5 : 3; iStage < nStages; ++iStage)
		{
			size_t nGroups = 1ull << (nStages - iStage - 1);
			size_t nButterflies = 1ull << iStage;

			assert(nGroups * nButterflies == nFFT / 2);
			assert(twiddle.size() >= 2 * nButterflies);

			auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
			auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

			for(size_t iGroup = 0; iGroup < nGroups; ++iGroup)
			{
				assert(nButterflies % Simd::Size == 0);

				for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
				{
					size_t i = 2 * nButterflies * iGroup + iButterfly;
					size_t j = i + nButterflies;

					SimdComplex ci(&real[i], &imag[i]);
					SimdComplex cj(&real[j], &imag[j]);
					SimdComplex ct(&twiddleReal[iButterfly], &twiddleImag[iButterfly]);

					SimdComplex ctmp = cj * ct;

					(ci + ctmp).store(&real[i], &imag[i]);
					(ci - ctmp).store(&real[j], &imag[j]);
				}
			}
		}
	}
};
