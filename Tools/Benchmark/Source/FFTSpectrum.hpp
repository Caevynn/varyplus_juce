#pragma once

#include "Test.hpp"

#include <cmath>

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(name) name(input, cos, name ## Result)

class FFTSpectrum
{
	using Container = RTC::Array<RTC::Complex<float>>;
    using RealContainer = RTC::Array<float>;
public:
	static void run(size_t dataSize, size_t nRepetitions, size_t nMeasurements)
	{
        std::cout << "FFTSpectrum::run(" << dataSize << ", " << nMeasurements << ")" << std::endl;

        TIME_AND_RESULT(loop);
        TIME_AND_RESULT(sse);
        AVX(TIME_AND_RESULT(avx));
        AVX512(TIME_AND_RESULT(avx512));

		Container input;
		input.allocate(dataSize);

        Container cos;
		cos.allocate(dataSize / 2);

        for (size_t i = 0; i < dataSize / 2; ++i)
        {
            float angle = static_cast<float>(M_PI * i / dataSize);

            if (i > 0)
            {
                cos.real()[i - 1] = std::cos(angle);
            }
            cos.imag()[dataSize / 2 - i] = std::cos(angle);
        }

		Random random;

		for (size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
		{
			random.fill(input);

            RUN_TEST(loop);
            RUN_TEST(sse);
            AVX(RUN_TEST(avx));
            AVX512(RUN_TEST(avx512));
            
            compare("sse", loopResult, sseResult);
            AVX(compare("avx", loopResult, avxResult));
            AVX512(compare("avx512", loopResult, avx512Result));
        }

		print("loop", loopTime);
        print("sse", sseTime);
        AVX(print("avx", avxTime));
        AVX512(print("avx512", avx512Time));
	}

private:
	static void loop(const Container& input, const Container& cos, Container& output)
	{
        size_t nFFT = input.size();

        auto inr = input.real();
        auto ini = input.imag();
        auto outr = output.real();
        auto outi = output.imag();

		{
            for (size_t i = 1; i < nFFT / 2; ++i)
            {
                size_t j = nFFT - i;
                
                float rs = inr[i] + inr[j];
                float is = ini[i] + ini[j];

                float rd = inr[i] - inr[j];
                float id = ini[i] - ini[j];

                float ci = cos.real()[i - 1];
                float cj = cos.imag()[i - 1];

                float rp = is * ci + rd * cj;
                float ip = rd * ci - is * cj;

                outr[i] = rp + rs;
                outi[i] = ip - id;

                outr[j] = rs - rp;
                outi[j] = ip + id;
            }
		}

        {
            for (size_t i = 1; i < nFFT / 2; ++i)
            {
                size_t j = nFFT - i;

                float rs = (outr[i] + outr[j]) / 2;
                float is = (outi[i] + outi[j]) / 2;

                float rd = (outr[j] - outr[i]) / 2;
                float id = (outi[i] - outi[j]) / 2;

                float ci = cos.real()[i - 1];
                float cj = cos.imag()[i - 1];

                float rp = is * ci + rd * cj;
                float ip = rd * ci - is * cj;

                outr[i] = rs + rp;
                outi[i] = ip + id;

                outr[j] = rs - rp;
                outi[j] = ip - id;
            }
        }

        outr[0] = inr[0];
        outi[0] = ini[0];
        outr[nFFT / 2] = inr[nFFT / 2];
        outi[nFFT / 2] = ini[nFFT / 2];
	}

    static void sse(const Container& input, const Container& cos, Container& output)
    {
        simd<4>(input, cos, output);
    }

#ifdef __AVX__
    static void avx(const Container& input, const Container& cos, Container& output)
    {
        simd<8>(input, cos, output);
    }
#endif

#ifdef __AVX512F__
    static void avx512(const Container& input, const Container& cos, Container& output)
    {
        simd<16>(input, cos, output);
    }
#endif

    template<size_t SimdSize>
    static void simd(const Container& input, const Container& cos, Container& output)
    {
        using Simd = RTC::Simd<float, SimdSize>;

        size_t nFFT = input.size();

        auto inr = input.real();
        auto ini = input.imag();
        auto outr = output.real();
        auto outi = output.imag();

        {
            for (size_t i = 0; i < nFFT / 2; i += SimdSize)
            {
                size_t j = nFFT - SimdSize - i;

                Simd ri, ii, rj, ij;

                ri.loadUnaligned(&inr[i + 1]);
                ii.loadUnaligned(&ini[i + 1]);
                rj.loadReversed(&inr[j]);
                ij.loadReversed(&ini[j]);

                Simd rs = ri + rj;
                Simd is = ii + ij;

                Simd rd = ri - rj;
                Simd id = ii - ij;

                Simd ci(&cos.real()[i]);
                Simd cj(&cos.imag()[i]);

                Simd rp = is * ci + rd * cj;
                Simd ip = rd * ci - is * cj;

                (rp + rs).storeUnaligned(&outr[i + 1]);
                (ip - id).storeUnaligned(&outi[i + 1]);
                (rs - rp).storeReversed(&outr[j]);
                (ip + id).storeReversed(&outi[j]);
            }
        }

        {
            const Simd point5(0.5);

            for (size_t i = 0; i < nFFT / 2; i += SimdSize)
            {
                size_t j = nFFT - SimdSize - i;

                Simd ri, ii, rj, ij;

                ri.loadUnaligned(&outr[i + 1]);
                ii.loadUnaligned(&outi[i + 1]);
                rj.loadReversed(&outr[j]);
                ij.loadReversed(&outi[j]);

                Simd rs = (ri + rj) * point5;
                Simd is = (ii + ij) * point5;

                Simd rd = (rj - ri) * point5;
                Simd id = (ii - ij) * point5;

                Simd ci(&cos.real()[i]);
                Simd cj(&cos.imag()[i]);

                Simd rp = is * ci + rd * cj;
                Simd ip = rd * ci - is * cj;

                (rs + rp).storeUnaligned(&outr[i + 1]);
                (ip + id).storeUnaligned(&outi[i + 1]);
                (rs - rp).storeReversed(&outr[j]);
                (ip - id).storeReversed(&outi[j]);
            }
        }

        outr[0] = inr[0];
        outi[0] = ini[0];
        outr[nFFT / 2] = inr[nFFT / 2];
        outi[nFFT / 2] = ini[nFFT / 2];
    }
};

