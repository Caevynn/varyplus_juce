#pragma once

#include <iostream>
#ifdef _WIN32
#   include <intrin.h>
#elif defined(__linux__)
#   include <x86intrin.h>
#else
#   error Operating system not supported
#endif

#include "Random.hpp"
#include "Time.hpp"

#ifdef TIME_AND_RESULT
#   undef TIME_AND_RESULT
#endif

#define TIME_AND_RESULT(name) \
Container name ## Result; \
name ## Result.allocate(dataSize); \
Time::AccumulatedMeasurement name ## Time

#ifdef RUN_TEST
#   undef RUN_TEST
#endif

#define RUN_TEST(name) \
name ## Time.start(); \
for(size_t i = 0; i < nRepetitions; ++i) \
{ \
    RUN_SPECIFIC_TEST(name); \
} \
name ## Time.stop()

#define PRINT_TIME(name) print(#name, name ## Time)

#if defined(__AVX__)
#   define AVX(command) command
#else
#   define AVX(command) (void)0
#endif

#if defined(__AVX2__)
#   define AVX2(command) command
#else
#   define AVX2(command) (void)0
#endif

#if defined(__AVX512F__)
#   define AVX512(command) command
#else
#   define AVX512(command) (void)0
#endif

template<typename A, typename B, template<typename, typename> typename Test, typename = void>
struct binary_tester : std::false_type {};
template<typename A, typename B, template<typename, typename> typename Test>
struct binary_tester<A, B, Test, std::void_t<Test<A, B>>> : std::true_type {};

template<typename A, template<typename> typename Test, typename = void>
struct unary_tester : std::false_type {};
template<typename A, template<typename> typename Test>
struct unary_tester<A, Test, std::void_t<Test<A>>> : std::true_type {};

template<typename A, typename B, template<typename, typename> typename Test>
inline constexpr bool binary_tester_v = binary_tester<A, B, Test>::value;

template<typename A, template<typename> typename Test>
inline constexpr bool unary_tester_v = unary_tester<A, Test>::value;

bool compare(const std::string & name, const RTC::Range<float, RTC::Regular>&a, const RTC::Range<float, RTC::Regular>&b, double precision = 1e-6)
{
    double avg = 0;
    for (const auto& value : a)
    {
        avg += std::abs(value);
    }
    avg /= a.size();

    for (size_t i = 0; i < a.size(); ++i)
    {
        auto diff = std::abs(a[i] - b[i]);

        if (diff > avg * precision)
        {
            std::cout << i << ": " << name << " differs: "
                << a[i] << " / " << b[i] << " (" << diff << ")"
                << std::endl;

            return false;
        }
    }

    return true;
}

bool compare(const std::string& name, RTC::Range<RTC::Complex<float>, RTC::Regular> a, RTC::Range<RTC::Complex<float>, RTC::Regular> b, double precision = 1e-6)
{
    double avgReal = 0, avgImag = 0;
    for (const auto& value : a)
    {
        avgReal += std::abs(value.real());
        avgImag += std::abs(value.imag());
    }
    avgReal /= a.size();
    avgImag /= a.size();
    double avg = std::sqrt(std::pow(avgReal, 2) + std::pow(avgImag, 2));

    for (size_t i = 0; i < a.size(); ++i)
    {
        auto realDiff = std::abs(a.real()[i] - b.real()[i]);
        auto imagDiff = std::abs(a.imag()[i] - b.imag()[i]);

        if (realDiff > avg * precision ||
            imagDiff > avg * precision)
        {
            std::cout << i << ": " << name << " differs:"
                << a.real()[i] << " / " << b.real()[i] << "(" << realDiff << "), "
                << a.imag()[i] << " / " << b.imag()[i] << "(" << imagDiff << ")"
                << std::endl;

            return false;
        }
    }

    return true;
}

void print(const std::string& name, const Time::AccumulatedMeasurement& measurement)
{
    std::cout << "\t" << name << ": " << measurement.getMinimum() << " / " << measurement.getMaximum() << " / " << measurement.getAverage() << std::endl;
}
