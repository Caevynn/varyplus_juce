#pragma once

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

class FFTCoreOriginal
{
    using Real = float;
    using Complex = RTC::Complex<Real>;
    using Simd = RTC::Simd<Real, RTC_SIMD_SIZE>;
    using SimdComplex = RTC::SimdComplex<Real, RTC_SIMD_SIZE>;
	using Data = RTC::Range<Complex, RTC::Regular>;

    using SwapIndex = uint16_t;
    using SwapPair = std::pair<SwapIndex, SwapIndex>;
public:
	FFTCoreOriginal(size_t dataSize) : nFFT(dataSize)
	{
		nStages = 1;
		for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

		twiddle.allocate(nFFT);
		for (size_t iStage = 0; iStage < nStages; ++iStage)
		{
			size_t nButterflies = 1ull << iStage;

			auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
			auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

			for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
			{
				double angle = M_PI * iButterfly / nButterflies;
				twiddleReal[iButterfly] = static_cast<Real>(std::cos(angle));
				twiddleImag[iButterfly] = static_cast<Real>(-std::sin(angle));
			}
		}
	}

	void run(Data& data)
	{
		butterflies(data);
	}

private:
	RTC::Array<Complex> twiddle;
	size_t nFFT, nStages;

	void butterflies(Data& data)
	{
		float* real = data.data().realPointer();
		float* imag = data.data().imagPointer();

		size_t length = data.size();

		if (nStages > 0)
		{
			auto twiddleReal = twiddle.real().range(4, 4);
			auto twiddleImag = twiddle.imag().range(4, 4);

			RTC::Simd<float, 4> rt(&twiddleReal[0]);
			RTC::Simd<float, 4> it(&twiddleImag[0]);

            // First stage (iStage = 0, nGroups = nFFT / 2, nButterflies = 1)

            if (nStages > 0)
            {
                for (size_t iGroup = 0; iGroup < nFFT / 2; ++iGroup)
                {
                    size_t i = 2 * iGroup;
                    size_t j = i + 1;

                    // Twiddle = (1, 0)
                    Real reTmp = real[j];
                    Real imTmp = imag[j];

                    real[j] = real[i] - reTmp;
                    imag[j] = imag[i] - imTmp;
                    real[i] = real[i] + reTmp;
                    imag[i] = imag[i] + imTmp;
                }
            }
        }

        // Second stage (iStage = 1, nGroups = nFFT / 4, nButterflies = 2)

        if (nStages > 1)
        {
            for (size_t iGroup = 0; iGroup < nFFT / 4; ++iGroup)
            {
                size_t i = 2 * 2 * iGroup;
                size_t j = i + 2;

                // Twiddle = (1, 0)
                Real reTmp0 = real[j];
                Real imTmp0 = imag[j];

                real[j] = real[i] - reTmp0;
                imag[j] = imag[i] - imTmp0;
                real[i] = real[i] + reTmp0;
                imag[i] = imag[i] + imTmp0;

                // Twiddle = (0, -1)
                Real reTmp1 = imag[j + 1];
                Real imTmp1 = -real[j + 1];

                real[j + 1] = real[i + 1] - reTmp1;
                imag[j + 1] = imag[i + 1] - imTmp1;
                real[i + 1] = real[i + 1] + reTmp1;
                imag[i + 1] = imag[i + 1] + imTmp1;
            }
        }

#if RTC_SIMD_SIZE > 4
        // Third stage (iStage = 2, nGroups = nFFT / 8, nButterflies = 4)
    
        if (nStages > 2)
        {
            auto twiddleReal = twiddle.real().range(4, 4);
            auto twiddleImag = twiddle.imag().range(4, 4);
    
            RTC::Simd<float, 4> rt(&twiddleReal[0]);
            RTC::Simd<float, 4> it(&twiddleImag[0]);
    
            for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
            {
                size_t i = 2 * 4 * iGroup;
                size_t j = i + 4;
    
                RTC::Simd<float, 4> ri(&real[i]);
                RTC::Simd<float, 4> ii(&imag[i]);
                RTC::Simd<float, 4> rj(&real[j]);
                RTC::Simd<float, 4> ij(&imag[j]);

                RTC::Simd<float, 4> rtmp = rj * rt - ij * it;
                RTC::Simd<float, 4> itmp = ij * rt + rj * it;
    
                rj = ri - rtmp;
                ij = ii - itmp;
                ri = ri + rtmp;
                ii = ii + itmp;
    
                ri.store(&real[i]);
                ii.store(&imag[i]);
                rj.store(&real[j]);
                ij.store(&imag[j]);
            }
        }
#endif

#if RTC_SIMD_SIZE > 8
		// Fourth stage (iStage = 3, nGroups = nFFT / 16, nButterflies = 8)

		if (nStages > 3)
		{
			auto twiddleReal = twiddle.real().range(8, 8);
			auto twiddleImag = twiddle.imag().range(8, 8);

			RTC::Simd<float, 8> rt(&twiddleReal[0]);
			RTC::Simd<float, 8> it(&twiddleImag[0]);

			for (size_t iGroup = 0; iGroup < length / 16; ++iGroup)
			{
				size_t i = 2 * 8 * iGroup;
				size_t j = i + 8;

				RTC::Simd<float, 8> ri(real + i);
				RTC::Simd<float, 8> ii(imag + i);
				RTC::Simd<float, 8> rj(real + j);
				RTC::Simd<float, 8> ij(imag + j);

				RTC::Simd<float, 8> rtmp = rj * rt - ij * it;
				RTC::Simd<float, 8> itmp = ij * rt + rj * it;

				rj = ri - rtmp;
				ij = ii - itmp;
				ri = ri + rtmp;
				ii = ii + itmp;

				ri.store(real + i);
				ii.store(imag + i);
				rj.store(real + j);
				ij.store(imag + j);
			}
		}
#endif

		for (size_t iStage = Simd::Size > 4 ? (Simd::Size > 8 ? 4 : 3) : 2; iStage < nStages; ++iStage)
		{
			size_t nButterflies = 1ull << iStage;
			size_t nGroups = length / (2 * nButterflies);

			assert(nGroups * nButterflies == length / 2);
			assert(twiddle.size() >= 2 * nButterflies);

			auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
			auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

			for(size_t iGroup = 0; iGroup < nGroups; ++iGroup)
			{
				assert(nButterflies % Simd::Size == 0);

				for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
				{
					size_t i = 2 * nButterflies * iGroup + iButterfly;
					size_t j = i + nButterflies;

					Simd ri(&real[i]);
					Simd ii(&imag[i]);
					Simd rj(&real[j]);
					Simd ij(&imag[j]);
					Simd rt(&twiddleReal[iButterfly]);
					Simd it(&twiddleImag[iButterfly]);

					Simd rtmp = rj * rt - ij * it;
					Simd itmp = ij * rt + rj * it;

					rj = ri - rtmp;
					ij = ii - itmp;
					ri = ri + rtmp;
					ii = ii + itmp;

					ri.store(&real[i]);
					ii.store(&imag[i]);
					rj.store(&real[j]);
					ij.store(&imag[j]);
				}
			}
		}
	}
};
