#pragma once

#include "Test.hpp"

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(name) name(input, tmp, name ## Result)

class FFTSignal
{
	using Container = RTC::Array<float>;
    using ComplexContainer = RTC::Array<RTC::Complex<float>>;

public:
	static void run(size_t dataSize, size_t nRepetitions, size_t nMeasurements)
	{
        std::cout << "FFTSignal::run(" << dataSize << ", " << nMeasurements << ")" << std::endl;

        TIME_AND_RESULT(loop);
        TIME_AND_RESULT(sse);
        TIME_AND_RESULT(ssertc);
        AVX(TIME_AND_RESULT(avx));
        AVX(TIME_AND_RESULT(avxrtc));
        AVX2(TIME_AND_RESULT(avx2));
        AVX512(TIME_AND_RESULT(avx512));
        AVX512(TIME_AND_RESULT(avx512rtc));

        Container input;
        input.allocate(dataSize);

        ComplexContainer tmp;
        tmp.allocate(dataSize / 2);

        Random random;

        for (size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
        {
            random.fill(input);

            RUN_TEST(loop);
            RUN_TEST(sse);
            RUN_TEST(ssertc);
            AVX(RUN_TEST(avx));
            AVX(RUN_TEST(avxrtc));
            AVX2(RUN_TEST(avx2));
            AVX512(RUN_TEST(avx512));
            AVX512(RUN_TEST(avx512rtc));

            compare("loop", input, loopResult);
            compare("sse", input, sseResult);
            compare("ssertc", input, ssertcResult);
            AVX(compare("avx", input, avxResult));
            AVX(compare("avxrtc", input, avxrtcResult));
            AVX2(compare("avx2", input, avx2Result));
            AVX512(compare("avx512", input, avx512Result));
            AVX512(compare("avx512rtc", input, avx512rtcResult));
        }

        print("loop", loopTime);
        print("sse", sseTime);
        print("ssertc", ssertcTime);
        AVX(print("avx", avxTime));
        AVX(print("avxrtc", avxrtcTime));
        AVX2(print("avx2", avx2Time));
        AVX512(print("avx512", avx512Time));
        AVX512(print("avx512rtc", avx512rtcTime));
	}

private:
    static void loop(const Container& input, ComplexContainer& tmp, Container& output)
    {
        {
            auto it = input.begin();
            auto reIt = tmp.real().begin();
            auto imIt = tmp.imag().begin();

            while (it != input.end())
            {
                *reIt++ = *it++;
                *imIt++ = *it++;
            }
        }

        {
            auto it = output.begin();
            auto reIt = tmp.real().begin();
            auto imIt = tmp.imag().begin();

            while (it != output.end())
            {
                *it++ = *reIt++;
                *it++ = *imIt++;
            }
        }
    }

    static void ssertc(const Container& input, ComplexContainer& tmp, Container& output)
    {
        rtc<4>(input, tmp, output);
    }

#ifdef __AVX__
    static void avxrtc(const Container& input, ComplexContainer& tmp, Container& output)
    {
        rtc<8>(input, tmp, output);
    }
#endif

#ifdef __AVX512F__
    static void avx512rtc(const Container& input, ComplexContainer& tmp, Container& output)
    {
        rtc<16>(input, tmp, output);
    }
#endif

    template<size_t SimdSize>
    static void rtc(const Container& input, ComplexContainer& tmp, Container& output)
    {
        using Simd = RTC::Simd<float, SimdSize>;

        auto real = tmp.real().data();
        auto imag = tmp.imag().data();

        for (size_t i = 0; i < input.size() / 2; i += SimdSize)
        {
            Simd first(&input[2 * i]);
            Simd second(&input[2 * i + SimdSize]);

            auto [even, odd] = Simd::deinterleave(first, second);

            even.store(&real[i]);
            odd.store(&imag[i]);
        }

        for (size_t i = 0; i < input.size() / 2; i += SimdSize)
        {
            Simd even(&real[i]);
            Simd odd(&imag[i]);

            auto [first, second] = Simd::interleave(even, odd);

            first.store(&output[2 * i]);
            second.store(&output[2 * i + SimdSize]);
        }
    }

    static void sse(const Container& input, ComplexContainer& tmp, Container& output)
    {
        constexpr size_t Step = 4;

        {
            auto ptr = input.data();
            auto rePtr = tmp.real().data();
            auto imPtr = tmp.imag().data();

            for (; ptr != input.data() + input.size();
                ptr += 2 * Step, rePtr += Step, imPtr += Step)
            {
                __m128 in1 = _mm_load_ps(ptr);
                __m128 in2 = _mm_load_ps(ptr + Step);

                __m128 even = _mm_shuffle_ps(in1, in2, _MM_SHUFFLE(2, 0, 2, 0));
                __m128 odd = _mm_shuffle_ps(in1, in2, _MM_SHUFFLE(3, 1, 3, 1));

                _mm_store_ps(rePtr, even);
                _mm_store_ps(imPtr, odd);
            }
        }

        {
            auto ptr = output.data();
            auto rePtr = tmp.real().data();
            auto imPtr = tmp.imag().data();

            for (; ptr != output.data() + output.size();
                ptr += 2 * Step, rePtr += Step, imPtr += Step)
            {
                __m128 even = _mm_load_ps(rePtr);
                __m128 odd = _mm_load_ps(imPtr);

                __m128 out1 = _mm_shuffle_ps(even, odd, _MM_SHUFFLE(1, 0, 1, 0));
                __m128 out2 = _mm_shuffle_ps(even, odd, _MM_SHUFFLE(3, 2, 3, 2));

                out1 = _mm_shuffle_ps(out1, out1, _MM_SHUFFLE(3, 1, 2, 0));
                out2 = _mm_shuffle_ps(out2, out2, _MM_SHUFFLE(3, 1, 2, 0));

                _mm_store_ps(ptr, out1);
                _mm_store_ps(ptr + Step, out2);
            }
        }
    }

#ifdef __AVX__
    static void avx(const Container& input, ComplexContainer& tmp, Container& output)
    {
        constexpr size_t Step = 8;

        {
            auto ptr = input.data();
            auto rePtr = tmp.real().data();
            auto imPtr = tmp.imag().data();

            for (; ptr != input.data() + input.size();
                ptr += 2 * Step, rePtr += Step, imPtr += Step)
            {
                __m256 in1 = _mm256_load_ps(ptr);
                __m256 in2 = _mm256_load_ps(ptr + Step);

                __m256 low = _mm256_permute2f128_ps(in1, in2, _MM_SHUFFLE(0, 2, 0, 0));
                __m256 high = _mm256_permute2f128_ps(in1, in2, _MM_SHUFFLE(0, 3, 0, 1));

                __m256 even = _mm256_shuffle_ps(low, high, _MM_SHUFFLE(2, 0, 2, 0));
                __m256 odd = _mm256_shuffle_ps(low, high, _MM_SHUFFLE(3, 1, 3, 1));
                
                _mm256_store_ps(rePtr, even);
                _mm256_store_ps(imPtr, odd);
            }
        }

        {
            auto ptr = output.data();
            auto rePtr = tmp.real().data();
            auto imPtr = tmp.imag().data();

            for (; ptr != output.data() + output.size();
                ptr += 2 * Step, rePtr += Step, imPtr += Step)
            {
                __m256 even = _mm256_load_ps(rePtr);
                __m256 odd = _mm256_load_ps(imPtr);

                __m256 low = _mm256_shuffle_ps(even, odd, _MM_SHUFFLE(1, 0, 1, 0));
                __m256 high = _mm256_shuffle_ps(even, odd, _MM_SHUFFLE(3, 2, 3, 2));

                low = _mm256_shuffle_ps(low, low, _MM_SHUFFLE(3, 1, 2, 0));
                high = _mm256_shuffle_ps(high, high, _MM_SHUFFLE(3, 1, 2, 0));

                __m256 out1 = _mm256_permute2f128_ps(low, high, _MM_SHUFFLE(0, 2, 0, 0));
                __m256 out2 = _mm256_permute2f128_ps(low, high, _MM_SHUFFLE(0, 3, 0, 1));

                _mm256_store_ps(ptr, out1);
                _mm256_store_ps(ptr + Step, out2);
            }
        }
    }
#endif

#ifdef __AVX2__
    static void avx2(const Container& input, ComplexContainer& tmp, Container& output)
    {
        constexpr size_t Step = 8;

        {
            __m256i separateEvenOdd = _mm256_set_epi32(7, 5, 3, 1, 6, 4, 2, 0);

            auto ptr = input.data();
            auto rePtr = tmp.real().data();
            auto imPtr = tmp.imag().data();

            for (; ptr != input.data() + input.size();
                ptr += 2 * Step, rePtr += Step, imPtr += Step)
            {
                __m256 in1 = _mm256_load_ps(ptr);
                __m256 in2 = _mm256_load_ps(ptr + Step);

                __m256 low = _mm256_permutevar8x32_ps(in1, separateEvenOdd);
                __m256 high = _mm256_permutevar8x32_ps(in2, separateEvenOdd);

                __m256 even = _mm256_permute2f128_ps(low, high, _MM_SHUFFLE(0, 2, 0, 0));
                __m256 odd = _mm256_permute2f128_ps(low, high, _MM_SHUFFLE(0, 3, 0, 1));

                _mm256_store_ps(rePtr, even);
                _mm256_store_ps(imPtr, odd);
            }
        }

        {
            __m256i combineEvenOdd = _mm256_set_epi32(7, 3, 6, 2, 5, 1, 4, 0);

            auto ptr = output.data();
            auto rePtr = tmp.real().data();
            auto imPtr = tmp.imag().data();

            for (; ptr != output.data() + output.size();
                ptr += 2 * Step, rePtr += Step, imPtr += Step)
            {
                __m256 even = _mm256_load_ps(rePtr);
                __m256 odd = _mm256_load_ps(imPtr);

                __m256 low = _mm256_permute2f128_ps(even, odd, _MM_SHUFFLE(0, 2, 0, 0));
                __m256 high = _mm256_permute2f128_ps(even, odd, _MM_SHUFFLE(0, 3, 0, 1));

                __m256 out1 = _mm256_permutevar8x32_ps(low, combineEvenOdd);
                __m256 out2 = _mm256_permutevar8x32_ps(high, combineEvenOdd);

                _mm256_store_ps(ptr, out1);
                _mm256_store_ps(ptr + Step, out2);
            }
        }
    }
#endif

#ifdef __AVX512F__
    static void avx512(const Container& input, ComplexContainer& tmp, Container& output)
    {
        constexpr size_t Step = 16;

        {
            __m512i evenIndices = _mm512_set_epi32(
                30, 28, 26, 24,
                22, 20, 18, 16,
                14, 12, 10, 8,
                6, 4, 2, 0);

            __m512i oddIndices = _mm512_set_epi32(
                31, 29, 27, 25,
                23, 21, 19, 17,
                15, 13, 11, 9,
                7, 5, 3, 1);

            auto ptr = input.data();
            auto rePtr = tmp.real().data();
            auto imPtr = tmp.imag().data();

            for (; ptr != input.data() + input.size();
                ptr += 2 * Step, rePtr += Step, imPtr += Step)
            {
                __m512 in1 = _mm512_load_ps(ptr);
                __m512 in2 = _mm512_load_ps(ptr + Step);

                __m512 even = _mm512_permutex2var_ps(in1, evenIndices, in2);
                __m512 odd = _mm512_permutex2var_ps(in1, oddIndices, in2);

                _mm512_store_ps(rePtr, even);
                _mm512_store_ps(imPtr, odd);
            }
        }

        {
            __m512i firstIndices = _mm512_set_epi32(
                23, 7, 22, 6,
                21, 5, 20, 4,
                19, 3, 18, 2,
                17, 1, 16, 0);

            __m512i secondIndices = _mm512_set_epi32(
                31, 15, 30, 14,
                29, 13, 28, 12,
                27, 11, 26, 10,
                25, 9, 24, 8);

            auto ptr = output.data();
            auto rePtr = tmp.real().data();
            auto imPtr = tmp.imag().data();

            for (; ptr != output.data() + output.size();
                ptr += 2 * Step, rePtr += Step, imPtr += Step)
            {
                __m512 even = _mm512_load_ps(rePtr);
                __m512 odd = _mm512_load_ps(imPtr);

                __m512 out1 = _mm512_permutex2var_ps(even, firstIndices, odd);
                __m512 out2 = _mm512_permutex2var_ps(even, secondIndices, odd);

                _mm512_store_ps(ptr, out1);
                _mm512_store_ps(ptr + Step, out2);
            }
        }
    }
#endif
};
