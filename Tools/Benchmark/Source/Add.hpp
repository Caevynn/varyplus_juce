#pragma once

#include "Test.hpp"

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(name) name(a, b, name ## Result)

class Add
{
    using Container = RTC::Array<float>;
public:
    static void run(size_t dataSize, size_t nRepetitions, size_t nMeasurements)
    {
        std::cout << "Add::run(" << dataSize << ", " << nMeasurements << ")" << std::endl;

        TIME_AND_RESULT(loop);
        TIME_AND_RESULT(stl);
        TIME_AND_RESULT(sse);
        AVX(TIME_AND_RESULT(avx));
        AVX512(TIME_AND_RESULT(avx512));
        TIME_AND_RESULT(rtc);

        Container a, b;

        a.allocate(dataSize);
        b.allocate(dataSize);

        Random random;

        for (size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
        {
            random.fill(a);
            random.fill(b);


            RUN_TEST(loop);
            RUN_TEST(stl);
            RUN_TEST(sse);
            AVX(RUN_TEST(avx));
            AVX512(RUN_TEST(avx512));
            RUN_TEST(rtc);

            compare("stl", loopResult, stlResult);
            compare("sse", loopResult, sseResult);
            AVX(compare("avx", loopResult, avxResult));
            AVX512(compare("avx512", loopResult, avx512Result));
            compare("rtc", loopResult, rtcResult);
        }

        print("loop", loopTime);
        print("stl", stlTime);
        print("sse", sseTime);
        AVX(print("avx", avxTime));
        AVX512(print("avx512", avx512Time));
        print("rtc", rtcTime);
    }

private:
    static void loop(const RTC::Array<float>& a, const RTC::Array<float>& b, RTC::Array<float>& result)
    {
        auto aIt = a.begin();
        auto bIt = b.begin();
        for (auto resultIt = result.begin(); resultIt != result.end(); ++aIt, ++bIt, ++resultIt)
        {
            *resultIt = *aIt + *bIt;
        }
    }

    static void stl(const RTC::Array<float>& a, const RTC::Array<float>& b, RTC::Array<float>& result)
    {
        std::transform(a.begin(), a.end(), b.begin(), result.begin(), std::plus<float>());
    }

    static void sse(const RTC::Array<float>& a, const RTC::Array<float>& b, RTC::Array<float>& result)
    {
        const float* aPtr = a.data();
        const float* bPtr = b.data();
        auto* startPtr = result.data();
        auto* endPtr = result.data() + result.size();

        constexpr size_t Step = 4;
        for (auto resultPtr = startPtr; resultPtr < endPtr;
            aPtr += Step, bPtr += Step, resultPtr += Step)
        {
            __m128 aVec = _mm_load_ps(aPtr);
            __m128 bVec = _mm_load_ps(bPtr);
            __m128 resultVec = _mm_add_ps(aVec, bVec);
            _mm_store_ps(resultPtr, resultVec);
        }
    }

#ifdef __AVX__
    static void avx(const RTC::Array<float>& a, const RTC::Array<float>& b, RTC::Array<float>& result)
    {
        const float* aPtr = a.data();
        const float* bPtr = b.data();
        auto* startPtr = result.data();
        auto* endPtr = result.data() + result.size();

        constexpr size_t Step = 8;
        for (auto resultPtr = startPtr; resultPtr < endPtr;
            aPtr += Step, bPtr += Step, resultPtr += Step)
        {
            __m256 aVec = _mm256_load_ps(aPtr);
            __m256 bVec = _mm256_load_ps(bPtr);
            __m256 resultVec = _mm256_add_ps(aVec, bVec);
            _mm256_store_ps(resultPtr, resultVec);
        }
    }
#endif

#ifdef __AVX512F__
    static void avx512(const RTC::Array<float>& a, const RTC::Array<float>& b, RTC::Array<float>& result)
    {
        const float* aPtr = a.data();
        const float* bPtr = b.data();
        auto* startPtr = result.data();
        auto* endPtr = result.data() + result.size();

        constexpr size_t Step = 16;
        for (auto resultPtr = startPtr; resultPtr < endPtr;
            aPtr += Step, bPtr += Step, resultPtr += Step)
        {
            __m512 aVec = _mm512_load_ps(aPtr);
            __m512 bVec = _mm512_load_ps(bPtr);
            __m512 resultVec = _mm512_add_ps(aVec, bVec);
            _mm512_store_ps(resultPtr, resultVec);
        }
    }
#endif

    static void rtc(const RTC::Array<float>& a, const RTC::Array<float>& b, RTC::Array<float>& result)
    {
        result.sum(a, b);
    }
};