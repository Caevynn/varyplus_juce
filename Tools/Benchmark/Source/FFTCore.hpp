#pragma once

#include "Test.hpp"

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(name) name(twiddle, name ## Result)

class FFTCore
{
	using Container = RTC::Array<RTC::Complex<float>>;

public:
    inline static size_t nStages;

	static void run(size_t dataSize, size_t nRepetitions, size_t nMeasurements)
	{
        std::cout << "FFTCore::run(" << dataSize << ", " << nMeasurements << ")" << std::endl;

		TIME_AND_RESULT(loop);
        TIME_AND_RESULT(sse);
        AVX(TIME_AND_RESULT(avx));
        AVX512(TIME_AND_RESULT(avx512));

		Container twiddle, tmp;
        twiddle.allocate(dataSize);
        tmp.allocate(dataSize);

        nStages = 1;
        for (size_t i = dataSize / 2; i > 1; ++nStages, i /= 2);
        for (size_t iStage = 0; iStage < nStages; ++iStage)
        {
            size_t nButterflies = 1ull << iStage;

            auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
            auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

            for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
            {
                float angle = static_cast<float>(M_PI * iButterfly / nButterflies);
                twiddleReal[iButterfly] = std::cos(angle);
                twiddleImag[iButterfly] = -std::sin(angle);
            }
        }

        Random random;

        for (size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
        {
            random.fill(tmp);

            loopResult.copy(tmp);
            sseResult.copy(tmp);
            AVX(avxResult.copy(tmp));
            AVX512(avx512Result.copy(tmp));

            RUN_TEST(loop);
            RUN_TEST(sse);
            AVX(RUN_TEST(avx));
            AVX512(RUN_TEST(avx512));

            compare("sse", loopResult, sseResult);
            AVX(compare("avx", loopResult, avxResult));
            AVX512(compare("avx512", loopResult, avx512Result));
        }

        print("loop", loopTime);
        print("sse", sseTime);
        AVX(print("avx", avxTime));
        AVX512(print("avx512", avx512Time));
	}

private:
    static void firstStages(Container& data)
    {
        auto real = data.real();
        auto imag = data.imag();

        for (size_t iGroup = 0; iGroup < data.size() / 2; ++iGroup)
        {
            size_t i = 2 * iGroup;
            size_t j = i + 1;

            // Twiddle = (1, 0)
            float reTmp = real[j];
            float imTmp = imag[j];

            real[j] = real[i] - reTmp;
            imag[j] = imag[i] - imTmp;
            real[i] = real[i] + reTmp;
            imag[i] = imag[i] + imTmp;
        }

        for (size_t iGroup = 0; iGroup < data.size() / 4; ++iGroup)
        {
            size_t i = 2 * 2 * iGroup;
            size_t j = i + 2;

            // Twiddle = (1, 0)
            float reTmp0 = real[j];
            float imTmp0 = imag[j];

            real[j] = real[i] - reTmp0;
            imag[j] = imag[i] - imTmp0;
            real[i] = real[i] + reTmp0;
            imag[i] = imag[i] + imTmp0;

            // Twiddle = (0, -1)
            float reTmp1 = imag[j + 1];
            float imTmp1 = -real[j + 1];

            real[j + 1] = real[i + 1] - reTmp1;
            imag[j + 1] = imag[i + 1] - imTmp1;
            real[i + 1] = real[i + 1] + reTmp1;
            imag[i + 1] = imag[i + 1] + imTmp1;
        }
    }
    
	static void loop(const Container& twiddle, Container& data)
	{
        auto real = data.real();
        auto imag = data.imag();

        firstStages(data);

        for (size_t iStage = 2; iStage < nStages; ++iStage)
        {
            size_t nGroups = 1ull << (nStages - iStage - 1);
            size_t nButterflies = 1ull << iStage;

            auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
            auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

            for (size_t iGroup = 0; iGroup < nGroups; ++iGroup)
            {
                for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
                {
                    size_t i = 2 * nButterflies * iGroup + iButterfly;
                    size_t j = i + nButterflies;

                    float rtmp = (real[j] * twiddleReal[iButterfly]) - (imag[j] * twiddleImag[iButterfly]);
                    float itmp = (real[j] * twiddleImag[iButterfly]) + (imag[j] * twiddleReal[iButterfly]);

                    real[j] = real[i] - rtmp;
                    imag[j] = imag[i] - itmp;
                    real[i] = real[i] + rtmp;
                    imag[i] = imag[i] + itmp;
                }
            }
        }
	}

    static void sse(const Container& twiddle, Container& data)
    {
        auto real = data.real();
        auto imag = data.imag();

        firstStages(data);

        for (size_t iStage = 2; iStage < nStages; ++iStage)
        {
            size_t nGroups = 1ull << (nStages - iStage - 1);
            size_t nButterflies = 1ull << iStage;

            auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
            auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

            for (size_t iGroup = 0; iGroup < nGroups; ++iGroup)
            {
                for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += 4)
                {
                    size_t i = 2 * nButterflies * iGroup + iButterfly;
                    size_t j = i + nButterflies;

                    RTC::Simd<float, 4> ri(&real[i]);
                    RTC::Simd<float, 4> ii(&imag[i]);
                    RTC::Simd<float, 4> rj(&real[j]);
                    RTC::Simd<float, 4> ij(&imag[j]);
                    RTC::Simd<float, 4> rt(&twiddleReal[iButterfly]);
                    RTC::Simd<float, 4> it(&twiddleImag[iButterfly]);

                    RTC::Simd<float, 4> rtmp = rj * rt - ij * it;
                    RTC::Simd<float, 4> itmp = ij * rt + rj * it;

                    rj = ri - rtmp;
                    ij = ii - itmp;
                    ri = ri + rtmp;
                    ii = ii + itmp;

                    ri.store(&real[i]);
                    ii.store(&imag[i]);
                    rj.store(&real[j]);
                    ij.store(&imag[j]);
                }
            }
        }
    }

#ifdef __AVX__
    static void avx(const Container& twiddle, Container& data)
    {
        auto real = data.real();
        auto imag = data.imag();

        firstStages(data);

        {
            auto twiddleReal = twiddle.real().range(4, 4);
            auto twiddleImag = twiddle.imag().range(4, 4);

            RTC::Simd<float, 4> rt(&twiddleReal[0]);
            RTC::Simd<float, 4> it(&twiddleImag[0]);

            for (size_t iGroup = 0; iGroup < data.size() / 8; ++iGroup)
            {
                size_t i = 2 * 4 * iGroup;
                size_t j = i + 4;

                RTC::Simd<float, 4> ri(&real[i]);
                RTC::Simd<float, 4> ii(&imag[i]);
                RTC::Simd<float, 4> rj(&real[j]);
                RTC::Simd<float, 4> ij(&imag[j]);

                RTC::Simd<float, 4> rtmp = rj * rt - ij * it;
                RTC::Simd<float, 4> itmp = ij * rt + rj * it;

                rj = ri - rtmp;
                ij = ii - itmp;
                ri = ri + rtmp;
                ii = ii + itmp;

                ri.store(&real[i]);
                ii.store(&imag[i]);
                rj.store(&real[j]);
                ij.store(&imag[j]);
            }
        }

        for (size_t iStage = 3; iStage < nStages; ++iStage)
        {
            size_t nGroups = 1ull << (nStages - iStage - 1);
            size_t nButterflies = 1ull << iStage;

            auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
            auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

            for (size_t iGroup = 0; iGroup < nGroups; ++iGroup)
            {
                for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += 8)
                {
                    size_t i = 2 * nButterflies * iGroup + iButterfly;
                    size_t j = i + nButterflies;

                    RTC::Simd<float, 8> ri(&real[i]);
                    RTC::Simd<float, 8> ii(&imag[i]);
                    RTC::Simd<float, 8> rj(&real[j]);
                    RTC::Simd<float, 8> ij(&imag[j]);
                    RTC::Simd<float, 8> rt(&twiddleReal[iButterfly]);
                    RTC::Simd<float, 8> it(&twiddleImag[iButterfly]);

                    RTC::Simd<float, 8> rtmp = rj * rt - ij * it;
                    RTC::Simd<float, 8> itmp = ij * rt + rj * it;

                    rj = ri - rtmp;
                    ij = ii - itmp;
                    ri = ri + rtmp;
                    ii = ii + itmp;

                    ri.store(&real[i]);
                    ii.store(&imag[i]);
                    rj.store(&real[j]);
                    ij.store(&imag[j]);
                }
            }
        }
    }
#endif

#ifdef __AVX512F__
    static void avx512(const Container& twiddle, Container& data)
    {
        auto real = data.real();
        auto imag = data.imag();

        firstStages(data);

        {
            auto twiddleReal = twiddle.real().range(4, 4);
            auto twiddleImag = twiddle.imag().range(4, 4);

            RTC::Simd<float, 4> rt(&twiddleReal[0]);
            RTC::Simd<float, 4> it(&twiddleImag[0]);

            for (size_t iGroup = 0; iGroup < data.size() / 8; ++iGroup)
            {
                size_t i = 2 * 4 * iGroup;
                size_t j = i + 4;

                RTC::Simd<float, 4> ri(&real[i]);
                RTC::Simd<float, 4> ii(&imag[i]);
                RTC::Simd<float, 4> rj(&real[j]);
                RTC::Simd<float, 4> ij(&imag[j]);

                RTC::Simd<float, 4> rtmp = rj * rt - ij * it;
                RTC::Simd<float, 4> itmp = ij * rt + rj * it;

                rj = ri - rtmp;
                ij = ii - itmp;
                ri = ri + rtmp;
                ii = ii + itmp;

                ri.store(&real[i]);
                ii.store(&imag[i]);
                rj.store(&real[j]);
                ij.store(&imag[j]);
            }
        }

        {
            auto twiddleReal = twiddle.real().range(8, 8);
            auto twiddleImag = twiddle.imag().range(8, 8);

            RTC::Simd<float, 8> rt(&twiddleReal[0]);
            RTC::Simd<float, 8> it(&twiddleImag[0]);

            for (size_t iGroup = 0; iGroup < data.size() / 16; ++iGroup)
            {
                size_t i = 2 * 8 * iGroup;
                size_t j = i + 8;

                RTC::Simd<float, 8> ri(&real[i]);
                RTC::Simd<float, 8> ii(&imag[i]);
                RTC::Simd<float, 8> rj(&real[j]);
                RTC::Simd<float, 8> ij(&imag[j]);

                RTC::Simd<float, 8> rtmp = rj * rt - ij * it;
                RTC::Simd<float, 8> itmp = ij * rt + rj * it;

                rj = ri - rtmp;
                ij = ii - itmp;
                ri = ri + rtmp;
                ii = ii + itmp;

                ri.store(&real[i]);
                ii.store(&imag[i]);
                rj.store(&real[j]);
                ij.store(&imag[j]);
            }
        }

        for (size_t iStage = 4; iStage < nStages; ++iStage)
        {
            size_t nGroups = 1ull << (nStages - iStage - 1);
            size_t nButterflies = 1ull << iStage;

            auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
            auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

            for (size_t iGroup = 0; iGroup < nGroups; ++iGroup)
            {
                for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += 16)
                {
                    size_t i = 2 * nButterflies * iGroup + iButterfly;
                    size_t j = i + nButterflies;

                    RTC::Simd<float, 16> ri(&real[i]);
                    RTC::Simd<float, 16> ii(&imag[i]);
                    RTC::Simd<float, 16> rj(&real[j]);
                    RTC::Simd<float, 16> ij(&imag[j]);
                    RTC::Simd<float, 16> rt(&twiddleReal[iButterfly]);
                    RTC::Simd<float, 16> it(&twiddleImag[iButterfly]);

                    RTC::Simd<float, 16> rtmp = rj * rt - ij * it;
                    RTC::Simd<float, 16> itmp = ij * rt + rj * it;

                    rj = ri - rtmp;
                    ij = ii - itmp;
                    ri = ri + rtmp;
                    ii = ii + itmp;

                    ri.store(&real[i]);
                    ii.store(&imag[i]);
                    rj.store(&real[j]);
                    ij.store(&imag[j]);
                }
            }
        }
    }
#endif
};
