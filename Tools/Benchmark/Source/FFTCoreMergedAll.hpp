#pragma once

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

class FFTCoreMergedAll
{
    using Real = float;
    using Complex = RTC::Complex<Real>;
    using Simd = RTC::Simd<Real, RTC_SIMD_SIZE>;
    using SimdComplex = RTC::SimdComplex<Real, RTC_SIMD_SIZE>;
	using Data = RTC::Range<Complex, RTC::Regular>;

    using SwapIndex = uint16_t;
    using SwapPair = std::pair<SwapIndex, SwapIndex>;
public:
	FFTCoreMergedAll(size_t dataSize) : nFFT(dataSize)
	{
		nStages = 1;
		for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

		twiddle.allocate(nFFT);
		for (size_t iStage = 0; iStage < nStages; ++iStage)
		{
			size_t nButterflies = 1ull << iStage;

			auto rt = twiddle.real().range(nButterflies, nButterflies);
			auto it = twiddle.imag().range(nButterflies, nButterflies);

			for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
			{
				double angle = M_PI * iButterfly / nButterflies;
				rt[iButterfly] = static_cast<Real>(std::cos(angle));
				it[iButterfly] = static_cast<Real>(-std::sin(angle));
			}
		}
	}

	void run(Data& data)
	{
		butterflies(data);
	}

private:
	RTC::Array<Complex> twiddle;
	size_t nFFT, nStages;

	void butterflies(Data& data)
	{
		float* real = data.data().realPointer();
		float* imag = data.data().imagPointer();

		// Third stage (iStage = 2, nGroups = nFFT / 8, nButterflies = 4)

		if (nStages > 2)
		{
            float* rt = twiddle.data().realPointer() + 4;
            float* it = twiddle.data().imagPointer() + 4;

			RTC::SimdComplex<float, 4> ct(&rt[0], &it[0]);

			for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
			{
				size_t i = 8 * iGroup;

				float r0 = real[i + 0];
				float i0 = imag[i + 0];
				float r1 = real[i + 1];
				float i1 = imag[i + 1];
				float r2 = real[i + 2];
				float i2 = imag[i + 2];
				float r3 = real[i + 3];
				float i3 = imag[i + 3];
				float r4 = real[i + 4];
				float i4 = imag[i + 4];
				float r5 = real[i + 5];
				float i5 = imag[i + 5];
				float r6 = real[i + 6];
				float i6 = imag[i + 6];
				float r7 = real[i + 7];
				float i7 = imag[i + 7];

				RTC::SimdComplex<float, 4> c0(
					RTC::Simd<float, 4>(
						(r0 + r1) + (r2 + r3),
						(r0 - r1) + (i2 - i3),
						(r0 + r1) - (r2 + r3),
						(r0 - r1) - (i2 - i3)
					), 
					RTC::Simd<float, 4>(
						(i0 + i1) + (i2 + i3),
						(i0 - i1) - (r2 - r3),
						(i0 + i1) - (i2 + i3),
						(i0 - i1) + (r2 - r3)
					)
				);

				RTC::SimdComplex<float, 4> c4(
					RTC::Simd<float, 4>(
						(r4 + r5) + (r6 + r7),
						(r4 - r5) + (i6 - i7),
						(r4 + r5) - (r6 + r7),
						(r4 - r5) - (i6 - i7)
					), 
					RTC::Simd<float, 4>(
						(i4 + i5) + (i6 + i7),
						(i4 - i5) - (r6 - r7),
						(i4 + i5) - (i6 + i7),
						(i4 - i5) + (r6 - r7)
					)
				);

				RTC::SimdComplex<float, 4> c4tmp = c4 * ct;

				(c0 + c4tmp).store(&real[i + 0], &imag[i + 0]);
				(c0 - c4tmp).store(&real[i + 4], &imag[i + 4]);
			}
		}

#if RTC_SIMD_SIZE > 4
		// Fourth stage (iStage = 3, nGroups = nFFT / 16, nButterflies = 8)

		if (nStages > 4)
		{
			auto rt0 = twiddle.data().realPointer() + 8;
			auto it0 = twiddle.data().imagPointer() + 8;
			auto rt1 = twiddle.data().realPointer() + 16;
			auto it1 = twiddle.data().imagPointer() + 16;

			RTC::SimdComplex<float, 8> ct0(&rt0[0], &it0[0]);
			RTC::SimdComplex<float, 8> ct10(&rt1[0], &it1[0]);
			RTC::SimdComplex<float, 8> ct11(&rt1[8], &it1[8]);

			for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
			{
				size_t i0 = 32 * iGroup;
				size_t i1 = i0 + 8;
				size_t i2 = i0 + 16;
				size_t i3 = i0 + 24;

				RTC::SimdComplex<float, 8> c0(&real[i0], &imag[i0]);
				RTC::SimdComplex<float, 8> c1(&real[i1], &imag[i1]);
				RTC::SimdComplex<float, 8> c2(&real[i2], &imag[i2]);
				RTC::SimdComplex<float, 8> c3(&real[i3], &imag[i3]);

				RTC::SimdComplex<float, 8> c1tmp0 = c1 * ct0;
				RTC::SimdComplex<float, 8> c3tmp0 = c3 * ct0;
				RTC::SimdComplex<float, 8> c2tmp1 = (c2 + c3tmp0) * ct10;
				RTC::SimdComplex<float, 8> c3tmp1 = (c2 - c3tmp0) * ct11;
				
			    (c0 + c1tmp0 + c2tmp1).store(&real[i0], &imag[i0]);
				(c0 - c1tmp0 + c3tmp1).store(&real[i1], &imag[i1]);
				(c0 + c1tmp0 - c2tmp1).store(&real[i2], &imag[i2]);
				(c0 - c1tmp0 - c3tmp1).store(&real[i3], &imag[i3]);
            }
		}
#endif

#if RTC_SIMD_SIZE == 16
		// Fourth stage (iStage = 3, nGroups = nFFT / 16, nButterflies = 8)

		if (nStages > 4)
		{
			auto rt0 = twiddle.data().realPointer() + 8;
			auto it0 = twiddle.data().imagPointer() + 8;
			auto rt1 = twiddle.data().realPointer() + 16;
			auto it1 = twiddle.data().imagPointer() + 16;

			RTC::SimdComplex<float, 8> ct0(&rt0[0], &it0[0]);
			RTC::SimdComplex<float, 16> ct1(&rt1[0], &it1[0]);
            
			for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
			{
				size_t i0 = 32 * iGroup;
				size_t i1 = i0 + 8;
				size_t i2 = i0 + 16;
				size_t i3 = i0 + 24;

				RTC::SimdComplex<float, 8> c0(&real[i0], &imag[i0]);
				RTC::SimdComplex<float, 8> c1(&real[i1], &imag[i1]);
				RTC::SimdComplex<float, 8> c2(&real[i2], &imag[i2]);
				RTC::SimdComplex<float, 8> c3(&real[i3], &imag[i3]);

				RTC::SimdComplex<float, 8> c1tmp0 = c1 * ct0;
				RTC::SimdComplex<float, 8> c3tmp0 = c3 * ct0;

                RTC::SimdComplex<float, 16> c01(c0 + c1tmp0, c0 - c1tmp0);
                RTC::SimdComplex<float, 16> c23(c2 + c3tmp0, c2 - c3tmp0);

				RTC::SimdComplex<float, 16> c23tmp1 = c23 * ct1;
				
				(c01 + c23tmp1).store(&real[i0], &imag[i0]);
				(c01 - c23tmp1).store(&real[i2], &imag[i2]);
			}
		}
#endif

        if(nStages % 2 == 0)
        {
            size_t iStage = Simd::Size > 4 ? 5 : 3;

            if(iStage < nStages)
            {
                size_t nGroups = 1ull << (nStages - iStage - 1);
                size_t nButterflies = 1ull << iStage;

                assert(nGroups * nButterflies == nFFT / 2);
                assert(twiddle.size() >= 2 * nButterflies);

                auto rt = twiddle.data().realPointer() + nButterflies;
                auto it = twiddle.data().imagPointer() + nButterflies;

                for(size_t iGroup = 0; iGroup < nGroups; ++iGroup)
                {
                    assert(nButterflies % Simd::Size == 0);

                    for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
                    {
                        size_t i0 = 2 * nButterflies * iGroup + iButterfly;
                        size_t i1 = i0 + nButterflies;

                        SimdComplex ct(&rt[iButterfly], &it[iButterfly]);

                        SimdComplex c0(&real[i0], &imag[i0]);
                        SimdComplex c1(&real[i1], &imag[i1]);

                        SimdComplex c1tmp = c1 * ct;

                        (c0 + c1tmp).store(&real[i0], &imag[i0]);
                        (c0 - c1tmp).store(&real[i1], &imag[i1]);
                    }
                }
            }
        }

        for (size_t iStage = Simd::Size > 4 ? 6 : 4; iStage < nStages; iStage += 2)
		{
			size_t nGroups = 1ull << (nStages - iStage - 1);
			size_t nButterflies = 1ull << iStage;

			assert(nGroups * nButterflies == nFFT / 2);
			assert(twiddle.size() >= 2 * nButterflies);

			auto rt0 = twiddle.data().realPointer() + nButterflies;
			auto it0 = twiddle.data().imagPointer() + nButterflies;
            auto rt1 = twiddle.data().realPointer() + 2 * nButterflies;
            auto it1 = twiddle.data().imagPointer() + 2 * nButterflies;

			for(size_t iGroup = 0; iGroup < nGroups / 2; ++iGroup)
			{
				assert(nButterflies % Simd::Size == 0);

				for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
				{
					size_t i0 = 4 * nButterflies * iGroup + iButterfly;
					size_t i1 = i0 + nButterflies;
                    size_t i2 = i0 + 2 * nButterflies;
                    size_t i3 = i0 + 3 * nButterflies;

					SimdComplex ct0(&rt0[iButterfly], &it0[iButterfly]);
					SimdComplex ct10(&rt1[iButterfly], &it1[iButterfly]);
					SimdComplex ct11(&rt1[iButterfly + nButterflies], &it1[iButterfly + nButterflies]);
					
					SimdComplex c0(&real[i0], &imag[i0]);
					SimdComplex c1(&real[i1], &imag[i1]);
					SimdComplex c2(&real[i2], &imag[i2]);
					SimdComplex c3(&real[i3], &imag[i3]);
                    
                    SimdComplex c1tmp0 = c1 * ct0;
                    SimdComplex c3tmp0 = c3 * ct0;
                    SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
                    SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;
                    
                    (c0 + c1tmp0 + c2tmp1).store(&real[i0], &imag[i0]);
                    (c0 - c1tmp0 + c3tmp1).store(&real[i1], &imag[i1]);
                    (c0 + c1tmp0 - c2tmp1).store(&real[i2], &imag[i2]);
                    (c0 - c1tmp0 - c3tmp1).store(&real[i3], &imag[i3]);
				}
			}
		}
	}
};
