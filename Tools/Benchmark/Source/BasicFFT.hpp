#pragma once

#include "Test.hpp"

#include "../../../Source/AudioProcessor/Tools/RealFFT.hpp"
#include "ReferenceFFT/ReferenceFFT.hpp"

class BasicFFT
{
	using ComplexContainer = RTC::Array<RTC::Complex<float>>;
	using RealContainer = RTC::Array<float>;

public:
	static bool run()
	{
		return forward(256) && forward(512) && inverse(256) && inverse(512) && both(256) && both(512);
	}

private:
	static constexpr double precision = 1e-6;

	static bool forward(size_t dataLength)
	{
		RealFFT realFFT;
		ReferenceFFT refFFT;

		realFFT.allocate(dataLength);
		refFFT.allocate(dataLength);

		RealContainer realSignal, refSignal;
		ComplexContainer realSpectrum, refSpectrum;

		realSignal.allocate(realFFT.signalLength());
		refSignal.allocate(refFFT.signalLength());
		realSpectrum.allocate(realFFT.spectrumLength());
		refSpectrum.allocate(refFFT.spectrumLength());

		Random random;

		random.fill(realSignal);
		refSignal.copy(realSignal);

		realFFT.forward(realSignal, realSpectrum);
		refFFT.forward(refSignal, refSpectrum);

		return compare("BasicFFT::forward", realSpectrum.sameRange(refSpectrum), refSpectrum, 1e-5);
	}

	static bool inverse(size_t dataLength)
	{
		RealFFT realFFT;
		ReferenceFFT refFFT;

		realFFT.allocate(dataLength);
		refFFT.allocate(dataLength);

		RealContainer realSignal, refSignal;
		ComplexContainer realSpectrum, refSpectrum;

		realSignal.allocate(realFFT.signalLength());
		refSignal.allocate(refFFT.signalLength());
		realSpectrum.allocate(realFFT.spectrumLength());
		refSpectrum.allocate(refFFT.spectrumLength());

		Random random;

		random.fill(refSpectrum);
		realSpectrum.sameRange(refSpectrum).copy(refSpectrum);

		realFFT.inverse(realSpectrum, realSignal);
		refFFT.inverse(refSpectrum, refSignal);

		return compare("BasicFFT::inverse", realSignal, refSignal, 1e-5);
	}

	static bool both(size_t dataLength)
	{
		RealFFT realFFT;
		ReferenceFFT refFFT;

		realFFT.allocate(dataLength);
		refFFT.allocate(dataLength);

		RealContainer realSignal, refSignal;
		ComplexContainer realSpectrum, refSpectrum;

		realSignal.allocate(realFFT.signalLength());
		refSignal.allocate(refFFT.signalLength());
		realSpectrum.allocate(realFFT.spectrumLength());
		refSpectrum.allocate(refFFT.spectrumLength());

		Random random;

		random.fill(realSignal);
		refSignal.copy(realSignal);

		realFFT.forward(realSignal, realSpectrum);
		refFFT.forward(refSignal, refSpectrum);

		realFFT.inverse(realSpectrum, realSignal);
		refFFT.inverse(refSpectrum, refSignal);

		return compare("BasicFFT::both", realSignal, refSignal, 1e-5);
	}
};
