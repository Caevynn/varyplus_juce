#pragma once

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

class FFTCoreMergedMore
{
    using Real = float;
    using Complex = RTC::Complex<Real>;
    using Simd = RTC::Simd<Real, RTC_SIMD_SIZE>;
    using SimdComplex = RTC::SimdComplex<Real, RTC_SIMD_SIZE>;
	using Data = RTC::Range<Complex, RTC::Regular>;

    using SwapIndex = uint16_t;
    using SwapPair = std::pair<SwapIndex, SwapIndex>;
public:
	FFTCoreMergedMore(size_t dataSize) : nFFT(dataSize)
	{
		nStages = 1;
		for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

		twiddle.allocate(nFFT);
		for (size_t iStage = 0; iStage < nStages; ++iStage)
		{
			size_t nButterflies = 1ull << iStage;

			auto rt = twiddle.real().range(nButterflies, nButterflies);
			auto it = twiddle.imag().range(nButterflies, nButterflies);

			for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
			{
				double angle = M_PI * iButterfly / nButterflies;
				rt[iButterfly] = static_cast<Real>(std::cos(angle));
				it[iButterfly] = static_cast<Real>(-std::sin(angle));
			}
		}
	}

	void run(Data& data)
	{
		butterflies(data);
	}

private:
	RTC::Array<Complex> twiddle;
	size_t nFFT, nStages;

	void butterflies(Data& data)
	{
		float* real = data.data().realPointer();
		float* imag = data.data().imagPointer();

#if RTC_SIMD_SIZE == 4
		// Stages 1 - 3

		if (nStages > 2)
		{
            float* rt = twiddle.data().realPointer() + 4;
            float* it = twiddle.data().imagPointer() + 4;

			RTC::SimdComplex<float, 4> ct(&rt[0], &it[0]);

			for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
			{
				size_t i = 8 * iGroup;

				float r0 = real[i + 0];
				float i0 = imag[i + 0];
				float r1 = real[i + 1];
				float i1 = imag[i + 1];
				float r2 = real[i + 2];
				float i2 = imag[i + 2];
				float r3 = real[i + 3];
				float i3 = imag[i + 3];
				float r4 = real[i + 4];
				float i4 = imag[i + 4];
				float r5 = real[i + 5];
				float i5 = imag[i + 5];
				float r6 = real[i + 6];
				float i6 = imag[i + 6];
				float r7 = real[i + 7];
				float i7 = imag[i + 7];

				RTC::SimdComplex<float, 4> c0(
					RTC::Simd<float, 4>(
						(r0 + r1) + (r2 + r3),
						(r0 - r1) + (i2 - i3),
						(r0 + r1) - (r2 + r3),
						(r0 - r1) - (i2 - i3)
					),
					RTC::Simd<float, 4>(
						(i0 + i1) + (i2 + i3),
						(i0 - i1) - (r2 - r3),
						(i0 + i1) - (i2 + i3),
						(i0 - i1) + (r2 - r3)
					)
				);

				RTC::SimdComplex<float, 4> c4(
					RTC::Simd<float, 4>(
						(r4 + r5) + (r6 + r7),
						(r4 - r5) + (i6 - i7),
						(r4 + r5) - (r6 + r7),
						(r4 - r5) - (i6 - i7)
					),
					RTC::Simd<float, 4>(
						(i4 + i5) + (i6 + i7),
						(i4 - i5) - (r6 - r7),
						(i4 + i5) - (i6 + i7),
						(i4 - i5) + (r6 - r7)
					)
				);

				RTC::SimdComplex<float, 4> c4tmp = c4 * ct;

				(c0 + c4tmp).store(&real[i + 0], &imag[i + 0]);
				(c0 - c4tmp).store(&real[i + 4], &imag[i + 4]);
			}
		}
#elif RTC_SIMD_SIZE > 4
        // Stages 1 - 4

		if (nStages > 3)
		{
            float* rt0 = twiddle.data().realPointer() + 4;
            float* it0 = twiddle.data().imagPointer() + 4;
            float* rt1 = twiddle.data().realPointer() + 8;
            float* it1 = twiddle.data().imagPointer() + 8;

			RTC::SimdComplex<float, 4> ct0(&rt0[0], &it0[0]);
			RTC::SimdComplex<float, 8> ct1(&rt1[0], &it1[0]);

			for (size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
			{
				size_t i = 16 * iGroup;

				float r0 = real[i + 0];
				float i0 = imag[i + 0];
				float r1 = real[i + 1];
				float i1 = imag[i + 1];
				float r2 = real[i + 2];
				float i2 = imag[i + 2];
				float r3 = real[i + 3];
				float i3 = imag[i + 3];
				float r4 = real[i + 4];
				float i4 = imag[i + 4];
				float r5 = real[i + 5];
				float i5 = imag[i + 5];
				float r6 = real[i + 6];
				float i6 = imag[i + 6];
				float r7 = real[i + 7];
				float i7 = imag[i + 7];
				float r8 = real[i + 8];
				float i8 = imag[i + 8];
				float r9 = real[i + 9];
				float i9 = imag[i + 9];
				float r10 = real[i + 10];
				float i10 = imag[i + 10];
				float r11 = real[i + 11];
				float i11 = imag[i + 11];
				float r12 = real[i + 12];
				float i12 = imag[i + 12];
				float r13 = real[i + 13];
				float i13 = imag[i + 13];
				float r14 = real[i + 14];
				float i14 = imag[i + 14];
				float r15 = real[i + 15];
				float i15 = imag[i + 15];

				RTC::SimdComplex<float, 4> c0(
					RTC::Simd<float, 4>(
						(r0 + r1) + (r2 + r3),
						(r0 - r1) + (i2 - i3),
						(r0 + r1) - (r2 + r3),
						(r0 - r1) - (i2 - i3)
					),
					RTC::Simd<float, 4>(
						(i0 + i1) + (i2 + i3),
						(i0 - i1) - (r2 - r3),
						(i0 + i1) - (i2 + i3),
						(i0 - i1) + (r2 - r3)
					)
				);

				RTC::SimdComplex<float, 4> c1(
					RTC::Simd<float, 4>(
						(r4 + r5) + (r6 + r7),
						(r4 - r5) + (i6 - i7),
						(r4 + r5) - (r6 + r7),
						(r4 - r5) - (i6 - i7)
					),
					RTC::Simd<float, 4>(
						(i4 + i5) + (i6 + i7),
						(i4 - i5) - (r6 - r7),
						(i4 + i5) - (i6 + i7),
						(i4 - i5) + (r6 - r7)
					)
				);

				RTC::SimdComplex<float, 4> c2(
					RTC::Simd<float, 4>(
						(r8 + r9) + (r10 + r11),
						(r8 - r9) + (i10 - i11),
						(r8 + r9) - (r10 + r11),
						(r8 - r9) - (i10 - i11)
					),
					RTC::Simd<float, 4>(
						(i8 + i9) + (i10 + i11),
						(i8 - i9) - (r10 - r11),
						(i8 + i9) - (i10 + i11),
						(i8 - i9) + (r10 - r11)
					)
				);

				RTC::SimdComplex<float, 4> c3(
					RTC::Simd<float, 4>(
						(r12 + r13) + (r14 + r15),
						(r12 - r13) + (i14 - i15),
						(r12 + r13) - (r14 + r15),
						(r12 - r13) - (i14 - i15)
					),
					RTC::Simd<float, 4>(
						(i12 + i13) + (i14 + i15),
						(i12 - i13) - (r14 - r15),
						(i12 + i13) - (i14 + i15),
						(i12 - i13) + (r14 - r15)
					)
				);

				RTC::SimdComplex<float, 4> c1tmp0 = c1 * ct0;
				RTC::SimdComplex<float, 4> c3tmp0 = c3 * ct0;

                RTC::SimdComplex<float, 8> c01(c0 + c1tmp0, c0 - c1tmp0);
                RTC::SimdComplex<float, 8> c23(c2 + c3tmp0, c2 - c3tmp0);

                RTC::SimdComplex<float, 8> c23tmp1 = c23 * ct1;

				(c01 + c23tmp1).store(&real[i + 0], &imag[i + 0]);
				(c01 - c23tmp1).store(&real[i + 8], &imag[i + 8]);
			}
		}

/*#   if RTC_SIMD_SIZE > 8
        if(nStages > 4)
        {
            size_t iStage = 4;

            if(iStage < nStages)
            {
                size_t nGroups = 1ull << (nStages - iStage - 1);
                size_t nButterflies = 1ull << iStage;

                assert(nGroups * nButterflies == nFFT / 2);
                assert(twiddle.size() >= 2 * nButterflies);

                auto rt = twiddle.data().realPointer() + nButterflies;
                auto it = twiddle.data().imagPointer() + nButterflies;

                for(size_t iGroup = 0; iGroup < nGroups; ++iGroup)
                {
                    assert(nButterflies % Simd::Size == 0);

                    for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
                    {
                        size_t i0 = 2 * nButterflies * iGroup + iButterfly;
                        size_t i1 = i0 + nButterflies;

                        SimdComplex ct(&rt[iButterfly], &it[iButterfly]);

                        SimdComplex c0(&real[i0], &imag[i0]);
                        SimdComplex c1(&real[i1], &imag[i1]);

                        SimdComplex c1tmp = c1 * ct;

                        (c0 + c1tmp).store(&real[i0], &imag[i0]);
                        (c0 - c1tmp).store(&real[i1], &imag[i1]);
                    }
                }
            }
        }
#   endif*/
#endif

#if RTC_SIMD_SIZE == 4
        size_t startStage = 3;
#elif RTC_SIMD_SIZE == 8
        size_t startStage = 4;
#elif RTC_SIMD_SIZE == 16
        if(nStages > 4)
        {
            singleStage(data, 4);
        }

        size_t startStage = 5;
#endif

        if(startStage >= nStages)
        {
            return;
        }

        if((nStages - startStage) % 3 == 1)
        {
            if(startStage < nStages)
            {
                singleStage(data, startStage);
            }

            ++startStage;
        }

        if((nStages - startStage) % 3 == 2)
        {
            if(startStage < nStages)
            {
                doubleStage(data, startStage);
            }

            startStage += 2;
        }

        for (size_t iStage = startStage; iStage < nStages; iStage += 3)
		{
            tripleStage(data, iStage);
		}
	}

    void singleStage(Data& data, size_t iStage)
    {
        auto real = data.data().realPointer();
        auto imag = data.data().imagPointer();

        size_t nGroups = 1ull << (nStages - iStage - 1);
        size_t nButterflies = 1ull << iStage;

        assert(nGroups * nButterflies == nFFT / 2);
        assert(twiddle.size() >= 2 * nButterflies);

        auto rt = twiddle.data().realPointer() + nButterflies;
        auto it = twiddle.data().imagPointer() + nButterflies;

        for(size_t iGroup = 0; iGroup < nGroups; ++iGroup)
        {
            assert(nButterflies % Simd::Size == 0);

            for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
            {
                size_t i0 = 2 * nButterflies * iGroup + iButterfly;
                size_t i1 = i0 + nButterflies;

                SimdComplex ct(&rt[iButterfly], &it[iButterfly]);

                SimdComplex c0(&real[i0], &imag[i0]);
                SimdComplex c1(&real[i1], &imag[i1]);

                SimdComplex c1tmp = c1 * ct;

                (c0 + c1tmp).store(&real[i0], &imag[i0]);
                (c0 - c1tmp).store(&real[i1], &imag[i1]);
            }
        }
    }

    void doubleStage(Data& data, size_t iStage)
    {
        auto real = data.data().realPointer();
        auto imag = data.data().imagPointer();

        size_t nGroups = 1ull << (nStages - iStage - 1);
        size_t nButterflies = 1ull << iStage;

        assert(nGroups * nButterflies == nFFT / 2);
        assert(twiddle.size() >= 2 * nButterflies);

        auto rt0 = twiddle.data().realPointer() + nButterflies;
        auto it0 = twiddle.data().imagPointer() + nButterflies;
        auto rt1 = twiddle.data().realPointer() + 2 * nButterflies;
        auto it1 = twiddle.data().imagPointer() + 2 * nButterflies;

        for(size_t iGroup = 0; iGroup < nGroups / 2; ++iGroup)
        {
            assert(nButterflies % Simd::Size == 0);

            for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
            {
                size_t i0 = 4 * nButterflies * iGroup + iButterfly;
                size_t i1 = i0 + nButterflies;
                size_t i2 = i0 + 2 * nButterflies;
                size_t i3 = i0 + 3 * nButterflies;

                SimdComplex ct0(&rt0[iButterfly], &it0[iButterfly]);
                SimdComplex ct10(&rt1[iButterfly], &it1[iButterfly]);
                SimdComplex ct11(&rt1[iButterfly + nButterflies], &it1[iButterfly + nButterflies]);
                
                SimdComplex c0(&real[i0], &imag[i0]);
                SimdComplex c1(&real[i1], &imag[i1]);
                SimdComplex c2(&real[i2], &imag[i2]);
                SimdComplex c3(&real[i3], &imag[i3]);
                
                SimdComplex c1tmp0 = c1 * ct0;
                SimdComplex c3tmp0 = c3 * ct0;
                SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
                SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;
                
                (c0 + c1tmp0 + c2tmp1).store(&real[i0], &imag[i0]);
                (c0 - c1tmp0 + c3tmp1).store(&real[i1], &imag[i1]);
                (c0 + c1tmp0 - c2tmp1).store(&real[i2], &imag[i2]);
                (c0 - c1tmp0 - c3tmp1).store(&real[i3], &imag[i3]);
            }
        }
    }

    void tripleStage(Data& data, size_t iStage)
    {
        auto real = data.data().realPointer();
        auto imag = data.data().imagPointer();

        size_t nGroups = 1ull << (nStages - iStage - 1);
        size_t nButterflies = 1ull << iStage;

        assert(nGroups * nButterflies == nFFT / 2);
        assert(twiddle.size() >= 2 * nButterflies);

        auto rt0 = twiddle.data().realPointer() + nButterflies;
        auto it0 = twiddle.data().imagPointer() + nButterflies;
        auto rt1 = twiddle.data().realPointer() + 2 * nButterflies;
        auto it1 = twiddle.data().imagPointer() + 2 * nButterflies;
        auto rt2 = twiddle.data().realPointer() + 4 * nButterflies;
        auto it2 = twiddle.data().imagPointer() + 4 * nButterflies;

        for(size_t iGroup = 0; iGroup < nGroups / 4; ++iGroup)
        {
            assert(nButterflies % Simd::Size == 0);

            for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
            {
                size_t i0 = 8 * nButterflies * iGroup + iButterfly;
                size_t i1 = i0 + nButterflies;
                size_t i2 = i0 + 2 * nButterflies;
                size_t i3 = i0 + 3 * nButterflies;
                size_t i4 = i0 + 4 * nButterflies;
                size_t i5 = i0 + 5 * nButterflies;
                size_t i6 = i0 + 6 * nButterflies;
                size_t i7 = i0 + 7 * nButterflies;

                SimdComplex ct0(&rt0[iButterfly], &it0[iButterfly]);
                SimdComplex ct10(&rt1[iButterfly], &it1[iButterfly]);
                SimdComplex ct11(&rt1[iButterfly + nButterflies], &it1[iButterfly + nButterflies]);
                SimdComplex ct20(&rt2[iButterfly], &it2[iButterfly]);
                SimdComplex ct21(&rt2[iButterfly + nButterflies], &it2[iButterfly + nButterflies]);
                SimdComplex ct22(&rt2[iButterfly + 2 * nButterflies], &it2[iButterfly + 2 * nButterflies]);
                SimdComplex ct23(&rt2[iButterfly + 3 * nButterflies], &it2[iButterfly + 3 * nButterflies]);
                
                SimdComplex c0(&real[i0], &imag[i0]);
                SimdComplex c1(&real[i1], &imag[i1]);
                SimdComplex c2(&real[i2], &imag[i2]);
                SimdComplex c3(&real[i3], &imag[i3]);
                SimdComplex c4(&real[i4], &imag[i4]);
                SimdComplex c5(&real[i5], &imag[i5]);
                SimdComplex c6(&real[i6], &imag[i6]);
                SimdComplex c7(&real[i7], &imag[i7]);
                
                SimdComplex c1tmp0 = c1 * ct0;
                SimdComplex c3tmp0 = c3 * ct0;
                SimdComplex c5tmp0 = c5 * ct0;
                SimdComplex c7tmp0 = c7 * ct0;

                SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
                SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;
                SimdComplex c6tmp1 = (c6 + c7tmp0) * ct10;
                SimdComplex c7tmp1 = (c6 - c7tmp0) * ct11;
                
                SimdComplex c4tmp2 = (c4 + c5tmp0 + c6tmp1) * ct20;
                SimdComplex c5tmp2 = (c4 - c5tmp0 + c7tmp1) * ct21;
                SimdComplex c6tmp2 = (c4 + c5tmp0 - c6tmp1) * ct22;
                SimdComplex c7tmp2 = (c4 - c5tmp0 - c7tmp1) * ct23;

                (c0 + c1tmp0 + c2tmp1 + c4tmp2).store(&real[i0], &imag[i0]);
                (c0 - c1tmp0 + c3tmp1 + c5tmp2).store(&real[i1], &imag[i1]);
                (c0 + c1tmp0 - c2tmp1 + c6tmp2).store(&real[i2], &imag[i2]);
                (c0 - c1tmp0 - c3tmp1 + c7tmp2).store(&real[i3], &imag[i3]);
                (c0 + c1tmp0 + c2tmp1 - c4tmp2).store(&real[i4], &imag[i4]);
                (c0 - c1tmp0 + c3tmp1 - c5tmp2).store(&real[i5], &imag[i5]);
                (c0 + c1tmp0 - c2tmp1 - c6tmp2).store(&real[i6], &imag[i6]);
                (c0 - c1tmp0 - c3tmp1 - c7tmp2).store(&real[i7], &imag[i7]);
            }
        }
    }
};
