#include "RealFFT_v0.hpp"

#include <cassert>

void RealFFT_v0::allocate(size_t signalLength)
{
    nFFT = signalLength / 2;

    fftData.allocate(nFFT);

    nStages = 1;
    for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

    table.twiddleFactor.allocate(nFFT);
    for (size_t iStage = 0; iStage < nStages; ++iStage)
    {
        size_t nButterflies = 1ull << iStage;

        auto twiddleReal = table.twiddleFactor.real().range(nButterflies, nButterflies);
        auto twiddleImag = table.twiddleFactor.imag().range(nButterflies, nButterflies);

        for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
        {
            double angle = M_PI * iButterfly / nButterflies;
            twiddleReal[iButterfly] = static_cast<Real>(std::cos(angle));
            twiddleImag[iButterfly] = static_cast<Real>(-std::sin(angle));
        }
    }

    table.cos.allocate(nFFT / 2);
    for (size_t i = 0; i < nFFT / 2; ++i)
    {
        size_t posReal = i + 1;
        size_t posImag = nFFT / 2 - 1 - i;

        table.cos.real()[i] = static_cast<Real>(std::cos(M_PI * posReal / nFFT));
        table.cos.imag()[i] = static_cast<Real>(std::cos(M_PI * posImag / nFFT));
    }

    table.swaps.allocate(nFFT / 2);
    for (size_t i = 0, j = 0; i < nFFT - 1; ++i)
    {
        if (i < j)
        {
            table.swaps.push_back({ static_cast<SwapIndex>(i), static_cast<SwapIndex>(j) });
        }

        size_t k = nFFT / 2;
        while (k <= j) { j -= k; k /= 2; }
        j += k;
    }
}

void RealFFT_v0::deallocate()
{
    fftData.deallocate();
    table.twiddleFactor.deallocate();
    table.cos.deallocate();
    table.swaps.deallocate();
}

void RealFFT_v0::forwardConverted(ConstSignal& signal, Spectrum& spectrum)
{
    signalToFFTData(signal);
    run();
    spectrumFromFFTData(spectrum);
}

void RealFFT_v0::inverseConverted(ConstSpectrum& spectrum, Signal& signal)
{
    spectrumToFFTData(spectrum);
    run();
    signalFromFFTData(signal);
}

void RealFFT_v0::signalToFFTData(ConstSignal& signal)
{
    size_t nValues = signal.size();

    assert(nValues <= 2 * nFFT);

    auto real = fftData.real();
    auto imag = fftData.imag();

    size_t i = 0;

    size_t nValuesBySimd = nValues - (nValues % Simd::Size);
    for (; i < nValuesBySimd / 2; i += Simd::Size)
    {
        // Don't assume input to be aligned

        Simd first, second;

        first.loadUnaligned(&signal[2 * i]);
        second.loadUnaligned(&signal[2 * i + Simd::Size]);

        auto [even, odd] = Simd::deinterleave(first, second);

        even.store(&real[i]);
        odd.store(&imag[i]);
    }

    size_t nValuesByPair = nValues - (nValues % 2);
    for (; i < nValuesByPair / 2; ++i)
    {
        real[i] = signal[2 * i];
        imag[i] = signal[2 * i + 1];
    }

    if (nValuesByPair != nValues)
    {
        real[i] = signal[2 * i];
        imag[i] = 0;
    }

    size_t nValuesCopied = nValues + (nValues % 2);
    real.range(nValuesCopied / 2, nFFT - nValuesCopied / 2).setToZero();
    imag.range(nValuesCopied / 2, nFFT - nValuesCopied / 2).setToZero();
}

void RealFFT_v0::spectrumFromFFTData(Spectrum& spectrum)
{
    const auto inReal = fftData.real();
    const auto inImag = fftData.imag();
    auto outReal = spectrum.real();
    auto outImag = spectrum.imag();

    const Simd point25(0.25);

    for (size_t i = 0; i < nFFT / 2; i += Simd::Size)
    {
        size_t j = nFFT - Simd::Size - i;

        Simd ri, ii, rj, ij, ci, cj;

        ri.loadUnaligned(&inReal[i + 1]);
        ii.loadUnaligned(&inImag[i + 1]);
        rj.loadReversed(&inReal[j]);
        ij.loadReversed(&inImag[j]);
        ci.load(&table.cos.real()[i]);
        cj.load(&table.cos.imag()[i]);

        Simd rs = (ri + rj) * point25;
        Simd is = (ii + ij) * point25;

        Simd rd = (rj - ri) * point25;
        Simd id = (ii - ij) * point25;

        Simd rp = is * ci + rd * cj;
        Simd ip = rd * ci - is * cj;

        (rs + rp).storeUnaligned(&outReal[i + 1]);
        (ip + id).storeUnaligned(&outImag[i + 1]);
        (rs - rp).storeReversed(&outReal[j]);
        (ip - id).storeReversed(&outImag[j]);
    }

    outReal[0] = (inReal[0] + inImag[0]) / 2;
    outImag[0] = 0;
    outReal[nFFT] = (inReal[0] - inImag[0]) / 2;
    outImag[nFFT] = 0;
    outReal[nFFT / 2] = inReal[nFFT / 2] / 2;
    outImag[nFFT / 2] = -inImag[nFFT / 2] / 2;
}

void RealFFT_v0::spectrumToFFTData(ConstSpectrum& spectrum)
{
    const auto cosReal = table.cos.real();
    const auto cosImag = table.cos.imag();
    const auto inReal = spectrum.real();
    const auto inImag = spectrum.imag();
    auto outReal = fftData.real();
    auto outImag = fftData.imag();

    for (size_t i = 0; i < nFFT / 2; i += Simd::Size)
    {
        size_t j = nFFT - Simd::Size - i;

        Simd ri, ii, rj, ij, ci, cj;

        ri.loadUnaligned(&inReal[i + 1]);
        ii.loadUnaligned(&inImag[i + 1]);
        rj.loadReversed(&inReal[j]);
        ij.loadReversed(&inImag[j]);
        ci.load(&cosReal[i]);
        cj.load(&cosImag[i]);

        Simd rs = ri + rj;
        Simd is = ii + ij;

        Simd rd = ri - rj;
        Simd id = ii - ij;

        Simd rp = is * ci + rd * cj;
        Simd ip = rd * ci - is * cj;

        (rs + rp).storeUnaligned(&outReal[i + 1]);
        (ip - id).storeUnaligned(&outImag[i + 1]);
        (rs - rp).storeReversed(&outReal[j]);
        (ip + id).storeReversed(&outImag[j]);
    }

    outReal[0] = inReal[0] + inReal[nFFT];
    outImag[0] = inReal[0] - inReal[nFFT];
    outReal[nFFT / 2] = inReal[nFFT / 2] * 2;
    outImag[nFFT / 2] = -inImag[nFFT / 2] * 2;
}

void RealFFT_v0::signalFromFFTData(Signal& signal)
{
    assert(signal.size() == 2 * nFFT);

    const auto real = fftData.real();
    const auto imag = fftData.imag();

    const Simd norm(1 / static_cast<Real>(nFFT));

    for (size_t i = 0; i < nFFT; i += Simd::Size)
    {
        Simd even(&real[i]);
        Simd odd(&imag[i]);

        auto [first, second] = Simd::interleave(even, odd);

        first *= norm;
        second *= norm;

        first.store(&signal[2 * i]);
        second.store(&signal[2 * i + Simd::Size]);
    }
}

void RealFFT_v0::run()
{
    auto real = fftData.real();
    auto imag = fftData.imag();

    assert(real.size() == nFFT);
    assert(imag.size() == nFFT);

    // Reorder values according to bit reversal

    for (const auto& [a, b] : table.swaps)
    {
        std::swap(real[a], real[b]);
        std::swap(imag[a], imag[b]);
    }

    // First stage (iStage = 0, nGroups = nFFT / 2, nButterflies = 1)

    if (nStages > 0)
    {
        for (size_t iGroup = 0; iGroup < nFFT / 2; ++iGroup)
        {
            size_t i = 2 * iGroup;
            size_t j = i + 1;

            // Twiddle = (1, 0)
            Real reTmp = real[j];
            Real imTmp = imag[j];

            real[j] = real[i] - reTmp;
            imag[j] = imag[i] - imTmp;
            real[i] = real[i] + reTmp;
            imag[i] = imag[i] + imTmp;
        }
    }

    // Second stage (iStage = 1, nGroups = nFFT / 4, nButterflies = 2)

    if (nStages > 1)
    {
        for (size_t iGroup = 0; iGroup < nFFT / 4; ++iGroup)
        {
            size_t i = 2 * 2 * iGroup;
            size_t j = i + 2;

            // Twiddle = (1, 0)
            Real reTmp0 = real[j];
            Real imTmp0 = imag[j];

            real[j] = real[i] - reTmp0;
            imag[j] = imag[i] - imTmp0;
            real[i] = real[i] + reTmp0;
            imag[i] = imag[i] + imTmp0;

            // Twiddle = (0, -1)
            Real reTmp1 = imag[j + 1];
            Real imTmp1 = -real[j + 1];

            real[j + 1] = real[i + 1] - reTmp1;
            imag[j + 1] = imag[i + 1] - imTmp1;
            real[i + 1] = real[i + 1] + reTmp1;
            imag[i + 1] = imag[i + 1] + imTmp1;
        }
    }

#if RTC_SIMD_SIZE > 4
    // Third stage (iStage = 2, nGroups = nFFT / 8, nButterflies = 4)

    if (nStages > 2)
    {
        auto twiddleReal = table.twiddleFactor.real().range(4, 4);
        auto twiddleImag = table.twiddleFactor.imag().range(4, 4);

        RTC::Simd<float, 4> rt(&twiddleReal[0]);
        RTC::Simd<float, 4> it(&twiddleImag[0]);

        for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
        {
            size_t i = 2 * 4 * iGroup;
            size_t j = i + 4;

            RTC::Simd<float, 4> ri(&real[i]);
            RTC::Simd<float, 4> ii(&imag[i]);
            RTC::Simd<float, 4> rj(&real[j]);
            RTC::Simd<float, 4> ij(&imag[j]);

            RTC::Simd<float, 4> rtmp = rj * rt - ij * it;
            RTC::Simd<float, 4> itmp = ij * rt + rj * it;

            rj = ri - rtmp;
            ij = ii - itmp;
            ri = ri + rtmp;
            ii = ii + itmp;

            ri.store(&real[i]);
            ii.store(&imag[i]);
            rj.store(&real[j]);
            ij.store(&imag[j]);
        }
    }
#endif

#if RTC_SIMD_SIZE > 8
    // Fourth stage (iStage = 3, nGroups = nFFT / 16, nButterflies = 8)

    if (nStages > 3)
    {
        auto twiddleReal = table.twiddleFactor.real().range(8, 8);
        auto twiddleImag = table.twiddleFactor.imag().range(8, 8);

        RTC::Simd<float, 8> rt(&twiddleReal[0]);
        RTC::Simd<float, 8> it(&twiddleImag[0]);

        for (size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
        {
            size_t i = 2 * 8 * iGroup;
            size_t j = i + 8;

            RTC::Simd<float, 8> ri(&real[i]);
            RTC::Simd<float, 8> ii(&imag[i]);
            RTC::Simd<float, 8> rj(&real[j]);
            RTC::Simd<float, 8> ij(&imag[j]);

            RTC::Simd<float, 8> rtmp = rj * rt - ij * it;
            RTC::Simd<float, 8> itmp = ij * rt + rj * it;

            rj = ri - rtmp;
            ij = ii - itmp;
            ri = ri + rtmp;
            ii = ii + itmp;

            ri.store(&real[i]);
            ii.store(&imag[i]);
            rj.store(&real[j]);
            ij.store(&imag[j]);
        }
    }
#endif

    // Butterfly

    for (size_t iStage = Simd::Size > 4 ? (Simd::Size > 8 ? 4 : 3) : 2; iStage < nStages; ++iStage)
    {
        size_t nGroups = 1ull << (nStages - iStage - 1);
        size_t nButterflies = 1ull << iStage;

        assert(nGroups * nButterflies == nFFT / 2);
        assert(table.twiddleFactor.size() >= 2 * nButterflies);

        auto twiddleReal = table.twiddleFactor.real().range(nButterflies, nButterflies);
        auto twiddleImag = table.twiddleFactor.imag().range(nButterflies, nButterflies);

        for(size_t iGroup = 0; iGroup < nGroups; ++iGroup)
        {
            assert(nButterflies % Simd::Size == 0);

            for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
            {
                size_t i = 2 * nButterflies * iGroup + iButterfly;
                size_t j = i + nButterflies;

                Simd ri(&real[i]);
                Simd ii(&imag[i]);
                Simd rj(&real[j]);
                Simd ij(&imag[j]);
                Simd rt(&twiddleReal[iButterfly]);
                Simd it(&twiddleImag[iButterfly]);

                Simd rtmp = rj * rt - ij * it;
                Simd itmp = ij * rt + rj * it;

                rj = ri - rtmp;
                ij = ii - itmp;
                ri = ri + rtmp;
                ii = ii + itmp;

                ri.store(&real[i]);
                ii.store(&imag[i]);
                rj.store(&real[j]);
                ij.store(&imag[j]);
            }
        }
    }
}
