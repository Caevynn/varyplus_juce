#pragma once

#include <random>

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

class Random
{
public:
    Random() : randomDevice(), randomGenerator(randomDevice()), normalDistribution(0, 1) {}

    void fill(RTC::Array<float>& vector)
    {
        for (auto& sample : vector)
        {
            sample = normalDistribution(randomGenerator);
        }
    }

    void fill(RTC::Array<RTC::Complex<float>>& vector)
    {
        for (auto& sample : vector.real())
        {
            sample = normalDistribution(randomGenerator);
        }

        for (auto& sample : vector.imag())
        {
            sample = normalDistribution(randomGenerator);
        }
    }

private:
    std::random_device randomDevice;
    std::mt19937 randomGenerator;
    std::normal_distribution<float> normalDistribution;
};
