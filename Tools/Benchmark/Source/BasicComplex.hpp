#pragma once

#include "Test.hpp"

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

template<class A, class B> using assign_t = decltype(std::declval<A>() = std::declval<B>());
template<class A, class B> using add_assign_t = decltype(std::declval<A>() += std::declval<B>());
template<class A, class B> using constructible_t = decltype(A(std::declval<B>()));

inline void testConstConversion1(const RTC::Complex<float>&) {}
inline void testConstConversion2(RTC::ComplexConstReference<float>) {}
inline void testNonConstConversion(RTC::ComplexReference<float>) {}
template<class A> using const_function_1_t = decltype(testConstConversion1(std::declval<A>()));
template<class A> using const_function_2_t = decltype(testConstConversion2(std::declval<A>()));
template<class A> using non_const_function_t = decltype(testNonConstConversion(std::declval<A>()));

class BasicComplex
{
public:
	static bool run()
	{
		RTC::Array<RTC::Complex<float>> a, b;
		a.allocate(8);
		b.allocate(8);

		Random random;
		random.fill(b);

		return conversions(a, b) && arithmetics(a, b);
	}

private:
	static bool equal(const RTC::Complex<float>& a, const RTC::Complex<float>& b)
	{
		return std::abs(a.real() - b.real()) < 1e-6 && std::abs(a.imag() - b.imag()) < 1e-6;
	}

	static bool equal(float a, float b)
	{
		return std::abs(a - b) < 1e-6;
	}

	static bool conversions(RTC::Range<RTC::Complex<float>, RTC::Regular> a, RTC::Range<const RTC::Complex<float>, RTC::Regular> b)
	{
		RTC::ComplexPointer<const float> constPtr = b.data();
		RTC::ComplexPointer<float> nonConstPtr = a.data();
		//RTC::ComplexPointer<float> convertedNonConstPointer = b.data(); // Must NOT compile
		[[maybe_unused]] RTC::ComplexPointer<const float> convertedConstPointer = a.data();
		RTC::ComplexConstReference<float> constRef = b[0];
		RTC::ComplexReference<float> nonConstRef = a[0];
		//RTC::ComplexReference<float> convertedNonConstRef = b[0]; // Must NOT compile
		[[maybe_unused]] RTC::ComplexConstReference<float> convertedConstRef = a[0];

		bool constructible =
			binary_tester_v<RTC::ComplexPointer<const float>, decltype(b.data()), constructible_t> &&
			binary_tester_v<RTC::ComplexPointer<float>, decltype(a.data()), constructible_t> &&
			!binary_tester_v<RTC::ComplexPointer<float>, decltype(b.data()), constructible_t> &&
			binary_tester_v<RTC::ComplexPointer<const float>, decltype(a.data()), constructible_t> &&
			binary_tester_v<RTC::ComplexConstReference<float>, decltype(b.data()), constructible_t> &&
			binary_tester_v<RTC::ComplexReference<float>, decltype(a.data()), constructible_t> &&
			!binary_tester_v<RTC::ComplexReference<float>, decltype(b.data()), constructible_t> &&
			binary_tester_v<RTC::ComplexConstReference<float>, decltype(a.data()), constructible_t>;

		nonConstRef = b[0];
		nonConstPtr[0] = b[0];
		//constRef = b[0]; // Must NOT compile
		//constPtr[0] = b[0]; // Must NOT compile
		//a[0].conjugate() = b[0]; // Must NOT compile

		bool assignable =
			binary_tester_v<decltype(nonConstRef), decltype(b[0]), assign_t>&&
			binary_tester_v<decltype(nonConstPtr[0]), decltype(b[0]), assign_t> &&
			!binary_tester_v<decltype(constRef), decltype(b[0]), assign_t> &&
			!binary_tester_v<decltype(constPtr[0]), decltype(b[0]), assign_t> &&
			!binary_tester_v<decltype(a[0].conjugate()), decltype(b[0]), assign_t>;

		nonConstRef += b[0];
		nonConstPtr[0] += b[0];
		//constRef += b[0]; // Must NOT compile
		//constPtr[0] += b[0]; // Must NOT compile
		//a[0].conjugate() += b[0]; // Must NOT compile

		bool addAssignable =
			binary_tester_v<decltype(nonConstRef), decltype(b[0]), add_assign_t>&&
			binary_tester_v<decltype(nonConstPtr[0]), decltype(b[0]), add_assign_t> &&
			!binary_tester_v<decltype(constRef), decltype(b[0]), add_assign_t> &&
			!binary_tester_v<decltype(constPtr[0]), decltype(b[0]), add_assign_t> &&
			!binary_tester_v<decltype(a[0].conjugate()), decltype(b[0]), add_assign_t>;

		testConstConversion1(a[0]);
		testConstConversion1(nonConstPtr[0]);
		testConstConversion1(nonConstRef);
		testConstConversion1(b[0]);
		testConstConversion1(constPtr[0]);
		testConstConversion1(constRef);

		bool convertibleToConst1 =
			unary_tester_v<decltype(a[0]), const_function_1_t>&&
			unary_tester_v<decltype(nonConstPtr[0]), const_function_1_t>&&
			unary_tester_v<decltype(nonConstRef), const_function_1_t>&&
			unary_tester_v<decltype(b[0]), const_function_1_t>&&
			unary_tester_v<decltype(constPtr[0]), const_function_1_t>&&
			unary_tester_v<decltype(constRef), const_function_1_t>;

		testConstConversion2(a[0]);
		testConstConversion2(nonConstPtr[0]);
		testConstConversion2(nonConstRef);
		testConstConversion2(b[0]);
		testConstConversion2(constPtr[0]);
		testConstConversion2(constRef);

		bool convertibleToConst2 =
			unary_tester_v<decltype(a[0]), const_function_2_t>&&
			unary_tester_v<decltype(nonConstPtr[0]), const_function_2_t>&&
			unary_tester_v<decltype(nonConstRef), const_function_2_t>&&
			unary_tester_v<decltype(b[0]), const_function_2_t>&&
			unary_tester_v<decltype(constPtr[0]), const_function_2_t>&&
			unary_tester_v<decltype(constRef), const_function_2_t>;

		testNonConstConversion(a[0]);
		testNonConstConversion(nonConstPtr[0]);
		testNonConstConversion(nonConstRef);
		//testNonConstConversion(b[0]); // Must NOT compile
		//testNonConstConversion(constPtr[0]); // Must NOT compile
		//testNonConstConversion(constRef); // Must NOT compile

		bool convertibleToNonConst =
			unary_tester_v<decltype(a[0]), non_const_function_t> &&
			unary_tester_v<decltype(nonConstPtr[0]), non_const_function_t> &&
			unary_tester_v<decltype(nonConstRef), non_const_function_t> &&
			!unary_tester_v<decltype(b[0]), non_const_function_t>&&
			!unary_tester_v<decltype(constPtr[0]), non_const_function_t>&&
			!unary_tester_v<decltype(constRef), non_const_function_t>;

		return constructible && assignable && addAssignable && convertibleToConst1 && convertibleToConst2 && convertibleToNonConst;
	}

	static bool arithmetics(RTC::Range<RTC::Complex<float>, RTC::Regular> a, RTC::Range<const RTC::Complex<float>, RTC::Regular> b)
	{
		RTC::ComplexPointer<const float> constPtr = b.data();

		const RTC::Complex<float>& constRef = b[0];
		
		auto ref23 = a[3];
		auto ref24 = a[4];
		auto ref25 = a[5];

		RTC::Complex<float> complex1, complex2;
		complex1 = constPtr[0];
		complex2 = complex1;

		a[0] = b[0] + b[0];
		a[1] = b[1];
		a[1] += b[1];
		a[2] = (a[7] = b[2].conjugate()).conjugate();
		ref23 = b[3];
		ref24 = (complex1 = constPtr[0]);
		ref25 = constRef.conjugate() + constRef.conjugate();
		a[6] = (a[7] = b[6].rotateCW()).rotateCCW();

		bool assignments = equal(constRef, b[0])
			&& equal(a[0], 2 * b[0])
			&& equal(a[1], b[1] * 2)
			&& equal(a[2], b[2])
			&& equal(a[2], b[2])
			&& equal(a[3], b[3])
			&& equal(a[4], b[0])
			&& equal(a[5], (b[0] + b[0]).conjugate())
			&& equal(a[6], b[6])
			&& equal(a[7], b[6].rotateCW());

		RTC::Complex<float> orig = b[0];
		RTC::Complex<float> rotCW = b[0].rotateCW();
		RTC::Complex<float> rotCCW = b[0].rotateCCW();
		RTC::Complex<float> conj = b[0].conjugate();

		bool modifications = equal(rotCW.real(), orig.imag())
			&& equal(rotCW.imag(), -orig.real())
			&& equal(rotCCW.real(), -orig.imag())
			&& equal(rotCCW.imag(), orig.real())
			&& equal(conj.real(), orig.real())
			&& equal(conj.imag(), -orig.imag());

		return assignments && modifications;
	}
};
