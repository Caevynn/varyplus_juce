#include <iostream>

#ifdef COMPILE_STARTER
#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCSimdInstructionSet.hpp"

void execute(const std::string& name)
{
#ifdef _WIN32
    std::string executable = "bin\\" + name + ".exe";
#else
    std::string executable = "./bin/" + name;
#endif

    [[maybe_unused]] int result = system(executable.c_str());
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    RTC::SimdInstructionSet instructionSet;

    if (instructionSet.AVX512CD())
    {
        execute("Benchmark_AVX512");
    }
    else if (instructionSet.AVX())
    {
        execute("Benchmark_AVX");
    }
    else if (instructionSet.SSE2())
    {
        execute("Benchmark_SSE2");
    }
    else
    {
        std::cout << "Simd not supported" << std::endl;
    }

    return 0;
}
#else
#include "BasicSimd.hpp"
#include "BasicFFT.hpp"
#include "BasicComplex.hpp"
#include "Add.hpp"
#include "MultiplyAdd.hpp"
#include "FFTSignal.hpp"
#include "FFTSpectrum.hpp"
#include "FFTCore.hpp"
#include "FFTBitReversal.hpp"
#include "FFTCoreComparison.hpp"
#include "FFTComplete.hpp"

int main ([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    if (BasicSimd::run() && BasicFFT::run() && BasicComplex::run())
    {
        constexpr size_t NumMeasurementes = 10000;

        Add::run(64, 64, NumMeasurementes);
        Add::run(256, 16, NumMeasurementes);
        Add::run(1024, 4, NumMeasurementes);
        Add::run(4096, 1, NumMeasurementes);

        MultiplyAdd::run(64, 64, NumMeasurementes);
        MultiplyAdd::run(256, 16, NumMeasurementes);
        MultiplyAdd::run(1024, 4, NumMeasurementes);
        MultiplyAdd::run(4096, 1, NumMeasurementes);

        FFTSignal::run(64, 64, NumMeasurementes);
        FFTSignal::run(256, 16, NumMeasurementes);
        FFTSignal::run(1024, 4, NumMeasurementes);
        FFTSignal::run(4096, 1, NumMeasurementes);

        FFTSpectrum::run(64, 64, NumMeasurementes);
        FFTSpectrum::run(256, 16, NumMeasurementes);
        FFTSpectrum::run(1024, 4, NumMeasurementes);
        FFTSpectrum::run(4096, 1, NumMeasurementes);

        FFTCore::run(64, 64, NumMeasurementes);
        FFTCore::run(256, 16, NumMeasurementes);
        FFTCore::run(1024, 4, NumMeasurementes);
        FFTCore::run(4096, 1, NumMeasurementes);

        FFTBitReversal::run(64, 64, NumMeasurementes);
        FFTBitReversal::run(256, 16, NumMeasurementes);
        FFTBitReversal::run(1024, 4, NumMeasurementes);
        FFTBitReversal::run(4096, 1, NumMeasurementes);

        FFTCoreComparison::run(64, 64, NumMeasurementes);
        FFTCoreComparison::run(256, 16, NumMeasurementes);
        FFTCoreComparison::run(1024, 4, NumMeasurementes);
        FFTCoreComparison::run(4096, 1, NumMeasurementes);

        FFTComplete::run(64, 64, NumMeasurementes);
        FFTComplete::run(256, 16, NumMeasurementes);
        FFTComplete::run(1024, 4, NumMeasurementes);
        FFTComplete::run(4096, 1, NumMeasurementes);

        std::cout << "Done." << std::endl;
    }
    else
    {
        std::cout << "Test failed." << std::endl;
    }

#ifndef __linux__
    std::getchar();
#endif

    return 0;
}
#endif
