#pragma once

#include "Test.hpp"

#include "FFTCoreOriginal.hpp"
#include "FFTCoreMerged123.hpp"
#include "FFTCoreMerged45.hpp"
#include "FFTCoreMergedAll.hpp"
#include "FFTCoreMergedMore.hpp"
#include "FFTCoreFinal.hpp"

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(name) name ## FFT.run(name ## Result);

class FFTCoreComparison
{
	using Container = RTC::Array<RTC::Complex<float>>;

public:
	static void run(size_t dataSize, size_t nRepetitions, size_t nMeasurements)
	{
        std::cout << "FFTCore::run(" << dataSize << ", " << nMeasurements << ")" << std::endl;

        FFTCoreOriginal origFFT(dataSize);
        FFTCoreMerged123 m123FFT(dataSize);
        FFTCoreMerged45 m45FFT(dataSize);
        FFTCoreMergedAll mAllFFT(dataSize);
        FFTCoreMergedMore mMoreFFT(dataSize);
        FFTCoreFinal finalFFT(dataSize);

		TIME_AND_RESULT(orig);
        TIME_AND_RESULT(m123);
        TIME_AND_RESULT(m45);
        TIME_AND_RESULT(mAll);
        TIME_AND_RESULT(mMore);
        TIME_AND_RESULT(final);

        Container input;
        input.allocate(dataSize);

        Random random;

        for (size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
        {
            random.fill(input);

            origResult.copy(input);
            m123Result.copy(input);
            m45Result.copy(input);
            mAllResult.copy(input);
            mMoreResult.copy(input);
            finalResult.copy(input);

            RUN_TEST(orig);
            RUN_TEST(m123);
            RUN_TEST(m45);
            RUN_TEST(mAll);
            RUN_TEST(mMore);
            RUN_TEST(final);

            compare("m123", origResult, m123Result);
            compare("m45", origResult, m45Result);
            compare("mAll", origResult, mAllResult);
            compare("mMore", origResult, mMoreResult);
            compare("final", origResult, finalResult);
        }

        print("orig", origTime);
        print("m123", m123Time);
        print("m45", m45Time);
        print("mAll", mAllTime);
        print("mMore", mMoreTime);
        print("final", finalTime);
	}
};
