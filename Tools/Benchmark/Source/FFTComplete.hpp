#pragma once

#include "Test.hpp"

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(name) \
name ## FFT.forward(name ## Result, tmp); \
name ## FFT.inverse(tmp, name ## Result)

#include "RealFFT_v0.hpp"
#include "RealFFT_v1.hpp"
#include "RealFFT_v2.hpp"
#include "RealFFT_v3.hpp"
#include "../../../Source/AudioProcessor/Tools/RealFFT.hpp"

class FFTComplete
{
    using Container = RTC::Array<float>;
    using ComplexContainer = RTC::Array<RTC::Complex<float>>;

public:
    inline static size_t nStages;

    static void run(size_t dataSize, size_t nRepetitions, size_t nMeasurements)
    {
        std::cout << "FFTComplete::run(" << dataSize << ", " << nMeasurements << ")" << std::endl;

        RealFFT_v1 v1FFT; v1FFT.allocate(dataSize);
        RealFFT_v0 v0FFT; v0FFT.allocate(dataSize);
        RealFFT_v2 v2FFT; v2FFT.allocate(dataSize);
        RealFFT_v3 v3FFT; v3FFT.allocate(dataSize);
        RealFFT rtcFFT; rtcFFT.allocate(dataSize);

        TIME_AND_RESULT(v1);
        TIME_AND_RESULT(v0);
        TIME_AND_RESULT(v2);
        TIME_AND_RESULT(v3);
        TIME_AND_RESULT(rtc);

        Container input;
        input.allocate(v1FFT.signalLength());

        ComplexContainer tmp;
        tmp.allocate(v1FFT.spectrumLength());

        Random random;

        for (size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
        {
            random.fill(input);

            v1Result.copy(input);
            v0Result.copy(input);
            v2Result.copy(input);
            v3Result.copy(input);
            rtcResult.copy(input);

            RUN_TEST(v0);
            RUN_TEST(v1);
            RUN_TEST(v2);
            RUN_TEST(v3);
            RUN_TEST(rtc);

            compare("v0", input, v0Result, 1e-3);
            compare("v1", input, v1Result, 1e-3);
            compare("v2", input, v2Result, 1e-3);
            compare("v3", input, v3Result, 1e-3);
            compare("rtc", input, rtcResult, 1e-3);
        }

        print("v0", v0Time);
        print("v1", v1Time);
        print("v2", v2Time);
        print("v3", v3Time);
        print("rtc", rtcTime);
    }
};
