#pragma once

#include "Test.hpp"

#define EQUAL(a, b) (std::abs((a) - (b)) < 1e-6)

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(size) \
succeed &= testReversed<size>(); \
succeed &= testUnaligned<size>(); \
succeed &= testValueConstructor<size>(); \
succeed &= testInterleave<size>(); \
succeed &= testDeinterleave<size>()

class BasicSimd
{
public:
    static bool run()
    {
        bool succeed{ true };

        RUN_SPECIFIC_TEST(4);
        AVX(RUN_SPECIFIC_TEST(8));
        AVX512(RUN_SPECIFIC_TEST(16));

        return succeed;
    }

private:
    template<size_t SimdSize>
    static bool checkReversed(float input[SimdSize])
    {
        for (size_t i = 0; i < SimdSize; ++i)
        {
            float expected = static_cast<float>(SimdSize - 1 - i);

            if (!EQUAL(input[i], expected))
            {
                return false;
            }
        }

        return true;
    }

    template<size_t SimdSize>
    static bool checkUnaligned(float input[SimdSize + 1], bool loadNotStore)
    {
        if (loadNotStore)
        {
            for (size_t i = 0; i < SimdSize; ++i)
            {
                float expected = static_cast<float>(i + 1);
                
                if (!EQUAL(input[i], expected))
                {
                    return false;
                }
            }
        }
        else
        {
            for (size_t i = 1; i < SimdSize + 1; ++i)
            {
                float expected = static_cast<float>(i - 1);

                if (!EQUAL(input[i], expected))
                {
                    return false;
                }
            }
        }

        return true;
    }

    template<size_t SimdSize>
    static bool checkSame(float input[SimdSize], float expected)
    {
        for (size_t i = 0; i < SimdSize; ++i)
        {
            if (!EQUAL(input[i], expected))
            {
                return false;
            }
        }

        return true;
    }

    template<size_t SimdSize>
    static bool checkInterleaved(float first[SimdSize], float second[SimdSize])
    {
        for (size_t i = 0; i < SimdSize; ++i)
        {
            float expected = static_cast<float>((i / 2) + (i % 2) * SimdSize);

            if (!EQUAL(first[i], expected) || !EQUAL(second[i], expected + SimdSize / 2))
            {
                return false;
            }
        }

        return true;
    }

    template<size_t SimdSize>
    static bool checkDeinterleaved(float even[SimdSize], float odd[SimdSize])
    {
        for (size_t i = 0; i < SimdSize; ++i)
        {
            float expected = static_cast<float>(2 * i);

            if (!EQUAL(even[i], expected) || !EQUAL(odd[i], expected + 1))
            {
                return false;
            }
        }

        return true;
    }

    template<size_t SimdSize>
    static bool testReversed()
    {
        using Simd = RTC::Simd<float, SimdSize>;

        float input[SimdSize];
        for (size_t i = 0; i < SimdSize; ++i) input[i] = static_cast<float>(i);
        float output1[SimdSize], output2[SimdSize];
        Simd reverse;
        reverse.loadReversed(input);
        reverse.store(output1);
        reverse.load(input);
        reverse.storeReversed(output2);

        if (!checkReversed<SimdSize>(output1) ||
            !checkReversed<SimdSize>(output2))
        {
            std::cout << "testReversed - " << SimdSize << std::endl;
            std::cout << "Originial: ";
            for (size_t i = 0; i < SimdSize; ++i) std::cout << input[i] << " ";
            std::cout << std::endl;
            std::cout << "Reversed load: ";
            for (size_t i = 0; i < SimdSize; ++i) std::cout << output1[i] << " ";
            std::cout << std::endl;
            std::cout << "Reversed store: ";
            for (size_t i = 0; i < SimdSize; ++i) std::cout << output2[i] << " ";
            std::cout << std::endl;

            return false;
        }

        return true;
    }

    template<size_t SimdSize>
    static bool testUnaligned()
    {
        using Simd = RTC::Simd<float, SimdSize>;

        float input[SimdSize + 1];
        float output1[SimdSize + 1], output2[SimdSize + 1];

        for (size_t i = 0; i < SimdSize + 1; ++i)
        {
            input[i] = static_cast<float>(i);
            output1[i] = 0;
            output2[i] = 0;
        }

        Simd simd;
        simd.loadUnaligned(input + 1);
        simd.store(output1);
        simd.load(input);
        simd.storeUnaligned(output2 + 1);

        if (!checkUnaligned<SimdSize>(output1, true) ||
            !checkUnaligned<SimdSize>(output2, false))
        {
            std::cout << "testUnaligned - " << SimdSize << std::endl;
            std::cout << "Originial: ";
            for (size_t i = 0; i < SimdSize + 1; ++i) std::cout << input[i] << " ";
            std::cout << std::endl;
            std::cout << "Unaligned load: ";
            for (size_t i = 0; i < SimdSize + 1; ++i) std::cout << output1[i] << " ";
            std::cout << std::endl;
            std::cout << "Unaligned store: ";
            for (size_t i = 0; i < SimdSize + 1; ++i) std::cout << output2[i] << " ";
            std::cout << std::endl;

            return false;
        }

        return true;
    }

    template<size_t SimdSize>
    static bool testValueConstructor()
    {
        using Simd = RTC::Simd<float, SimdSize>;

        float output[SimdSize];
        Simd simd(0.5);
        simd.store(output);
        
        if (!checkSame<SimdSize>(output, 0.5))
        {
            std::cout << "testValueConstructor - " << SimdSize << std::endl;
            std::cout << "Expected: " << 0.5 << std::endl;
            std::cout << "Value constructor: ";
            for (size_t i = 0; i < SimdSize; ++i) std::cout << output[i] << " ";
            std::cout << std::endl;

            return false;
        }

        return true;
    }

    template<size_t SimdSize>
    static bool testInterleave()
    {
        using Simd = RTC::Simd<float, SimdSize>;

        float input[2 * SimdSize];
        float output1[SimdSize], output2[SimdSize];

        for (size_t i = 0; i < 2 * SimdSize; ++i)
        {
            input[i] = static_cast<float>(i);
        }

        Simd even(input);
        Simd odd(input + SimdSize);
        auto [first, second] = Simd::interleave(even, odd);
        first.store(output1);
        second.store(output2);

        if (!checkInterleaved<SimdSize>(output1, output2))
        {
            std::cout << "testInterleaved - " << SimdSize << std::endl;
            std::cout << "Original: ";
            for (size_t i = 0; i < 2 * SimdSize; ++i) std::cout << input[i] << " ";
            std::cout << std::endl;
            std::cout << "First: ";
            for (size_t i = 0; i < SimdSize; ++i) std::cout << output1[i] << " ";
            std::cout << std::endl;
            std::cout << "Second: ";
            for (size_t i = 0; i < SimdSize; ++i) std::cout << output2[i] << " ";
            std::cout << std::endl;

            return false;
        }

        return true;
    }

    template<size_t SimdSize>
    static bool testDeinterleave()
    {
        using Simd = RTC::Simd<float, SimdSize>;

        float input[2 * SimdSize];
        float output1[SimdSize], output2[SimdSize];

        for (size_t i = 0; i < 2 * SimdSize; ++i)
        {
            input[i] = static_cast<float>(i);
        }

        Simd first(input);
        Simd second(input + SimdSize);
        auto [even, odd] = Simd::deinterleave(first, second);
        even.store(output1);
        odd.store(output2);

        if (!checkDeinterleaved<SimdSize>(output1, output2))
        {
            std::cout << "testDeinterleaved - " << SimdSize << std::endl;
            std::cout << "Original: ";
            for (size_t i = 0; i < 2 * SimdSize; ++i) std::cout << input[i] << " ";
            std::cout << std::endl;
            std::cout << "Even: ";
            for (size_t i = 0; i < SimdSize; ++i) std::cout << output1[i] << " ";
            std::cout << std::endl;
            std::cout << "Odd: ";
            for (size_t i = 0; i < SimdSize; ++i) std::cout << output2[i] << " ";
            std::cout << std::endl;

            return false;
        }

        return true;
    }
};