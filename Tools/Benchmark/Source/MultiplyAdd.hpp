#pragma once

#include "Test.hpp"

#ifdef RUN_SPECIFIC_TEST
#   undef RUN_SPECIFIC_TEST
#endif

#define RUN_SPECIFIC_TEST(name) name(a, b, name ## Result)

template<typename InIt, typename OutIt>
void stl_fma(InIt ar, InIt arend, InIt ai, InIt br, InIt bi, OutIt rr, OutIt ri)
{
	for (; ar != arend; ++ar, ++ai, ++br, ++bi, ++rr, ++ri)
	{
		*rr += (*ar * *br) - (*ai * *bi);
		*ri += (*ar * *bi) + (*ai * *br);
	}
}

class MultiplyAdd
{
	using Container = RTC::Array<RTC::Complex<float>>;
public:
	static void run(size_t dataSize, size_t nRepetitions, size_t nMeasurements)
	{
		std::cout << "MultiplyAdd::run(" << dataSize << ", " << nMeasurements << ")" << std::endl;

		TIME_AND_RESULT(loop);
		TIME_AND_RESULT(stl);
		TIME_AND_RESULT(sse);
		AVX(TIME_AND_RESULT(avx));
		AVX512(TIME_AND_RESULT(avx512));
		TIME_AND_RESULT(rtc);
		
		Container a, b;
		
		a.allocate(dataSize);
		b.allocate(dataSize);

		Random random;

		for (size_t iMeasurement = 0; iMeasurement < nMeasurements; ++iMeasurement)
		{
			random.fill(a);
			random.fill(b);

			RUN_TEST(loop);
			RUN_TEST(stl);
			RUN_TEST(sse);
			AVX(RUN_TEST(avx));
			AVX512(RUN_TEST(avx512));
			RUN_TEST(rtc);

			compare("stl", loopResult, stlResult);
			compare("sse", loopResult, sseResult);
			AVX(compare("avx", loopResult, avxResult));
			AVX512(compare("avx512", loopResult, avx512Result));
			compare("rtc", loopResult, rtcResult);
		}

		print("loop", loopTime);
		print("stl", stlTime);
		print("sse", sseTime);
		AVX(print("avx", avxTime));
		AVX512(print("avx512", avx512Time));
		print("rtc", rtcTime);
	}

private:
	static void loop(const Container& a, const Container& b, Container& result)
	{
		auto ar = a.real().begin();
		auto ai = a.imag().begin();
		auto br = b.real().begin();
		auto bi = b.imag().begin();
		auto rr = result.real().begin();
		auto ri = result.imag().begin();

		for (; rr != result.real().end(); ++ar, ++ai, ++br, ++bi, ++rr, ++ri)
		{
			*rr += (*ar * *br) - (*ai * *bi);
			*ri += (*ar * *bi) + (*ai * *br);
		}
	}

	static void stl(const Container& a, const Container& b, Container& result)
	{
		auto ar = a.real().begin();
		auto ai = a.imag().begin();
		auto br = b.real().begin();
		auto bi = b.imag().begin();
		auto rr = result.real().begin();
		auto ri = result.imag().begin();

		stl_fma(ar, ai, ai, br, bi, rr, ri);
	}

	static void sse(const Container& a, const Container& b, Container& result)
	{
		auto ar = a.real().data();
		auto ai = a.imag().data();
		auto br = b.real().data();
		auto bi = b.imag().data();
		auto rr = result.real().data();
		auto ri = result.imag().data();

		constexpr size_t Step = 4;
		for (; rr != result.real().data() + result.size();
			ar += Step, ai += Step, br += Step, bi += Step, rr += Step, ri += Step)
		{
			__m128 arVec = _mm_load_ps(ar);
			__m128 aiVec = _mm_load_ps(ai);
			__m128 brVec = _mm_load_ps(br);
			__m128 biVec = _mm_load_ps(bi);
			__m128 rrVec = _mm_load_ps(rr);
			__m128 riVec = _mm_load_ps(ri);

			rrVec = _mm_add_ps(rrVec, _mm_sub_ps(_mm_mul_ps(arVec, brVec), _mm_mul_ps(aiVec, biVec)));
			riVec = _mm_add_ps(riVec, _mm_add_ps(_mm_mul_ps(arVec, biVec), _mm_mul_ps(aiVec, brVec)));

			_mm_store_ps(rr, rrVec);
			_mm_store_ps(ri, riVec);
		}
	}

#ifdef __AVX__
	static void avx(const Container& a, const Container& b, Container& result)
	{
		auto ar = a.real().data();
		auto ai = a.imag().data();
		auto br = b.real().data();
		auto bi = b.imag().data();
		auto rr = result.real().data();
		auto ri = result.imag().data();

		constexpr size_t Step = 8;
		for (; rr != result.real().data() + result.size();
			ar += Step, ai += Step, br += Step, bi += Step, rr += Step, ri += Step)
		{
			__m256 arVec = _mm256_load_ps(ar);
			__m256 aiVec = _mm256_load_ps(ai);
			__m256 brVec = _mm256_load_ps(br);
			__m256 biVec = _mm256_load_ps(bi);
			__m256 rrVec = _mm256_load_ps(rr);
			__m256 riVec = _mm256_load_ps(ri);

			rrVec = _mm256_add_ps(rrVec, _mm256_sub_ps(_mm256_mul_ps(arVec, brVec), _mm256_mul_ps(aiVec, biVec)));
			riVec = _mm256_add_ps(riVec, _mm256_add_ps(_mm256_mul_ps(arVec, biVec), _mm256_mul_ps(aiVec, brVec)));

			_mm256_store_ps(rr, rrVec);
			_mm256_store_ps(ri, riVec);
		}
	}
#endif

#ifdef __AVX512F__
	static void avx512(const Container& a, const Container& b, Container& result)
	{
		auto ar = a.real().data();
		auto ai = a.imag().data();
		auto br = b.real().data();
		auto bi = b.imag().data();
		auto rr = result.real().data();
		auto ri = result.imag().data();

		constexpr size_t Step = 16;
		for (; rr != result.real().data() + result.size();
			ar += Step, ai += Step, br += Step, bi += Step, rr += Step, ri += Step)
		{
			__m512 arVec = _mm512_load_ps(ar);
			__m512 aiVec = _mm512_load_ps(ai);
			__m512 brVec = _mm512_load_ps(br);
			__m512 biVec = _mm512_load_ps(bi);
			__m512 rrVec = _mm512_load_ps(rr);
			__m512 riVec = _mm512_load_ps(ri);

			rrVec = _mm512_add_ps(rrVec, _mm512_sub_ps(_mm512_mul_ps(arVec, brVec), _mm512_mul_ps(aiVec, biVec)));
			riVec = _mm512_add_ps(riVec, _mm512_add_ps(_mm512_mul_ps(arVec, biVec), _mm512_mul_ps(aiVec, brVec)));

			_mm512_store_ps(rr, rrVec);
			_mm512_store_ps(ri, riVec);
		}
	}
#endif

	static void rtc(const Container & a, const Container & b, Container & result)
	{
		result.multiplyAndAdd(a, b);
	}
};
