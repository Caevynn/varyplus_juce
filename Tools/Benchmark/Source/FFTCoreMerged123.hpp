#pragma once

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCArray.hpp"

class FFTCoreMerged123
{
    using Real = float;
    using Complex = RTC::Complex<Real>;
    using Simd = RTC::Simd<Real, RTC_SIMD_SIZE>;
    using SimdComplex = RTC::SimdComplex<Real, RTC_SIMD_SIZE>;
	using Data = RTC::Range<Complex, RTC::Regular>;

    using SwapIndex = uint16_t;
    using SwapPair = std::pair<SwapIndex, SwapIndex>;
public:
	FFTCoreMerged123(size_t dataSize) : nFFT(dataSize)
	{
		nStages = 1;
		for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

		twiddle.allocate(nFFT);
		for (size_t iStage = 0; iStage < nStages; ++iStage)
		{
			size_t nButterflies = 1ull << iStage;

			auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
			auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

			for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
			{
				double angle = M_PI * iButterfly / nButterflies;
				twiddleReal[iButterfly] = static_cast<Real>(std::cos(angle));
				twiddleImag[iButterfly] = static_cast<Real>(-std::sin(angle));
			}
		}
	}

	void run(Data& data)
	{
		butterflies(data);
	}

private:
	RTC::Array<Complex> twiddle;
	size_t nFFT, nStages;

	void butterflies(Data& data)
	{
		float* real = data.data().realPointer();
		float* imag = data.data().imagPointer();

		// Third stage (iStage = 2, nGroups = nFFT / 8, nButterflies = 4)

		if (nStages > 2)
		{
			auto twiddleReal = twiddle.real().range(4, 4);
			auto twiddleImag = twiddle.imag().range(4, 4);

			RTC::SimdComplex<float, 4> ct(&twiddleReal[0], &twiddleImag[0]);

			for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
			{
				size_t i = 2 * 4 * iGroup;
				size_t j = i + 4;

				float ri0 = real[i + 0];
				float ii0 = imag[i + 0];
				float ri1 = real[i + 1];
				float ii1 = imag[i + 1];
				float ri2 = real[i + 2];
				float ii2 = imag[i + 2];
				float ri3 = real[i + 3];
				float ii3 = imag[i + 3];

				RTC::SimdComplex<float, 4> ci(
					RTC::Simd<float, 4>(
						(ri0 + ri1) + (ri2 + ri3),
						(ri0 - ri1) + (ii2 - ii3),
						(ri0 + ri1) - (ri2 + ri3),
						(ri0 - ri1) - (ii2 - ii3)
					), 
					RTC::Simd<float, 4>(
						(ii0 + ii1) + (ii2 + ii3),
						(ii0 - ii1) - (ri2 - ri3),
						(ii0 + ii1) - (ii2 + ii3),
						(ii0 - ii1) + (ri2 - ri3)
					)
				);

				float rj0 = real[j + 0];
				float ij0 = imag[j + 0];
				float rj1 = real[j + 1];
				float ij1 = imag[j + 1];
				float rj2 = real[j + 2];
				float ij2 = imag[j + 2];
				float rj3 = real[j + 3];
				float ij3 = imag[j + 3];

				RTC::SimdComplex<float, 4> cj(
					RTC::Simd<float, 4>(
						(rj0 + rj1) + (rj2 + rj3),
						(rj0 - rj1) + (ij2 - ij3),
						(rj0 + rj1) - (rj2 + rj3),
						(rj0 - rj1) - (ij2 - ij3)
					), 
					RTC::Simd<float, 4>(
						(ij0 + ij1) + (ij2 + ij3),
						(ij0 - ij1) - (rj2 - rj3),
						(ij0 + ij1) - (ij2 + ij3),
						(ij0 - ij1) + (rj2 - rj3)
					)
				);

				RTC::SimdComplex<float, 4> ctmp = cj * ct;

				cj = ci - ctmp;
				ci = ci + ctmp;

				ci.store(&real[i], &imag[i]);
				cj.store(&real[j], &imag[j]);
			}
		}

#if RTC_SIMD_SIZE > 8
		// Fourth stage (iStage = 3, nGroups = nFFT / 16, nButterflies = 8)

		if (nStages > 3)
		{
			auto twiddleReal = twiddle.real().range(8, 8);
			auto twiddleImag = twiddle.imag().range(8, 8);

			RTC::SimdComplex<float, 8> ct(&twiddleReal[0], &twiddleImag[0]);

			for (size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
			{
				size_t i0 = 16 * iGroup;
				size_t i1 = i0 + 8;

				RTC::SimdComplex<float, 8> c0(&real[i0], &imag[i0]);
				RTC::SimdComplex<float, 8> c1(&real[i1], &imag[i1]);

				RTC::SimdComplex<float, 8> c1tmp = c1 * ct;

				c1 = c0 - c1tmp;
				c0 = c0 + c1tmp;

				c0.store(&real[i0], &imag[i0]);
				c1.store(&real[i1], &imag[i1]);
			}
		}
#endif
		// Butterfly

		for (size_t iStage = Simd::Size > 8 ? 4 : 3; iStage < nStages; ++iStage)
		{
			size_t nGroups = 1ull << (nStages - iStage - 1);
			size_t nButterflies = 1ull << iStage;

			assert(nGroups * nButterflies == nFFT / 2);
			assert(twiddle.size() >= 2 * nButterflies);

			auto twiddleReal = twiddle.real().range(nButterflies, nButterflies);
			auto twiddleImag = twiddle.imag().range(nButterflies, nButterflies);

			for(size_t iGroup = 0; iGroup < nGroups; ++iGroup)
			{
				assert(nButterflies % Simd::Size == 0);

				for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
				{
					size_t i = 2 * nButterflies * iGroup + iButterfly;
					size_t j = i + nButterflies;

					SimdComplex ci(&real[i], &imag[i]);
					SimdComplex cj(&real[j], &imag[j]);
					SimdComplex ct(&twiddleReal[iButterfly], &twiddleImag[iButterfly]);

					SimdComplex ctmp = cj * ct;

					(ci + ctmp).store(&real[i], &imag[i]);
					(ci - ctmp).store(&real[j], &imag[j]);
				}
			}
		}
	}
};
