#pragma once

#include <JuceHeader.h>

class MainComponent  : public juce::Component, public juce::Timer
{
public:
    MainComponent();
    ~MainComponent() override;

private:
    juce::Image startScreen;
    bool firstTimerEvent{ true };
    bool executed{ false };

    void paint(juce::Graphics&) override;
    void resized() override;
    void timerCallback() override;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
