#include "MainComponent.h"

#include <BinaryData.h>

#include "../../../Source/AudioProcessor/Utils/RealTimeContainers/RTCSimdInstructionSet.hpp"

#ifdef _WIN32
#include <Windows.h>

bool execute(const std::string& name)
{
    std::string filename = "bin\\" + name + ".exe";

    if (GetFileAttributes(filename.c_str()) == INVALID_FILE_ATTRIBUTES)
    {
        return false;
    }

    STARTUPINFO startupInfo = { 0 };
    PROCESS_INFORMATION processInfo;

    if (!CreateProcess(filename.c_str(), NULL, NULL, NULL, FALSE, 0, NULL, NULL, &startupInfo, &processInfo))
    {
        return false;
    }
    
    CloseHandle(processInfo.hProcess);
    CloseHandle(processInfo.hThread);

    return true;
}
#elif defined(__linux__)
#include <spawn.h>

bool execute(const std::string& name)
{
    std::string path = "bin/" + name;
    std::string arg = name;
    
    pid_t pid;
    
    char *const argv[] = { arg.data(), NULL };
    
    return posix_spawn(&pid, path.c_str(), NULL, NULL, argv, NULL) == 0;
}
#else
#   error Operating system not supported
#endif


MainComponent::MainComponent()
{
    startScreen = juce::ImageCache::getFromMemory(BinaryData::Shrimp_Startscreen_png, BinaryData::Shrimp_Startscreen_pngSize);

    setInterceptsMouseClicks(false, false);
    setSize(startScreen.getWidth(), startScreen.getHeight());

    startTimer(1000);
}

MainComponent::~MainComponent()
{

}

void MainComponent::paint (juce::Graphics& g)
{
    g.drawImageAt(startScreen, 0, 0);
}

void MainComponent::resized()
{
    
}

void MainComponent::timerCallback()
{
    if (firstTimerEvent)
    {
        RTC::SimdInstructionSet instructionSet;

        if (instructionSet.AVX512F())
        {
            executed = execute("Shrimp_AVX512");
        }

        if (!executed && instructionSet.AVX())
        {
            executed = execute("Shrimp_AVX");
        }

        if (!executed && instructionSet.SSE2())
        {
            executed = execute("Shrimp_SSE2");
        }

        if (!executed)
        {
            auto options = juce::MessageBoxOptions()
                .withIconType(juce::MessageBoxIconType::WarningIcon)
                .withTitle("Error")
                .withMessage("Failed to start Shrimp")
                .withButton("Ok");

            auto quit = [](int) { juce::JUCEApplication::quit(); };

            juce::AlertWindow::showAsync(options, quit);
        }
    }
    else if (executed)
    {
        juce::JUCEApplication::quit();
    }

    firstTimerEvent = false;
}
