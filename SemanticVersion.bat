@echo off

setlocal enabledelayedexpansion

if not exist SemanticVersion mkdir SemanticVersion
set HPP_FILE=SemanticVersion\SemanticVersion.hpp

::Read branch name
for /f %%i in ('git rev-parse --abbrev-ref HEAD') do set BRANCH=%%i

::Find latest tag for master branch
set TAG_FOUND=0
for /f %%l in ('git tag --list v[0-9]*.[0-9]*.[0-9]* --sort=-version:refname --merged') do (
	for /f "tokens=1,2,3 delims=." %%a in ("%%l") do (
		set TMP_A=%%a
		set /a MAJOR=!TMP_A:~1! > nul 2> nul
		set /a MINOR=%%b > nul 2> nul
		set /a PATCH=%%c > nul 2> nul
	)
	
	if "v!MAJOR!.!MINOR!.!PATCH!" == "%%l" (
		set FOUND_TAG=v!MAJOR!.!MINOR!.!PATCH!
		set TAG_FOUND=1
		goto OUT_OF_LOOP
	)
)

:OUT_OF_LOOP

::Parse commits and update version
if "!TAG_FOUND!" == "1" (
	set LOG_COMMAND=git log --oneline --reverse !FOUND_TAG!..HEAD
) else (
	set LOG_COMMAND=git log --oneline --reverse
	set /a MAJOR=0
	set /a MINOR=0
	set /a PATCH=0
)

set LATEST_TAG=v!MAJOR!.!MINOR!.!PATCH!

for /f "tokens=2 delims=: " %%a in ('%LOG_COMMAND%') do (
	if "%%a" == "fix" (
		set /a PATCH+=1
	)
	if "%%a" == "feat" (
		set /a MINOR+=1
		set /a PATCH=0
	)
	if "%%a" == "BREAKING" (
		set /a MAJOR+=1
		set /a MINOR=0
		set /a PATCH=0
	)
)

set NEW_TAG=v!MAJOR!.!MINOR!.!PATCH!

::Create new tag if necessary
if not "%NEW_TAG%" == "!LATEST_TAG!" (
	echo Tag has changed from !LATEST_TAG! to %NEW_TAG%
	if not "%BRANCH%" == "master" (
		set NEW_TAG=%NEW_TAG%_%BRANCH%
	)
	git tag %NEW_TAG% --force
	git push origin %NEW_TAG% --force
) else (
	echo Tag has not changed !LATEST_TAG!
)

::Create header file
echo #pragma once > %HPP_FILE%
echo: >> %HPP_FILE%
echo #include ^<string_view^> >> %HPP_FILE%
echo: >> %HPP_FILE%
if "%BRANCH%" == "master" (
	echo static constexpr std::string_view SemanticVersion = "!MAJOR!.!MINOR!.!PATCH!"; >> %HPP_FILE%
) else (
	echo static constexpr std::string_view SemanticVersion = "!MAJOR!.!MINOR!.!PATCH! (%BRANCH%)"; >> %HPP_FILE%
)
echo: >> %HPP_FILE%

endlocal
