#! /bin/bash

bash SemanticVersion.sh

mkdir -p Deploy/bin

echo Compiling Shrimp_SSE2
(cd Builds/LinuxMakefile && make -j 4 CONFIG=Release_SSE2)
cp Builds/LinuxMakefile/build/Shrimp_SSE2 Deploy/bin/Shrimp_SSE2

echo Compiling Shrimp_AVX
(cd Builds/LinuxMakefile && make -j 4 CONFIG=Release_AVX)
cp Builds/LinuxMakefile/build/Shrimp_AVX Deploy/bin/Shrimp_AXV

echo Compiling Shrimp_AVX512
(cd Builds/LinuxMakefile && make -j 4 CONFIG=Release_AVX512)
cp Builds/LinuxMakefile/build/Shrimp_AVX512 Deploy/bin/Shrimp_AVX512

echo Compiling Shrimp_Starter
(cd Tools/ShrimpStarter/Builds/LinuxMakefile && make CONFIG=Release)
cp Tools/ShrimpStarter/Builds/LinuxMakefile/build/Shrimp Deploy/Shrimp

echo Compiling Benchmark_SSE2
(cd Tools/Benchmark/Builds/LinuxMakefile && make CONFIG=Release_SSE2)
cp Tools/Benchmark/Builds/LinuxMakefile/build/Benchmark_SSE2 Deploy/bin/Benchmark_SSE2

echo Compiling Benchmark_AVX
(cd Tools/Benchmark/Builds/LinuxMakefile && make CONFIG=Release_AVX)
cp Tools/Benchmark/Builds/LinuxMakefile/build/Benchmark_AVX Deploy/bin/Benchmark_AVX

echo Compiling Benchmark_AVX512
(cd Tools/Benchmark/Builds/LinuxMakefile && make CONFIG=Release_AVX512)
cp Tools/Benchmark/Builds/LinuxMakefile/build/Benchmark_AVX512 Deploy/bin/Benchmark_AVX512

echo Compiling Benchmark_Starter
(cd Tools/Benchmark/Builds/LinuxMakefile && make CONFIG=Release_Starter)
cp Tools/Benchmark/Builds/LinuxMakefile/build/Benchmark Deploy/Benchmark
