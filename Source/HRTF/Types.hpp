/*
  ==============================================================================

    Types.hpp
    Created: 13 Jan 2024 10:44:30am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioProcessor/AudioTypes.hpp"

namespace HRTF
{
    template<typename ...Args>
    class Measurements : public RTC::InterfaceBase<Args...>
    {
    public:
        RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(measurement, element)
        RTC_FUNCTION_ALIAS_CONST(nMeasurements, nElements)
        RTC_FUNCTION_ALIAS_CONST(measurementSize, elementSize)
    };

    template<typename ...Args>
    class Receivers : public RTC::InterfaceBase<Args...>
    {
    public:
        RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(receiver, element)
        RTC_FUNCTION_ALIAS_CONST(nReceivers, nElements)
        RTC_FUNCTION_ALIAS_CONST(receiverSize, elementSize)
    };

    template<typename ...Args>
    class Emitters : public RTC::InterfaceBase<Args...>
    {
    public:
        RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(emitter, element)
        RTC_FUNCTION_ALIAS_CONST(nEmitters, nElements)
        RTC_FUNCTION_ALIAS_CONST(emitterSize, elementSize)
    };

    using ImpulseResponseData = RTC::Array<AudioValueType, Receivers, Emitters, Samples>;
    using TransferFunctionData = RTC::Array<RTC::Complex<AudioValueType>, Segments, Receivers, Emitters, Samples>;
    using SpectrumBufferData = RTC::Array<RTC::Complex<AudioValueType>, RTC::Regular, Segments, Channels, Samples>;
}
