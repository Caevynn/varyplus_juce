/*
  ==============================================================================

    Filter.hpp
    Created: 5 Nov 2023 5:07:47pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <vector>

#include "ImpulsResponse.hpp"
#include "TransferFunction.hpp"
#include "OverlapAdd.hpp"
#include "SpectrumBuffer.hpp"
#include "../AudioProcessor/AudioFilterBase.hpp"
#include "../AudioProcessor/Utils/Assert.hpp"

namespace HRTF
{
    class Filter : public AudioFilterBase
    {
    public:
        Filter(unsigned sampleRate, unsigned blockSize, const std::vector<ImpulseResponse>& impulseResponses)
        {
            ASSERT(impulseResponses.size() > 0);

            size_t nOutputChannels = impulseResponses[0].getData().nReceivers();
            ASSERT(nOutputChannels == 2);
            size_t nInputChannels = impulseResponses[0].getData().receiver(0).nEmitters();
            ASSERT(nInputChannels > 0);

            transferFunctions.reserve(impulseResponses.size());
            for (const auto& impulseResponse : impulseResponses)
            {
                transferFunctions.emplace_back(impulseResponse, blockSize);
            }

            size_t nSegments = transferFunctions[0].getData().nSegments();

            overlapAdd.allocate(nInputChannels, nOutputChannels, blockSize);
            spectrumBuffer.allocate(nInputChannels, nSegments, overlapAdd.getSpectrumSize());

            inputSpectrum.allocate(RTC::Size()
                .with<Channels>(nInputChannels)
                .with<Samples>(overlapAdd.getSpectrumSize()));
            outputSpectrum.allocate(RTC::Size()
                .with<Channels>(nOutputChannels)
                .with<Samples>(overlapAdd.getSpectrumSize()));

            requiredInputChannels = nInputChannels;
            requiredOutputChannels = nOutputChannels;
            requiredBlockSize = blockSize;
            requiredSampleRate = sampleRate;
        }
        
        virtual ~Filter()
        {
            overlapAdd.deallocate();
            spectrumBuffer.deallocate();
            inputSpectrum.deallocate();
            outputSpectrum.deallocate();
        }

        virtual void run(AudioSignalWithChannels::const_reference input, AudioSignalWithChannels::reference output, const HRTF::ListenerOrientation& listenerOrientation)
        {
            ASSERT(input.nChannels() == requiredInputChannels);
            ASSERT(output.nChannels() == requiredOutputChannels);
            ASSERT(input.channel(0).nSamples() == requiredBlockSize);

            overlapAdd.input(input, inputSpectrum);
            spectrumBuffer.push(inputSpectrum);
            spectrumBuffer.convolve(bestTransferFunction(listenerOrientation), outputSpectrum);
            overlapAdd.output(outputSpectrum, output);
        }

    protected:
        Filter() = default;

    private:
        OverlapAdd overlapAdd;
        SpectrumBuffer spectrumBuffer;
        AudioSpectrumWithChannels inputSpectrum;
        AudioSpectrumWithChannels outputSpectrum;
        std::vector<TransferFunction> transferFunctions;

        const TransferFunction& bestTransferFunction(const HRTF::ListenerOrientation& listenerOrientation)
        {
            size_t bestIndex = 0;
            double minDistance = listenerOrientation.distance(transferFunctions[0].getListenerOrientation());

            for (size_t i = 1; i < transferFunctions.size(); ++i)
            {
                double distance = listenerOrientation.distance(transferFunctions[i].getListenerOrientation());

                if (distance < minDistance)
                {
                    minDistance = distance;
                    bestIndex = i;
                }
            }

            return transferFunctions[bestIndex];
        }
    };
}
