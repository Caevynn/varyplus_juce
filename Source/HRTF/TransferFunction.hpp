/*
  ==============================================================================

    TransferFunctionWithListenerPosition.hpp
    Created: 25 Oct 2023 11:39:27pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "ImpulsResponse.hpp"
#include "Types.hpp"
#include "../AudioProcessor/Tools/RealFFT.hpp"

namespace HRTF
{
    class TransferFunction
    {
    public:
        TransferFunction(const ImpulseResponse& impulseResponse, size_t blockSize) :
            listenerOrientation(impulseResponse.getListenerOrientation())
        {
            size_t segmentSize = 2 * blockSize;

            size_t numReceivers = impulseResponse.getData().nReceivers();
            ASSERT(numReceivers == 2);
            size_t numEmitters = impulseResponse.getData().receiver(0).nEmitters();
            ASSERT(numEmitters > 0);
            size_t numSamples = impulseResponse.getData().receiver(0).emitter(0).nSamples();
            ASSERT(numSamples > 0);
            size_t numSegments = numSamples / segmentSize + (numSamples % segmentSize > 0 ? 1 : 0);
            ASSERT(numSegments > 0);

            RealFFT fft;
            fft.allocate(2 * segmentSize);
            data.allocate(RTC::Size()
                .with<Segments>(numSegments)
                .with<Receivers>(numReceivers)
                .with<Emitters>(numEmitters)
                .with<Samples>(fft.spectrumLength()));

            for (size_t iReceiver = 0; iReceiver < numReceivers; ++iReceiver)
            {
                for (size_t iEmitter = 0; iEmitter < numEmitters; ++iEmitter)
                {
                    size_t segmentStart = 0;

                    for (auto& tfSegment : data)
                    {
                        auto tfData = tfSegment
                            .receiver(iReceiver)
                            .emitter(iEmitter);
                        auto irData = impulseResponse.getData()
                            .receiver(iReceiver)
                            .emitter(iEmitter)
                            .range(segmentStart, segmentSize);

                        fft.forward(irData, tfData);

                        segmentStart += segmentSize;
                    }
                }
            }

            fft.deallocate();
        }

        TransferFunction(TransferFunction&&) = default;

        ~TransferFunction()
        {
            data.deallocate();
        }

        const TransferFunctionData& getData() const { return data; }
        const ListenerOrientation& getListenerOrientation() const { return listenerOrientation; }

    private:
        TransferFunctionData data;
        ListenerOrientation listenerOrientation;
    };
}
