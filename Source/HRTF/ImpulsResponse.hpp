/*
  ==============================================================================

    ImpulsResponceWithListenerPosition.hpp
    Created: 25 Oct 2023 11:14:31pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "ListenerOrientation.hpp"
#include "Types.hpp"
#include "../AudioProcessor/AudioTypes.hpp"
#include "../AudioProcessor/Utils/Assert.hpp"

namespace HRTF
{
    class ImpulseResponse
    {
    public:
        ImpulseResponse(ImpulseResponseData&& data, const ListenerOrientation& listenerOrientation) :
            data(std::move(data)),
            listenerOrientation(listenerOrientation)
        {}

        ImpulseResponse(ImpulseResponse&&) = default;

        ~ImpulseResponse()
        {
            data.deallocate();
        }

        const ImpulseResponseData& getData() const { return data; }
        const ListenerOrientation& getListenerOrientation() const { return listenerOrientation; }

    private:
        ImpulseResponseData data;
        ListenerOrientation listenerOrientation;
    };
}
