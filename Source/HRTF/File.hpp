/*
  ==============================================================================

    File.hpp
    Created: 6 Nov 2023 10:06:29am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <vector>
#include <string>
#include <optional>

#include "ListenerOrientation.hpp"
#include "ImpulsResponse.hpp"
#include "Filter.hpp"

#include "Test/TestFilters.hpp"

// Declare here to allow friend class outside namepsace
class SofaFileLoader;

namespace HRTF
{
    class File
    {
    public:
        class LoaderBase
        {
        public:
            virtual ~LoaderBase() = default;
            virtual HRTF::File loadHRTF(const std::string& filename) = 0;
        };

        File(const std::string& filename) : filename(filename) {}

        std::unique_ptr<Filter> makeFilter(unsigned wantedSampleRate, unsigned wantedBlockSize)
        {
            if (!isValid())
            {
                return std::unique_ptr<Filter>();
            }
            else if (isTest())
            {
                if (filename == "bypass.testfilter") { return std::make_unique<HRTF::BypassFilter>(); }
                else if (filename == "delay.testfilter") { return std::make_unique<HRTF::DelayFilter>(wantedSampleRate, wantedBlockSize); }
                else if (filename == "dirac.testfilter") { return std::make_unique<HRTF::DiracFilter>(wantedSampleRate, wantedBlockSize); }
                else if (filename == "longdirac.testfilter") { return std::make_unique<HRTF::LongDiracFilter>(wantedSampleRate, wantedBlockSize); }
                else if (filename == "fft.testfilter") { return std::make_unique<HRTF::FFTFilter>(wantedBlockSize); }
                else if (filename == "lowpass.testfilter") { return std::make_unique<HRTF::LowpassFilter>(wantedSampleRate, wantedBlockSize); }
                else if (filename == "overlapadd.testfilter") { return std::make_unique<HRTF::OverlapAddFilter>(wantedBlockSize); }
                else { error = "Invalid test filter"; return std::unique_ptr<Filter>(); }
            }
            else
            {
                return std::make_unique<Filter>(sampleRate, wantedBlockSize, impulseResponses);
            }
        }

        bool isValid() const { return !error; }
        bool isTest() const { return !error && impulseResponses.empty(); }
        const std::string& getFilename() const { return filename; }
        const std::optional<std::string>& getErrorMsg() const { return error; }

        size_t getNumEmitters() const { return nEmitters; }
        size_t getNumReceivers() const { return nReceivers; }
        size_t getNumSamples() const { return nSamples; }
        size_t getNumMeasurements() const { return nMeasurements; }
        unsigned getSampleRate() const { return sampleRate; }

        const std::vector<HRTF::ImpulseResponse>& getImpulseResponses() const { return impulseResponses; }

    private:
        friend class ::SofaFileLoader;

        File() = default;

        std::string filename;
        std::optional<std::string> error;

        size_t nEmitters{ 0 }, nReceivers{ 0 }, nSamples{ 0 }, nMeasurements{ 0 };
        unsigned sampleRate{ 0 };

        std::vector<HRTF::ImpulseResponse> impulseResponses;
    };
}
