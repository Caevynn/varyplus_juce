/*
  ==============================================================================

    BypassFilter.hpp
    Created: 25 Oct 2023 4:19:37pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../Filter.hpp"

namespace HRTF
{
    class BypassFilter : public Filter
    {
    public:
        void run(AudioSignalWithChannels::const_reference input, AudioSignalWithChannels::reference output, const HRTF::ListenerOrientation&) override
        {
            for (auto& channel : output)
            {
                ASSUME(channel.copy(input.channel(0)));
            }
        }
    };
}
