/*
  ==============================================================================

    OverlapAddFilter.hpp
    Created: 25 Oct 2023 10:35:29pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../Filter.hpp"
#include "../OverlapAdd.hpp"
#include "../../AudioProcessor/Utils/Assert.hpp"

namespace HRTF
{
    class OverlapAddFilter : public Filter
    {
    public:
        OverlapAddFilter(unsigned blockSize)
        {
            overlapAdd.allocate(1, 2, blockSize);
            
            inputSpectrum.allocate(RTC::Size()
                .with<Channels>(1)
                .with<Samples>(overlapAdd.getSpectrumSize()));
            outputSpectrum.allocate(RTC::Size()
                .with<Channels>(2)
                .with<Samples>(overlapAdd.getSpectrumSize()));

            requiredInputChannels = 1;
            requiredOutputChannels = 2;
            requiredBlockSize = blockSize;
        }

        ~OverlapAddFilter()
        {
            overlapAdd.deallocate();
            inputSpectrum.deallocate();
            outputSpectrum.deallocate();
        }

        void run(AudioSignalWithChannels::const_reference input, AudioSignalWithChannels::reference output, const HRTF::ListenerOrientation&) override
        {
            ASSERT(input.nChannels() == requiredInputChannels);
            ASSERT(output.nChannels() == requiredOutputChannels);
            ASSERT(input.channelSize() == requiredBlockSize);

            overlapAdd.input(input, inputSpectrum);
            ASSUME(outputSpectrum.channel(0).copy(inputSpectrum.channel(0)));
            ASSUME(outputSpectrum.channel(1).copy(inputSpectrum.channel(0)));
            overlapAdd.output(outputSpectrum, output);
        }

    private:
        OverlapAdd overlapAdd;
        AudioSpectrumWithChannels inputSpectrum;
        AudioSpectrumWithChannels outputSpectrum;
    };
}
