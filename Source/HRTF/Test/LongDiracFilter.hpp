/*
  ==============================================================================

    LongDiracFilter.hpp
    Created: 21 Jan 2024 5:59:32pm
    Author:  kevin

  ==============================================================================
*/

#pragma once

#include "../Filter.hpp"

namespace HRTF
{
    class LongDiracFilter : public Filter
    {
    public:
        LongDiracFilter(unsigned sampleRate, unsigned blockSize) :
            Filter(sampleRate, blockSize, createImpulseResponse(sampleRate))
        {}

    private:
        std::vector<ImpulseResponse> createImpulseResponse(unsigned sampleRate)
        {
            size_t length = 10 * sampleRate;
            
            ImpulseResponseData signal;
            signal.allocate(RTC::Size()
                .with<Receivers>(2)
                .with<Emitters>(1)
                .with<Samples>(length));
            signal.receiver(0).emitter(0).sample(0) = 1;
            signal.receiver(1).emitter(0).sample(0) = 1;

            std::vector<ImpulseResponse> impulseResponses;
            impulseResponses.emplace_back(std::move(signal), HRTF::ListenerOrientation(0, 0));

            return impulseResponses;
        }
    };
}
