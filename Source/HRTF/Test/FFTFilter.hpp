/*
  ==============================================================================

    FFTFilter.hpp
    Created: 25 Oct 2023 5:33:25pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../Filter.hpp"
#include "../../AudioProcessor/Tools/RealFFT.hpp"
#include "../../AudioProcessor/Utils/Assert.hpp"

namespace HRTF
{
    class FFTFilter : public Filter
    {
    public:
        FFTFilter(unsigned blockSize)
        {
            fft.allocate(4 * blockSize);
            tmpSignal.allocate(fft.signalLength());
            tmpSpectrum.allocate(RTC::Size().with<Samples>(fft.spectrumLength()));

            requiredInputChannels = 1;
            requiredOutputChannels = 2;
            requiredBlockSize = blockSize;
        }

        ~FFTFilter()
        {
            fft.deallocate();
            tmpSignal.deallocate();
            tmpSpectrum.deallocate();
        }

        void run(AudioSignalWithChannels::const_reference input, AudioSignalWithChannels::reference output, const HRTF::ListenerOrientation&) override
        {
            ASSERT(input.nChannels() == requiredInputChannels);
            ASSERT(output.nChannels() == requiredOutputChannels);
            ASSERT(input.channel(0).nSamples() == requiredBlockSize);

            fft.forward(input.channel(0), tmpSpectrum);

            for (auto& channel : output)
            {
                fft.inverse(tmpSpectrum, tmpSignal);
                ASSUME(channel.copy(tmpSignal.sameRange(channel)));
            }
        }

    private:
        RealFFT fft;
        AudioSignal tmpSignal;
        AudioSpectrum tmpSpectrum;
    };
}
