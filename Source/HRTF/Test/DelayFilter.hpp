/*
  ==============================================================================

    DelayFilter.hpp
    Created: 5 Nov 2023 9:27:55pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../Filter.hpp"

namespace HRTF
{
    class DelayFilter : public Filter
    {
    public:
        DelayFilter(unsigned sampleRate, unsigned blockSize) :
            Filter(sampleRate, blockSize, createImpulseResponse(sampleRate))
        {}

    private:
        std::vector<ImpulseResponse> createImpulseResponse(unsigned sampleRate)
        {
            size_t delayInSamples = static_cast<size_t>(0.5 * sampleRate);
            
            ImpulseResponseData signal;
            signal.allocate(RTC::Size()
                .with<Receivers>(2)
                .with<Emitters>(1)
                .with<Samples>(delayInSamples + 1));

            signal.receiver(0).emitter(0).sample(0) = 1;
            signal.receiver(1).emitter(0).sample(delayInSamples) = 0.25;

            std::vector<ImpulseResponse> impulseResponses;
            impulseResponses.emplace_back(std::move(signal), HRTF::ListenerOrientation(0, 0));

            return impulseResponses;
        }
    };
}
