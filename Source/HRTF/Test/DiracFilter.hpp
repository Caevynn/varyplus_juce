/*
  ==============================================================================

    DiracFilter.hpp
    Created: 11 Dec 2023 10:57:42am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../Filter.hpp"

namespace HRTF
{
    class DiracFilter : public Filter
    {
    public:
        DiracFilter(unsigned sampleRate, unsigned blockSize) :
            Filter(sampleRate, blockSize, createImpulseResponse())
        {}

    private:
        std::vector<ImpulseResponse> createImpulseResponse()
        {
            ImpulseResponseData signal;
            signal.allocate(RTC::Size()
                .with<Receivers>(2)
                .with<Emitters>(1)
                .with<Samples>(1));
            signal.receiver(0).emitter(0).sample(0) = 1;
            signal.receiver(1).emitter(0).sample(0) = 1;

            std::vector<ImpulseResponse> impulseResponses;
            impulseResponses.emplace_back(std::move(signal), HRTF::ListenerOrientation(0, 0));

            return impulseResponses;
        }
    };
}