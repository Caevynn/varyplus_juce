/*
  ==============================================================================

    LowpassFilter.hpp
    Created: 5 Nov 2023 9:28:07pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../Filter.hpp"
#include "../../AudioProcessor/Tools/Sinc.hpp"

namespace HRTF
{
    class LowpassFilter : public Filter
    {
    public:
        LowpassFilter(unsigned sampleRate, unsigned blockSize) :
            Filter(sampleRate, blockSize, createImpulseResponse(sampleRate))
        {}

    private:
        static constexpr double LowpassFrequency = 1000;

        std::vector<ImpulseResponse> createImpulseResponse(unsigned sampleRate)
        {
            AudioSignal sinc;
            Sinc::createWithAllocation(sinc, sampleRate, LowpassFrequency, 16);

            AudioValueType sum = 0;
            for (const auto& sample : sinc) { sum += sample; }
            for (auto& sample : sinc) { sample /= sum; }

            ImpulseResponseData signal;
            signal.allocate(RTC::Size()
                .with<Receivers>(2)
                .with<Emitters>(1)
                .with<Samples>(sinc.nSamples()));
            ASSUME(signal.receiver(0).emitter(0).copy(sinc));
            ASSUME(signal.receiver(1).emitter(0).copy(sinc));

            std::vector<ImpulseResponse> impulseResponses;
            impulseResponses.emplace_back(std::move(signal), HRTF::ListenerOrientation(0, 0));

            sinc.deallocate();

            return impulseResponses;
        }
    };
}
