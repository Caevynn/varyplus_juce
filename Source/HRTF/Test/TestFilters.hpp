/*
  ==============================================================================

    TestFilters.hpp
    Created: 5 Nov 2023 9:28:18pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "BypassFilter.hpp"
#include "DelayFilter.hpp"
#include "DiracFilter.hpp"
#include "LongDiracFilter.hpp"
#include "FFTFilter.hpp"
#include "LowpassFilter.hpp"
#include "OverlapAddFilter.hpp"
