/*
  ==============================================================================

    OverlapAdd.hpp
    Created: 25 Oct 2023 9:50:30pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioProcessor/AudioTypes.hpp"
#include "../AudioProcessor/Tools/HannWindow.hpp"
#include "../AudioProcessor/Tools/RealFFT.hpp"

class OverlapAdd
{
public:
    void allocate(size_t nInputChannels, size_t nOutputChannels, size_t blockSize)
    {
        lastInput.allocate(RTC::Size()
            .with<Channels>(nInputChannels)
            .with<Samples>(blockSize));
        fftInput.allocate(RTC::Size()
            .with<Channels>(nInputChannels)
            .with<Samples>(2 * blockSize));

        fftOutput.allocate(RTC::Size()
            .with<Channels>(nOutputChannels)
            .with<Samples>(4 * blockSize));
        overlappingOutput.allocate(RTC::Size()
            .with<Channels>(nOutputChannels)
            .with<Samples>(4 * blockSize));

        HannWindow::createWithAllocation(hannWindow, 2 * blockSize);
        fft.allocate(4 * blockSize);
    }

    void deallocate()
    {
        lastInput.deallocate();
        fftInput.deallocate();
        fftOutput.deallocate();
        overlappingOutput.deallocate();
        hannWindow.deallocate();
        fft.deallocate();
    }

    void input(AudioSignalWithChannels::const_reference input, AudioSpectrumWithChannels::reference output)
    {
        size_t blockSize = fft.signalLength() / 4;
        size_t nChannels = fftInput.nChannels();

        ASSERT(input.nChannels() == nChannels);
        ASSERT(input.channelSize() == blockSize);
        ASSERT(output.nChannels() == nChannels);
        ASSERT(output.channelSize() == fft.spectrumLength());

        for (size_t c = 0; c < nChannels; ++c)
        {
            ASSUME(fftInput.channel(c).range(0, blockSize).copy(lastInput.channel(c)));
            ASSUME(fftInput.channel(c).range(blockSize, blockSize).copy(input.channel(c)));
            ASSUME(lastInput.channel(c).copy(input.channel(c)));
            ASSUME(fftInput.channel(c).multiply(hannWindow));

            fft.forward(fftInput.channel(c), output.channel(c));
        }
    }

    void output(AudioSpectrumWithChannels::const_reference input, AudioSignalWithChannels::reference output)
    {
        size_t nChannels = fftOutput.nChannels();
        size_t blockSize = fft.signalLength() / 4;

        ASSERT(input.nChannels() == nChannels);
        ASSERT(input.channelSize() == fft.spectrumLength());
        ASSERT(output.nChannels() == nChannels);
        ASSERT(output.channelSize() == blockSize);

        for (size_t i = 0; i < nChannels; ++i)
        {
            fft.inverse(input.channel(i), fftOutput.channel(i));
            ASSUME(overlappingOutput.channel(i).shiftAndAdd(fftOutput.channel(i), blockSize));
            ASSUME(output.channel(i).copy(overlappingOutput.channel(i).range(0, blockSize)));
        }
    }

    size_t getSpectrumSize() const { return fft.spectrumLength(); }

private:
    AudioSignalWithChannels lastInput;
    AudioSignalWithChannels fftInput;

    AudioSignalWithChannels fftOutput;
    AudioSignalWithChannels overlappingOutput;

    AudioSignal hannWindow;
    RealFFT fft;
};
