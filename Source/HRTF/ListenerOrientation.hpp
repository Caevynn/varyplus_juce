#pragma once

#include <cmath>

namespace HRTF
{
    class ListenerOrientation
    {
    public:
        ListenerOrientation() = default;
        ListenerOrientation(double azimuth, double elevation) :
            azimuth(adjustDegree(azimuth)),
            elevation(adjustDegree(elevation))
        {}

        double distance(const ListenerOrientation& other) const
        {
            return std::sqrt(
                std::pow(azimuth - other.azimuth, 2) +
                std::pow(elevation - other.elevation, 2));
        }

        double getAzimuth() const { return azimuth; }
        double getElevation() const { return elevation; }

        static constexpr size_t NumDimensions = 3;

    private:
        double azimuth{ 0 }, elevation{ 0 };

        double adjustDegree(double value)
        {
            if (value > 180) return value - 360;
            else if (value < -180) return value + 360;
            else return value;
        }
    };
}
