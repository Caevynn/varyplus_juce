/*
  ==============================================================================

    SpectrumBuffer.hpp
    Created: 5 Nov 2023 4:26:45pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioProcessor/AudioTypes.hpp"
#include "../AudioProcessor/Utils/Assert.hpp"
#include "TransferFunction.hpp"

namespace HRTF
{
    class SpectrumBuffer
    {
    public:
        void allocate(size_t nChannels, size_t nSegments, size_t spectrumSize)
        {
            ASSERT(nChannels > 0);
            ASSERT(nSegments > 0);
            ASSERT(spectrumSize > 0);

            data.allocate(RTC::Size()
                .with<RTC::Regular>(2)
                .with<Segments>(nSegments)
                .with<Channels>(nChannels)
                .with<Samples>(spectrumSize));
        }

        void deallocate()
        {
            data.deallocate();
        }

        void push(AudioSpectrumWithChannels::const_reference newSpectrum)
        {
            moveIndex();

            auto currentSegment = data[currentBufferIndex()].segment(currentSegmentIndex());

            UNUSED(size_t nChannels = currentSegment.nChannels());
            UNUSED(size_t nSamples = currentSegment.channel(0).nSamples());
            ASSERT(newSpectrum.nChannels() == nChannels);
            ASSERT(newSpectrum.channel(0).nSamples() == nSamples);

            ASSUME(currentSegment.copy(newSpectrum));
        }

        void convolve(const TransferFunction& transferFunction, AudioSpectrumWithChannels::reference output)
        {
            output.setToZero();

            auto currentBuffer = data[currentBufferIndex()];

            UNUSED(size_t nOutputChannels = output.nChannels());
            UNUSED(size_t nSegments = currentBuffer.nSegments());
            UNUSED(size_t nInputChannels = currentBuffer.segment(0).nChannels());
            UNUSED(size_t nSamples = currentBuffer.segment(0).channel(0).nSamples());
            ASSERT(output.nChannels() > 0);
            ASSERT(output.channel(0).nSamples() == nSamples);
            ASSERT(transferFunction.getData().nSegments() == nSegments);
            ASSERT(transferFunction.getData().segment(0).nReceivers() == nOutputChannels);
            ASSERT(transferFunction.getData().segment(0).receiver(0).nEmitters() == nInputChannels);
            ASSERT(transferFunction.getData().segment(0).receiver(0).emitter(0).nSamples() == nSamples);

            const auto& tf = transferFunction.getData();
            auto& sb = currentBuffer;

            auto tfSegment = tf.begin();
            for(const auto& sbSegment : sb.range(currentSegmentIndex(), sb.nSegments() - currentSegmentIndex()))
            {
                auto tfReceiver = tfSegment->begin();
                for (auto& outputChannel : output)
                {
                    auto tfChannel = tfReceiver->begin();
                    for (const auto& sbChannel : sbSegment)
                    {
                        ASSUME(outputChannel.multiplyAndAdd(*tfChannel, sbChannel));
                        ++tfChannel;
                    }
                    ++tfReceiver;
                }
                ++tfSegment;
            }
            for (const auto& sbSegment : sb.range(0, currentSegmentIndex()))
            {
                auto tfReceiver = tfSegment->begin();
                for(auto& outputChannel : output)
                {
                    auto tfChannel = tfReceiver->begin();
                    for (const auto& sbChannel : sbSegment)
                    {
                        ASSUME(outputChannel.multiplyAndAdd(*tfChannel, sbChannel));
                        ++tfChannel;
                    }
                    ++tfReceiver;
                }
                ++tfSegment;
            }
        }

    private:
        size_t currentIndex{ 0 };
        SpectrumBufferData data;

        void moveIndex()
        {
            size_t totalNumSegments = data.size() * data[0].nSegments();
            currentIndex = (currentIndex + totalNumSegments - 1) % totalNumSegments;
        }

        size_t currentBufferIndex() const { return currentIndex % 2; }
        size_t currentSegmentIndex() const { return currentIndex / 2; }
    };
}
