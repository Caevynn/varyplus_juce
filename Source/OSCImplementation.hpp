/*
  ==============================================================================

    OSC.hpp
    Created: 14 Nov 2023 12:27:52pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "AudioProcessor/OSCBase.hpp"

class OSCImplementation
{
public:
    class Reader :
        public OSC::ReaderBase,
        private juce::OSCReceiver,
        private juce::OSCReceiver::Listener<juce::OSCReceiver::RealtimeCallback>
    {
    public:
        Reader()
        {
            addListener(this);
            udpSocket.setEnablePortReuse(false);
            connectToSocket(udpSocket);
        }

        bool startReading(unsigned port) override
        {
            return udpSocket.bindToPort(port);
        }

    private:
        juce::DatagramSocket udpSocket;

        void oscMessageReceived(const juce::OSCMessage& message) override
        {
            OSC::Message oscMessage(message.getAddressPattern().toString().toStdString());

            for (const auto& value : message)
            {
                if (value.isInt32()) { oscMessage.pushValue(value.getInt32()); }
                else if (value.isFloat32()) { oscMessage.pushValue(value.getFloat32()); }
                else if (value.isString()) { oscMessage.pushValue(value.getString().toStdString()); }
            }

            handleMessage(oscMessage);
        }
    };

    class Writer :
        public OSC::WriterBase,
        private juce::OSCSender
    {
    public:
        bool startWriting(const std::string& destinationAddress, unsigned destinationPort) override
        {
            return connect(destinationAddress, destinationPort);
        }

        bool write(const OSC::Message& message) override
        {
            juce::OSCMessage oscMessage({ message.getAddress() });

            for (size_t i = 0; i < message.numValues(); ++i)
            {
                if (message.holdsValue<int>(i)) { oscMessage.addInt32(message.getValue<int>(i)); }
                else if (message.holdsValue<float>(i)) { oscMessage.addFloat32(message.getValue<float>(i)); }
                else if (message.holdsValue<std::string>(i)) { oscMessage.addString(message.getValue<std::string>(i)); }
            }

            return send(oscMessage);
        }
    };
};
