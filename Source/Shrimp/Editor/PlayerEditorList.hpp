/*
  ==============================================================================

    PlayerEditorList.hpp
    Created: 10 Nov 2023 10:02:07am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "PlayerEditor.hpp"
#include "../Processor/MainInterface.hpp"

namespace Shrimp
{
    class PlayerEditorList :
        private juce::ListBoxModel,
        public juce::ListBox
    {
    public:
        PlayerEditorList(MainInterface& mainInterface) :
            numPlayers(mainInterface.getNumPlayers()),
            settings(numPlayers),
            fileInterface(mainInterface),
            stateInterface(mainInterface),
            remoteInterface(mainInterface)
        {
            setModel(this);
            setRowHeight(172);
            setColour(juce::ListBox::ColourIds::backgroundColourId, juce::Colours::transparentBlack);
        }

        int getNumRows() override
        {
            return static_cast<int>(numPlayers);
        }

        juce::Component* refreshComponentForRow(int rowNumber, bool, Component* existingComponentToUpdate) override
        {
            PlayerEditor* player = static_cast<PlayerEditor*>(existingComponentToUpdate);

            if (player == nullptr)
            {
                player = new PlayerEditor(rowNumber);
                player->commandCallback = [this](const AudioPlayer::Command& command)
                {
                    remoteInterface.sendPlayerCommand(command);
                };
            }
            else if (rowNumber != static_cast<int>(player->getIndex()) && rowNumber < static_cast<int>(numPlayers))
            {
                settings[player->getIndex()] = player->getSettings();
                player->setIndex(rowNumber);
                player->setSettings(settings[rowNumber]);
            }

            unsigned numOutputChannels = stateInterface.getNumOutputChannels();
            unsigned numDirectOutputs = numOutputChannels >= 2 ? numOutputChannels - 2 : 0;
            unsigned directOutputsOffset = stateInterface.getMaxNumFilters();

            player->updateState(stateInterface.getPlayerStates(), stateInterface.getSampleRate());
            player->updateAvailableSources(fileInterface.getAvailableSources());
            player->updateAvailableBuses(fileInterface.getAvailableFilters(), numDirectOutputs, directOutputsOffset);

            return player;
        }

        void paintListBoxItem(int, juce::Graphics&, int, int, bool) override
        {
            // Must be overridden
        }

        void paint(juce::Graphics&) override {}

        void update()
        {
            updateContent();
        }

    private:
        size_t numPlayers;
        std::vector<PlayerEditorSettings> settings;

        FileInterface& fileInterface;
        StateInterface& stateInterface;
        RemoteInterface& remoteInterface;

        void resized() override
        {
            ListBox::resized();

            // ListBox::resized() calls setSingleStepSizes(0, getRowHeight())
            // For a different scroll speed it has to be reset every time after resizing

            getViewport()->setSingleStepSizes(10, 10);
        }
    };
}