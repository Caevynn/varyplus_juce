/*
  ==============================================================================

    MasterOutputEditor.hpp
    Created: 9 Nov 2023 2:31:51pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../Processor/MainInterface.hpp"
#include "../../Components/LevelSlider.hpp"
#include "../../Components/LevelMeter.hpp"
#include "../../Utils/FilenameHelper.hpp"

namespace Shrimp
{
    class MasterOutputEditor :
        public juce::GroupComponent
    {
    public:
        MasterOutputEditor(MainInterface& mainInterface) :
            stateInterface(mainInterface),
            remoteInterface(mainInterface),
            fileInterface(mainInterface)
        {
            setText("Master control");

            startButton.setButtonText("Start");
            startButton.onClick = [this]()
            {
                remoteInterface.sendPlayerCommand({ std::nullopt, AudioPlayer::Command::Play() });
            };
            addAndMakeVisible(startButton);

            pauseButton.setButtonText("Pause");
            pauseButton.onClick = [this]()
            {
                remoteInterface.sendPlayerCommand({ std::nullopt, AudioPlayer::Command::Pause() });
            };
            addAndMakeVisible(pauseButton);

            stopButton.setButtonText("Stop");
            stopButton.onClick = [this]()
            {
                remoteInterface.sendPlayerCommand({ std::nullopt, AudioPlayer::Command::Stop() });
            };
            addAndMakeVisible(stopButton);

            levelSlider.disableAutomaticValueChange();
            levelSlider.onChange = [this](double value)
            {
                remoteInterface.setMasterVolume(value);
            };
            addAndMakeVisible(levelSlider);

            for (auto& levelMeter : levelMeters)
            {
                levelMeter.showCompression(true);
                addAndMakeVisible(levelMeter);
            }

            addAndMakeVisible(filterLabel);

            filterRemoveButton.setButtonText("Unload");
            filterRemoveButton.onClick = [this]()
                {
                    remoteInterface.unloadMasterFilterFile();
                };
            addAndMakeVisible(filterRemoveButton);

            update();
        }

        void update()
        {
            levelSlider.setValue(stateInterface.getMasterVolume());

            const auto& outputStates = stateInterface.getOutputStates();

            size_t nActiveOutputs = std::min(NumLevelMeters, outputStates.size());

            size_t iMeter = 0;
            for (; iMeter < nActiveOutputs; ++iMeter)
            {
                const auto& outputState = outputStates[iMeter];
                levelMeters[iMeter].setValue(outputState.peak, outputState.compression);
            }

            for (; iMeter < NumLevelMeters; ++iMeter)
            {
                levelMeters[iMeter].setEnabled(false);
            }

            auto loadedFilter = fileInterface.getMasterFilterName();
            if (loadedFilter.empty())
            {
                filterLabel.setText("No filter loaded", juce::dontSendNotification);
            }
            else
            {
                filterLabel.setText(FilenameHelper::wihtoutPath(loadedFilter), juce::dontSendNotification);
            }
            filterLabel.setEnabled(!loadedFilter.empty());
            filterRemoveButton.setEnabled(!loadedFilter.empty());
        }

    private:
        static constexpr size_t NumLevelMeters = 8;

        StateInterface& stateInterface;
        RemoteInterface& remoteInterface;
        FileInterface& fileInterface;

        juce::TextButton startButton;
        juce::TextButton pauseButton;
        juce::TextButton stopButton;
        LevelSlider levelSlider;
        std::array<LevelMeter, NumLevelMeters> levelMeters;

        juce::Label filterLabel;
        juce::TextButton filterRemoveButton;

        void resized() override
        {
            auto area = getLocalBounds().reduced(10);
            area.removeFromTop(8);

            auto buttonArea = area.removeFromTop(24);
            startButton.setBounds(buttonArea.removeFromLeft(area.getWidth() / 3).reduced(3, 0));
            pauseButton.setBounds(buttonArea.removeFromLeft(area.getWidth() / 3).reduced(3, 0));
            stopButton.setBounds(buttonArea.reduced(3, 0));

            levelSlider.setBounds(area.removeFromTop(24));

            auto filterArea = area.removeFromBottom(24);
            filterRemoveButton.setBounds(filterArea.removeFromRight(area.getWidth() / 3).reduced(3, 0));
            filterLabel.setBounds(filterArea);

            for (auto& levelMeter : levelMeters)
            {
                levelMeter.setBounds(area.removeFromLeft(30).reduced(3));
            }
        }
    };
}
