/*
  ==============================================================================

    ListenerOrientationEditor.hpp
    Created: 13 Nov 2023 12:58:39pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../Processor/MainInterface.hpp"
#include "../../Components/Slider2d.hpp"

namespace Shrimp
{
    class ListenerOrientationEditor : public juce::GroupComponent
    {
    public:
        ListenerOrientationEditor(MainInterface& mainInterface) :
            stateInterface(mainInterface),
            remoteInterface(mainInterface)
        {
            setText("Listener orientation");

            activateButton.setButtonText("Activate control");
            activateButton.setToggleState(false, juce::dontSendNotification);
            addAndMakeVisible(activateButton);

            slider.disableAutomaticValueChange();
            slider.onValueChange = [this](double x, double y)
            {
                if (activateButton.getToggleState())
                {
                    remoteInterface.setListenerOrientation({ -90 * x, -90 * y });
                }
            };
            addAndMakeVisible(slider);

            azimuthLabel.setText("Azimuth: 0", juce::dontSendNotification);
            addAndMakeVisible(azimuthLabel);

            elevationLabel.setText("Elevation: 0", juce::dontSendNotification);
            addAndMakeVisible(elevationLabel);
        }

        void update()
        {
            auto listenerOrientation = stateInterface.getListenerOrientation();

            slider.setValue(-listenerOrientation.getAzimuth() / 90, -listenerOrientation.getElevation() / 90);

            azimuthLabel.setText("Azimuth: " + std::to_string(static_cast<int>(listenerOrientation.getAzimuth())), juce::dontSendNotification);
            elevationLabel.setText("Elevation: " + std::to_string(static_cast<int>(listenerOrientation.getElevation())), juce::dontSendNotification);
        }

    private:
        StateInterface& stateInterface;
        RemoteInterface& remoteInterface;

        juce::ToggleButton activateButton;
        Slider2d slider;
        juce::Label azimuthLabel, elevationLabel;

        void resized() override
        {
            auto area = getLocalBounds().reduced(10);
            area.removeFromTop(8);

            activateButton.setBounds(area.removeFromTop(24));
            slider.setBounds(area.removeFromTop(area.getHeight() - 24));
            azimuthLabel.setBounds(area.removeFromLeft(area.getWidth() / 2));
            elevationLabel.setBounds(area);
        }
    };
}
