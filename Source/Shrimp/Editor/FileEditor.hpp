/*
  ==============================================================================

    FileEditor.hpp
    Created: 15 Nov 2023 11:02:47am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../Processor/MainInterface.hpp"
#include "../../Components/Table.hpp"
#include "../../Utils/FilenameHelper.hpp"

namespace Shrimp
{
    class FileEditor : public juce::Component, private juce::Timer
    {
    public:
        FileEditor(MainInterface& mainInterface) :
            fileInterface(mainInterface),
            remoteInterface(mainInterface)
        {
            sourceTable.addColumn("Slot", 50);
            sourceTable.addColumn("Name");
            sourceTable.addColumn("Size", 100);
            sourceTable.addColumn("Sample rate", 100);
            sourceTable.onDelete = [this](size_t sourceToDelete)
            {
                if (juce::ModifierKeys::currentModifiers.isCtrlDown())
                {
                    if (juce::ModifierKeys::currentModifiers.isShiftDown())
                    {
                        remoteInterface.unloadAllSourceAndFilterFiles();
                    }
                    else
                    {
                        remoteInterface.unloadAllSourceFiles();
                    }
                }
                else
                {
                    remoteInterface.unloadSourceFile(sourceToDelete);
                }
            };
            addAndMakeVisible(sourceTable);

            filterTable.addColumn("Bus", 50);
            filterTable.addColumn("Name");
            filterTable.addColumn("Size", 100);
            filterTable.addColumn("Sample rate", 100);
            filterTable.onDelete = [this](size_t filterToDelete)
            {
                if (juce::ModifierKeys::currentModifiers.isCtrlDown())
                {
                    if (juce::ModifierKeys::currentModifiers.isShiftDown())
                    {
                        remoteInterface.unloadAllSourceAndFilterFiles();
                    }
                    else
                    {
                        remoteInterface.unloadAllFilterFiles();
                    }
                }
                else
                {
                    remoteInterface.unloadFilterFile(filterToDelete);
                }
            };
            addAndMakeVisible(filterTable);

            update();
            startTimer(100);
        }

        void update()
        {
            const auto& sources = fileInterface.getSourceStates();
            if (sources.size() != numSources)
            {
                int selected = sourceTable.getSelectedRow();

                sourceTable.removeAllRows();
                
                size_t id = 0;
                for (const auto& source : sources)
                {
                    sourceTable.addRow({ std::to_string(id++), FilenameHelper::wihtoutPath(source.name), source.size, source.sampleRate });
                }

                if (selected < sourceTable.getNumRows())
                {
                    sourceTable.selectRow(selected);
                }
                else
                {
                    sourceTable.deselectAllRows();
                }

                numSources = sources.size();
            }

            size_t iSource{ 0 };
            for (const auto& source : sources)
            {
                sourceTable.setRowActive(iSource++, source.loaded);
            }

            const auto& filters = fileInterface.getFilterStates();
            if (filters.size() != numFilters)
            {
                int selected = filterTable.getSelectedRow();

                filterTable.removeAllRows();

                size_t id = 0;
                for (const auto& filter : filters)
                {
                    filterTable.addRow({ std::to_string(id++), FilenameHelper::wihtoutPath(filter.name), filter.size, filter.sampleRate });
                }

                if (selected < filterTable.getNumRows())
                {
                    filterTable.selectRow(selected);
                }
                else
                {
                    filterTable.deselectAllRows();
                }

                numFilters = filters.size();
            }

            size_t iFilter{ 0 };
            for (const auto& filter : filters)
            {
                filterTable.setRowActive(iFilter++, filter.loaded);
            }
        }

    private:
        FileInterface& fileInterface;
        RemoteInterface& remoteInterface;

        Table sourceTable, filterTable;

        size_t numSources{ 0 }, numFilters{ 0 };

        void resized() override
        {
            auto area = getLocalBounds();

            sourceTable.setBounds(area.removeFromTop(area.getHeight() / 2).reduced(3));
            filterTable.setBounds(area.reduced(3));
        }

        void timerCallback() override
        {
            update();
        }
    };
}
