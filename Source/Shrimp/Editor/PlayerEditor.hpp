/*
  ==============================================================================

    PlayerEditor.hpp
    Created: 9 Nov 2023 3:00:42pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "PlayerEditorSettingsEditor.hpp"
#include "../../AudioProcessor/AudioPlayer.hpp"
#include "../../Components/LevelSlider.hpp"
#include "../../Components/LabeledCombo.hpp"
#include "../../Components/PlaybackSlider.hpp"
#include "../../Components/WindowWithContent.hpp"
#include "../../Utils/FilenameHelper.hpp"

namespace Shrimp
{
    class PlayerEditor : public juce::GroupComponent
    {
    public:
        PlayerEditor(size_t index) :
            index(index),
            settingsEditor("Player " + std::to_string(index + 1), 300, 200, false)
        {
            setText("Player " + std::to_string(index + 1));

            startButton.setButtonText("Start");
            startButton.onClick = [this]()
            {
                sendCommand({ this->index, AudioPlayer::Command::Play() });
            };
            addAndMakeVisible(startButton);

            pauseButton.setButtonText("Pause");
            pauseButton.onClick = [this]()
            {
                sendCommand({ this->index, AudioPlayer::Command::Pause() });
            };
            addAndMakeVisible(pauseButton);

            stopButton.setButtonText("Stop");
            stopButton.onClick = [this]()
            {
                sendCommand({ this->index, AudioPlayer::Command::Stop() });
            };
            addAndMakeVisible(stopButton);

            levelSlider.disableAutomaticValueChange();
            levelSlider.onChange = [this](double value)
            {
                sendCommand({ this->index, AudioPlayer::Command::Volume(static_cast<AudioValueType>(value)) });
            };
            addAndMakeVisible(levelSlider);

            sourceCombo.setLabelText("Input:");
            sourceCombo.disableAutomaticSelectionChange();
            sourceCombo.addItem("None", Invalid);
            sourceCombo.setSelectedId(Invalid);
            sourceCombo.onChange = [this](unsigned newId)
            {
                if (newId == Invalid)
                {
                    sendCommand({ this->index, AudioPlayer::Command::Source(std::nullopt) });
                }
                else
                {
                    sendCommand({ this->index, AudioPlayer::Command::Source(newId) });
                }
            };
            addAndMakeVisible(sourceCombo);

            busCombo.setLabelText("Output:");
            busCombo.disableAutomaticSelectionChange();
            busCombo.addItem("None", Invalid);
            busCombo.setSelectedId(Invalid);
            busCombo.onChange = [this](unsigned newId)
            {
                std::optional<size_t> targetId;
                if (newId != Invalid)
                {
                    targetId = newId;
                }

                auto settings = settingsEditor.getContent().getPlayerEditorSettings();

                switch (settings.busFadeType)
                {
                case PlayerEditorSettings::BusFadeType::NoFade: sendCommand({ this->index, AudioPlayer::Command::Bus(targetId) }); break;
                case PlayerEditorSettings::BusFadeType::FadeOutAndIn: sendCommand({ this->index, AudioPlayer::Command::BusFade(targetId, settings.busFadeOutTime, settings.busFadePauseTime, settings.busFadeInTime) }); break;
                case PlayerEditorSettings::BusFadeType::Crossfade: sendCommand({ this->index, AudioPlayer::Command::BusCrossfade(targetId, settings.busFadeOutTime) }); break;
                }
            };
            addAndMakeVisible(busCombo);

            playbackSlider.onChange = [this](size_t value)
            {
                sendCommand({ this->index, AudioPlayer::Command::Position(value) });
            };
            addAndMakeVisible(playbackSlider);

            settingsButton.setButtonText("Settings...");
            settingsButton.onClick = [this]()
                {
                    settingsEditor.show();
                };
            addAndMakeVisible(settingsButton);

            settingsEditor.getContent().onLoopChange = [this](bool isEnabled)
                {
                    sendCommand({ this->index, AudioPlayer::Command::Loop(isEnabled) });
                };

            settingsEditor.getContent().closeRequest = [this]()
                {
                    settingsEditor.hide();
                };

            settingsEditor.setAlwaysOnTop(true);
        }

        void setIndex(size_t newIndex)
        {
            index = newIndex;
            setText("Player " + std::to_string(index + 1));
            settingsEditor.hide();
            settingsEditor.setTitle("Player " + std::to_string(index + 1));
        }

        size_t getIndex() const { return index; }

        void setSettings(const PlayerEditorSettings& settings)
        {
            settingsEditor.getContent().setPlayerEditorSettings(settings);
        }

        PlayerEditorSettings getSettings() const { return settingsEditor.getContent().getPlayerEditorSettings(); }

        void updateState(const RTC::Array<AudioPlayer::State>& states, unsigned sampleRate)
        {
            if (index < states.size())
            {
                auto& state = states[index];

                startButton.setEnabled(state.isPlaying == false && state.source);
                pauseButton.setEnabled(state.isPlaying == true);
                stopButton.setEnabled(state.isPlaying == true || (getLengthForSource(state.source) && state.position != 0));

                levelSlider.setValue(state.volume);

                sourceCombo.setSelectedId(static_cast<unsigned>(state.source ? *state.source : Invalid));
                busCombo.setSelectedId(static_cast<unsigned>(state.bus ? *state.bus : Invalid));

                playbackSlider.setValues(state.position, getLengthForSource(state.source), sampleRate);

                settingsEditor.getContent().setLooped(state.isLooping);
            }
        }

        void updateAvailableSources(const std::vector<FileInterface::SourceInfo>& sources)
        {
            if (sources.size() != numAvailableSources)
            {
                numAvailableSources = sources.size();

                unsigned previousID = sourceCombo.getSelectedId();

                sourceCombo.clear();
                sourceCombo.addItem("None", Invalid);
                for (const auto& source : sources)
                {
                    sourceCombo.addItem(FilenameHelper::wihtoutPath(source.name), sourceCombo.getNumItems() - 1);
                }

                sourceCombo.setSelectedId(previousID);

                sourceLengths.clear();
                for (const auto& source : sources)
                {
                    sourceLengths.push_back(source.length);
                }
            }
        }

        void updateAvailableBuses(const std::vector<FileInterface::FilterInfo>& filters, unsigned numDirectOutputs, unsigned directOutputsOffset)
        {
            if (filters.size() != numAvailableFilters)
            {
                numAvailableFilters = filters.size();

                unsigned previousID = busCombo.getSelectedId();

                busCombo.clear();
                busCombo.addItem("None", Invalid);

                unsigned iFilter = 0;
                for (const auto& filter : filters)
                {
                    busCombo.addItem(FilenameHelper::wihtoutPath(filter.name), iFilter++);
                }

                if (numDirectOutputs > 0)
                {
                    for (unsigned i = 0; i < numDirectOutputs; ++i)
                    {
                        busCombo.addItem("Direct out " + std::to_string(i), i + directOutputsOffset);
                    }
                }

                busCombo.setSelectedId(previousID);
            }
        }

        std::function<void(const AudioPlayer::Command&)> commandCallback;

    private:
        static constexpr unsigned Invalid{ 255 };

        size_t index;

        std::vector<std::optional<size_t>> sourceLengths;

        size_t numAvailableSources{ 0 };
        size_t numAvailableFilters{ 0 };

        juce::TextButton startButton;
        juce::TextButton pauseButton;
        juce::TextButton stopButton;
        LevelSlider levelSlider;
        LabeledCombo sourceCombo;
        LabeledCombo busCombo;
        PlaybackSlider playbackSlider;

        juce::TextButton settingsButton;

        WindowWithContent<PlayerEditorSettingsEditor> settingsEditor;

        void resized() override
        {
            auto area = getLocalBounds().reduced(10);
            area.removeFromTop(8);

            auto buttonArea = area.removeFromTop(24);
            startButton.setBounds(buttonArea.removeFromLeft(area.getWidth() / 3).reduced(3, 0));
            pauseButton.setBounds(buttonArea.removeFromLeft(area.getWidth() / 3).reduced(3, 0));
            stopButton.setBounds(buttonArea.reduced(3, 0));

            levelSlider.setBounds(area.removeFromTop(24));
            sourceCombo.setBounds(area.removeFromTop(24));
            busCombo.setBounds(area.removeFromTop(24));
            playbackSlider.setBounds(area.removeFromTop(24));
            settingsButton.setBounds(area.removeFromTop(24));
        }

        void sendCommand(const AudioPlayer::Command& command)
        {
            if (commandCallback) { commandCallback(command); }
        }

        std::optional<size_t> getLengthForSource(std::optional<size_t> source)
        {
            if (source && *source < sourceLengths.size())
            {
                return sourceLengths[*source];
            }

            return std::nullopt;
        }
    };
}
