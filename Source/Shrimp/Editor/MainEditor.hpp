/*
  ==============================================================================

    VARyEditor.hpp
    Created: 8 Nov 2023 3:18:01pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "MasterOutputEditor.hpp"
#include "ListenerOrientationEditor.hpp"
#include "PlayerEditorList.hpp"

namespace Shrimp
{
    class MainEditor :
        public juce::Component,
        public juce::Timer
    {
    public:
        MainEditor(MainInterface& mainInterface) :
            masterOutputEditor(mainInterface),
            listenerOrientationEditor(mainInterface),
            playerEditorList(mainInterface)
        {
            addAndMakeVisible(masterOutputEditor);
            addAndMakeVisible(listenerOrientationEditor);
            addAndMakeVisible(playerEditorList);

            timerCallback();
            startTimer(10);
        }

    private:
        MasterOutputEditor masterOutputEditor;
        ListenerOrientationEditor listenerOrientationEditor;
        PlayerEditorList playerEditorList;

        void resized() override
        {
            auto area = getLocalBounds();
            auto leftColumn = area.removeFromLeft(260);

            masterOutputEditor.setBounds(leftColumn.removeFromTop(getHeight() / 2));
            listenerOrientationEditor.setBounds(leftColumn);
            playerEditorList.setBounds(area);
        }

        void timerCallback() override
        {
            masterOutputEditor.update();
            listenerOrientationEditor.update();
            playerEditorList.update();
        }
    };
}