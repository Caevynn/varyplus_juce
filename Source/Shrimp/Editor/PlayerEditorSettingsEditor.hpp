/*
  ==============================================================================

    PlayerSettingsEditor.hpp
    Created: 26 Jan 2024 10:59:12pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "PlayerEditorSettings.hpp"
#include "../../Components/LabeledCombo.hpp"
#include "../../Components/NumericLabel.hpp"

namespace Shrimp
{
    class PlayerEditorSettingsEditor : public juce::Component
    {
    public:
        PlayerEditorSettingsEditor()
        {
            busFadeCombo.setLabelText("Bus fade:");
            busFadeCombo.addItem("No fade", 0);
            busFadeCombo.addItem("Fade out and in", 1);
            busFadeCombo.addItem("Crossfade", 2);
            busFadeCombo.setSelectedId(0);
            busFadeCombo.onChange = [this](unsigned)
                {
                    updateBusFadeEditStates();
                };
            addAndMakeVisible(busFadeCombo);

            busFadeOutLabel.setText("Fade out:", juce::dontSendNotification);
            addAndMakeVisible(busFadeOutLabel);

            busFadeOutEdit.setUnit("s");
            busFadeOutEdit.setValue(1);
            addAndMakeVisible(busFadeOutEdit);

            busFadePauseLabel.setText("Fade pause:", juce::dontSendNotification);
            addAndMakeVisible(busFadePauseLabel);

            busFadePauseEdit.setUnit("s");
            busFadePauseEdit.setValue(0);
            addAndMakeVisible(busFadePauseEdit);

            busFadeInLabel.setText("Fade in:", juce::dontSendNotification);
            addAndMakeVisible(busFadeInLabel);

            busFadeInEdit.setUnit("s");
            busFadeInEdit.setValue(1);
            addAndMakeVisible(busFadeInEdit);

            loopButton.setButtonText("Enable looping");
            loopButton.onClick = [this]()
                {
                    if (onLoopChange) { onLoopChange(loopButton.getToggleState()); }
                };
            addAndMakeVisible(loopButton);

            closeButton.setButtonText("Close");
            closeButton.onClick = [this]()
                {
                    if (closeRequest) { closeRequest(); }
                };
            addAndMakeVisible(closeButton);

            updateBusFadeEditStates();
        }

        bool isLooped() const { return loopButton.getToggleState(); }
        void setLooped(bool isEnabled) { loopButton.setToggleState(isEnabled, juce::dontSendNotification); }

        PlayerEditorSettings getPlayerEditorSettings() const
        {
            PlayerEditorSettings settings;

            settings.busFadeType = static_cast<PlayerEditorSettings::BusFadeType>(busFadeCombo.getSelectedId());
            settings.busFadeOutTime = busFadeOutEdit.getValue();
            settings.busFadePauseTime = busFadePauseEdit.getValue();
            settings.busFadeInTime = busFadeInEdit.getValue();

            return settings;
        }

        void setPlayerEditorSettings(const PlayerEditorSettings& settings)
        {
            busFadeCombo.setSelectedId(static_cast<unsigned>(settings.busFadeType));
            busFadeOutEdit.setValue(settings.busFadeInTime);
            busFadePauseEdit.setValue(settings.busFadePauseTime);
            busFadeInEdit.setValue(settings.busFadeInTime);

            updateBusFadeEditStates();
        }

        std::function<void(bool)> onLoopChange;
        std::function<void()> closeRequest;

    private:
        LabeledCombo busFadeCombo;
        juce::Label busFadeOutLabel;
        juce::Label busFadePauseLabel;
        juce::Label busFadeInLabel;
        NumericLabel busFadeOutEdit;
        NumericLabel busFadePauseEdit;
        NumericLabel busFadeInEdit;

        juce::ToggleButton loopButton;
        juce::TextButton closeButton;

        void updateBusFadeEditStates()
        {
            busFadeOutEdit.setEnabled(busFadeCombo.getSelectedId() > 0);
            busFadePauseEdit.setEnabled(busFadeCombo.getSelectedId() == 1);
            busFadeInEdit.setEnabled(busFadeCombo.getSelectedId() == 1);
        }

        void paint(juce::Graphics& g) override
        {
            g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));
        }

        void resized() override
        {
            auto area = getLocalBounds().reduced(10);

            busFadeCombo.setBounds(area.removeFromTop(30).reduced(3));

            auto busFadeOutArea = area.removeFromTop(30).reduced(3);
            busFadeOutLabel.setBounds(busFadeOutArea.removeFromLeft(area.getWidth() / 3));
            busFadeOutEdit.setBounds(busFadeOutArea);
            auto busFadePauseArea = area.removeFromTop(30).reduced(3);
            busFadePauseLabel.setBounds(busFadePauseArea.removeFromLeft(area.getWidth() / 3));
            busFadePauseEdit.setBounds(busFadePauseArea);
            auto busFadeInArea = area.removeFromTop(30).reduced(3);
            busFadeInLabel.setBounds(busFadeInArea.removeFromLeft(area.getWidth() / 3));
            busFadeInEdit.setBounds(busFadeInArea);

            loopButton.setBounds(area.removeFromTop(30).reduced(3));

            closeButton.setBounds(area.removeFromBottom(30).reduced(3));
        }
    };
}