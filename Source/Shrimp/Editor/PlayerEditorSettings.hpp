/*
  ==============================================================================

    PlayerEditorSettings.hpp
    Created: 27 Jan 2024 1:24:22pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

namespace Shrimp
{
    struct PlayerEditorSettings
    {
        enum class BusFadeType : unsigned
        {
            NoFade,
            FadeOutAndIn,
            Crossfade
        } busFadeType;

        double busFadeOutTime{ 0 };
        double busFadePauseTime{ 0 };
        double busFadeInTime{ 0 };
    };
}
