/*
  ==============================================================================

    RemoteInterface.hpp
    Created: 17 Nov 2023 3:50:31pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#ifdef _WIN32
#ifndef NOMINMAX
#   define NOMINMAX
#endif
#include <WinSock2.h>
#endif

#include "ThreadCommunication.hpp"
#include "FileInterface.hpp"
#include "../../AudioProcessor/AudioCallbackTypes.hpp"
#include "../../AudioProcessor/OSCBase.hpp"

namespace Shrimp
{
    class RemoteInterface
    {
        struct Addresses
        {
            static inline const std::string shrimp{ "/shrimp" };

            static inline const std::string ListenerOrientation{ shrimp + "/listener_orientation" };
            static inline const std::string MasterVolume{ shrimp + "/master_volume" };

            static inline const std::string StartAll{ shrimp + "/start_all" };
            static inline const std::string PauseAll{ shrimp + "/pause_all" };
            static inline const std::string StopAll{ shrimp + "/stop_all" };

            struct Player
            {
                static inline const std::string player{ "/player" };

                static inline const std::string Start{ shrimp + player + "/start" };
                static inline const std::string Pause{ shrimp + player + "/pause" };
                static inline const std::string Stop{ shrimp + player + "/stop" };
                static inline const std::string Source{ shrimp + player + "/source" };
                static inline const std::string Bus{ shrimp + player + "/bus" };
                static inline const std::string BusFade{ shrimp + player + "/bus_fade" };
                static inline const std::string BusCrossfade{ shrimp + player + "/bus_crossfade" };
                static inline const std::string Volume{ shrimp + player + "/volume" };
                static inline const std::string Position{ shrimp + player + "/position" };
                static inline const std::string Loop{ shrimp + player + "/loop" };
            };

            static inline const std::string LoadSource{ shrimp + "/load_source" };
            static inline const std::string UnloadSource{ shrimp + "/unload_source" };
            static inline const std::string UnloadAllSources{ shrimp + "/unload_all_sources" };
            static inline const std::string LoadFilter{ shrimp + "/load_filter" };
            static inline const std::string UnloadFilter{ shrimp + "/unload_filter" };
            static inline const std::string UnloadAllFilters{ shrimp + "/unload_all_filters" };
            static inline const std::string UnloadAllSourcesAndFilters{ shrimp + "/unload_all_sources_and_filters" };
            static inline const std::string LoadMasterFilter{ shrimp + "/load_master_filter" };
            static inline const std::string UnloadMasterFilter{ shrimp + "/unload_master_filter" };

            static inline const std::string TestMessage{ shrimp + "/test_message" };
        };

    public:
        template<typename OSCImplementation>
        RemoteInterface(ThreadCommunication::RemoteThreadInterface& audioThread, [[maybe_unused]] AudioCallback::DebugOutput& debugOutput, FileInterface& fileInterface, std::in_place_type_t<OSCImplementation>) :
            audioThread(audioThread),
            fileInterface(fileInterface)
        {
            static_assert(std::is_base_of_v<OSC::ReaderBase, typename OSCImplementation::Reader>);
            static_assert(std::is_base_of_v<OSC::WriterBase, typename OSCImplementation::Writer>);

            oscReader = std::make_unique<typename OSCImplementation::Reader>();
            oscWriter = std::make_unique<typename OSCImplementation::Writer>();

            oscReader->addHandler(Addresses::ListenerOrientation, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<float, float>())
                    {
                        double azimuth = message.getValue<float>(0);
                        double elevation = message.getValue<float>(1);

                        this->audioThread.pushListenerOrientation({ azimuth, elevation });
                    }
                });

            oscReader->addHandler(Addresses::MasterVolume, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<float>())
                    {
                        this->audioThread.pushMasterVolume(message.getValue<float>(0));
                    }
                });

            oscReader->addHandler(Addresses::StartAll, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<>())
                    {
                        this->audioThread.pushPlayerCommand({ std::nullopt, AudioPlayer::Command::Play() });
                    }
                });

            oscReader->addHandler(Addresses::PauseAll, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<>())
                    {
                        this->audioThread.pushPlayerCommand({ std::nullopt, AudioPlayer::Command::Pause() });
                    }
                });

            oscReader->addHandler(Addresses::StopAll, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<>())
                    {
                        this->audioThread.pushPlayerCommand({ std::nullopt, AudioPlayer::Command::Stop() });
                    }
                });

            oscReader->addHandler(Addresses::Player::Start, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int>())
                    {
                        this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Play() });
                    }
                });

            oscReader->addHandler(Addresses::Player::Pause, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int>())
                    {
                        this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Pause() });
                    }
                });

            oscReader->addHandler(Addresses::Player::Stop, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int>())
                    {
                        this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Stop() });
                    }
                });

            oscReader->addHandler(Addresses::Player::Source, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int, int>())
                    {
                        if (message.getValue<int>(1) >= 0)
                        {
                            this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Source(message.getValue<int>(1)) });
                        }
                        else
                        {
                            this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Source(std::nullopt) });
                        }
                    }
                });

            oscReader->addHandler(Addresses::Player::Bus, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int, int>())
                    {
                        if (message.getValue<int>(1) >= 0)
                        {
                            this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Bus(message.getValue<int>(1)) });
                        }
                        else
                        {
                            this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Bus(std::nullopt) });
                        }
                    }
                });

            oscReader->addHandler(Addresses::Player::BusFade, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int, int, float, float, float>())
                    {
                        if (message.getValue<int>(1) >= 0)
                        {
                            this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::BusFade(message.getValue<int>(1),
                                message.getValue<float>(2),
                                message.getValue<float>(3),
                                message.getValue<float>(4)) });
                        }
                        else
                        {
                            this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::BusFade(std::nullopt,
                                message.getValue<float>(2),
                                message.getValue<float>(3),
                                message.getValue<float>(4)) });
                        }
                    }
                });

            oscReader->addHandler(Addresses::Player::BusCrossfade, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int, int, float>())
                    {
                        if (message.getValue<int>(1) >= 0)
                        {
                            this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::BusCrossfade(message.getValue<int>(1), message.getValue<float>(2)) });
                        }
                        else
                        {
                            this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::BusCrossfade(std::nullopt, message.getValue<float>(2)) });
                        }
                    }
                });

            oscReader->addHandler(Addresses::Player::Volume, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int, float>())
                    {
                        this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Volume(message.getValue<float>(1)) });
                    }
                });

            oscReader->addHandler(Addresses::Player::Position, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int, int>())
                    {
                        this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Position(message.getValue<int>(1)) });
                    }
                });

            oscReader->addHandler(Addresses::Player::Loop, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int, int>())
                    {
                        this->audioThread.pushPlayerCommand({ message.getValue<int>(0), AudioPlayer::Command::Loop(message.getValue<int>(1) == 1) });
                    }
                });

            oscReader->addHandler(Addresses::LoadSource, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<std::string>())
                    {
                        this->fileInterface.addSourceFromFile(message.getValue<std::string>(0));
                    }
                });

            oscReader->addHandler(Addresses::UnloadSource, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int>())
                    {
                        this->fileInterface.removeSource(static_cast<size_t>(message.getValue<int>(0)));
                    }
                });

            oscReader->addHandler(Addresses::UnloadAllSources, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<>())
                    {
                        this->fileInterface.removeAllSources();
                    }
                });

            oscReader->addHandler(Addresses::LoadFilter, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<std::string>())
                    {
                        this->fileInterface.addFilterFromFile(message.getValue<std::string>(0));
                    }
                });

            oscReader->addHandler(Addresses::UnloadFilter, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<int>())
                    {
                        this->fileInterface.removeFilter(static_cast<size_t>(message.getValue<int>(0)));
                    }
                });

            oscReader->addHandler(Addresses::UnloadAllFilters, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<>())
                    {
                        this->fileInterface.removeAllFilters();
                    }
                });

            oscReader->addHandler(Addresses::UnloadAllSourcesAndFilters, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<>())
                    {
                        this->fileInterface.removeAllSources();
                        this->fileInterface.removeAllFilters();
                    }
                });

            oscReader->addHandler(Addresses::LoadMasterFilter, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<std::string>())
                    {
                        this->fileInterface.addMasterFilterFromFile(message.getValue<std::string>(0));
                    }
                });

            oscReader->addHandler(Addresses::UnloadMasterFilter, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<>())
                    {
                        this->fileInterface.removeMasterFilter();
                    }
                });

            oscReader->addHandler(Addresses::TestMessage, [this](const OSC::Message& message)
                {
                    if (message.holdsValues<>())
                    {
                        isTestSuccessful.store(true);
                    }
                });

            isBindSuccessful = oscReader->startReading(OSCPort) && oscWriter->startWriting("127.0.0.1", OSCPort);

            if (!isBindSuccessful)
            {
#ifdef _WIN32
                bindErrorCode = WSAGetLastError();
#elif __linux__
                bindErrorCode = errno;
#else
#error Not supported
#endif
            }

            oscWriter->write(OSC::Message(Addresses::TestMessage));
        }

        void setListenerOrientation(const HRTF::ListenerOrientation& listenerOrientation)
        {
            oscWriter->write(OSC::Message(Addresses::ListenerOrientation)
                .withValue<float>(listenerOrientation.getAzimuth())
                .withValue<float>(listenerOrientation.getElevation()));
        }

        void setMasterVolume(double volume)
        {
            oscWriter->write(OSC::Message(Addresses::MasterVolume)
                .withValue<float>(volume));
        }

        void sendPlayerCommand(const AudioPlayer::Command& command)
        {
            if (!command.iChannel)
            {
                oscWriter->write(std::visit(VariantHelpers::overloaded{
                    [](auto) { return OSC::Message(""); },
                    [](AudioPlayer::Command::Play) { return OSC::Message(Addresses::StartAll); },
                    [](AudioPlayer::Command::Pause) { return OSC::Message(Addresses::PauseAll); },
                    [](AudioPlayer::Command::Stop) { return OSC::Message(Addresses::StopAll); },
                    }, command.variant));
            }
            else
            {
                oscWriter->write(std::visit(VariantHelpers::overloaded{
                    [](auto) { return OSC::Message(""); },
                    [&](AudioPlayer::Command::Play) { return OSC::Message(Addresses::Player::Start).withValue<int>(*command.iChannel); },
                    [&](AudioPlayer::Command::Pause) { return OSC::Message(Addresses::Player::Pause).withValue<int>(*command.iChannel); },
                    [&](AudioPlayer::Command::Stop) { return OSC::Message(Addresses::Player::Stop).withValue<int>(*command.iChannel); },
                    [&](AudioPlayer::Command::Source)
                    {
                        auto source = std::get<AudioPlayer::Command::Source>(command.variant).source;

                        return OSC::Message(Addresses::Player::Source)
                            .withValue<int>(*command.iChannel)
                            .withValue<int>(source ? *source : -1);
                    },
                    [&](AudioPlayer::Command::Bus)
                    {
                        auto bus = std::get<AudioPlayer::Command::Bus>(command.variant).bus;

                        return OSC::Message(Addresses::Player::Bus)
                            .withValue<int>(*command.iChannel)
                            .withValue<int>(bus ? *bus : -1);
                    },
                    [&](AudioPlayer::Command::BusFade)
                    {
                        auto busFade = std::get<AudioPlayer::Command::BusFade>(command.variant);

                        return OSC::Message(Addresses::Player::BusFade)
                            .withValue<int>(*command.iChannel)
                            .withValue<int>(busFade.bus ? *busFade.bus : -1)
                            .withValue<float>(busFade.outTime)
                            .withValue<float>(busFade.pauseTime)
                            .withValue<float>(busFade.inTime);
                    },
                    [&](AudioPlayer::Command::BusCrossfade)
                    {
                        auto busFade = std::get<AudioPlayer::Command::BusCrossfade>(command.variant);

                        return OSC::Message(Addresses::Player::BusCrossfade)
                            .withValue<int>(*command.iChannel)
                            .withValue<int>(busFade.bus ? *busFade.bus : -1)
                            .withValue<float>(busFade.time);
                    },
                    [&](AudioPlayer::Command::Volume)
                    {
                        auto volume = std::get<AudioPlayer::Command::Volume>(command.variant).volume;

                        return OSC::Message(Addresses::Player::Volume)
                            .withValue<int>(*command.iChannel)
                            .withValue<float>(volume);
                    },
                    [&](AudioPlayer::Command::Position)
                    {
                        auto position = std::get<AudioPlayer::Command::Position>(command.variant).position;

                        return OSC::Message(Addresses::Player::Position)
                            .withValue<int>(*command.iChannel)
                            .withValue<int>(position);
                    },
                    [&](AudioPlayer::Command::Loop)
                    {
                        auto enable = std::get<AudioPlayer::Command::Loop>(command.variant).enable;

                        return OSC::Message(Addresses::Player::Loop)
                            .withValue<int>(*command.iChannel)
                            .withValue<int>(enable ? 1 : 0);
                    },
                    }, command.variant));
            }
        }

        void loadSourceFile(const std::string& filename)
        {
            oscWriter->write(OSC::Message(Addresses::LoadSource)
                .withValue<std::string>(filename));
        }

        void unloadSourceFile(size_t index)
        {
            oscWriter->write(OSC::Message(Addresses::UnloadSource)
                .withValue<int>(index));
        }

        void unloadAllSourceFiles()
        {
            oscWriter->write(OSC::Message(Addresses::UnloadAllSources));
        }

        void loadFilterFile(const std::string& filename)
        {
            oscWriter->write(OSC::Message(Addresses::LoadFilter)
                .withValue<std::string>(filename));
        }

        void unloadFilterFile(size_t index)
        {
            oscWriter->write(OSC::Message(Addresses::UnloadFilter)
                .withValue<int>(index));
        }

        void unloadAllFilterFiles()
        {
            oscWriter->write(OSC::Message(Addresses::UnloadAllFilters));
        }

        void unloadAllSourceAndFilterFiles()
        {
            oscWriter->write(OSC::Message(Addresses::UnloadAllSourcesAndFilters));
        }

        void loadMasterFilterFile(const std::string& filename)
        {
            oscWriter->write(OSC::Message(Addresses::LoadMasterFilter)
                .withValue<std::string>(filename));
        }

        void unloadMasterFilterFile()
        {
            oscWriter->write(OSC::Message(Addresses::UnloadMasterFilter));
        }

        std::optional<std::string> checkOSCConnection()
        {
            if (!isBindSuccessful)
            {
                return "Failed to bind OSC to port " + std::to_string(OSCPort) + " (error code: " + std::to_string(*bindErrorCode) + ")";
            }
            else if (!isTestSuccessful.load())
            {
                return "Failed to receive OSC messages";
            }

            return std::nullopt;
        }

    private:
        static constexpr unsigned OSCPort = 27388;

        bool isBindSuccessful{ false };
        std::optional<int> bindErrorCode;
        std::atomic<bool> isTestSuccessful{ false };

        ThreadCommunication::RemoteThreadInterface& audioThread;
        FileInterface& fileInterface;

        std::unique_ptr<OSC::ReaderBase> oscReader;
        std::unique_ptr<OSC::WriterBase> oscWriter;
    };
}
