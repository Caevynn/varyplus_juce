/*
  ==============================================================================

    ThreadCommunication.cpp
    Created: 8 Nov 2023 10:02:46am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../../AudioProcessor/Utils/RealTimeContainers/RTCAtomicRingBuffer.hpp"
#include "../../AudioProcessor/Utils/RealTimeContainers/RTCAtomicTASBuffer.hpp"
#include "../../AudioProcessor/Utils/RealTimeContainers/RTCUniquePointer.hpp"

#include "../../AudioProcessor/AudioPlayer.hpp"
#include "../../AudioProcessor/LimitingFilter.hpp"
#include "../../AudioProcessor/AudioSource.hpp"
#include "../../AudioProcessor/FrequencyDomainConvolution/FDCFFilter.hpp"
#include "../../HRTF/Filter.hpp"

namespace Shrimp
{
    class ThreadCommunication
    {
        static_assert(std::atomic<double>::is_always_lock_free);

    public:
        class Data
        {
        public:
            Data() = default;
            Data(size_t index) : index(index) {}
            Data(RTC::UniquePointer<HRTF::Filter>&& filter) : filter(std::move(filter)) {}
            Data(RTC::UniquePointer<AudioSource>&& source) : source(std::move(source)) {}
            Data(RTC::UniquePointer<FDCF::Filter>&& masterFilter) : masterFilter(std::move(masterFilter)) {}

            bool hasIndex() const { return index.operator bool(); }
            bool hasFilter() const { return filter.operator bool(); }
            bool hasSource() const { return source.operator bool(); }
            bool hasMasterFilter() const { return masterFilter.operator bool(); }

            operator bool() const { return hasIndex() || hasFilter() || hasSource() || hasMasterFilter(); }

            [[nodiscard]] size_t getIndex() { return *index; }
            [[nodiscard]] RTC::UniquePointer<HRTF::Filter> getFilter() { return std::move(filter); }
            [[nodiscard]] RTC::UniquePointer<AudioSource> getSource() { return std::move(source); }
            [[nodiscard]] RTC::UniquePointer<FDCF::Filter> getMasterFilter() { return std::move(masterFilter); }

        private:
            std::optional<size_t> index;
            RTC::UniquePointer<HRTF::Filter> filter;
            RTC::UniquePointer<AudioSource> source;
            RTC::UniquePointer<FDCF::Filter> masterFilter;
        };

        class DataCommand : public Data
        {
        public:
            enum class Type { Default, LoadSource, UnloadSource, LoadFilter, UnloadFilter, LoadMasterFilter, UnloadMasterFilter };

            DataCommand() = default;

            [[nodiscard]] static DataCommand loadFilter(std::unique_ptr<HRTF::Filter>&& filter) { return DataCommand(Type::LoadFilter, std::move(filter)); }
            [[nodiscard]] static DataCommand unloadFilter(size_t index) { return DataCommand(Type::UnloadFilter, index); }
            [[nodiscard]] static DataCommand loadSource(std::unique_ptr<AudioSource>&& source) { return DataCommand(Type::LoadSource, std::move(source)); }
            [[nodiscard]] static DataCommand unloadSource(size_t index) { return DataCommand(Type::UnloadSource, index); }
            [[nodiscard]] static DataCommand loadMasterFilter(std::unique_ptr<FDCF::Filter>&& masterFilter) { return DataCommand(Type::LoadMasterFilter, std::move(masterFilter)); }
            [[nodiscard]] static DataCommand unloadMasterFilter() { return DataCommand(Type::UnloadMasterFilter); }

            Type getType() const { return type; }

        private:
            Type type{ Type::Default };

            DataCommand(Type type) : type(type) {}
            DataCommand(Type type, size_t index) : Data(index), type(type) {}
            DataCommand(Type type, std::unique_ptr<HRTF::Filter>&& filter) : Data(std::move(filter)), type(type) {}
            DataCommand(Type type, std::unique_ptr<AudioSource>&& source) : Data(std::move(source)), type(type) {}
            DataCommand(Type type, std::unique_ptr<FDCF::Filter>&& masterFilter) : Data(std::move(masterFilter)), type(type) {}
        };

        class RemoteThreadInterface
        {
        public:
            RemoteThreadInterface(ThreadCommunication& data) : data(data) {}

            bool pushListenerOrientation(const HRTF::ListenerOrientation& listenerOrientation) { return data.listenerOrientationBuffer.push(listenerOrientation); }
            bool pushPlayerCommand(const AudioPlayer::Command& playerCommand) { return data.playerCommandBuffer.push(playerCommand); }
            bool pushMasterVolume(double volume) { data.masterVolumeBuffer.store(volume, std::memory_order_relaxed); return true; }

        private:
            ThreadCommunication& data;
        };

        class GuiThreadInterface
        {
        public:
            GuiThreadInterface(ThreadCommunication& data, unsigned numPlayers, unsigned maxNumSources, unsigned maxNumFilters, unsigned numInputBusChannels, unsigned numOutputBusChannels) :
                data(data),
                numPlayers(numPlayers),
                maxNumSources(maxNumSources),
                maxNumFilters(maxNumFilters),
                numInputBusChannels(numInputBusChannels),
                numOutputBusChannels(numOutputBusChannels),
                numInputChannels(0),
                numOutputChannels(0),
                sampleRate(0),
                blockSize(0),
                running(false)
            {
                currentPlayerStates.allocate(numPlayers);
            }

            ~GuiThreadInterface()
            {
                currentPlayerStates.deallocate();
            }

            void load(unsigned wantedNumInputChannels, unsigned wantedNumOutputChannels, unsigned wantedSampleRate, unsigned wantedBlockSize)
            {
                numInputChannels = wantedNumInputChannels;
                numOutputChannels = wantedNumOutputChannels;
                sampleRate = wantedSampleRate;
                blockSize = wantedBlockSize;

                running = true;

                currentOutputStates.allocate(numOutputChannels);
            }

            void setNumInputBusChannels(unsigned wantedNumInputBusChannels)
            {
                numInputBusChannels = wantedNumInputBusChannels;
            }

            void unload()
            {
                numInputChannels = 0;
                numOutputChannels = 0;
                sampleRate = 0;
                blockSize = 0;

                running = false;

                currentOutputStates.deallocate();
            }

            void timer()
            {
                if (running)
                {
                    data.listenerOrientationReturnBuffer.tryPop(currentListenerOrientation);
                    data.playerStatesBuffer.tryPop(currentPlayerStates);
                    data.outputStatesBuffer.tryPop(currentOutputStates);
                    currentMasterVolume = data.masterVolumeReturnBuffer.load(std::memory_order_relaxed);
                }
                else
                {
                    // Ensure the buffer does not overflow
                    data.listenerOrientationBuffer.popLatest(currentListenerOrientation);
                }
            }

            unsigned getNumPlayers() const { return numPlayers; }
            unsigned getMaxNumSources() const { return maxNumSources; }
            unsigned getMaxNumFilters() const { return maxNumFilters; }
            unsigned getNumInputBusChannels() const { return numInputBusChannels; }
            unsigned getNumOutputBusChannels() const { return numOutputBusChannels; }
            unsigned getNumInputChannels() const { return numInputChannels; }
            unsigned getNumOutputChannels() const { return numOutputChannels; }
            unsigned getSampleRate() const { return sampleRate; }
            unsigned getBlockSize() const { return blockSize; }
            bool isRunning() const { return running; }

            bool pushDataCommand(DataCommand&& dataCommand) { return data.dataCommandBuffer.push(std::move(dataCommand)); }
            bool pushFilter(std::unique_ptr<HRTF::Filter>&& filter) { return pushDataCommand(DataCommand::loadFilter(std::move(filter))); }
            bool pushSource(std::unique_ptr<AudioSource>&& source) { return pushDataCommand(DataCommand::loadSource(std::move(source))); }
            bool pushMasterFilter(std::unique_ptr<FDCF::Filter>&& masterFilter) { return pushDataCommand(DataCommand::loadMasterFilter(std::move(masterFilter))); }

            const HRTF::ListenerOrientation& getCurrentListenerOrientation() const { return currentListenerOrientation; }
            const RTC::Array<AudioPlayer::State>& getCurrentPlayerStates() const { return currentPlayerStates; }
            const RTC::Array<LimitingFilter::State>& getCurrentOutputStates() const { return currentOutputStates; }
            double getCurrentMasterVolume() const { return currentMasterVolume; }

        private:
            ThreadCommunication& data;

            // Copies of current settings
            unsigned numPlayers, maxNumSources, maxNumFilters, numInputBusChannels, numOutputBusChannels;
            unsigned numInputChannels, numOutputChannels, sampleRate, blockSize;
            bool running{ false };

            // Copies of current state
            HRTF::ListenerOrientation currentListenerOrientation;
            RTC::Array<AudioPlayer::State> currentPlayerStates;
            RTC::Array<LimitingFilter::State> currentOutputStates;
            double currentMasterVolume{ 1.0 };
        };

        class AudioThreadInterface
        {
        public:
            AudioThreadInterface(ThreadCommunication& data) : data(data) {}

            bool popDataCommand(DataCommand& dataCommand) { return data.dataCommandBuffer.pop(dataCommand); }
            bool popLatestListenerOrientation(HRTF::ListenerOrientation& listenerOrientation) { return data.listenerOrientationBuffer.popLatest(listenerOrientation); }
            bool popPlayerCommand(AudioPlayer::Command& playerCommand) { return data.playerCommandBuffer.pop(playerCommand); }
            bool popMasterVolume(double& volume) { volume = data.masterVolumeBuffer.load(std::memory_order_relaxed); return true; }

            bool pushForDeletion(Data&& dataToDelete) { return data.dataDeletionBuffer.push(std::move(dataToDelete)); }
            bool tryPushListenerOrientation(const HRTF::ListenerOrientation& listenerOrientation) { return data.listenerOrientationReturnBuffer.tryPush(listenerOrientation); }
            bool tryPushPlayerState(size_t index, const AudioPlayer::State& playerState) { return data.playerStatesBuffer.tryPush(playerState, index); }
            bool tryPushOutputStates(const RTC::Array<LimitingFilter::State>& outputStates) { return data.outputStatesBuffer.tryPush(outputStates); }
            bool tryPushMasterVolume(double volume) { data.masterVolumeReturnBuffer.store(volume, std::memory_order_relaxed); return true; }

        private:
            ThreadCommunication& data;
        };

        ThreadCommunication(unsigned numPlayers, unsigned maxNumSources, unsigned maxNumFilters, unsigned numInputBusChannels, unsigned numOutputBusChannels) :
            guiThreadInterface(*this, numPlayers, maxNumSources, maxNumFilters, numInputBusChannels, numOutputBusChannels),
            remoteThreadInterface(*this),
            audioThreadInterface(*this)
        {
            playerStatesBuffer.allocate(numPlayers);
            listenerOrientationBuffer.push(HRTF::ListenerOrientation(0, 0));
        }

        void setNumInputBusChannels(unsigned wantedNumINputBusChannels)
        {
            guiThreadInterface.setNumInputBusChannels(wantedNumINputBusChannels);
        }

        void load(unsigned wantedNumInputChannels, unsigned wantedNumOutputChannels, unsigned wantedSampleRate, unsigned wantedBlockSize)
        {
            ASSERT(listenerOrientationBuffer.getResetOverflowCount() == 0);

            outputStatesBuffer.allocate(wantedNumOutputChannels);

            // Don't clear listenerOrientationBuffer as it may receive when not running

            dataCommandBuffer.clearWithDeallocation();
            playerCommandBuffer.clearWithDeallocation();

            // Reset overflow count as the buffer is not read while not running
            playerCommandBuffer.getResetOverflowCount();

            guiThreadInterface.load(wantedNumInputChannels, wantedNumOutputChannels, wantedSampleRate, wantedBlockSize);
        }

        void unload()
        {
            ASSERT(dataCommandBuffer.getResetOverflowCount() == 0);
            ASSERT(dataDeletionBuffer.getResetOverflowCount() == 0);
            ASSERT(listenerOrientationBuffer.getResetOverflowCount() == 0);
            ASSERT(playerCommandBuffer.getResetOverflowCount() == 0);

            outputStatesBuffer.deallocate();

            guiThreadInterface.unload();
        }

        void timer()
        {
            for (Data dataToDelete; dataDeletionBuffer.pop(dataToDelete);)
            {
                dataToDelete.getFilter().deallocate();
                dataToDelete.getSource().deallocate();
                dataToDelete.getMasterFilter().deallocate();
            }

            guiThreadInterface.timer();
        }

        GuiThreadInterface& getGuiThreadInterface() { return guiThreadInterface; }
        RemoteThreadInterface& getRemoteThreadInterface() { return remoteThreadInterface; }
        AudioThreadInterface& getAudioThreadInterface() { return audioThreadInterface; }

    private:
        friend class AudioThreadInterface;
        friend class GUIThreadInterface;

        GuiThreadInterface guiThreadInterface;
        RemoteThreadInterface remoteThreadInterface;
        AudioThreadInterface audioThreadInterface;

        // Communication for laoding and removing sources and filters
        RTC::AtomicRingBuffer<DataCommand> dataCommandBuffer;
        RTC::AtomicRingBuffer<Data> dataDeletionBuffer;

        // Communication with MainProcessor
        std::atomic<double> masterVolumeBuffer{ 1.0 };
        std::atomic<double> masterVolumeReturnBuffer{ 1.0 };
        RTC::AtomicTASBuffer<LimitingFilter::State> outputStatesBuffer;

        // Communication with FilterProcessor
        RTC::AtomicRingBuffer<HRTF::ListenerOrientation> listenerOrientationBuffer;
        RTC::AtomicTASBuffer<HRTF::ListenerOrientation> listenerOrientationReturnBuffer;

        // Communication with SourceProcessor
        RTC::AtomicRingBuffer<AudioPlayer::Command> playerCommandBuffer;
        RTC::AtomicTASBuffer<AudioPlayer::State> playerStatesBuffer;
    };
}
