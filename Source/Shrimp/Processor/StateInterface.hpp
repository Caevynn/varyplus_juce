/*
  ==============================================================================

    StateInterface.hpp
    Created: 17 Nov 2023 5:49:03pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "ThreadCommunication.hpp"

namespace Shrimp
{
    class StateInterface
    {
    public:
        StateInterface(ThreadCommunication::GuiThreadInterface& audioThread, [[maybe_unused]] AudioCallback::DebugOutput& debugOutput) :
            audioThread(audioThread)
        {}

        unsigned getNumPlayers() const { return audioThread.getNumPlayers(); }
        unsigned getMaxNumSources() const { return audioThread.getMaxNumSources(); }
        unsigned getMaxNumFilters() const { return audioThread.getMaxNumFilters(); }
        unsigned getNumInputBusChannels() const { return audioThread.getNumInputBusChannels(); }
        unsigned getNumOutputBusChannels() const { return audioThread.getNumOutputBusChannels(); }
        unsigned getNumInputChannels() const { return audioThread.getNumInputChannels(); }
        unsigned getNumOutputChannels() const { return audioThread.getNumOutputChannels(); }
        unsigned getSampleRate() const { return audioThread.getSampleRate(); }
        unsigned getBlockSize() const { return audioThread.getBlockSize(); }
        bool isRunning() const { return audioThread.isRunning(); }

        HRTF::ListenerOrientation getListenerOrientation() const { return audioThread.getCurrentListenerOrientation(); }
        const RTC::Array<AudioPlayer::State>& getPlayerStates() const { return audioThread.getCurrentPlayerStates(); }
        const RTC::Array<LimitingFilter::State>& getOutputStates() const { return audioThread.getCurrentOutputStates(); }
        double getMasterVolume() const { return audioThread.getCurrentMasterVolume(); }

    private:
        ThreadCommunication::GuiThreadInterface& audioThread;
    };
}
