/*
  ==============================================================================

    FilterHandler.hpp
    Created: 25 Oct 2023 4:21:46pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "ThreadCommunication.hpp"
#include "../../HRTF/Filter.hpp"
#include "../../AudioProcessor/Utils/Assert.hpp"

namespace Shrimp
{
    class FilterProcessor
    {
    public:
        FilterProcessor(ThreadCommunication::AudioThreadInterface& guiThread) :
            guiThread(guiThread)
        {}

        void run(const AudioSignalBuses& inputBuses, AudioSignalBuses& outputBuses)
        {
            ASSERT(inputBuses.nBuses() >= filters.size() && outputBuses.nBuses() >= filters.size());

            HRTF::ListenerOrientation listenerOrientation;
            ASSUME(guiThread.popLatestListenerOrientation(listenerOrientation));

            for (size_t iFilter = 0; iFilter < filters.size(); ++ iFilter)
            {
                filters[iFilter]->run(inputBuses.bus(iFilter), outputBuses.bus(iFilter), listenerOrientation);
            }

            guiThread.tryPushListenerOrientation(listenerOrientation);
        }

        void addFilter(RTC::UniquePointer<HRTF::Filter>&& newFilter)
        {
            ASSUME(filters.push_back(std::move(newFilter)));
        }

        RTC::UniquePointer<HRTF::Filter> removeFilter(size_t index)
        {
            RTC::UniquePointer<HRTF::Filter> removedFilter;
            ASSUME(filters.pop(index, removedFilter));
            return removedFilter;
        }

        size_t getNumFilters() const
        {
            return filters.size();
        }

        void allocate(size_t maxNumFilters)
        {
            filters.allocate(maxNumFilters);
        }

        void deallocate()
        {
            filters.deallocate();
        }

    private:
        ThreadCommunication::AudioThreadInterface& guiThread;

        RTC::Vector<RTC::UniquePointer<HRTF::Filter>> filters;
    };
}
