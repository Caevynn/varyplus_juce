/*
  ==============================================================================

    FileHandler.hpp
    Created: 6 Nov 2023 11:00:38am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <vector>

#include "ThreadCommunication.hpp"
#include "../../AudioProcessor/AudioFile.hpp"
#include "../../AudioProcessor/AudioSource.hpp"
#include "../../AudioProcessor/FrequencyDomainConvolution/FDCFFile.hpp"
#include "../../AudioProcessor/FrequencyDomainConvolution/FDCFFilter.hpp"
#include "../../HRTF/File.hpp"
#include "../../HRTF/Filter.hpp"

namespace Shrimp
{
    class FileInterface
    {
    public:
        struct State
        {
            std::string name;
            std::string size{ "" };
            std::string sampleRate{ "" };
            bool loaded{ false };

            State(const std::string& name) : name(name) {}
        };

        struct SourceInfo
        {
            std::string name;
            std::optional<size_t> length;

            SourceInfo(const std::string& name, std::optional<size_t> length = std::nullopt) : name(name), length(length) {}
        };

        struct FilterInfo
        {
            std::string name;

            FilterInfo(const std::string& name) : name(name) {}
        };

        bool addSourceFromFile(const std::string& filename)
        {
            std::lock_guard lock(fileLoaderMutex);

            if (sourceFiles.size() + sourceFilesToBeLoaded.size() - sourcesToBeRemoved.size() >= audioThread.getMaxNumSources())
            {
                deferredErrors.push_back("Failed to open " + filename + " (maximum number of sources exceeded)");
                return false;
            }

            for (const auto& source : sourceFiles)
            {
                if (source->getFilename() == filename)
                {
                    deferredErrors.push_back("Failed to open " + filename + " (already loaded)");
                    return false;
                }
            }

            for (const auto& source : sourceFilesToBeLoaded)
            {
                if (source == filename)
                {
                    deferredErrors.push_back("Failed to open " + filename + " (already loaded)");
                    return false;
                }
            }

            sourceFilesToBeLoaded.push_back(filename);

            return true;
        }

        bool removeSource(size_t index)
        {
            std::lock_guard lock(fileLoaderMutex);

            if (index >= sourceFiles.size())
            {
                deferredErrors.push_back("Can't remove non-existing source");
                return false;
            }

            sourcesToBeRemoved.push_back(index);

            return true;
        }

        void removeAllSources()
        {
            std::lock_guard lock(fileLoaderMutex);

            for ([[maybe_unused]] const auto& sourceFile : sourceFiles)
            {
                sourcesToBeRemoved.push_back(0);
            }
        }

        bool addFilterFromFile(const std::string& filename)
        {
            std::lock_guard lock(fileLoaderMutex);

            if (filterFiles.size() + filterFilesToBeLoaded.size() - filtersToBeRemoved.size() >= audioThread.getMaxNumFilters())
            {
                deferredErrors.push_back("Failed to open " + filename + " (maximum number of filters exceeded)");
                return false;
            }

            for (const auto& filter : filterFiles)
            {
                if (filter->getFilename() == filename)
                {
                    deferredErrors.push_back("Failed to open " + filename + " (already loaded)");
                    return false;
                }
            }

            for (const auto& filter : filterFilesToBeLoaded)
            {
                if (filter == filename)
                {
                    deferredErrors.push_back("Failed to open " + filename + " (already loaded)");
                    return false;
                }
            }

            filterFilesToBeLoaded.push_back(filename);

            return true;
        }

        bool removeFilter(size_t index)
        {
            std::lock_guard lock(fileLoaderMutex);

            if (index >= filterFiles.size())
            {
                deferredErrors.push_back("Can't remove non-existing filter");
                return false;
            }

            filtersToBeRemoved.push_back(index);

            return true;
        }

        void removeAllFilters()
        {
            std::lock_guard lock(fileLoaderMutex);

            for ([[maybe_unused]] const auto& filterFile : filterFiles)
            {
                filtersToBeRemoved.push_back(0);
            }
        }

        void addMasterFilterFromFile(const std::string& filename)
        {
            std::lock_guard lock(fileLoaderMutex);

            auto sampleRate = audioThread.getSampleRate();
            auto blockSize = audioThread.getBlockSize();

            auto file = std::make_unique<FDCF::File>(masterFilterFileLoader->loadFDCF(filename));

            if (!file->isValid())
            {
                deferredErrors.push_back("Failed to open " + file->getFilename() + "(" + *file->getErrorMsg() + ")");
                return;
            }

            masterFilterFile = std::move(file);

            if (audioThread.isRunning())
            {
                auto masterFilter = masterFilterFile->makeFilter(sampleRate, blockSize);

                if (!masterFilter)
                {
                    deferredErrors.push_back("Failed to open " + masterFilterFile->getFilename() + "(" + *masterFilterFile->getErrorMsg() + ")");
                    return;
                }

                audioThread.pushMasterFilter(std::move(masterFilter));
            }
        }

        void removeMasterFilter()
        {
            masterFilterFile.reset();

            if (audioThread.isRunning())
            {
                ASSUME(audioThread.pushDataCommand(ThreadCommunication::DataCommand::unloadMasterFilter()));
            }
        }

        const std::vector<State>& getSourceStates() const { return sourceStates; }
        const std::vector<State>& getFilterStates() const { return filterStates; }

        const std::vector<SourceInfo>& getAvailableSources() const { return availableSources; }
        const std::vector<FilterInfo>& getAvailableFilters() const { return availableFilters; }

        std::string getMasterFilterName() const { return masterFilterFile ? masterFilterFile->getFilename() : ""; }

    protected:
        template<typename SourceFileLoader, typename FilterFileLoader>
        FileInterface(ThreadCommunication::GuiThreadInterface& audioThread, AudioCallback::DebugOutput& debugOutput, std::in_place_type_t<SourceFileLoader>, std::in_place_type_t<FilterFileLoader>) :
            audioThread(audioThread),
            debugOutput(debugOutput),
            sourceFileLoader(std::make_unique<SourceFileLoader>()),
            filterFileLoader(std::make_unique<FilterFileLoader>()),
            masterFilterFileLoader(std::make_unique<FilterFileLoader>())
        {
            static_assert(std::is_base_of_v<AudioFile::LoaderBase, SourceFileLoader>);
            static_assert(std::is_base_of_v<HRTF::File::LoaderBase, FilterFileLoader>);
            static_assert(std::is_base_of_v<FDCF::File::LoaderBase, FilterFileLoader>);
        }

        void load()
        {
            std::lock_guard lock(fileLoaderMutex);

            for (size_t iSourceFile = 0; iSourceFile < sourceFiles.size(); ++iSourceFile)
            {
                sourcesToBeLoaded.push_back(iSourceFile);
            }

            for (size_t iFilterFile = 0; iFilterFile < filterFiles.size(); ++iFilterFile)
            {
                filtersToBeLoaded.push_back(iFilterFile);
            }

            updateAvailableSources();
            updateAvailableBuses();

            if (masterFilterFile)
            {
                auto masterFilter = masterFilterFile->makeFilter(audioThread.getSampleRate(), audioThread.getBlockSize());

                if (!masterFilter)
                {
                    deferredErrors.push_back("Failed to open " + masterFilterFile->getFilename() + "(" + *masterFilterFile->getErrorMsg() + ")");
                    return;
                }

                audioThread.pushMasterFilter(std::move(masterFilter));
            }
        }

        void unload()
        {
            std::lock_guard lock(fileLoaderMutex);

            if (loadingSource.valid())
            {
                loadingSource.wait();
                loadingSource.get();
            }

            if (loadingFilter.valid())
            {
                loadingFilter.wait();
                loadingFilter.get();
            }

            sourcesToBeLoaded.clear();
            filtersToBeLoaded.clear();

            for (auto& sourceState : sourceStates) { sourceState.loaded = false; }
            for (auto& filterState : filterStates) { filterState.loaded = false; }
        }

        void timer()
        {
            std::lock_guard lock(fileLoaderMutex);

            // Send errors
            for (const auto& error : deferredErrors)
            {
                sendError(error);
            }
            deferredErrors.clear();

            // Check for source file currently being loaded
            if (loadingSourceFile.valid())
            {
                if (loadingSourceFile.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
                {
                    if (addSourceFile(loadingSourceFile.get()) && audioThread.isRunning())
                    {
                        sourcesToBeLoaded.push_back(sourceFiles.size() - 1);
                    }

                    sourceFilesToBeLoaded.pop_front();
                }
            }

            // Check for source currently being loaded
            if (loadingSource.valid())
            {
                if (loadingSource.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
                {
                    loadSource(sourcesToBeLoaded.front(), loadingSource.get());
                    sourcesToBeLoaded.pop_front();
                }
            }

            if (!loadingSourceFile.valid() && !loadingSource.valid())
            {
                if (!sourcesToBeRemoved.empty())
                {
                    // Remove sources if requested

                    while (!sourcesToBeRemoved.empty())
                    {
                        size_t iSourceFile = sourcesToBeRemoved.front();

                        if (iSourceFile < sourceFiles.size())
                        {
                            unloadSource(iSourceFile);
                            removeSourceFile(iSourceFile);
                        }

                        sourcesToBeRemoved.pop_front();
                    }
                }

                if (!sourceFilesToBeLoaded.empty())
                {
                    // Start loading source file if requested

                    const std::string& filename = sourceFilesToBeLoaded.front();

                    loadingSourceFile = std::async(std::launch::async, [this, filename]()
                        {
                            return std::make_unique<AudioFile>(sourceFileLoader->load(filename, audioThread.getNumInputBusChannels() == 1));
                        });
                }
                else if (!sourcesToBeLoaded.empty())
                {
                    // Start loading source if requested

                    ASSERT(audioThread.isRunning());

                    size_t iSourceFile = sourcesToBeLoaded.front();

                    if (iSourceFile < sourceFiles.size())
                    {
                        ASSERT(!sourceStates[iSourceFile].loaded);

                        auto& sourceFile = *sourceFiles[iSourceFile];
                        auto sampleRate = audioThread.getSampleRate();

                        loadingSource = std::async(std::launch::async, [&sourceFile, sampleRate]()
                            {
                                return sourceFile.makeSource(sampleRate);
                            });
                    }
                    else
                    {
                        // File has been removed in the meanwhile

                        sourcesToBeLoaded.pop_front();
                    }
                }
            }

            // Check for filter file currently being loaded
            if (loadingFilterFile.valid())
            {
                if (loadingFilterFile.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
                {
                    if (addFilterFile(loadingFilterFile.get()) && audioThread.isRunning())
                    {
                        filtersToBeLoaded.push_back(filterFiles.size() - 1);
                    }

                    filterFilesToBeLoaded.pop_front();
                }
            }

            // Check for filter currently being loaded
            if (loadingFilter.valid())
            {
                if (loadingFilter.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready)
                {
                    loadFilter(filtersToBeLoaded.front(), loadingFilter.get());
                    filtersToBeLoaded.pop_front();
                }
            }

            if (!loadingFilterFile.valid() && !loadingFilter.valid())
            {
                if (!filtersToBeRemoved.empty())
                {
                    // Remove filters if requested

                    while (!filtersToBeRemoved.empty())
                    {
                        size_t iFilterFile = filtersToBeRemoved.front();

                        if (iFilterFile < filterFiles.size())
                        {
                            unloadFilter(iFilterFile);
                            removeFilterFile(iFilterFile);
                        }

                        filtersToBeRemoved.pop_front();
                    }
                }
                
                if (!filterFilesToBeLoaded.empty())
                {
                    // Start loading filter file if requested

                    const std::string& filename = filterFilesToBeLoaded.front();

                    loadingFilterFile = std::async(std::launch::async, [this, filename]()
                        {
                            return std::make_unique<HRTF::File>(filterFileLoader->loadHRTF(filename));
                        });
                }
                else if (!filtersToBeLoaded.empty())
                {
                    // Start loading filter if requested

                    ASSERT(audioThread.isRunning());

                    size_t iFilterFile = filtersToBeLoaded.front();

                    if (iFilterFile < filterFiles.size())
                    {
                        ASSERT(!filterStates[iFilterFile].loaded);

                        auto& filterFile = *filterFiles[iFilterFile];
                        auto sampleRate = audioThread.getSampleRate();
                        auto blockSize = audioThread.getBlockSize();

                        loadingFilter = std::async(std::launch::async, [&filterFile, sampleRate, blockSize]()
                            {
                                return filterFile.makeFilter(sampleRate, blockSize);
                            });
                    }
                    else
                    {
                        // File has been removed in the meanwhile

                        filtersToBeLoaded.pop_front();
                    }
                }
            }
        }

        std::function<void(const std::string&)> onError;

    private:
        ThreadCommunication::GuiThreadInterface& audioThread;
        AudioCallback::DebugOutput& debugOutput;

        std::unique_ptr<AudioFile::LoaderBase> sourceFileLoader;
        std::unique_ptr<HRTF::File::LoaderBase> filterFileLoader;

        std::mutex fileLoaderMutex;
        std::deque<std::string> sourceFilesToBeLoaded;
        std::deque<std::string> filterFilesToBeLoaded;
        std::future<std::unique_ptr<AudioFile>> loadingSourceFile;
        std::future<std::unique_ptr<HRTF::File>> loadingFilterFile;
        std::deque<size_t> sourcesToBeLoaded;
        std::deque<size_t> filtersToBeLoaded;
        std::future<std::unique_ptr<AudioSource>> loadingSource;
        std::future<std::unique_ptr<HRTF::Filter>> loadingFilter;
        std::deque<size_t> sourcesToBeRemoved;
        std::deque<size_t> filtersToBeRemoved;

        std::vector<std::unique_ptr<AudioFile>> sourceFiles;
        std::vector<std::unique_ptr<HRTF::File>> filterFiles;

        std::vector<State> sourceStates;
        std::vector<State> filterStates;

        std::vector<SourceInfo> availableSources;
        std::vector<FilterInfo> availableFilters;

        std::unique_ptr<FDCF::File::LoaderBase> masterFilterFileLoader;
        std::unique_ptr<FDCF::File> masterFilterFile;

        // Buffer errors from threads other than message thread
        std::vector<std::string> deferredErrors;

        void sendError(const std::string& error)
        {
            if (onError)
            {
                onError(error);
            }
        }

        bool addSourceFile(std::unique_ptr<AudioFile> sourceFile)
        {
            if (!sourceFile->isValid())
            {
                sendError("Failed to open " + sourceFile->getFilename() + " (" + *sourceFile->getErrorMsg() + ")");
                return false;
            }

            debugOutput.addLine("Source added: " + sourceFile->getFilename() + " - " + std::to_string(sourceFiles.size()));

            ASSERT(sourceStates.size() == sourceFiles.size());

            sourceStates.push_back({ sourceFile->getFilename() });
            if (!sourceFile->isTest())
            {
                sourceStates.back().size = std::to_string(sourceFile->getNumChannels()) + "x" + std::to_string(sourceFile->getNumSamples());
                sourceStates.back().sampleRate = std::to_string(sourceFile->getSampleRate());
            }
            sourceFiles.push_back(std::move(sourceFile));

            return true;
        }

        void removeSourceFile(size_t iSourceFile)
        {
            debugOutput.addLine("Source removed: " + sourceFiles[iSourceFile]->getFilename() + " - " + std::to_string(iSourceFile));

            ASSERT(sourceStates.size() == sourceFiles.size());

            sourceFiles.erase(sourceFiles.begin() + iSourceFile);
            sourceStates.erase(sourceStates.begin() + iSourceFile);
        }

        void loadSource(size_t iSourceFile, std::unique_ptr<AudioSource> source)
        {
            if (audioThread.isRunning())
            {
                auto& sourceFile = *sourceFiles[iSourceFile];
                auto& sourceState = sourceStates[iSourceFile];

                if (!source)
                {
                    sendError("Failed to load " + sourceFile.getFilename() + " (" + *sourceFile.getErrorMsg() + ")");
                    return;
                }

                if (!source->supports(audioThread.getNumInputBusChannels(), audioThread.getSampleRate(), audioThread.getBlockSize()))
                {
                    sendError("Failed to load " + sourceFile.getFilename() + " (settings do not match)");
                    return;
                }

                ASSUME(audioThread.pushSource(std::move(source)));
                sourceState.loaded = true;
                updateAvailableSources();
            }
        }

        void unloadSource(size_t iSourceFile)
        {
            if (sourceStates[iSourceFile].loaded)
            {
                size_t iLoaded = 0;

                for (size_t i = 0; i < iSourceFile; ++i)
                {
                    iLoaded += sourceStates[i].loaded ? 1 : 0;
                }

                ASSUME(audioThread.pushDataCommand(ThreadCommunication::DataCommand::unloadSource(iLoaded)));
                sourceStates[iSourceFile].loaded = false;
                updateAvailableSources();
            }
        }

        bool addFilterFile(std::unique_ptr<HRTF::File> filterFile)
        {
            if (!filterFile->isValid())
            {
                sendError("Failed to open " + filterFile->getFilename() + " (" + *filterFile->getErrorMsg() + ")");
                return false;
            }

            debugOutput.addLine("Filter added: " + filterFile->getFilename() + " - " + std::to_string(filterFiles.size()));

            ASSERT(filterStates.size() == filterFiles.size());

            filterStates.push_back({ filterFile->getFilename() });
            if (!filterFile->isTest())
            {
                filterStates.back().size = std::to_string(filterFile->getNumMeasurements()) + "x" + std::to_string(filterFile->getNumEmitters()) + "x" + std::to_string(filterFile->getNumReceivers()) + "x" + std::to_string(filterFile->getNumSamples());
                filterStates.back().sampleRate = std::to_string(filterFile->getSampleRate());
            }
            filterFiles.push_back(std::move(filterFile));

            return true;
        }

        void removeFilterFile(size_t iFilterFile)
        {
            debugOutput.addLine("Filter removed: " + filterFiles[iFilterFile]->getFilename() + " - " + std::to_string(iFilterFile));

            ASSERT(filterStates.size() == filterFiles.size());

            filterFiles.erase(filterFiles.begin() + iFilterFile);
            filterStates.erase(filterStates.begin() + iFilterFile);
        }

        void loadFilter(size_t iFilterFile, std::unique_ptr<HRTF::Filter>&& filter)
        {
            if (audioThread.isRunning())
            {
                auto& filterFile = *filterFiles[iFilterFile];
                auto& filterState = filterStates[iFilterFile];

                if (!filter)
                {
                    sendError("Failed to load " + filterFile.getFilename() + " (" + *filterFile.getErrorMsg() + ")");
                    return;
                }

                if (!filter->supports(audioThread.getNumInputBusChannels(), audioThread.getNumOutputBusChannels(), audioThread.getSampleRate(), audioThread.getBlockSize()))
                {
                    sendError("Failed to load " + filterFile.getFilename() + " (settings do not match)");
                    return;
                }

                ASSUME(audioThread.pushFilter(std::move(filter)));
                filterState.loaded = true;
                updateAvailableBuses();
            }
        }

        void unloadFilter(size_t iFilterFile)
        {
            if (filterStates[iFilterFile].loaded)
            {
                size_t iLoaded = 0;

                for (size_t i = 0; i < iFilterFile; ++i)
                {
                    iLoaded += filterStates[i].loaded ? 1 : 0;
                }

                ASSUME(audioThread.pushDataCommand(ThreadCommunication::DataCommand::unloadFilter(iLoaded)));
                filterStates[iFilterFile].loaded = false;
                updateAvailableBuses();
            }
        }

        void updateAvailableSources()
        {
            availableSources.clear();

            for (size_t i = 0; i < sourceFiles.size(); ++i)
            {
                auto& state = sourceStates[i];
                auto& file = *sourceFiles[i];

                if (state.loaded)
                {
                    if (!file.isTest())
                    {
                        availableSources.push_back({ file.getFilename(), file.getNumSamples() });
                    }
                    else
                    {
                        availableSources.push_back({ file.getFilename() });
                    }
                }
            }
        }

        void updateAvailableBuses()
        {
            availableFilters.clear();

            for (size_t i = 0; i < filterFiles.size(); ++i)
            {
                auto& state = filterStates[i];
                auto& file = *filterFiles[i];

                if (state.loaded)
                {
                    availableFilters.push_back({ file.getFilename() });
                }
            }
        }
    };
}
