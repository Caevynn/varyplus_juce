/*
  ==============================================================================

    MainInterface.hpp
    Created: 7 Nov 2023 1:53:13pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "FileInterface.hpp"
#include "RemoteInterface.hpp"
#include "StateInterface.hpp"

namespace Shrimp
{
    class MainInterface :
        public FileInterface,
        public StateInterface,
        public RemoteInterface
    {
    public:
        template<typename SourceFileLoader, typename FilterFileLoader, typename OSCImplementation>
        MainInterface(ThreadCommunication::GuiThreadInterface& guiThreadInterface, ThreadCommunication::RemoteThreadInterface& remoteThreadInterface, AudioCallback::DebugOutput& debugOutput, std::in_place_type_t<SourceFileLoader>, std::in_place_type_t<FilterFileLoader>, std::in_place_type_t<OSCImplementation>) :
            FileInterface(guiThreadInterface, debugOutput, std::in_place_type<SourceFileLoader>, std::in_place_type<FilterFileLoader>),
            StateInterface(guiThreadInterface, debugOutput),
            RemoteInterface(remoteThreadInterface, debugOutput, *static_cast<FileInterface*>(this), std::in_place_type<OSCImplementation>)
        {
            FileInterface::onError = [this](const std::string& error)
            {
                if (onError) { onError(error); }
            };
        }

        void load()
        {
            FileInterface::load();
            if (auto error = RemoteInterface::checkOSCConnection())
            {
                if (onError) { onError(*error); }
            }
            loadPreviouslyLoadedState();
        }

        void unload()
        {
            FileInterface::unload();
        }

        void timer()
        {
            FileInterface::timer();
        }

        std::function<void(const std::string&)> onError;

    private:
        void loadPreviouslyLoadedState()
        {
            size_t iPlayer{ 0 };
            for (const auto& state : StateInterface::getPlayerStates())
            {
                if (state.source)
                {
                    RemoteInterface::sendPlayerCommand({ iPlayer, AudioPlayer::Command::Source(*state.source) });
                }

                if (state.bus)
                {
                    RemoteInterface::sendPlayerCommand({ iPlayer, AudioPlayer::Command::Bus(*state.bus) });
                }

                RemoteInterface::sendPlayerCommand({ iPlayer++, AudioPlayer::Command::Volume(state.volume) });
            }

            RemoteInterface::setMasterVolume(StateInterface::getMasterVolume());
        }
    };
}
