/*
  ==============================================================================

    AudioProcessor.hpp
    Created: 25 Oct 2023 3:09:17pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "SourceProcessor.hpp"
#include "FilterProcessor.hpp"
#include "ThreadCommunication.hpp"
#include "MainInterface.hpp"
#include "../../AudioProcessor/AudioCallbackBase.hpp"
#include "../../AudioProcessor/VolumeFilter.hpp"
#include "../../AudioProcessor/LimitingFilter.hpp"
#include "../../AudioProcessor/FrequencyDomainConvolution/FDCFFilter.hpp"

namespace Shrimp
{
    template<typename CallbackImplementation, typename OSCImplementation, typename SourceFileLoader, typename FilterFileLoader>
    class MainProcessor :
        public CallbackImplementation
    {
        static_assert(std::is_base_of_v<AudioCallback::Base, CallbackImplementation>);
        static_assert(std::is_base_of_v<OSC::ReaderBase, typename OSCImplementation::Reader>);
        static_assert(std::is_base_of_v<OSC::WriterBase, typename OSCImplementation::Writer>);
        static_assert(std::is_base_of_v<AudioFile::LoaderBase, SourceFileLoader>);
        static_assert(std::is_base_of_v<HRTF::File::LoaderBase, FilterFileLoader>);

        static constexpr size_t NumPlayers = 8;
        static constexpr size_t MaxNumSources = 8;
        static constexpr size_t MaxNumFilters = 8;
        static constexpr size_t NumOutputBusChannels = 2;

        unsigned NumInputBusChannels = 1;

    public:
        MainProcessor(AudioCallback::DebugOutput& debugOutput) :
            CallbackImplementation(debugOutput),
            threadCommunication(NumPlayers, MaxNumSources, MaxNumFilters, NumInputBusChannels, NumOutputBusChannels),
            mainInterface(threadCommunication.getGuiThreadInterface(), threadCommunication.getRemoteThreadInterface(), debugOutput, std::in_place_type<SourceFileLoader>, std::in_place_type<FilterFileLoader>, std::in_place_type<OSCImplementation>),
            guiThread(threadCommunication.getAudioThreadInterface()),
            sourceProcessor(threadCommunication.getAudioThreadInterface(), NumPlayers),
            filterProcessor(threadCommunication.getAudioThreadInterface())
        {
            mainInterface.onError = [this](const std::string& error)
            {
                if (CallbackImplementation::onError) { CallbackImplementation::onError(error); }
            };

            volumeFilter.allocate();
        }

        ~MainProcessor()
        {
            volumeFilter.deallocate();
        }

        void setInputBusChannels(unsigned wantedNumInputBusChannels)
        {
            NumInputBusChannels = wantedNumInputBusChannels;
            threadCommunication.setNumInputBusChannels(wantedNumInputBusChannels);
        }

        MainInterface& getMainInterface() { return mainInterface; }

    private:
        ThreadCommunication threadCommunication;
        MainInterface mainInterface;

        ThreadCommunication::AudioThreadInterface& guiThread;

        SourceProcessor sourceProcessor;
        FilterProcessor filterProcessor;

        RTC::UniquePointer<FDCF::Filter> masterFilter;
        RTC::UniquePointer<VolumeFilter> volumeFilter;
        RTC::UniquePointer<LimitingFilter> limitingFilter;

        AudioSignalBuses inputBuses;
        AudioSignalBuses outputBuses;

        void process(const AudioSignalWithChannels& /*input*/, AudioSignalWithChannels& output) override
        {
            checkSourceAndFilterBuffers();

            double masterVolume;
            ASSUME(guiThread.popMasterVolume(masterVolume));

            inputBuses.setToZero();

            sourceProcessor.run(inputBuses);
            filterProcessor.run(inputBuses, outputBuses);

            for(auto it = outputBuses.begin(); it != outputBuses.begin() + filterProcessor.getNumFilters(); ++it)
            {
                ASSUME(output.range(0, std::min(output.nChannels(), NumOutputBusChannels)).add(*it));
            }

            if (output.nChannels() > NumOutputBusChannels)
            {
                auto bus = inputBuses.begin() + MaxNumFilters;
                auto outputChannel = output.begin() + NumOutputBusChannels;

                for(; outputChannel != output.end(); ++outputChannel, ++bus)
                {
                    ASSUME(outputChannel->copy(bus->channel(0)));
                }
            }

            if (masterFilter)
            {
                auto masterSignal = output.range(0, 2);
                masterFilter->run(masterSignal, masterSignal);
            }

            volumeFilter->run(output, masterVolume);
            limitingFilter->run(output);

            guiThread.tryPushOutputStates(limitingFilter->getStates());
            guiThread.tryPushMasterVolume(masterVolume);
        }

        void load() override
        {
            unsigned numInputChannels = CallbackImplementation::getNumInputChannels();
            unsigned numOutputChannels = CallbackImplementation::getNumOutputChannels();
            unsigned sampleRate = CallbackImplementation::getSampleRate();
            unsigned blockSize = CallbackImplementation::getBlockSize();

            size_t nDirectOuts = NumInputBusChannels == 1 ? (numOutputChannels > 2 ? numOutputChannels - 2 : 0) : 0;

            sourceProcessor.allocate(sampleRate, blockSize, NumInputBusChannels, MaxNumSources);
            filterProcessor.allocate(MaxNumFilters);

            limitingFilter.allocate(numOutputChannels, sampleRate);

            inputBuses.allocate(RTC::Size()
                .with<Buses>(MaxNumFilters + nDirectOuts)
                .with<Channels>(NumInputBusChannels)
                .with<Samples>(blockSize));
            outputBuses.allocate(RTC::Size()
                .with<Buses>(MaxNumFilters)
                .with<Channels>(NumOutputBusChannels)
                .with<Samples>(blockSize));

            threadCommunication.load(numInputChannels, numOutputChannels, sampleRate, blockSize);
            mainInterface.load();
        }

        void unload() override
        {
            sourceProcessor.deallocate();
            filterProcessor.deallocate();

            masterFilter.deallocate();
            limitingFilter.deallocate();

            inputBuses.deallocate();
            outputBuses.deallocate();

            threadCommunication.unload();
            mainInterface.unload();
        }

        void timer() override
        {
            threadCommunication.timer();
            mainInterface.timer();
        }

        void checkSourceAndFilterBuffers()
        {
            using DataCommand = ThreadCommunication::DataCommand;

            for (DataCommand dataCommand; guiThread.popDataCommand(dataCommand);)
            {
                switch (dataCommand.getType())
                {
                case DataCommand::Type::Default:
                    ASSERT(false);
                    break;

                case DataCommand::Type::LoadSource:
                    ASSERT(dataCommand.hasSource());
                    sourceProcessor.addSource(dataCommand.getSource());
                    break;

                case DataCommand::Type::UnloadSource:
                    ASSERT(dataCommand.hasIndex());
                    guiThread.pushForDeletion(sourceProcessor.removeSource(dataCommand.getIndex()));
                    sourceProcessor.notifySourceRemoved(dataCommand.getIndex());
                    break;

                case DataCommand::Type::LoadFilter:
                    ASSERT(dataCommand.hasFilter());
                    filterProcessor.addFilter(dataCommand.getFilter());
                    break;

                case DataCommand::Type::UnloadFilter:
                    ASSERT(dataCommand.hasIndex());
                    guiThread.pushForDeletion(filterProcessor.removeFilter(dataCommand.getIndex()));
                    sourceProcessor.notifyFilterRemoved(dataCommand.getIndex(), MaxNumFilters);
                    break;

                case DataCommand::Type::LoadMasterFilter:
                    ASSERT(dataCommand.hasMasterFilter());
                    if (masterFilter)
                    {
                        guiThread.pushForDeletion(std::move(masterFilter));
                    }
                    masterFilter = dataCommand.getMasterFilter();
                    break;

                case DataCommand::Type::UnloadMasterFilter:
                    ASSERT(!dataCommand);
                    if (masterFilter)
                    {
                        guiThread.pushForDeletion(std::move(masterFilter));
                    }
                    break;

                default:
                    ASSERT(false);
                    break;
                }
            }
        }
    };
}
