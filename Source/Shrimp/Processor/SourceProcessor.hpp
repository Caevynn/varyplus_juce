/*
  ==============================================================================

    SourceHandler.hpp
    Created: 25 Oct 2023 2:53:42pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "ThreadCommunication.hpp"
#include "../../AudioProcessor/AudioPlayer.hpp"
#include "../../AudioProcessor/Utils/Assert.hpp"

namespace Shrimp
{
    class SourceProcessor
    {
    public:
        SourceProcessor(ThreadCommunication::AudioThreadInterface& guiThread, size_t numPlayers) :
            guiThread(guiThread)
        {
            audioPlayers.allocate(numPlayers);
        }

        ~SourceProcessor()
        {
            audioPlayers.deallocate();
        }

        void run(AudioSignalBuses& buses)
        {
            checkPlayerCommandBuffer();

            size_t iPlayer{ 0 };
            for (auto& audioPlayer : audioPlayers)
            {
                guiThread.tryPushPlayerState(iPlayer++, audioPlayer.read(sources, buses));
            }
        }

        void addSource(RTC::UniquePointer<AudioSource>&& newSource)
        {
            ASSUME(sources.push_back(std::move(newSource)));
        }

        [[nodiscard]] RTC::UniquePointer<AudioSource> removeSource(size_t index)
        {
            RTC::UniquePointer<AudioSource> removedSource;
            ASSUME(sources.pop(index, removedSource));
            return removedSource;
        }

        void notifySourceRemoved(size_t index)
        {
            for (auto& audioPlayer : audioPlayers)
            {
                audioPlayer.notifySourceRemoved(index);
            }
        }

        void notifyFilterRemoved(size_t index, size_t maxNumFilters)
        {
            for (auto& audioPlayer : audioPlayers)
            {
                audioPlayer.notifyBusRemoved(index, maxNumFilters);
            }
        }

        void allocate(unsigned sampleRate, size_t blockSize, size_t nInputChannels, size_t maxNumSources)
        {
            for (auto& audioPlayer : audioPlayers)
            {
                audioPlayer.allocate(nInputChannels, sampleRate, blockSize);
            }

            sources.allocate(maxNumSources);
        }

        void deallocate()
        {
            for (auto& audioPlayer : audioPlayers)
            {
                audioPlayer.deallocate();
            }

            sources.deallocate();
        }

    private:
        ThreadCommunication::AudioThreadInterface& guiThread;

        RTC::Vector<RTC::UniquePointer<AudioSource>> sources;
        RTC::Array<AudioPlayer> audioPlayers;

        void checkPlayerCommandBuffer()
        {
            for (AudioPlayer::Command command; guiThread.popPlayerCommand(command);)
            {
                if (!command.iChannel)
                {
                    for (auto& player : audioPlayers)
                    {
                        player.applyCommand(command);
                    }
                }
                else if (command.iChannel < audioPlayers.size())
                {
                    audioPlayers[*command.iChannel].applyCommand(command);
                }
            }
        }
    };
}
