/*
  ==============================================================================

    PlotDebugOutput.hpp
    Created: 19 Oct 2023 4:48:59pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "DebugOutput.hpp"

#include "../Components/PlotWithStatistics.hpp"

class PlotDebugOutput : public juce::Component, public DebugOutput
{
public:
    PlotDebugOutput() :
        durationPlot("Duration [us]"),
        distancePlot("Distance [us]")
    {
        addAndMakeVisible(durationPlot);
        addAndMakeVisible(distancePlot);
    }

    void resetMeasurement() override
    {
        durationPlot.reset();
        distancePlot.reset();
        lastMeasurement.reset();
    }

    void addMeasurement(const Time::Measurement& measurement) override
    {
        durationPlot.push(static_cast<double>(measurement.getDuration()) * 1000000);

        if (lastMeasurement)
        {
            distancePlot.push(static_cast<double>(measurement.getDistance(*lastMeasurement)) * 1000000);
        }

        lastMeasurement = measurement;
    }

    void invalidateLastMeasurement() override
    {
        lastMeasurement.reset();
    }

private:
    PlotWithStatistics<double> durationPlot;
    PlotWithStatistics<double> distancePlot;

    std::optional<Time::Measurement> lastMeasurement;

    void resized() override
    {
        auto area = getLocalBounds();

        durationPlot.setBounds(area.removeFromTop(getHeight() / 2));
        distancePlot.setBounds(area);
    }

    void paint(juce::Graphics& g) override
    {
        g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));
    }
};
