/*
  ==============================================================================

    DebugOutput.hpp
    Created: 16 Oct 2023 1:31:30pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "DebugOutput.hpp"

class TextDebugOutput : public juce::Component, public DebugOutput
{
public:
    TextDebugOutput()
    {
        programStart = Time::now();

        addAndMakeVisible(text);
        text.setMultiLine(true);
        text.setReturnKeyStartsNewLine(true);
        text.setReadOnly(true);
        text.setScrollbarsShown(true);
        text.setCaretVisible(false);
        text.setPopupMenuEnabled(true);
        text.setScrollToShowCursor(true);

        addAndMakeVisible(toggle);
        toggle.setTitle("Follow new lines");
        toggle.setButtonText("Follow");
        toggle.setToggleState(true, juce::NotificationType::dontSendNotification);
        toggle.onClick = [this]() { text.setScrollToShowCursor(toggle.getToggleState()); };

        addLine("Welcome!");
    }

    void addError(const std::string& error) override
    {
        juce::MessageManagerLock mml;

        text.setColour(juce::TextEditor::textColourId, juce::Colours::red);
        addLine(error);
        text.setColour(juce::TextEditor::textColourId, getLookAndFeel().findColour(juce::TextEditor::textColourId));
    }

    void addLine(const std::string& line) override
    {
        juce::MessageManagerLock mml;

        Time::DurationT timeSinceStart = Time::duration(programStart, Time::now());
        unsigned seconds = static_cast<unsigned>(timeSinceStart);
        unsigned microseconds = static_cast<unsigned>((timeSinceStart - seconds) * 1000000);

        std::string secondsString = std::to_string(seconds);
        std::string microsecondsString = std::to_string(microseconds);
        std::string leadingZerosString = std::string(6 - microsecondsString.size(), '0');
        std::string timeString = "[" + secondsString + "." + leadingZerosString + microsecondsString + "]";

        text.moveCaretToEnd();
        text.insertTextAtCaret(" " + timeString + " " + line + "\n");
    }

private:
    juce::TextEditor text;
    juce::ToggleButton toggle;

    Time::TimePointT programStart;

    void resized() override
    {
        auto area = getLocalBounds();
        toggle.setBounds(area.removeFromBottom(24).reduced(3));
        text.setBounds(area.reduced(3));
    }

    void paint(juce::Graphics& g) override
    {
        g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));
    }
};
