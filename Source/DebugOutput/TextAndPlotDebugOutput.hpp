/*
  ==============================================================================

    TextAndPlotDebugOutput.hpp
    Created: 19 Oct 2023 4:13:42pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "TextDebugOutput.hpp"
#include "PlotDebugOutput.hpp"

class TextAndPlotDebugOutput : public juce::Component, public DebugOutput
{
public:
    TextAndPlotDebugOutput()
    {
        addAndMakeVisible(text);
        addAndMakeVisible(plot);
    }
    void addError(const std::string& error) override
    {
        text.addError(error);
    }

    void addLine(const std::string& line) override
    {
        text.addLine(line);
    }

    void resetMeasurement() override
    {
        plot.resetMeasurement();
    }

    void addMeasurement(const Time::Measurement& measurement) override
    {
        plot.addMeasurement(measurement);
    }

    void invalidateLastMeasurement() override
    {
        plot.invalidateLastMeasurement();
    }

private:
    PlotDebugOutput plot;
    TextDebugOutput text;

    std::optional<Time::Measurement> lastMeasurement;

    void resized() override
    {
        auto area = getLocalBounds();

        text.setBounds(area.removeFromTop(getHeight() / 2));
        plot.setBounds(area);
    }
};
