/*
  ==============================================================================

    DebugOutput.hpp
    Created: 19 Oct 2023 3:13:51pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioProcessor/AudioCallbackTypes.hpp"

using DebugOutput = AudioCallback::DebugOutput;
