/*
  ==============================================================================

    IsPowerOfTwo.hpp
    Created: 23 Oct 2023 5:28:40pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

constexpr auto IsPowerOfTwo = [](size_t n)
{
    return n != 0 && (n & (n - 1)) == 0;
};
