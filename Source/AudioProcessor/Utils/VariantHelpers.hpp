/*
  ==============================================================================

    VariantHelpers.hpp
    Created: 15 Dec 2023 11:38:59pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

namespace VariantHelpers
{
    template<class> inline constexpr bool always_false_v = false;
    template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
    template<class... Ts> overloaded(Ts...)->overloaded<Ts...>;
}
