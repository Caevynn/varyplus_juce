/*
  ==============================================================================

    RTCTemplateHelpers.hpp
    Created: 1 Nov 2023 5:01:51pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <complex>
#include <memory>
#include <utility>

namespace RTC
{
    template <template <typename...> typename T, typename P> struct is_specialization_of : std::false_type {};
    template <template <typename...> typename T, typename ...P> struct is_specialization_of<T, T<P...>> : std::true_type {};
    template <template <typename...> typename T, typename ...P> struct is_specialization_of<T, const T<P...>> : std::true_type {};
    template<template <typename...> typename T, typename P> inline constexpr bool is_specialization_of_v = is_specialization_of<T, P>::value;
    
    template<typename T> inline constexpr bool is_arithmetic_v = std::is_arithmetic_v<T> || is_specialization_of_v<std::complex, T>;
    
    template<typename T, typename R> struct is_compatible { static constexpr bool value = R::isCompatible(std::in_place_type<T>); };
    template<typename T, typename R> inline constexpr bool is_compatible_v = is_compatible<T, R>::value;

    template<typename T, typename = void> struct is_deallocatable : std::false_type {};
    template<typename T> struct is_deallocatable<T, decltype((void)&T::deallocate, void())> : std::true_type {};
    template<typename T> inline constexpr bool is_deallocatable_v = is_deallocatable<T>::value;

    template<typename T, typename = void> struct is_zero_settable : std::false_type {};
    template<typename T> struct is_zero_settable<T, decltype((void)&T::setToZero, void())> : std::true_type {};
    template<typename T> inline constexpr bool is_zero_settable_v = is_zero_settable<T>::value;
}

#define RTC_ENABLE_IF(Condition)\
template<bool Bool = (Condition), std::enable_if_t<\
    Bool == (Condition) && Bool, bool> = true>

#define RTC_ENABLE_IF_COMPATIBLE(OtherRange, Reference)\
template<typename OtherRange, std::enable_if_t<\
    RTC::is_compatible_v<OtherRange, Reference>, bool> = true>

#define RTC_ENABLE_IF_COMPATIBLE_2(OtherRange1, OtherRange2, Reference)\
template<typename OtherRange1, typename OtherRange2, std::enable_if_t<\
    RTC::is_compatible_v<OtherRange1, Reference> &&\
    RTC::is_compatible_v<OtherRange2, Reference>, bool> = true>

#define RTC_ENABLE_IF_CONVERTIBLE(From, To) \
template<typename From, std::enable_if_t< \
    std::is_convertible_v<From, To>, bool> = true>

#define RTC_ENABLE_IF_CONVERTIBLE_2(From1, From2, To) \
template<typename From1, typename From2, std::enable_if_t< \
    std::is_convertible_v<From1, To> && \
    std::is_convertible_v<From2, To>, bool> = true>

#define RTC_ENABLE_IF_ASSIGNABLE_AND_CONVERTIBLE_2(From1, From2, To) \
template<typename From1, typename From2, std::enable_if_t< \
    std::is_assignable_v<From1, From2> && \
    std::is_convertible_v<From1, To> && \
    std::is_convertible_v<From2, To>, bool> = true>

#define RTC_FUNCTION_ALIAS_CONST(Alias, Function) \
template <typename... FArgs> decltype(auto) Alias(FArgs &&... args) const { return this->Function(std::forward<FArgs>(args)...); }

#define RTC_FUNCTION_ALIAS_NON_CONST(Alias, Function) \
template <typename... FArgs> decltype(auto) Alias(FArgs &&... args) { return this->Function(std::forward<FArgs>(args)...); }

#define RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(NEW_NAME, ...) \
RTC_FUNCTION_ALIAS_CONST(NEW_NAME, __VA_ARGS__) RTC_FUNCTION_ALIAS_NON_CONST(NEW_NAME, __VA_ARGS__)

#ifndef NDEBUG
#	define RTC_DEBUG_RETURN_FALSE_IF(condition) if (condition) return false
#	define RTC_DEBUG_RETURN_TRUE return true
#	define RTC_DEBUG_BOOL [[nodiscard("Use RTC_DEBUG_ASSUME macro to check return value")]] bool
#	define RTC_DEBUG_ASSUME(expression) assert(expression == true)
#else
#	define RTC_DEBUG_RETURN_FALSE_IF(condition) (void)0
#	define RTC_DEBUG_RETURN_TRUE (void)0
#	define RTC_DEBUG_BOOL void
#	define RTC_DEBUG_ASSUME(expression) expression
#endif

#define RTC_USE_CRTP(DerivedClass) \
auto& derived() { return *static_cast<DerivedClass*>(this); } \
const auto& derived() const { return *static_cast<const DerivedClass*>(this); }
