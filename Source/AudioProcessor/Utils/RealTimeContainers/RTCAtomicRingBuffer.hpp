/*
  ==============================================================================

    AtomicRingBuffer.hpp
    Created: 16 Oct 2023 12:58:23pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <atomic>
#include <vector>
#include <cassert>

#include "../RealTimeContainers/RTCAllocator.hpp"

namespace RTC
{
    template<typename ElementType, size_t Size = 16>
    class AtomicRingBuffer
    {
        static_assert(std::atomic<size_t>::is_always_lock_free, "std::atomic<size_t> is not lock free.");

    public:
        AtomicRingBuffer()
            : writePtr(0), readPtr(0), overflowCnt(0)
        {
            data.allocate(Size);
        }

        AtomicRingBuffer(AtomicRingBuffer&) = delete;
        AtomicRingBuffer(AtomicRingBuffer&&) = delete;

        ~AtomicRingBuffer()
        {
            if constexpr (!std::is_trivially_destructible_v<ElementType>)
            {
                clearWithDeallocation();
            }

            data.deallocate();
        }

        bool push(std::conditional_t<std::is_trivially_copyable_v<ElementType>, const ElementType&, ElementType&&> element)
        {
            size_t currentRead = readPtr.load(std::memory_order_acquire);
            size_t currentWrite = writePtr.load(std::memory_order_relaxed);
            size_t nextWrite = (currentWrite + 1) % Size;

            if (nextWrite == currentRead)
            {
                overflowCnt.fetch_add(1, std::memory_order_relaxed);
                return false;
            }

            data.construct(currentWrite, std::move(element));

            writePtr.store(nextWrite, std::memory_order_release);

            return true;
        }

        bool pop(ElementType& element)
        {
            size_t currentWrite = writePtr.load(std::memory_order_acquire);
            size_t currentRead = readPtr.load(std::memory_order_relaxed);
            size_t nextRead = (currentRead + 1) % Size;

            if (currentRead == currentWrite)
            {
                return false;
            }

            element = std::move(data[currentRead]);

            data.destroy(currentRead);

            readPtr.store(nextRead, std::memory_order_release);

            return true;
        }

        template<typename Type = ElementType, std::enable_if_t<
            std::is_trivially_copyable_v<Type>, bool> = true>
        bool popLatest(ElementType& element)
        {
            size_t currentWrite = writePtr.load(std::memory_order_acquire);
            size_t currentRead = readPtr.load(std::memory_order_relaxed);
            size_t lastWrite = (currentWrite + Size - 1) % Size;

            if (currentRead == currentWrite)
            {
                return false;
            }

            element = data[lastWrite];

            readPtr.store(lastWrite, std::memory_order_release);

            return true;
        }

        template<typename Container, std::enable_if_t<
            std::is_same_v<typename Container::value_type, ElementType> &&
            std::is_trivially_copyable_v<typename Container::value_type>, bool> = true>
        size_t push(const Container& container)
        {
            size_t currentRead = readPtr.load(std::memory_order_acquire);
            size_t currentWrite = writePtr.load(std::memory_order_relaxed);
            size_t nextWrite = (currentWrite + 1) % Size;

            size_t availableSize = (currentRead + Size - nextWrite) % Size;
            size_t elementsToCopy = std::min(availableSize, container.size());

            for (size_t i = 0; i < elementsToCopy; ++i)
            {
                data[currentWrite] = container[i];
                currentWrite = (currentWrite + 1) % Size;
            }

            writePtr.store(currentWrite, std::memory_order_release);

            return elementsToCopy;
        }

        template<typename Container, std::enable_if_t<
            std::is_same_v<typename Container::value_type, ElementType> &&
            std::is_trivially_copyable_v<typename Container::value_type>, bool> = true>
        size_t pop(Container& container)
        {
            size_t currentWrite = writePtr.load(std::memory_order_acquire);
            size_t currentRead = readPtr.load(std::memory_order_relaxed);

            size_t availableSize = (currentWrite + Size - currentRead) % Size;
            size_t elementsToCopy = std::min(availableSize, container.size());

            for (size_t i = 0; i < elementsToCopy; ++i)
            {
                container[i] = data[currentRead];
                currentRead = (currentRead + 1) % Size;
            }

            readPtr.store(currentRead, std::memory_order_release);

            return elementsToCopy;
        }

        void clearWithDeallocation()
        {
            size_t currentWrite = writePtr.load(std::memory_order_acquire);
            size_t currentRead = readPtr.load(std::memory_order_relaxed);

            size_t elementsToDestroy = (currentWrite + Size - currentRead) % Size;

            for (size_t i = 0; i < elementsToDestroy; ++i)
            {
                if constexpr (is_deallocatable_v<ElementType>)
                {
                    data[i].deallocate();
                }

                data.destroy(currentRead);

                currentRead = (currentRead + 1) % Size;
            }

            readPtr.store(currentRead, std::memory_order_release);
        }

        size_t getResetOverflowCount()
        {
            return overflowCnt.exchange(0, std::memory_order_relaxed);
        }

    private:
        RTC::Allocator<ElementType> data;

        std::atomic<size_t> writePtr;
        std::atomic<size_t> readPtr;
        std::atomic<size_t> overflowCnt;
    };
}
