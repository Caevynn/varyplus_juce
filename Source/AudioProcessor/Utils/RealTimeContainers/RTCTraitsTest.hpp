/*
  ==============================================================================

    RTCTraitsTest.hpp
    Created: 26 Dec 2023 12:24:21pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCElementTraits.hpp"
#include "RTCArithmeticTraits.hpp"
#include "RTCComplexTraits.hpp"

namespace RTC
{
    class TraitsTest
    {
        struct TestStruct {};

    public:
        static void run()
        {
            float* pf = test<float>();
            ComplexPointer<float> pc = test<Complex<float>>();
            ComplexReference<float> rc = *test<Complex<float>>();
            TestStruct* pe = test<TestStruct>();
        }

        template<typename ElementType>
        static typename ElementTraits<ElementType>::pointer test()
        {
            using Traits = ElementTraits<ElementType>;

            Traits::allocator allocator;

            allocator.allocate(64);

            Traits::pointer pointer = allocator.pointer();

            allocator.deallocate();

            return pointer;
        }
    };
}
