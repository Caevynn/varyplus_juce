/*
  ==============================================================================

    RTCArrayHelpers.hpp
    Created: 1 Nov 2023 5:02:04pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <array>
#include <cstring>

namespace RTC
{
    template<size_t N, typename T>
    void init(std::array<T, N>& array)
    {
        std::memset(array.data(), 0, N * sizeof(T));
    }

    template<size_t N, typename T>
    void copy(const std::array<T, N>& source, std::array<T, N>& destination)
    {
        std::memcpy(destination.data(), source.data(), N * sizeof(T));
    }

    template<size_t Start, size_t End, size_t NSource, size_t NDestination, typename T>
    void copyPartial(const std::array<T, NSource>& source, std::array<T, NDestination>& destination)
    {
        static_assert(End >= Start);
        static_assert(NSource >= End);
        static_assert(NDestination >= End - Start);

        std::memcpy(destination.data(), source.data() + Start, (End - Start) * sizeof(T));
    }

    template<size_t N, typename T>
    T sum_recursive([[maybe_unused]] const T* array)
    {
        if constexpr (N == 0) { return 0; }
        else if constexpr (N == 1) { return array[0]; }
        else { return array[0] + sum_recursive<N - 1>(array + 1); }
    }

    template<size_t N, typename T>
    T sum(const std::array<T, N>& array)
    {
        return sum_recursive<N>(array.data());
    }

    template<size_t Start, size_t End, size_t N, typename T>
    T sumPartial(const std::array<T, N>& array)
    {
        static_assert(End >= Start);
        static_assert(N >= End);

        return sum_recursive<End - Start>(array.data() + Start);
    }

    template<size_t N, typename T>
    T product_recursive([[maybe_unused]] const T* array)
    {
        if constexpr (N == 0) { return 1; }
        else if constexpr (N == 1) { return array[0]; }
        else { return array[0] * product_recursive<N - 1>(array + 1); }
    }

    template<size_t N, typename T>
    T product(const std::array<T, N>& array)
    {
        return product_recursive<N>(array.data());
    }

    template<size_t Start, size_t End, size_t N, typename T>
    T productPartial(const std::array<T, N>& array)
    {
        static_assert(End >= Start);
        static_assert(N >= End);

        return product_recursive<End - Start>(array.data() + Start);
    }

    template<size_t N, typename T>
    bool same_recursive(const T* array1, const T* array2)
    {
        if constexpr (N == 1) { return array1[0] == array2[0]; }
        else { return array1[0] == array2[0] && same_recursive<N - 1>(array1 + 1, array2 + 1); }
    }

    template<size_t N, typename T>
    bool same(const std::array<T, N>& array1, const std::array<T, N>& array2)
    {
        return same_recursive<N>(array1.data(), array2.data());
    }

    template<size_t Start, size_t End, size_t N, typename T>
    bool samePartial(const std::array<T, N>& array1, const std::array<T, N>& array2)
    {
        static_assert(End >= Start);
        static_assert(N >= End);

        return same_recursive<End - Start>(array1.data() + Start, array2.data() + Start);
    }
}
