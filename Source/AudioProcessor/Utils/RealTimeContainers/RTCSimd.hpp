/*
  ==============================================================================

    RTCSimd.hpp
    Created: 10 Dec 2023 5:17:40pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

namespace RTC
{
    template<typename T, size_t N>
    class Simd
    {
        template<typename, size_t> static constexpr bool always_false_v = false;
        static_assert(always_false_v<T, N>);
    };
}

#if defined(__AVX512F__)
#   include "RTCSimdSSE2.hpp"
#   include "RTCSimdAVX.hpp"
#   include "RTCSimdAVX512.hpp"
#   define RTC_SIMD_SIZE 16
#elif defined(__AVX__)
#   include "RTCSimdSSE2.hpp"
#   include "RTCSimdAVX.hpp"
#   define RTC_SIMD_SIZE 8
#elif defined(__SSE2__) || defined(_M_X64) || defined(_M_AMD64)
#   include "RTCSimdSSE2.hpp"
#   define RTC_SIMD_SIZE 4
#else
#   error Simd instruction set not supported
#endif

namespace RTC
{
    template<typename T> Simd(T*) -> Simd<std::remove_const_t<T>, RTC_SIMD_SIZE>;
    template<typename T = float> Simd() -> Simd<T, RTC_SIMD_SIZE>;
}
