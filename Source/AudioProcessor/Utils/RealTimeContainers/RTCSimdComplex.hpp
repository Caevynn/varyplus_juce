/*
  ==============================================================================

    RTCSimdComplex.hpp
    Created: 14 Feb 2024 2:25:42pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCComplex.hpp"
#include "RTCSimd.hpp"

namespace RTC
{
    template<typename T, size_t N>
    class Complex<Simd<T, N>> : public ComplexArithmetics<Complex<Simd<T, N>>, Complex<Simd<T, N>>>
    {
    public:
        using value_type = Simd<T, N>;

        Complex() = default;
        Complex(const Complex&) = default;
        Complex(const Simd<T, N>& real, const Simd<T, N>& imag) : realValues(real), imagValues(imag) {}
        Complex(ComplexPointer<const T> pointer) : realValues(pointer.realPointer()), imagValues(pointer.imagPointer()) {}

        RTC_ENABLE_IF(N > 4) Complex(const Complex<Simd<T, N / 2>>& first, const Complex<Simd<T, N / 2>>& second) :
            realValues(first.real(), second.real()),
            imagValues(first.imag(), second.imag())
        {}

        RTC_ENABLE_IF(N == 4) Complex(const Complex<T>& v0, const Complex<T>& v1, const Complex<T>& v2, const Complex<T>& v3) :
            realValues(v0.real(), v1.real(), v2.real(), v3.real()),
            imagValues(v0.imag(), v1.imag(), v2.imag(), v3.imag())
        {}

        Complex& operator=(const Complex& a) { real() = a.real(); imag() = a.imag(); return *this; }
        RTC_ENABLE_IF_CONVERTIBLE(A, Complex) Complex& operator=(const A& a) { real() = a.realPN(); imag() = a.imagPN(); return *this; }

        Complex& load(ComplexPointer<const T> pointer)
        {
            realValues.load(pointer.realPointer());
            imagValues.load(pointer.imagPointer());
            return *this;
        }

        void store(ComplexPointer<T> pointer)
        {
            realValues.store(pointer.realPointer());
            imagValues.store(pointer.imagPointer());
        }

        Complex& reverse()
        {
            realValues.reverse();
            imagValues.reverse();
            return *this;
        }

        auto& real() { return realValues; }
        auto& imag() { return imagValues; }
        const auto& real() const { return realValues; }
        const auto& imag() const { return imagValues; }

    private:
        Simd<T, N> realValues;
        Simd<T, N> imagValues;

    public:
        [[deprecated]] void load(const T* realPointer, const T* imagPointer)
        {
            realValues.load(realPointer);
            imagValues.load(imagPointer);
        }

        [[deprecated]] void loadUnaligned(const T* realPointer, const T* imagPointer)
        {
            realValues.loadUnaligned(realPointer);
            imagValues.loadUnaligned(imagPointer);
        }

        [[deprecated]] void loadReversed(const T* realPointer, const T* imagPointer)
        {
            realValues.loadReversed(realPointer);
            imagValues.loadReversed(imagPointer);
        }

        [[deprecated]] void loadUnaligned(ComplexPointer<const T> pointer)
        {
            realValues.loadUnaligned(pointer.realPointer());
            imagValues.loadUnaligned(pointer.imagPointer());
        }

        [[deprecated]] void loadReversed(ComplexPointer<const T> pointer)
        {
            realValues.loadReversed(pointer.realPointer());
            imagValues.loadReversed(pointer.imagPointer());
        }

        [[deprecated]] void store(T* realPointer, T* imagPointer)
        {
            realValues.store(realPointer);
            imagValues.store(imagPointer);
        }

        [[deprecated]] void storeUnaligned(T* realPointer, T* imagPointer)
        {
            realValues.storeUnaligned(realPointer);
            imagValues.storeUnaligned(imagPointer);
        }

        [[deprecated]] void storeReversed(T* realPointer, T* imagPointer)
        {
            realValues.storeReversed(realPointer);
            imagValues.storeReversed(imagPointer);
        }

        [[deprecated]] void storeUnaligned(ComplexPointer<T> pointer)
        {
            realValues.storeUnaligned(pointer.realPointer());
            imagValues.storeUnaligned(pointer.imagPointer());
        }

        [[deprecated]] void storeReversed(ComplexPointer<T> pointer)
        {
            realValues.storeReversed(pointer.realPointer());
            imagValues.storeReversed(pointer.imagPointer());
        }
    };
}

namespace RTC
{
    template<typename T = float, size_t N = RTC_SIMD_SIZE>
    class SimdComplex : public Complex<Simd<T, N>>
    {
    public:
        template<typename ...Args>
        SimdComplex(Args&& ...args) : Complex<Simd<T, N>>(std::forward<Args>(args)...) {}
    };
}
