/*
  ==============================================================================

    Array2.hpp
    Created: 29 Oct 2023 2:40:07pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCRange.hpp"
#include "RTCInterface.hpp"

namespace RTC
{
    template<typename ElementType, template<typename...> typename Interface = Regular, template<typename...> typename ...Interfaces>
    class Array :
        public Range<ElementType, Interface, Interfaces...>
    {
        static constexpr size_t Dimensions = sizeof...(Interfaces) + 1;

        using DerivedRange = Range<ElementType, Interface, Interfaces...>;

    public:
        using value_type = ElementType;
        using reference = Range<ElementType, Interface, Interfaces...>;
        using const_reference = Range<const ElementType, Interface, Interfaces...>;

        RTC_ENABLE_IF(Dimensions == 1) void allocate(size_t wantedSize) { allocateWithDefaultConstructor(Size<Interface>({ wantedSize })); }
        RTC_ENABLE_IF(Dimensions == 1) void allocate(size_t wantedSize, const ElementType& initialElement) { allocateWithInitialElement(Size<Interface>({ wantedSize }), initialElement); }
        void allocate(const Size<Interface, Interfaces...>& wantedSize) { allocateWithDefaultConstructor(wantedSize); }
        void allocate(const Size<Interface, Interfaces...>& wantedSize, const ElementType& initialElement) { allocateWithInitialElement(wantedSize, initialElement); }

        void deallocate()
        {
            for (size_t i = 0; i < storage.size(); ++ i)
            {
                storage.destroy(i);
            }

            storage.deallocate();
            DerivedRange::updateSize(storage.pointer(), {});
        }

    private:
        void allocateWithDefaultConstructor(const Size<Interface, Interfaces...>& wantedSize)
        {
            storage.allocate(wantedSize.totalSize());

            DerivedRange::updateSize(storage.pointer(), wantedSize);

            if constexpr (is_zero_settable_v<DerivedRange>)
            {
                DerivedRange::setToZero();
            }
            else
            {
                for (size_t i = 0; i < storage.size(); ++i)
                {
                    storage.construct(i);
                }
            }
        }

        void allocateWithInitialElement(const Size<Interface, Interfaces...>& wantedSize, const ElementType& initialElement)
        {
            storage.allocate(wantedSize.totalSize());
            
            DerivedRange::updateSize(storage.pointer(), wantedSize);

            for (size_t i = 0; i < storage.size(); ++i)
            {
                storage.construct(i, initialElement);
            }
        }

        typename ElementTraits<ElementType>::allocator storage;
    };
}
