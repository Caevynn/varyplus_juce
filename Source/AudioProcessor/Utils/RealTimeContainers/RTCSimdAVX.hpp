/*
  ==============================================================================

    RTCSimdAVX.hpp
    Created: 26 Jan 2024 3:48:38pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <immintrin.h>

namespace RTC
{
    template<>
    class Simd<float, 8>
    {
    public:
        using value_type = float;
        static constexpr size_t Size = 8;

        Simd() = default;
        Simd(const Simd&) = default;
        Simd& operator=(const Simd&) = default;
        Simd(const float* address) { load(address); }
        Simd(__m256 data) : data(data) {}
        operator __m256() const { return data; }
        Simd(float value) { set(value); }
        Simd(const Simd<float, 4>& first, const Simd<float, 4>& second) { combine(first, second); }

        Simd& load(const float* address) { data = _mm256_loadu_ps(address); return *this; }
        void store(float* address) { _mm256_storeu_ps(address, data); }

        Simd& set(float value) { data = _mm256_set1_ps(value); return *this; }
        Simd& setZero() { data = _mm256_setzero_ps(); return *this; }

        [[nodiscard]] Simd operator-() const { return _mm256_sub_ps(_mm256_setzero_ps(), data); }

        [[nodiscard]] Simd operator+(const Simd& other) const { return _mm256_add_ps(data, other.data); }
        [[nodiscard]] Simd operator-(const Simd& other) const { return _mm256_sub_ps(data, other.data); }
        [[nodiscard]] Simd operator*(const Simd& other) const { return _mm256_mul_ps(data, other.data); }
        [[nodiscard]] Simd operator/(const Simd& other) const { return _mm256_div_ps(data, other.data); }

        Simd& operator+=(const Simd& other) { data = _mm256_add_ps(data, other.data); return *this; }
        Simd& operator-=(const Simd& other) { data = _mm256_sub_ps(data, other.data); return *this; }
        Simd& operator*=(const Simd& other) { data = _mm256_mul_ps(data, other.data); return *this; }
        Simd& operator/=(const Simd& other) { data = _mm256_div_ps(data, other.data); return *this; }

        static std::pair<Simd, Simd> deinterleave(const Simd& first, const Simd& second)
        {
            __m256 low = _mm256_permute2f128_ps(first.data, second.data, _MM_SHUFFLE(0, 2, 0, 0));
            __m256 high = _mm256_permute2f128_ps(first.data, second.data, _MM_SHUFFLE(0, 3, 0, 1));

            __m256 even = _mm256_shuffle_ps(low, high, _MM_SHUFFLE(2, 0, 2, 0));
            __m256 odd = _mm256_shuffle_ps(low, high, _MM_SHUFFLE(3, 1, 3, 1));

            return { even, odd };
        }

        static std::pair<Simd, Simd> interleave(const Simd& even, const Simd& odd)
        {
            __m256 low = _mm256_shuffle_ps(even.data, odd.data, _MM_SHUFFLE(1, 0, 1, 0));
            __m256 high = _mm256_shuffle_ps(even.data, odd.data, _MM_SHUFFLE(3, 2, 3, 2));

            low = _mm256_shuffle_ps(low, low, _MM_SHUFFLE(3, 1, 2, 0));
            high = _mm256_shuffle_ps(high, high, _MM_SHUFFLE(3, 1, 2, 0));

            __m256 first = _mm256_permute2f128_ps(low, high, _MM_SHUFFLE(0, 2, 0, 0));
            __m256 second = _mm256_permute2f128_ps(low, high, _MM_SHUFFLE(0, 3, 0, 1));

            return { first, second };
        }

        Simd& combine(const Simd<float, 4>& first, const Simd<float, 4>& second)
        {
            data = _mm256_set_m128(second, first);
            return *this;
        }

        Simd& reverse()
        {
            data = _mm256_permute_ps(_mm256_permute2f128_ps(data, data, _MM_SHUFFLE(0, 0, 0, 1)), _MM_SHUFFLE(0, 1, 2, 3));
            return *this;
        }

    private:
        __m256 data;

    public:
        [[deprecated]] void loadUnaligned(const float* address) { data = _mm256_loadu_ps(address); }
        [[deprecated]] void loadReversed(const float* address) { data = _mm256_load_ps(address); reverse(); }
        [[deprecated]] void storeUnaligned(float* address) { _mm256_storeu_ps(address, data); }
        [[deprecated]] void storeReversed(float* address) { reverse(); _mm256_store_ps(address, data); }
    };
}
