/*
  ==============================================================================

    RTCSimdAVX512.hpp
    Created: 26 Jan 2024 3:48:51pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <immintrin.h>

namespace RTC
{
    template<>
    class Simd<float, 16>
    {
    public:
        using value_type = float;
        static constexpr size_t Size = 16;

        Simd() = default;
        Simd(const Simd&) = default;
        Simd& operator=(const Simd&) = default;
        Simd(const float* address) { load(address); }
        Simd(__m512 data) : data(data) {}
        Simd(float value) { set(value); }
        Simd(const Simd<float, 8>& first, const Simd<float, 8>& second) { combine(first, second); }

        Simd& load(const float* address) { data = _mm512_loadu_ps(address); return *this; }
        void store(float* address) { _mm512_storeu_ps(address, data); }

        Simd& set(float value) { data = _mm512_set1_ps(value); return *this; }
        Simd& setZero() { data = _mm512_setzero_ps(); return *this; }

        [[nodiscard]] Simd operator-() const { return _mm512_sub_ps(_mm512_setzero_ps(), data); }

        [[nodiscard]] Simd operator+(const Simd& other) const { return _mm512_add_ps(data, other.data); }
        [[nodiscard]] Simd operator-(const Simd& other) const { return _mm512_sub_ps(data, other.data); }
        [[nodiscard]] Simd operator*(const Simd& other) const { return _mm512_mul_ps(data, other.data); }
        [[nodiscard]] Simd operator/(const Simd& other) const { return _mm512_div_ps(data, other.data); }

        Simd& operator+=(const Simd& other) { data = _mm512_add_ps(data, other.data); return *this; }
        Simd& operator-=(const Simd& other) { data = _mm512_sub_ps(data, other.data); return *this; }
        Simd& operator*=(const Simd& other) { data = _mm512_mul_ps(data, other.data); return *this; }
        Simd& operator/=(const Simd& other) { data = _mm512_div_ps(data, other.data); return *this; }

        static std::pair<Simd, Simd> deinterleave(const Simd& first, const Simd& second)
        {
            __m512i evenIndices = _mm512_set_epi32(
                30, 28, 26, 24,
                22, 20, 18, 16,
                14, 12, 10, 8,
                6, 4, 2, 0);

            __m512i oddIndices = _mm512_set_epi32(
                31, 29, 27, 25,
                23, 21, 19, 17,
                15, 13, 11, 9,
                7, 5, 3, 1);

            __m512 even = _mm512_permutex2var_ps(first.data, evenIndices, second.data);
            __m512 odd = _mm512_permutex2var_ps(first.data, oddIndices, second.data);

            return { even, odd };
        }

        static std::pair<Simd, Simd> interleave(const Simd& even, const Simd& odd)
        {
            __m512i firstIndices = _mm512_set_epi32(
                23, 7, 22, 6,
                21, 5, 20, 4,
                19, 3, 18, 2,
                17, 1, 16, 0);

            __m512i secondIndices = _mm512_set_epi32(
                31, 15, 30, 14,
                29, 13, 28, 12,
                27, 11, 26, 10,
                25, 9, 24, 8);

            __m512 first = _mm512_permutex2var_ps(even.data, firstIndices, odd.data);
            __m512 second = _mm512_permutex2var_ps(even.data, secondIndices, odd.data);

            return { first, second };
        }

        Simd& combine(const Simd<float, 8>& first, const Simd<float, 8>& second)
        {
            data = _mm512_insertf32x8(_mm512_castps256_ps512(first), second, 1);
            return *this;
        }

        Simd& reverse()
        {
            data = _mm512_permutexvar_ps(_mm512_set_epi32(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15), data);
            return *this;
        }

    private:
        __m512 data;

    public:
        [[deprecated]] void loadUnaligned(const float* address) { data = _mm512_loadu_ps(address); }
        [[deprecated]] void loadReversed(const float* address) { data = _mm512_load_ps(address); reverse(); }
        [[deprecated]] void storeUnaligned(float* address) { _mm512_storeu_ps(address, data); }
        [[deprecated]] void storeReversed(float* address) { reverse(); _mm512_store_ps(address, data); }
    };
}
