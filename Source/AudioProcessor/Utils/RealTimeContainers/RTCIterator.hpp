/*
  ==============================================================================

    Iterator.hpp
    Created: 29 Oct 2023 5:42:21pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCTemplateHelpers.hpp"

namespace RTC
{
    template<typename, template<typename...> typename, template<typename...> typename...> class Range;

    template<typename ElementType, template<typename...> typename ...Interfaces>
    class Iterator
    {
        static constexpr size_t Dimensions = sizeof...(Interfaces);

    public:
        using iterator_category = std::forward_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = Range<ElementType, Interfaces...>;
        using pointer = value_type*;
        using reference = value_type&;

        Iterator(typename ElementTraits<ElementType>::pointer dataPtr, const Size<Interfaces...>& size) :
            range(dataPtr, size)
        {}

        reference operator*() { return range; }
        pointer operator->() { return &range; }

        Iterator& operator+=(const difference_type& move) { range.dataPtr += move * range.sizeArray.totalSize(); return *this; }
        Iterator& operator-=(const difference_type& move) { range.dataPtr -= move * range.sizeArray.totalSize(); return *this; }
        Iterator operator+(const difference_type& move) { Iterator tmp = *this; tmp += move; return tmp; }
        Iterator operator-(const difference_type& move) { Iterator tmp = *this; tmp -= move; return tmp; }
        Iterator& operator++() { range.dataPtr += range.sizeArray.totalSize(); return *this; }
        Iterator operator++(int) { Iterator tmp = *this; ++(*this); return tmp; }
        friend bool operator== (const Iterator& a, const Iterator& b) { return a.getPtr() == b.getPtr(); };
        friend bool operator!= (const Iterator& a, const Iterator& b) { return a.getPtr() != b.getPtr(); };

        typename ElementTraits<ElementType>::pointer getPtr() const { return range.dataPtr; }
    private:
        Range<ElementType, Interfaces...> range;
    };

    template<typename ElementType>
    class Iterator<ElementType>
    {
    public:
        using iterator_category = std::random_access_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = typename ElementTraits<ElementType>::element_type;
        using pointer = typename ElementTraits<ElementType>::pointer;
        using reference = typename ElementTraits<ElementType>::reference;

        Iterator(pointer dataPtr, const Size<>& /*size*/ = Size<>()) :
            ptr(dataPtr)
        {}
        
        reference operator*() { return *ptr; }
        pointer operator->() { return ptr; }

        Iterator& operator+=(const difference_type& move) { ptr += move; return *this; }
        Iterator& operator-=(const difference_type& move) { ptr -= move; return *this; }
        Iterator operator+(const difference_type& move) { Iterator tmp = *this; tmp += move; return tmp; }
        Iterator operator-(const difference_type& move) { Iterator tmp = *this; tmp -= move; return tmp; }
        difference_type operator-(const Iterator& other) const { return ptr - other.ptr; }
        Iterator& operator++() { ++ptr; return *this; }
        Iterator operator++(int) { Iterator tmp = *this; ++(*this); return tmp; }
        Iterator& operator--() { --ptr; return *this; }
        Iterator operator--(int) { Iterator tmp = *this; --(*this); return tmp; }

        friend bool operator== (const Iterator& a, const Iterator& b) { return a.ptr == b.ptr; };
        friend bool operator!= (const Iterator& a, const Iterator& b) { return a.ptr != b.ptr; };
        friend bool operator< (const Iterator& a, const Iterator& b) { return a.ptr < b.ptr; };
        friend bool operator> (const Iterator& a, const Iterator& b) { return a.ptr > b.ptr; };

    private:
        pointer ptr;
    };

    template<typename ElementType>
    class AddIterator
    {
        static_assert(is_arithmetic_v<ElementType>);

    public:
        using iterator_category = std::random_access_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = typename ElementTraits<ElementType>::element_type;
        using pointer = typename ElementTraits<ElementType>::pointer;
        using reference = typename ElementTraits<ElementType>::reference;

        AddIterator(ElementType* dataPtr) :
            ptr(dataPtr)
        {}

        AddIterator& operator=(const ElementType& value) { *ptr += value; return *this; }
        AddIterator& operator=(ElementType&& value) { *ptr += value; return *this; }
        AddIterator& operator*() { return *this; }

        AddIterator& operator++() { ++ptr; return *this; }
        AddIterator operator++(int) { AddIterator tmp = *this; ++(*this); return tmp; }

    private:
        ElementType* ptr;
    };
}
