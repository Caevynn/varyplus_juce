/*
  ==============================================================================

    Rang2.hpp
    Created: 29 Oct 2023 2:40:15pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCElementTraits.hpp"
#include "RTCArithmeticTraits.hpp"
#include "RTCComplexTraits.hpp"

#include "RTCSize.hpp"
#include "RTCIterator.hpp"
#include "RTCTemplateHelpers.hpp"

namespace RTC
{
    template<typename ElementType, template<typename...> typename Interface, template<typename...> typename ...Interfaces>
    class Range :
        public Interface<Range<ElementType, Interface, Interfaces...>>,
        public ElementTraits<ElementType>::template AdditionalInterface<Range<ElementType, Interface, Interfaces...>, Interface, Interfaces...>
    {
        static constexpr size_t Dimensions = sizeof...(Interfaces) + 1;
        static constexpr bool isConstRange = std::is_const_v<ElementType>;

        using ConstRange = Range<const ElementType, Interface, Interfaces...>;
        using NonConstRange = Range<std::remove_const_t<ElementType>, Interface, Interfaces...>;
        
    public:
        RTC_ENABLE_IF(!isConstRange) Range(NonConstRange& other) : NonConstRange(other.dataPtr, other.sizeArray) {}
        RTC_ENABLE_IF(!isConstRange) Range(NonConstRange&& other) : NonConstRange(other.dataPtr, other.sizeArray) {}
        RTC_ENABLE_IF(isConstRange) Range(const ConstRange& other) : ConstRange(other.dataPtr, other.sizeArray) {}
        RTC_ENABLE_IF(isConstRange) Range(const NonConstRange& other) : ConstRange(other.dataPtr, other.sizeArray) {}

        using value_type = typename ElementTraits<ElementType>::element_type;
        using size_type = Size<Interface, Interfaces...>;
        using iterator = Iterator<ElementType, Interfaces...>;
        using const_iterator = Iterator<const ElementType, Interfaces...>;
        using pointer = typename ElementTraits<ElementType>::pointer;
        using const_pointer = typename ElementTraits<ElementType>::const_pointer;
        using reference = typename ElementTraits<ElementType>::reference;
        using const_reference = typename ElementTraits<ElementType>::const_reference;

        template<typename OtherType>
        static constexpr bool isCompatible(std::in_place_type_t<OtherType>)
        {
            return std::is_convertible_v<OtherType*, ConstRange*> || std::is_convertible_v<OtherType*, NonConstRange*>;
        }
        
        auto begin() { return iterator(dataPtr, sizeArray); }
        auto begin() const { return const_iterator(dataPtr, sizeArray); }
        auto end() { return iterator(dataPtr + sizeArray.totalSize(), sizeArray); }
        auto end() const { return const_iterator(dataPtr + sizeArray.totalSize(), sizeArray); }

        pointer data() { return dataPtr; }
        const_pointer data() const { return dataPtr; }
        const size_type& dimensions() const { return sizeArray; }

        auto range(size_t wantedStart, size_t wantedLength)
        {
            wantedStart = std::min(wantedStart, nElements());
            wantedLength = std::min(wantedLength, nElements() - wantedStart);

            return Range(dataPtr + wantedStart * sizeArray.stepSize(), sizeArray.resized(wantedLength));
        }

        auto range(size_t wantedStart, size_t wantedLength) const
        {
            wantedStart = std::min(wantedStart, nElements());
            wantedLength = std::min(wantedLength, nElements() - wantedStart);

            return ConstRange(dataPtr + wantedStart * sizeArray.stepSize(), sizeArray.resized(wantedLength));
        }

        RTC_ENABLE_IF_COMPATIBLE(OtherRange, Range)
            auto sameRange(const OtherRange& other)
        {
            return range(0, other.nElements());
        }

        RTC_ENABLE_IF_COMPATIBLE(OtherRange, Range)
            auto sameRange(const OtherRange& other) const
        {
            return range(0, other.nElements());
        }

        template<typename OtherElementType, template<typename...> typename ...OtherInterfaces,
            std::enable_if_t<sizeof...(OtherInterfaces) == Dimensions, bool> = true,
            std::enable_if_t<std::is_convertible_v<OtherElementType*, ElementType*>, bool> = true>
        static Range reinterpret(const RTC::Range<OtherElementType, OtherInterfaces...>& other)
        {
            return Range(other.dataPtr, size_type::reinterpret(other.sizeArray));
        }

    protected:
        Range() = default;

        Range(pointer dataPtr, const size_type& size) :
            dataPtr(dataPtr),
            sizeArray(size)
        {}

        void updateSize(pointer newDataPtr, const size_type& newSize)
        {
            dataPtr = newDataPtr;
            sizeArray = newSize;
        }

    private:
        template <typename, template<typename...> typename, template<typename...> typename...> friend class Range;
        template <typename, template<typename...> typename...> friend class Iterator;
        template <typename> friend class InterfaceBase;
        template <typename, typename> friend struct ElementTraits;

        pointer dataPtr{ nullptr };
        size_type sizeArray;

        RTC_ENABLE_IF(Dimensions == 1) reference element(size_t index) { return dataPtr[index]; }
        RTC_ENABLE_IF(Dimensions == 1) const_reference element(size_t index) const { return dataPtr[index]; }
        RTC_ENABLE_IF(Dimensions > 1) auto element(size_t index) { return Range<ElementType, Interfaces...>(dataPtr + index * sizeArray.stepSize(), sizeArray); }
        RTC_ENABLE_IF(Dimensions > 1) auto element(size_t index) const { return Range<const ElementType, Interfaces...>(dataPtr + index * sizeArray.stepSize(), sizeArray); }
        size_t nElements() const { return sizeArray[0]; }
        size_t elementSize() const { if constexpr (Dimensions == 1) { return 1; } else { return sizeArray[1]; } }
    };
}
