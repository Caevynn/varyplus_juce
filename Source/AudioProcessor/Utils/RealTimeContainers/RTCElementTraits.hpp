/*
  ==============================================================================

    RTCElementTraits.hpp
    Created: 26 Dec 2023 1:40:12am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCAllocator.hpp"

namespace RTC
{
    template<typename ElementType, typename DerivedRange, template<typename...> typename ...Interfaces>
    class ElementInterface {};

    template<typename ElementType, typename Enable = void>
    struct ElementTraits
    {
        static_assert(!std::is_arithmetic_v<ElementType>);

        using element_type = ElementType;
        using pointer = element_type*;
        using const_pointer = const element_type*;
        using reference = element_type&;
        using const_reference = const element_type&;
        using allocator = Allocator<element_type>;

        template<typename DerivedRange, template<typename...> typename ...Interfaces>
        class AdditionalInterface {};
    };
}
