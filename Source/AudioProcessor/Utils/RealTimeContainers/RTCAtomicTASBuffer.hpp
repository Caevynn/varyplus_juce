/*
  ==============================================================================

    AtomicTASBuffer.hpp
    Created: 17 Nov 2023 1:58:53pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <atomic>
#include <vector>
#include <cassert>

namespace RTC
{
    template<typename ElementType>
    class AtomicTASBuffer
    {
        static_assert(std::atomic<bool>::is_always_lock_free, "std::atomic<size_t> is not lock free.");
        static_assert(std::is_trivially_copyable_v<ElementType>, "Element type is not trivially copyable");

    public:
        AtomicTASBuffer(const ElementType& initialElement = ElementType())
            : data(1, initialElement)
        {}

        AtomicTASBuffer(AtomicTASBuffer&) = delete;
        AtomicTASBuffer(AtomicTASBuffer&&) = delete;

        void allocate(size_t size, const ElementType& initialElement = ElementType())
        {
            data.clear();
            data.resize(size, initialElement);
        }

        void deallocate()
        {
            data.clear();
        }

        bool tryPush(const ElementType& element, size_t index = 0)
        {
            assert(index < data.size());

            if (!lock.test_and_set(std::memory_order_acquire))
            {
                data[index] = element;
                lock.clear(std::memory_order_release);
                return true;
            }

            return false;
        }

        bool tryPop(ElementType& element, size_t index = 0)
        {
            assert(index < data.size());

            if (!lock.test_and_set(std::memory_order_acquire))
            {
                element = data[index];
                lock.clear(std::memory_order_release);
                return true;
            }

            return false;
        }

        template<typename Container, std::enable_if_t<std::is_same_v<typename Container::value_type, ElementType>, bool> = true>
        bool tryPush(const Container& container)
        {
            assert(container.size() == data.size());

            if (!lock.test_and_set(std::memory_order_acquire))
            {
                auto it = container.begin();
                for (auto& element : data)
                {
                    element = *it++;
                }
                lock.clear(std::memory_order_release);
                return true;
            }

            return false;
        }

        template<typename Container, std::enable_if_t<std::is_same_v<typename Container::value_type, ElementType>, bool> = true>
        bool tryPop(Container& container)
        {
            assert(container.size() == data.size());

            if (!lock.test_and_set(std::memory_order_acquire))
            {
                auto it = data.begin();
                for (auto& element : container)
                {
                    element = *it++;
                }
                lock.clear(std::memory_order_release);
                return true;
            }

            return false;
        }

    private:
        std::vector<ElementType> data;

        std::atomic_flag lock{ false };
    };
}
