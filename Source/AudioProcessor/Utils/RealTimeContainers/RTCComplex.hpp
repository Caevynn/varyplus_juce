/*
  ==============================================================================

    RTCComplex.hpp
    Created: 13 Feb 2024 1:25:34pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCTemplateHelpers.hpp"

namespace RTC
{
    template<typename> class ComplexReference;
    template<typename> class ComplexPointer;

    template<typename> class Negative;

    template<typename T>
    class Positive
    {
        using P = Positive<T>;
        using N = Negative<T>;

    public:
        Positive(const T& value) : value(value) {}

        operator T() const { return value; }

        N operator-() const { return { value }; }

        [[nodiscard]] P operator+(const P& positive) { return { value + positive.value }; }
        [[nodiscard]] P operator-(const P& positive) { return { value - positive.value }; }
        [[nodiscard]] P operator*(const P& positive) { return { value * positive.value }; }

        [[nodiscard]] P operator+(const N& negative) { return { value - negative.value }; }
        [[nodiscard]] P operator-(const N& negative) { return { value + negative.value }; }
        [[nodiscard]] N operator*(const N& negative) { return { value * negative.value }; }

        [[nodiscard]] P operator+(const T& type) { return { value + type }; }
        [[nodiscard]] P operator-(const T& type) { return { value - type }; }
        [[nodiscard]] P operator*(const T& type) { return { value * type }; }

        [[nodiscard]] P friend operator+(const T& type, const P& positive) { return { type + positive.value }; }
        [[nodiscard]] P friend operator-(const T& type, const P& positive) { return { type - positive.value }; }
        [[nodiscard]] P friend operator*(const T& type, const P& positive) { return { type * positive.value }; }

        friend T& operator+=(T& value, const P& positive) { value += positive.value; return value; }
        friend T& operator-=(T& value, const P& positive) { value -= positive.value; return value; }
        friend T& operator*=(T& value, const P& positive) { value *= positive.value; return value; }

    private:
        friend class Negative<T>;

        T value;
    };

    template<typename T>
    class Negative
    {
        using P = Positive<T>;
        using N = Negative<T>;

    public:
        Negative(const T& value) : value(value) {}

        operator T() const { return -value; }

        P operator-() const { return { value }; }

        [[nodiscard]] P operator+(const P& positive) { return { positive.value - value }; }
        [[nodiscard]] N operator-(const P& positive) { return { positive.value + value }; }
        [[nodiscard]] N operator*(const P& positive) { return { positive.value * value }; }

        [[nodiscard]] N operator+(const N& negative) { return { negative.value + value }; }
        [[nodiscard]] P operator-(const N& negative) { return { negative.value - value }; }
        [[nodiscard]] P operator*(const N& negative) { return { negative.value * value }; }

        [[nodiscard]] P operator+(const T& type) { return { type - value }; }
        [[nodiscard]] N operator-(const T& type) { return { type + value }; }
        [[nodiscard]] N operator*(const T& type) { return { type * value }; }

        [[nodiscard]] P friend operator+(const T& type, const N& negative) { return { type - negative.value }; }
        [[nodiscard]] P friend operator-(const T& type, const N& negative) { return { type + negative.value }; }
        [[nodiscard]] N friend operator*(const T& type, const N& negative) { return { type * negative.value }; }

        friend T& operator+=(T& type, const N& negative) { type -= negative.value; return type; }
        friend T& operator-=(T& type, const N& negative) { type += negative.value; return type; }
        friend T& operator*=(T& type, const N& negative) { type *= -negative.value; return type; }

    private:
        friend class Positive<T>;

        T value;
    };

    template<typename DerivedComplex, typename ComplexType = DerivedComplex>
    class ComplexConjugate
    {
        RTC_USE_CRTP(DerivedComplex)

    public:
        operator ComplexType() { return { realPN(), imagPN() }; }
        auto realPN() const { return Positive(derived().real()); }
        auto imagPN() const { return Negative(derived().imag()); }

    protected:
        ComplexConjugate() = default;
        ComplexConjugate(const ComplexConjugate&) = default;
    };

    template<typename DerivedComplex, typename ComplexType = DerivedComplex>
    class ComplexRotatedCW
    {
        RTC_USE_CRTP(DerivedComplex)

    public:
        operator ComplexType() { return { realPN(), imagPN() }; }
        auto realPN() const { return Positive(derived().imag()); }
        auto imagPN() const { return Negative(derived().real()); }

    protected:
        ComplexRotatedCW() = default;
        ComplexRotatedCW(const ComplexRotatedCW&) = default;
    };

    template<typename DerivedComplex, typename ComplexType = DerivedComplex>
    class ComplexRotatedCCW
    {
        RTC_USE_CRTP(DerivedComplex)

    public:
        operator ComplexType() { return { realPN(), imagPN() }; }
        auto realPN() const { return Negative(derived().imag()); }
        auto imagPN() const { return Positive(derived().real()); }

    protected:
        ComplexRotatedCCW() = default;
        ComplexRotatedCCW(const ComplexRotatedCCW&) = default;
    };

    template<typename ComplexType>
    class ComplexOperators
    {
    public:
        RTC_ENABLE_IF_CONVERTIBLE(A, ComplexType) [[nodiscard]] friend ComplexType operator-(const A& a) { return { -a.realPN(), -a.imagPN() }; }
        RTC_ENABLE_IF_CONVERTIBLE(A, ComplexType) [[nodiscard]] friend ComplexType operator*(const A& a, const typename A::value_type& value) { return { a.realPN() * value, a.imagPN() * value }; }
        RTC_ENABLE_IF_CONVERTIBLE(A, ComplexType) [[nodiscard]] friend ComplexType operator*(const typename A::value_type& value, const A& a) { return { a.realPN() * value, a.imagPN() * value }; }
        RTC_ENABLE_IF_CONVERTIBLE(A, ComplexType) friend A& operator*=(A& a, const typename A::value_type& value) { a.real() *= value; a.imag() *= value; return a; }

        RTC_ENABLE_IF_CONVERTIBLE_2(A, B, ComplexType) [[nodiscard]] friend ComplexType operator+(const A& a, const B& b) { return { a.realPN() + b.realPN(), a.imagPN() + b.imagPN() }; }
        RTC_ENABLE_IF_CONVERTIBLE_2(A, B, ComplexType) [[nodiscard]] friend ComplexType operator-(const A& a, const B& b) { return { a.realPN() - b.realPN(), a.imagPN() - b.imagPN() }; }
        RTC_ENABLE_IF_CONVERTIBLE_2(A, B, ComplexType) [[nodiscard]] friend ComplexType operator*(const A& a, const B& b) { return { a.realPN() * b.realPN() - a.imagPN() * b.imagPN(), a.realPN() * b.imagPN() + a.imagPN() * b.realPN() }; }

        RTC_ENABLE_IF_ASSIGNABLE_AND_CONVERTIBLE_2(A, B, ComplexType) friend A& operator+=(A&& a, const B& b) { a.real() += b.realPN(); a.imag() += b.imagPN(); return a; }
        RTC_ENABLE_IF_ASSIGNABLE_AND_CONVERTIBLE_2(A, B, ComplexType) friend A& operator-=(A&& a, const B& b) { a.real() -= b.realPN(); a.imag() -= b.imagPN(); return a; }
        RTC_ENABLE_IF_ASSIGNABLE_AND_CONVERTIBLE_2(A, B, ComplexType) friend A& operator*=(A&& a, const B& b) { auto realTmp = a.realPN(); a.real() = a.realPN() * b.realPN() - a.imagPN() * b.imagPN(); a.imag() = realTmp * b.imagPN() + a.imagPN() * b.realPN(); return a; }
    };

    template<typename DerivedComplex, typename ComplexType = DerivedComplex>
    class ComplexArithmetics :
        public ComplexOperators<ComplexType>,
        public ComplexConjugate<DerivedComplex, ComplexType>,
        public ComplexRotatedCW<DerivedComplex, ComplexType>,
        public ComplexRotatedCCW<DerivedComplex, ComplexType>
    {
        RTC_USE_CRTP(DerivedComplex)

    public:
        const ComplexConjugate<DerivedComplex, ComplexType>& conjugate() const { return *this; }
        const ComplexRotatedCW<DerivedComplex, ComplexType>& rotateCW() const { return *this; }
        const ComplexRotatedCCW<DerivedComplex, ComplexType>& rotateCCW() const { return *this; }

        operator ComplexType() { return { realPN(), imagPN() }; }
        auto realPN() const { return Positive(derived().real()); }
        auto imagPN() const { return Positive(derived().imag()); }

    protected:
        ComplexArithmetics() = default;
        ComplexArithmetics(const ComplexArithmetics&) = default;
    };

    template<typename ValueType>
    class Complex : public ComplexArithmetics<Complex<ValueType>>
    {
    public:
        using value_type = ValueType;

        Complex() = default;
        Complex(const ValueType& realValue, const ValueType& imagValue) : realValue(realValue), imagValue(imagValue) {}

        Complex(const Complex&) = default;
        Complex(const ComplexReference<ValueType>& reference) : realValue(reference.real()), imagValue(reference.imag()) {}
        RTC_ENABLE_IF_CONVERTIBLE(A, Complex) Complex(const A& a) : realValue(a.realPN()), imagValue(a.imagPN()) {}

        Complex& operator=(const Complex&) = default;
        Complex& operator=(const ComplexReference<ValueType>& reference) { real() = reference.real(); imag() = reference.imag(); return *this; }
        RTC_ENABLE_IF_CONVERTIBLE(A, Complex) Complex& operator=(const A& a) { real() = a.realPN(); imag() = a.imagPN(); return *this; }

        auto& real() { return realValue; }
        auto& imag() { return imagValue; }
        const auto& real() const { return realValue; }
        const auto& imag() const { return imagValue; }

    private:
        ValueType realValue;
        ValueType imagValue;
    };
    
    template<typename ValueType>
    class ComplexConstReference :
        public ComplexArithmetics<ComplexConstReference<ValueType>, Complex<std::remove_const_t<ValueType>>>
    {
        static_assert(!std::is_const_v<ValueType>);

        using ConstReference = ComplexConstReference;
        using NonConstReference = ComplexReference<ValueType>;

        using ConstPointer = ComplexPointer<const ValueType>;
        using NonConstPointer = ComplexPointer<ValueType>;

    public:
        using value_type = ValueType;

        ComplexConstReference(const ConstReference& other) : ComplexConstReference(other.real(), other.imag()) {}
        ComplexConstReference(const NonConstReference& other) : ComplexConstReference(other.real(), other.imag()) {}

        ComplexConstReference(const ConstPointer& pointer) : ComplexConstReference(*pointer.realPointer(), *pointer.imagPointer()) {}
        ComplexConstReference(const NonConstPointer& pointer) : ComplexConstReference(*pointer.realPointer(), *pointer.imagPointer()) {}

        const auto& real() const { return realValue; }
        const auto& imag() const { return imagValue; }

        [[nodiscard]] ConstPointer operator&() const { return ConstPointer(*this); }

    private:
        ComplexConstReference(const ValueType& real, const ValueType& imag) : realValue(real), imagValue(imag) {}

        const ValueType& realValue;
        const ValueType& imagValue;
    };

    template<typename ValueType>
    class ComplexReference :
        public ComplexArithmetics<ComplexReference<ValueType>, Complex<std::remove_const_t<ValueType>>>
    {
        static_assert(!std::is_const_v<ValueType>);

        using ConstPointer = ComplexPointer<const ValueType>;
        using NonConstPointer = ComplexPointer<std::remove_const_t<ValueType>>;

    public:
        using value_type = ValueType;

        ComplexReference(ComplexReference& other) : ComplexReference(other.real(), other.imag()) {}
        ComplexReference(ComplexReference&& other) : ComplexReference(other.real(), other.imag()) {}

        ComplexReference(NonConstPointer& pointer) : ComplexReference(*pointer.realPointer(), *pointer.imagPointer()) {}
        ComplexReference(NonConstPointer&& pointer) : ComplexReference(*pointer.realPointer(), *pointer.imagPointer()) {}

        ComplexReference& operator=(const ComplexReference& a) { real() = a.real(); imag() = a.imag(); return *this; }
        RTC_ENABLE_IF_CONVERTIBLE(A, Complex<ValueType>) ComplexReference& operator=(const A& a) { real() = a.realPN(); imag() = a.imagPN(); return *this; }

        auto& real() { return realValue; }
        auto& imag() { return imagValue; }
        const auto& real() const { return realValue; }
        const auto& imag() const { return imagValue; }

        [[nodiscard]] NonConstPointer operator&() { return NonConstPointer(*this); }
        [[nodiscard]] ConstPointer operator&() const { return ConstPointer(*this); }

    private:
        ComplexReference(ValueType& real, ValueType& imag) : realValue(real), imagValue(imag) {}

        ValueType& realValue;
        ValueType& imagValue;
    };

    template<typename ValueType>
    class ComplexPointer
    {
        using ConstReference = ComplexConstReference<std::remove_const_t<ValueType>>;
        using NonConstReference = ComplexReference<std::remove_const_t<ValueType>>;

        using Pointer = ComplexPointer;
        using ConstPointer = ComplexPointer<const ValueType>;
        using NonConstPointer = ComplexPointer<std::remove_const_t<ValueType>>;

        static constexpr bool isConstPointer = std::is_const_v<ValueType>;

        template<typename Reference>
        class ReferenceWrapper
        {
        public:
            template<typename ...Args> ReferenceWrapper(Args&& ...args) : reference(std::forward<Args>(args)...) {}
            Reference* operator->() { return &reference; }
        private:
            Reference reference;
        };

    public:
        ComplexPointer() = default;
        ComplexPointer(std::nullptr_t) {}
        ComplexPointer(ValueType* realPointer, ValueType* imagPointer) : realPtr(realPointer), imagPtr(imagPointer) {}

        RTC_ENABLE_IF(!isConstPointer) ComplexPointer(NonConstPointer& other) : NonConstPointer(other.realPointer(), other.imagPointer()) {}
        RTC_ENABLE_IF(!isConstPointer) ComplexPointer(NonConstPointer&& other) : NonConstPointer(other.realPointer(), other.imagPointer()) {}
        RTC_ENABLE_IF(isConstPointer) ComplexPointer(const ConstPointer& other) : ConstPointer(other.realPointer(), other.imagPointer()) {}
        RTC_ENABLE_IF(isConstPointer) ComplexPointer(const NonConstPointer& other) : ConstPointer(other.realPointer(), other.imagPointer()) {}

        RTC_ENABLE_IF(!isConstPointer) ComplexPointer(NonConstReference& reference) : NonConstPointer(&reference.real(), &reference.imag()) {}
        RTC_ENABLE_IF(!isConstPointer) ComplexPointer(NonConstReference&& reference) : NonConstPointer(&reference.real(), &reference.imag()) {}
        RTC_ENABLE_IF(isConstPointer) ComplexPointer(const ConstReference& reference) : ConstPointer(&reference.real(), &reference.imag()) {}
        RTC_ENABLE_IF(isConstPointer) ComplexPointer(const NonConstReference& reference) : ConstPointer(&reference.real(), &reference.imag()) {}

        ValueType* realPointer() { return realPtr; }
        ValueType* imagPointer() { return imagPtr; }
        const ValueType* realPointer() const { return realPtr; }
        const ValueType* imagPointer() const { return imagPtr; }

        RTC_ENABLE_IF(!isConstPointer) NonConstReference operator*() { return NonConstReference(*this); }
        RTC_ENABLE_IF(!isConstPointer) NonConstReference operator[](size_t index) { return NonConstReference(*this + index); }
        RTC_ENABLE_IF(!isConstPointer) ReferenceWrapper<NonConstReference> operator->() { return ReferenceWrapper<NonConstReference>{*this}; }
        ConstReference operator*() const { return ConstReference(*this); }
        ConstReference operator[](size_t index) const { return ConstReference(*this + index); }
        ReferenceWrapper<ConstReference> operator->() const { return ReferenceWrapper<ConstReference>{*this}; }

        Pointer& operator+=(const std::ptrdiff_t& distance) { realPtr += distance; imagPtr += distance; return *this; }
        Pointer& operator-=(const std::ptrdiff_t& distance) { realPtr -= distance; imagPtr -= distance; return *this; }
        Pointer operator+(const std::ptrdiff_t& distance) { return Pointer(realPtr + distance, imagPtr + distance); }
        Pointer operator-(const std::ptrdiff_t& distance) { return Pointer(realPtr - distance, imagPtr - distance); }
        ConstPointer operator+(const std::ptrdiff_t& distance) const { return ConstPointer(realPtr + distance, imagPtr + distance); }
        ConstPointer operator-(const std::ptrdiff_t& distance) const { return ConstPointer(realPtr - distance, imagPtr - distance); }
        Pointer& operator++() { ++realPtr; ++imagPtr; return *this; }
        Pointer& operator--() { --realPtr; ++imagPtr; return *this; }
        Pointer operator++(int) { Pointer tmp(*this); ++(*this); return tmp; }
        Pointer operator--(int) { Pointer tmp(*this); --(*this); return tmp; }

        friend bool operator==(const Pointer& a, const Pointer& b) { return a.realPointer() == b.realPointer() && a.imagPointer() == b.imagPointer(); }
        friend bool operator!=(const Pointer& a, const Pointer& b) { return a.realPointer() != b.realPointer() || a.imagPointer() != b.imagPointer(); }
        friend bool operator>(const Pointer& a, const Pointer& b) { return a.realPointer() > b.realPointer() && a.imagPointer() > b.imagPointer(); }
        friend bool operator<(const Pointer& a, const Pointer& b) { return a.realPointer() < b.realPointer() && a.imagPointer() < b.imagPointer(); }

    private:
        ValueType* realPtr{ nullptr };
        ValueType* imagPtr{ nullptr };
    };
}
