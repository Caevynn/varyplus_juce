/*
  ==============================================================================

    RTCTest.hpp
    Created: 3 Nov 2023 5:09:53pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCArray.hpp"
#include "RTCVector.hpp"
#include "RTCUniquePointerVector.hpp"

namespace RTC
{
    class Test
    {
    public:
        static void run()
        {
            std::cout << "RTC::Test" << std::endl;

            RTC::Array<int, 3, RTC::Bus, RTC::Channel, RTC::Sample> array1;
            array1.allocate({ 4, 4, 4 });
            int i = 0;
            for (auto& matrix : array1)
            {
                std::cout << "i: " << i << std::endl;
                int j = 10 * i++;
                for (auto& range : matrix)
                {
                    std::cout << "\tj: " << j << std::endl;
                    int k = 10 * j++;
                    for (auto& element : range)
                    {
                        std::cout << "\t\tk: " << k << std::endl;
                        element = k++;
                    }
                }
            }

            array1.bus(0).channel(3).sample(1) = 0;

            auto array2 = array1.range(1, 2);

            for (auto& matrix : array2)
            {
                for (auto& range : matrix)
                {
                    for (auto& element : range)
                    {
                        element = 0;
                    }
                }
            }

            auto array3 = array1.bus(0).range(1, 2);

            for (auto& range : array3)
            {
                for (auto& element : range)
                {
                    element = 0;
                }
            }

            RTC::Array<int, 3, RTC::Bus, RTC::Channel, RTC::Sample> array4;
            array4.allocate({ 4, 4, 4 });
            array4.copy(array1);
            array4.add(array1);
            array4.bus(1).channel(1).copy(array4.bus(0).channel(3));

            std::cout << "RTC::Array<int, 3>" << std::endl;
            const auto& carray = array4;
            for (auto& matrix : carray)
            {
                for (auto& range : matrix)
                {
                    for (auto& element : range)
                    {
                        std::cout << element << std::endl;
                    }
                }
            }

            std::cout << "RTC::Array<int, 1>" << std::endl;
            RTC::Array<int, 1> array5;
            array5.allocate({ 10 });
            i = 0;
            for (auto& element : array5)
            {
                element = i++;
            }

            for (const auto& element : array5)
            {
                std::cout << element << std::endl;
            }

            std::cout << "RTC::Vector<int>" << std::endl;
            RTC::Vector<int> vector1;
            vector1.allocate({ 10 });
            for (int j = 0; j < 5; ++j)
            {
                vector1.push_back(j);
            }

            for (const auto& element : vector1)
            {
                std::cout << element << std::endl;
            }

            std::cout << "RTC::UniquePointerVector<int>" << std::endl;
            RTC::UniquePointerVector<int> vector2;
            vector2.allocate({ 10 });
            for (int j = 0; j < 5; ++j)
            {
                vector2.push_back(std::make_unique<int>(j));
            }

            for (auto& element : vector2)
            {
                element *= 2;
            }

            for (const auto& element : vector2)
            {
                std::cout << element << std::endl;
            }

            std::cout << "RTC::Array<int, 1>::multiplyAndAdd" << std::endl;
            RTC::Array<float, 1> array6, array7, array8;
            array6.allocate({ 10 });
            array7.allocate({ 10 });
            array8.allocate({ 10 });
            for (int j = 0; j < 10; ++j)
            {
                array6[j] = static_cast<float>(j);
                array7[j] = j >= 5 ? 1 : static_cast<float>(j);
                array8[j] = 0.5f;
            }
            array8.multiplyAndAdd(array6, array7);
            for (const auto& element : array8)
            {
                std::cout << element << std::endl;
            }

            RTC::Array<float, 2> array9, array10, array11;
            array9.allocate({ 1, 10 }, 1);
            array10.allocate({ 1, 10 });
            array11.allocate({ 1, 10 });
            std::cout << "RTC::Array<float, 2>::addFading" << std::endl;
            array10.addFading(array9, 0, 1);
            for (const auto& element : array10[0])
            {
                std::cout << element << std::endl;
            }
            std::cout << "RTC::Array<float, 2>::addScaled" << std::endl;
            array11.addScaled(array9, 0.5);
            for (const auto& element : array11[0])
            {
                std::cout << element << std::endl;
            }
        }

        static void runComplex()
        {
            RTC::Array<RTC::Complex<float>, 1, RTC::Sample> cArray1, cArray2, cArray3;

            cArray1.allocate({ 4 });
            cArray2.allocate({ 4 });
            cArray3.allocate({ 4 });

            auto re1 = cArray1.real();
            auto im1 = cArray1.imag();

            cArray2.real().sample(0) = 1;
            cArray2.imag().sample(0) = 1;
            cArray2.real().sample(1) = 0;
            cArray2.imag().sample(1) = 1;

            cArray3.real(0) = 1;
            cArray3.imag(0) = 1;
            cArray3.real(1) = 0;
            cArray3.imag(1) = 1;

            cArray1.multiplyAndAdd(cArray2, cArray3);

            float* first = &re1.sample(0);
            for (float* it = first; it != first + 8; ++it)
            {
                std::cout << it << ": " << *it << std::endl;
            }
        }
    };
}
