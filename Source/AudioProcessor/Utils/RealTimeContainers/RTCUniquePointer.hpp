/*
  ==============================================================================

    RTCUniquePointer.hpp
    Created: 18 Dec 2023 9:30:02pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <cassert>

namespace RTC
{
    template<typename ElementType>
    class UniquePointer
    {
    public:
        using element_type = ElementType;

        UniquePointer(const UniquePointer&) = delete;
        UniquePointer& operator=(const UniquePointer&) = delete;

        UniquePointer() : dataPtr(nullptr) {}
        UniquePointer(ElementType&& element) : dataPtr(new ElementType(std::move(element))) {}
        UniquePointer(UniquePointer&& other) noexcept : dataPtr(other.release()) {}
        UniquePointer(std::unique_ptr<ElementType>&& other) noexcept : dataPtr(other.release()) {}
        
        UniquePointer& operator=(UniquePointer&& other) noexcept
        {
            assert(dataPtr == nullptr);

            swap(other);
            return *this;
        }

        ~UniquePointer()
        {
            assert(dataPtr == nullptr);

            if (dataPtr != nullptr)
            {
                deallocate();
            }
        }

        template<typename ...Args>
        void allocate(Args&& ...args)
        {
            if (dataPtr != nullptr)
            {
                deallocate();
            }

            dataPtr = new ElementType(std::forward<Args>(args)...);
        }

        void deallocate()
        {
            delete dataPtr;
            dataPtr = nullptr;
        }

        ElementType* release() noexcept { return std::exchange(dataPtr, nullptr); }
        void swap(UniquePointer& other) noexcept { std::swap(dataPtr, other.dataPtr); }

        ElementType& operator*() const noexcept { assert(operator bool()); return *dataPtr; }
        ElementType* operator->() const noexcept { assert(operator bool()); return dataPtr; }
        ElementType* get() const noexcept { assert(operator bool()); return dataPtr; }
        explicit operator bool() const noexcept { return dataPtr != nullptr; }

    private:
        ElementType* dataPtr{ nullptr };
    };
}
