/*
  ==============================================================================

    RTCSimdSSE.hpp
    Created: 26 Jan 2024 3:48:26pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <emmintrin.h>

namespace RTC
{
    template<>
    class Simd<float, 4>
    {
    public:
        using value_type = float;
        static constexpr size_t Size = 4;

        Simd() = default;
        Simd(const Simd&) = default;
        Simd& operator=(const Simd&) = default;
        Simd(const float* address) { load(address); }
        Simd(__m128 data) : data(data) {}
        operator __m128() const { return data; }
        Simd(float value) { set(value); }
        Simd(float v0, float v1, float v2, float v3) { set(v0, v1, v2, v3); }

        Simd& load(const float* address) { data = _mm_loadu_ps(address); return *this; }
        void store(float* address) { _mm_storeu_ps(address, data); }

        Simd& set(float value) { data = _mm_set1_ps(value); return *this; }
        Simd& set(float v0, float v1, float v2, float v3) { data = _mm_set_ps(v3, v2, v1, v0); return *this; }
        Simd& setZero() { data = _mm_setzero_ps(); return *this; }

        [[nodiscard]] Simd operator-() const { return _mm_sub_ps(_mm_setzero_ps(), data); }

        [[nodiscard]] Simd operator+(const Simd& other) const { return _mm_add_ps(data, other.data); }
        [[nodiscard]] Simd operator-(const Simd& other) const { return _mm_sub_ps(data, other.data); }
        [[nodiscard]] Simd operator*(const Simd& other) const { return _mm_mul_ps(data, other.data); }
        [[nodiscard]] Simd operator/(const Simd& other) const { return _mm_div_ps(data, other.data); }

        Simd& operator+=(const Simd& other) { data = _mm_add_ps(data, other.data); return *this; }
        Simd& operator-=(const Simd& other) { data = _mm_sub_ps(data, other.data); return *this; }
        Simd& operator*=(const Simd& other) { data = _mm_mul_ps(data, other.data); return *this; }
        Simd& operator/=(const Simd& other) { data = _mm_div_ps(data, other.data); return *this; }

        static std::pair<Simd, Simd> deinterleave(const Simd& first, const Simd& second)
        {
            __m128 even = _mm_shuffle_ps(first.data, second.data, _MM_SHUFFLE(2, 0, 2, 0));
            __m128 odd = _mm_shuffle_ps(first.data, second.data, _MM_SHUFFLE(3, 1, 3, 1));

            return { even, odd };
        }

        static std::pair<Simd, Simd> interleave(const Simd& even, const Simd& odd)
        {
            __m128 first = _mm_shuffle_ps(even.data, odd.data, _MM_SHUFFLE(1, 0, 1, 0));
            __m128 second = _mm_shuffle_ps(even.data, odd.data, _MM_SHUFFLE(3, 2, 3, 2));

            first = _mm_shuffle_ps(first, first, _MM_SHUFFLE(3, 1, 2, 0));
            second = _mm_shuffle_ps(second, second, _MM_SHUFFLE(3, 1, 2, 0));

            return { first, second };
        }

        Simd& reverse()
        {
            data = _mm_shuffle_ps(data, data, _MM_SHUFFLE(0, 1, 2, 3));
            return *this;
        }

    private:
        __m128 data;

    public:
        [[deprecated]] void loadUnaligned(const float* address) { data = _mm_loadu_ps(address); }
        [[deprecated]] void loadReversed(const float* address) { data = _mm_loadr_ps(address); }
        [[deprecated]] void storeUnaligned(float* address) { _mm_storeu_ps(address, data); }
        [[deprecated]] void storeReversed(float* address) { _mm_storer_ps(address, data); }
    };
}
