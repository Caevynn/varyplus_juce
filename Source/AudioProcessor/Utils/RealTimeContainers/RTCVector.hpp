/*
  ==============================================================================

    Vector2.hpp
    Created: 29 Oct 2023 2:40:37pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCRange.hpp"
#include "RTCAllocator.hpp"
#include "RTCInterface.hpp"

namespace RTC
{
    template<typename ElementType>
    class Vector :
        public Range<ElementType, RTC::Regular>
    {
        static constexpr bool isElementTypeCopyable = std::is_trivially_copyable_v<ElementType>;
        static constexpr bool isElementTypeSwappable = std::is_nothrow_swappable_v<ElementType>;

        static_assert(isElementTypeCopyable || isElementTypeSwappable);
        
        using DerivedRange = Range<ElementType, RTC::Regular>;

    public:
        using value_type = ElementType;
        using RangeType = Range<ElementType, RTC::Regular>;
        using ConstRangeType = Range<const ElementType, RTC::Regular>;

        void allocate(size_t wantedSize)
        {
            storage.allocate(wantedSize);
            DerivedRange::updateSize(storage.pointer(), {});
        }

        void deallocate()
        {
            for (size_t i = 0; i < DerivedRange::size(); ++i)
            {
                if constexpr (is_deallocatable_v<ElementType>)
                {
                    storage[i].deallocate();
                }

                storage.destroy(i);
            }

            storage.deallocate();
            DerivedRange::updateSize(storage.pointer(), {});
        }

        bool push_back(std::conditional_t<isElementTypeCopyable, const ElementType&, ElementType&&> element)
        {
            if (!isFull())
            {
                storage.construct(DerivedRange::size(), std::move(element));

                DerivedRange::updateSize(storage.pointer(), { { DerivedRange::size() + 1 } });

                return true;
            }

            return false;
        }

        bool pop_back(ElementType& element)
        {
            if (!isEmpty())
            {
                DerivedRange::updateSize(storage.pointer(), { { DerivedRange::size() - 1 } });

                element = std::move(storage[DerivedRange::size()]);

                storage.destroy(DerivedRange::size());

                return true;
            }

            return false;
        }

        bool pop(size_t index, ElementType& element)
        {
            if (index < DerivedRange::size())
            {
                element = std::move(storage[index]);

                for (size_t i = index + 1; i < DerivedRange::size(); ++i)
                {
                    storage[i - 1] = std::move(storage[i]);
                }

                storage.destroy(DerivedRange::size() - 1);

                DerivedRange::updateSize(storage.pointer(), { { DerivedRange::size() - 1 } });

                return true;
            }

            return false;
        }

        bool isFull() const { return DerivedRange::size() == storage.size(); }
        bool isEmpty() const { return DerivedRange::size() == 0; }

    private:
        Allocator<ElementType> storage;
    };
}
