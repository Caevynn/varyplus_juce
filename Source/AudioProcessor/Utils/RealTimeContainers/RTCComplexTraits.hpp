/*
  ==============================================================================

    RTCComplexTraits.hpp
    Created: 26 Dec 2023 1:05:46am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCElementTraits.hpp"
#include "RTCTemplateHelpers.hpp"
#include "RTCSimdComplex.hpp"

namespace RTC
{
    template <typename, template<typename...> typename, template<typename...> typename...> class Range;

    template<typename, size_t> class ComplexAllocator;
    
    template<typename ValueType>
    struct ElementTraits<ValueType, std::enable_if_t<is_specialization_of_v<Complex, ValueType>>>
    {
        static constexpr size_t data_alignment = 64;

        using element_type = std::conditional_t<std::is_const_v<ValueType>, const typename ValueType::value_type, typename ValueType::value_type>;
        using pointer = ComplexPointer<element_type>;
        using const_pointer = ComplexPointer<const element_type>;
        using reference = std::conditional_t<std::is_const_v<ValueType>, ComplexConstReference<typename ValueType::value_type>, ComplexReference<typename ValueType::value_type>>;
        using const_reference = ComplexConstReference<typename ValueType::value_type>;
        using allocator = ComplexAllocator<element_type, data_alignment>;

        static_assert(std::is_arithmetic_v<element_type>);

        template<typename DerivedRange, template<typename...> typename ...Interfaces>
        class AdditionalInterface
        {
            RTC_USE_CRTP(DerivedRange)

        public:
            auto real() { return Range<element_type, Interfaces...>(data().realPointer(), dimensions()); }
            auto real() const { return Range<const element_type, Interfaces... >(data().realPointer(), dimensions()); }
            auto imag() { return Range<element_type, Interfaces...>(data().imagPointer(), dimensions()); }
            auto imag() const { return Range<const element_type, Interfaces...>(data().imagPointer(), dimensions()); }

            void setToZero()
            {
                real().setToZero();
                imag().setToZero();
            }

            RTC_ENABLE_IF_COMPATIBLE(OtherRange, DerivedRange)
                RTC_DEBUG_BOOL copy(const OtherRange& other)
            {
                RTC_DEBUG_RETURN_FALSE_IF(other.dimensions() != dimensions());

                RTC_DEBUG_ASSUME(real().copy(other.real()));
                RTC_DEBUG_ASSUME(imag().copy(other.imag()));

                RTC_DEBUG_RETURN_TRUE;
            }

            RTC_ENABLE_IF_COMPATIBLE(OtherRange, DerivedRange)
                RTC_DEBUG_BOOL add(const OtherRange& other)
            {
                RTC_DEBUG_RETURN_FALSE_IF(other.dimensions() != dimensions());

                RTC_DEBUG_ASSUME(real().add(other.real()));
                RTC_DEBUG_ASSUME(imag().add(other.imag()));

                RTC_DEBUG_RETURN_TRUE;
            }

            RTC_ENABLE_IF_COMPATIBLE_2(OtherRange1, OtherRange2, DerivedRange)
                RTC_DEBUG_BOOL multiplyAndAdd(const OtherRange1& a, const OtherRange2& b)
            {
                RTC_DEBUG_RETURN_FALSE_IF(a.dimensions() != dimensions() || b.dimensions() != dimensions());

                size_t nElements = dimensions().totalSize();

                assert(nElements % RTC_SIMD_SIZE == 0);

                auto cp = data();
                auto cap = a.data();
                auto cbp = b.data();

                size_t index = 0;
                for (; index < nElements - RTC_SIMD_SIZE; index += 2 * RTC_SIMD_SIZE)
                {
                    SimdComplex c(&cp[index]);
                    SimdComplex ca(&cap[index]);
                    SimdComplex cb(&cbp[index]);
                    
                    (c + ca * cb).store(&cp[index]);

                    SimdComplex c1(&cp[index + RTC_SIMD_SIZE]);
                    SimdComplex ca1(&cap[index + RTC_SIMD_SIZE]);
                    SimdComplex cb1(&cbp[index + RTC_SIMD_SIZE]);

                    (c1 + ca1 * cb1).store(&cp[index + RTC_SIMD_SIZE]);
                }

                if (index < nElements)
                {
                    SimdComplex c(&cp[index]);
                    SimdComplex ca(&cap[index]);
                    SimdComplex cb(&cbp[index]);

                    (c + ca * cb).store(&cp[index]);
                }
                
                RTC_DEBUG_RETURN_TRUE;
            }

        private:
            auto data() { return derived().data(); }
            auto data() const { return derived().data(); }
            const auto& dimensions() const { return derived().dimensions(); }
        };
    };

    template<typename ValueType, size_t Alignment>
    class ComplexAllocator : public Allocator<ValueType, Alignment>
    {
    public:
        void allocate(size_t index)
        {
            Allocator<ValueType, Alignment>::allocate(2 * index);
        }

        ComplexPointer<ValueType> pointer()
        {
            auto* realPointer = Allocator<ValueType, Alignment>::pointer();
            auto* imagPointer = realPointer + size();
            return ComplexPointer<ValueType>(realPointer, imagPointer);
        }

        size_t size() const
        {
            return Allocator<ValueType, Alignment>::size() / 2;
        }

        void construct(size_t index)
        {
            pointer()[index].real() = 0;
            pointer()[index].imag() = 0;
        }

        void construct(size_t index, Complex<ValueType> value)
        {
            pointer()[index].real() = value.real();
            pointer()[index].imag() = value.imag();
        }

        void destroy(size_t) {}
    };
}
