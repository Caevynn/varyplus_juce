/*
  ==============================================================================

    RTCAllocator.hpp
    Created: 5 Nov 2023 12:44:32pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <cassert>

namespace RTC
{
    template<typename ElementType, size_t Alignment = 0>
    class Allocator
    {
        static_assert((Alignment & (Alignment - 1)) == 0, "Alignment must be power of 2");

    public:
        Allocator() = default;
        Allocator(const Allocator&) = delete;
        Allocator& operator=(const Allocator&) = delete;

        Allocator(Allocator&& other)
        {
            dataPtr = other.dataPtr;
            dataSize = other.dataSize;

            other.dataPtr = nullptr;
            other.dataSize = 0;
        }

        ~Allocator()
        {
            assert(dataPtr == nullptr);
            assert(dataSize == 0);

            if (dataPtr != nullptr)
            {
                deallocate();
            }
        }

        void allocate(size_t size)
        {
            if (dataPtr != nullptr)
            {
                deallocate();
            }

            if constexpr (Alignment > alignof(ElementType))
            {
                void* allocated = std::malloc(size * sizeof(ElementType) + Alignment + sizeof(void*));

                uintptr_t unaligned = reinterpret_cast<uintptr_t>(allocated);
                uintptr_t aligned = (unaligned + Alignment + sizeof(void*)) & ~(Alignment - 1);

                assert(aligned - unaligned >= sizeof(void*));
                assert(aligned - unaligned <= Alignment + sizeof(void*));

                void** allocatedStorage = reinterpret_cast<void**>(aligned - sizeof(void*));
                *allocatedStorage = allocated;

                dataPtr = reinterpret_cast<ElementType*>(aligned);
            }
            else
            {
                dataPtr = reinterpret_cast<ElementType*>(std::malloc(size * sizeof(ElementType)));
            }

            dataSize = size;
        }

        void deallocate()
        {
            if (dataPtr != nullptr)
            {
                if constexpr(Alignment > alignof(ElementType))
                {
                    uintptr_t aligned = reinterpret_cast<uintptr_t>(dataPtr);

                    void** allocatedStorage = reinterpret_cast<void**>(aligned - sizeof(void*));
                    void* allocated = *allocatedStorage;

                    std::free(allocated);
                }
                else
                {
                    std::free(dataPtr);
                }
                
                dataPtr = nullptr;
                dataSize = 0;
            }
        }

        template<typename ...Args>
        void construct(size_t index, Args&& ...args)
        {
            assert(index < dataSize);

            new (dataPtr + index) ElementType(std::forward<Args>(args)...);
        }

        void destroy(size_t index)
        {
            assert(index < dataSize);

            dataPtr[index].~ElementType();
        }

        size_t size() const { return dataSize; }
        ElementType* pointer() const { return dataPtr; }
        ElementType& operator[](size_t index) { assert(index < dataSize); return pointer()[index]; }
        const ElementType& operator[](size_t index) const { assert(index < dataSize); return pointer()[index]; }
        
    private:
        ElementType* dataPtr{ nullptr };
        size_t dataSize{ 0 };
    };
}
