/*
  ==============================================================================

    RTCInterface.hpp
    Created: 1 Nov 2023 5:21:12pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCTemplateHelpers.hpp"

namespace RTC
{
    template<typename DerivedRange>
    class InterfaceBase
    {
        RTC_USE_CRTP(DerivedRange)

    protected:
        decltype(auto) element(size_t index) { return derived().element(index); }
        decltype(auto) element(size_t index) const { return derived().element(index); }
        size_t nElements() const { return derived().nElements(); }
        size_t elementSize() const { return derived().elementSize(); }
    };

    template<typename ...Args>
    class Regular : public InterfaceBase<Args...>
    {
    public:
        RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(operator[], element)
        RTC_FUNCTION_ALIAS_CONST(size, nElements)
    };
}
