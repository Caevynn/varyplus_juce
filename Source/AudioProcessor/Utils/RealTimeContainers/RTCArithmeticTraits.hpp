/*
  ==============================================================================

    RTCArithmeticTraits.hpp
    Created: 26 Dec 2023 1:39:56am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <algorithm>

#include "RTCElementTraits.hpp"
#include "RTCTemplateHelpers.hpp"

namespace RTC
{
    template<typename, template<typename...> typename, template<typename...> typename...> class Range;
    template<typename, template<typename...> typename...> class Iterator;
    template<typename> class AddIterator;
    template<typename...> class Regular;

    template<typename ElementType>
    struct ElementTraits<ElementType, std::enable_if_t<std::is_arithmetic_v<ElementType>>>
    {
        static_assert(std::is_arithmetic_v<ElementType>);

        static constexpr size_t data_alignment = 64;

        using element_type = ElementType;
        using pointer = element_type*;
        using const_pointer = const element_type*;
        using reference = element_type&;
        using const_reference = const element_type&;
        using allocator = Allocator<element_type, data_alignment>;

        template<typename DerivedRange, template<typename...> typename ...Interfaces>
        class AdditionalInterface
        {
            RTC_USE_CRTP(DerivedRange)

            static constexpr size_t Dimensions = sizeof...(Interfaces);

        public:
            void setToZero()
            {
                using ValueType = typename DerivedRange::value_type;

                std::fill(startIt(), endIt(), ValueType{ 0 });
            }

            RTC_ENABLE_IF_COMPATIBLE(OtherRange, DerivedRange)
                RTC_DEBUG_BOOL copy(const OtherRange& other)
            {
                RTC_DEBUG_RETURN_FALSE_IF(other.dimensions() != dimensions());

                std::copy(other.startIt(), other.endIt(), startIt());

                RTC_DEBUG_RETURN_TRUE;
            }

            RTC_ENABLE_IF_COMPATIBLE(OtherRange, DerivedRange)
                RTC_DEBUG_BOOL add(const OtherRange& other)
            {
                RTC_DEBUG_RETURN_FALSE_IF(other.dimensions() != dimensions());

                std::transform(startIt(), endIt(), other.startIt(), startIt(), std::plus<typename DerivedRange::value_type>());
                   
                RTC_DEBUG_RETURN_TRUE;
            }

            void scale(double scale)
            {
                using ValueType = typename DerivedRange::value_type;

                ValueType amplitude = static_cast<ValueType>(scale);

                std::transform(startIt(), endIt(), startIt(),
                    [amplitude](const auto& value)
                    {
                        return value * amplitude;
                    });
            }

            RTC_ENABLE_IF_COMPATIBLE(OtherRange, DerivedRange)
                RTC_DEBUG_BOOL addScaled(const OtherRange& other, double scale)
            {
                RTC_DEBUG_RETURN_FALSE_IF(other.dimensions() != dimensions());

                using ValueType = typename DerivedRange::value_type;

                ValueType amplitude = static_cast<ValueType>(scale);

                std::transform(other.startIt(), other.endIt(), addIt(),
                    [amplitude](const auto& value)
                    {
                        return value * amplitude;
                    });

                RTC_DEBUG_RETURN_TRUE;
            }

            void fade(double startValue, double endValue)
            {
                if constexpr (Dimensions == 1)
                {
                    using ValueType = typename DerivedRange::value_type;

                    ValueType step = static_cast<ValueType>(endValue - startValue) / (dataSize() + 1);
                    ValueType current = static_cast<ValueType>(startValue);

                    std::transform(startIt(), endIt(), startIt(),
                        [&step, &current](const auto& value)
                        {
                            current += step;
                            return value * current;
                        });
                }
                else
                {
                    for (auto& x : *static_cast<DerivedRange*>(this))
                    {
                        x.fade(startValue, endValue);
                    }
                }
            }

            RTC_ENABLE_IF_COMPATIBLE(OtherRange, DerivedRange)
                RTC_DEBUG_BOOL addFading(const OtherRange& other, double startValue, double endValue)
            {
                RTC_DEBUG_RETURN_FALSE_IF(other.dimensions() != dimensions());

                if constexpr (Dimensions == 1)
                {
                    using ValueType = typename DerivedRange::value_type;

                    ValueType step = static_cast<ValueType>(endValue - startValue) / (dataSize() + 1);
                    ValueType current = static_cast<ValueType>(startValue);

                    std::transform(other.startIt(), other.endIt(), addIt(),
                        [&step, &current](const auto& value)
                        {
                            current += step;
                            return value * current;
                        });
                }
                else
                {
                    auto it = other.begin();

                    for (auto& x : *static_cast<DerivedRange*>(this))
                    {
                        RTC_DEBUG_ASSUME(x.addFading(*it++, startValue, endValue));
                    }
                }

                RTC_DEBUG_RETURN_TRUE;
            }

            RTC_ENABLE_IF_COMPATIBLE_2(OtherRange1, OtherRange2, DerivedRange)
                RTC_DEBUG_BOOL sum(const OtherRange1& a, const OtherRange2& b)
            {
                RTC_DEBUG_RETURN_FALSE_IF(a.dimensions() != dimensions() || b.dimensions() != dimensions());

                std::transform(a.startIt(), a.endIt(), b.startIt(), startIt(), std::plus<typename DerivedRange::value_type>());

                RTC_DEBUG_RETURN_TRUE;
            }

            RTC_ENABLE_IF_COMPATIBLE(OtherRange, DerivedRange)
                RTC_DEBUG_BOOL multiply(const OtherRange& other)
            {
                RTC_DEBUG_RETURN_FALSE_IF(other.dimensions() != dimensions());

                std::transform(startIt(), endIt(), other.startIt(), startIt(), std::multiplies<typename DerivedRange::value_type>());
                    
                RTC_DEBUG_RETURN_TRUE;
            }

            RTC_ENABLE_IF_COMPATIBLE(OtherRange, DerivedRange)
                RTC_DEBUG_BOOL shiftAndAdd(const OtherRange& other, size_t shift)
            {
                RTC_DEBUG_RETURN_FALSE_IF(other.dimensions() != dimensions());

                if constexpr (Dimensions == 1)
                {
                    std::transform(startIt() + shift, endIt(), other.startIt(), startIt(), std::plus<typename DerivedRange::value_type>());
                    std::copy(other.endIt() - shift, other.endIt(), endIt() - shift);
                }
                else
                {
                    auto it = other.begin();

                    for (auto& x : *static_cast<DerivedRange*>(this))
                    {
                        RTC_DEBUG_ASSUME(x.shiftAndAdd(*it++, shift));
                    }
                }

                RTC_DEBUG_RETURN_TRUE;
            }

            RTC_ENABLE_IF_COMPATIBLE_2(OtherRange1, OtherRange2, DerivedRange)
                RTC_DEBUG_BOOL multiplyAndAdd(const OtherRange1& a, const OtherRange2& b)
            {
                RTC_DEBUG_RETURN_FALSE_IF(a.dimensions() != dimensions() || b.dimensions() != dimensions());

                std::transform(a.startIt(), a.endIt(), b.startIt(), addIt(), std::multiplies<typename DerivedRange::value_type>());
                    
                RTC_DEBUG_RETURN_TRUE;
            }

        private:
            template<typename, typename> friend struct ElementTraits;

            auto startIt() { return Iterator<typename DerivedRange::value_type>(data()); }
            auto startIt() const { return Iterator<const typename DerivedRange::value_type>(data()); }
            auto endIt() { return Iterator<typename DerivedRange::value_type>(data() + dataSize()); }
            auto endIt() const { return Iterator<const typename DerivedRange::value_type>(data() + dataSize()); }
            auto addIt() { return AddIterator<typename DerivedRange::value_type>(data()); }

            auto data() { return derived().data(); }
            auto data() const { return derived().data(); }
            auto dimensions() const { return derived().dimensions(); }
            auto dataSize() const { return dimensions().totalSize(); }
        };
    };
}
