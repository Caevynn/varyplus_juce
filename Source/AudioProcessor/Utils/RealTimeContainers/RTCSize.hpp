/*
  ==============================================================================

    RTCSize.hpp
    Created: 1 Nov 2023 5:02:14pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "RTCArrayHelpers.hpp"
#include "RTCTemplateHelpers.hpp"

namespace RTC
{
    template<template<typename...> typename ...Interfaces>
    class Size
    {
        static constexpr size_t Dimensions = sizeof...(Interfaces);

    public:
        Size() { init(data); }
        Size(const std::array<size_t, Dimensions>& data) : data(data) {}
        template<template<typename...> typename Interface>
        Size(const Size<Interface, Interfaces...>& other) { copyPartial<1, Dimensions + 1>(other.data, data); }
        
        size_t operator[](size_t dimension) const { return data[dimension]; }
        bool operator==(const Size& other) const { return same(data, other.data); }
        bool operator!=(const Size& other) const { return !same(data, other.data); }
        
        size_t totalSize() const { return product(data); }
        size_t stepSize() const { return productPartial<1, Dimensions>(data); }

        template<template<typename...> typename ...OtherInterfaces,
            std::enable_if_t<sizeof...(OtherInterfaces) == Dimensions, bool> = true>
        static Size reinterpret(const Size<OtherInterfaces...>&other)
        {
            Size size;
            copy(other.data, size.data);
            return size;
        }

        [[nodiscard]] Size resized(size_t newSize) const
        {
            Size resizedArray(*this);
            resizedArray.data[0] = newSize;
            return resizedArray;
        }

        template<template<typename...> typename Interface>
        [[nodiscard]] Size<Interfaces..., Interface> with(size_t nElements) const
        {
            Size<Interfaces..., Interface> expandedArray;
            copyPartial<0, Dimensions>(data, expandedArray.data);
            expandedArray.data[Dimensions] = nElements;
            return expandedArray;
        }

    private:
        template<template<typename...> typename...> friend class Size;
        std::array<size_t, Dimensions> data;
    };

    // Deduction guide for constructor
    template<template<typename...> typename...> Size() -> Size<>;
}
