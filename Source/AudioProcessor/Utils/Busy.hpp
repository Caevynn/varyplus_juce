/*
  ==============================================================================

    Busy.hpp
    Created: 21 Oct 2023 5:27:31pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

class Busy
{
public:
    static void ms(unsigned milliseconds)
    {
        for (volatile unsigned i = 0; i < milliseconds * 1700; ++i)
        {
            for (volatile unsigned j = 0; j < 256; ++j)
            {
                // Busy of approximately n * 100 us
            }
        }
    }

    static void s(unsigned seconds)
    {
        ms(seconds * 1000);
    }
};
