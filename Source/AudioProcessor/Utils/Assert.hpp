/*
  ==============================================================================

    Assert.hpp
    Created: 24 Oct 2023 4:57:29pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <cassert>

#ifndef NDEBUG
#   define ASSERT(expression) assert(expression)
#   define ASSUME(expression) assert(expression == true)
#   define UNUSED(expression) expression
#else
#   define ASSERT(expression) ((void)0)
#   define ASSUME(expression) expression
#   define UNUSED(expression) ((void)0)
#endif
