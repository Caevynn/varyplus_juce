/*
  ==============================================================================

    Time.hpp
    Created: 16 Oct 2023 12:53:43pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <chrono>

class Time
{
private:
    using ClockT = std::chrono::steady_clock;
    using TimeT = std::chrono::microseconds;

public:
    using TimePointT = std::chrono::time_point<ClockT>;
    using DurationT = double;

    static TimePointT now() { return ClockT::now(); }
    static DurationT duration(const TimePointT& start, const TimePointT& end) { return static_cast<DurationT>(std::chrono::duration_cast<TimeT>(end - start).count()) / 1000000; }

    class Measurement
    {
    public:
        void setStart() { startPoint = Time::now(); }
        void setEnd() { endPoint = Time::now(); }

        TimePointT getStart() const { return startPoint;  }
        TimePointT getEnd() const { return endPoint; }

        DurationT getDuration() const { return duration(startPoint, endPoint); }
        DurationT getDistance(const Measurement& other) const { return duration(other.getStart(), startPoint); }

    private:
        TimePointT startPoint, endPoint;
    };
};
