/*
  ==============================================================================

    FDCFFilter.hpp
    Created: 29 Jan 2024 6:04:45pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <vector>

#include "FDCFTransferFunction.hpp"
#include "FDCFOverlapAdd.hpp"
#include "FDCFSpectrumBuffer.hpp"
#include "../AudioFilterBase.hpp"
#include "../Utils/Assert.hpp"

namespace FDCF
{
    class Filter : public AudioFilterBase
    {
    public:
        Filter(unsigned sampleRate, unsigned blockSize, const ImpulseResponse& impulseResponse) :
            transferFunction(impulseResponse, blockSize)
        {
            size_t nChannels = impulseResponse.nChannels();
            ASSERT(nChannels > 0);

            size_t nSegments = transferFunction.getData().nSegments();

            overlapAdd.allocate(nChannels, blockSize);
            spectrumBuffer.allocate(nChannels, nSegments, overlapAdd.getSpectrumSize());

            tmpSpectrum.allocate(RTC::Size()
                .with<Channels>(nChannels)
                .with<Samples>(overlapAdd.getSpectrumSize()));

            requiredInputChannels = nChannels;
            requiredOutputChannels = nChannels;
            requiredBlockSize = blockSize;
            requiredSampleRate = sampleRate;
        }

        virtual ~Filter()
        {
            overlapAdd.deallocate();
            spectrumBuffer.deallocate();
            tmpSpectrum.deallocate();
        }

        virtual void run(AudioSignalWithChannels::const_reference input, AudioSignalWithChannels::reference output)
        {
            ASSERT(input.nChannels() == requiredInputChannels);
            ASSERT(input.channel(0).nSamples() == requiredBlockSize);
            ASSERT(output.nChannels() == requiredOutputChannels);
            ASSERT(output.channel(0).nSamples() == requiredBlockSize);

            overlapAdd.input(input, tmpSpectrum);
            spectrumBuffer.push(tmpSpectrum);
            spectrumBuffer.convolve(transferFunction, tmpSpectrum);
            overlapAdd.output(tmpSpectrum, output);
        }

    private:
        OverlapAdd overlapAdd;
        SpectrumBuffer spectrumBuffer;
        AudioSpectrumWithChannels tmpSpectrum;
        
        TransferFunction transferFunction;
    };
}
