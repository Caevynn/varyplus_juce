/*
  ==============================================================================

    FDCFTransferFunction.hpp
    Created: 29 Jan 2024 6:14:37pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "FDCFTypes.hpp"
#include "../Tools/RealFFT.hpp"

namespace FDCF
{
    class TransferFunction
    {
    public:
        TransferFunction(const ImpulseResponse& impulseResponse, size_t blockSize)
        {
            size_t numChannels = impulseResponse.nChannels();
            ASSERT(numChannels > 0);
            size_t numSamples = impulseResponse.channel(0).nSamples();
            ASSERT(numSamples > 0);
            size_t numSegments = numSamples / blockSize + (numSamples % blockSize > 0 ? 1 : 0);
            ASSERT(numSegments > 0);

            RealFFT fft;
            fft.allocate(2 * blockSize);
            data.allocate(RTC::Size()
                .with<Segments>(numSegments)
                .with<Channels>(numChannels)
                .with<Samples>(fft.spectrumLength()));

            const auto& ir = impulseResponse;
            auto& tf = data;

            size_t segmentStart = 0;
            for (auto& tfSegment : tf)
            {
                auto irChannel = ir.begin();
                for (auto& tfChannel : tfSegment)
                {
                    fft.forward(irChannel->range(segmentStart, blockSize), tfChannel);

                    ++irChannel;
                }
                segmentStart += blockSize;
            }

            fft.deallocate();
        }

        TransferFunction(TransferFunction&&) = default;

        ~TransferFunction()
        {
            data.deallocate();
        }

        const TransferFunctionData& getData() const { return data; }
        
    private:
        TransferFunctionData data;
    };
}
