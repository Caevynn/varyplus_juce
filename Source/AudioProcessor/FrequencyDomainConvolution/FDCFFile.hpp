/*
  ==============================================================================

    FDCFFile.hpp
    Created: 29 Jan 2024 6:04:32pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <vector>
#include <string>
#include <optional>

#include "FDCFTypes.hpp"
#include "FDCFFilter.hpp"

// Declare here to allow friend class outside namepsace
class SofaFileLoader;

namespace FDCF
{
    class File
    {
    public:
        class LoaderBase
        {
        public:
            virtual ~LoaderBase() = default;
            virtual FDCF::File loadFDCF(const std::string& filename) = 0;
        };

        File(const std::string& filename) : filename(filename) {}
        File(File&&) = default;
        ~File() { impulseResponse.deallocate(); }

        std::unique_ptr<Filter> makeFilter([[maybe_unused]] unsigned wantedSampleRate, unsigned wantedBlockSize)
        {
            if (isTest())
            {
                if (filename == "dirac.testmasterfilter")
                {
                    impulseResponse.allocate(RTC::Size()
                        .with<Channels>(2)
                        .with<Samples>(1));

                    impulseResponse.channel(0).sample(0) = 1;
                    impulseResponse.channel(1).sample(0) = 1;
                }
                else if (filename == "longdirac.testmasterfilter")
                {
                    impulseResponse.allocate(RTC::Size()
                        .with<Channels>(2)
                        .with<Samples>(10 * wantedSampleRate));

                    impulseResponse.channel(0).sample(0) = 1;
                    impulseResponse.channel(1).sample(0) = 1;
                }
                else if (filename == "delay.testmasterfilter")
                {
                    impulseResponse.allocate(RTC::Size()
                        .with<Channels>(2)
                        .with<Samples>(wantedSampleRate / 2 + 1));

                    impulseResponse.channel(0).sample(0) = 1;
                    impulseResponse.channel(1).sample(wantedSampleRate / 2) = 0.25;
                }
                else if (filename == "lowpass.testmasterfilter")
                {
                    AudioSignal sinc;
                    Sinc::createWithAllocation(sinc, wantedSampleRate, 1000, 16);

                    AudioValueType sum = 0;
                    for (const auto& sample : sinc) { sum += sample; }
                    for (auto& sample : sinc) { sample /= sum; }

                    impulseResponse.allocate(RTC::Size()
                        .with<Channels>(2)
                        .with<Samples>(sinc.nSamples()));

                    ASSUME(impulseResponse.channel(0).copy(sinc));
                    ASSUME(impulseResponse.channel(1).copy(sinc));

                    sinc.deallocate();
                }
                else
                {
                    error = "Invalid test master filter";
                }
            }

            if (!isValid())
            {
                return std::unique_ptr<Filter>();
            }

            return std::make_unique<Filter>(sampleRate, wantedBlockSize, impulseResponse);
        }

        bool isValid() const { return !error; }
        bool isTest() const { return !error && impulseResponse.nChannels() == 0; }
        const std::string& getFilename() const { return filename; }
        const std::optional<std::string>& getErrorMsg() const { return error; }

        size_t getNumChannels() const { return nChannels; }
        size_t getNumSamples() const { return nSamples; }
        unsigned getSampleRate() const { return sampleRate; }

        const ImpulseResponse& getImpulseResponse() const { return impulseResponse; }

    private:
        friend class ::SofaFileLoader;

        File() = default;

        std::string filename;
        std::optional<std::string> error;

        size_t nChannels{ 0 }, nSamples{ 0 };
        unsigned sampleRate{ 0 };

        ImpulseResponse impulseResponse;
    };
}
