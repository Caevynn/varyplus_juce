/*
  ==============================================================================

    FDCFTypes.hpp
    Created: 29 Jan 2024 6:11:48pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioTypes.hpp"

namespace FDCF
{
    using ImpulseResponse = RTC::Array<AudioValueType, Channels, Samples>;
    using TransferFunctionData = RTC::Array<RTC::Complex<AudioValueType>, Segments, Channels, Samples>;
    using SpectrumBufferData = RTC::Array<RTC::Complex<AudioValueType>, Segments, Channels, Samples>;
}
