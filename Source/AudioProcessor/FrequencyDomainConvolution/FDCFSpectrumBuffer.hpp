/*
  ==============================================================================

    FDCFSpectrumBuffer.hpp
    Created: 29 Jan 2024 6:24:33pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "FDCFTransferFunction.hpp"

namespace FDCF
{
    class SpectrumBuffer
    {
    public:
        void allocate(size_t nChannels, size_t nSegments, size_t spectrumSize)
        {
            ASSERT(nChannels > 0);
            ASSERT(nSegments > 0);
            ASSERT(spectrumSize > 0);

            data.allocate(RTC::Size()
                .with<Segments>(nSegments)
                .with<Channels>(nChannels)
                .with<Samples>(spectrumSize));
        }

        void deallocate()
        {
            data.deallocate();
        }

        void push(AudioSpectrumWithChannels::const_reference newSpectrum)
        {
            moveIndex();
            
            UNUSED(size_t nChannels = data.segment(0).nChannels());
            UNUSED(size_t nSamples = data.segment(0).channel(0).nSamples());
            ASSERT(newSpectrum.nChannels() == nChannels);
            ASSERT(newSpectrum.channel(0).nSamples() == nSamples);
            
            ASSUME(data.segment(currentSegment).copy(newSpectrum));
        }

        void convolve(const TransferFunction& transferFunction, AudioSpectrumWithChannels::reference output)
        {
            output.setToZero();

            UNUSED(size_t nChannels = data.segment(0).nChannels());
            UNUSED(size_t nSegments = data.nSegments());
            UNUSED(size_t nSamples = data.segment(0).channel(0).nSamples());
            ASSERT(output.nChannels() == nChannels);
            ASSERT(output.channel(0).nSamples() == nSamples);
            ASSERT(transferFunction.getData().nSegments() == nSegments);
            ASSERT(transferFunction.getData().segment(0).nChannels() == nChannels);
            ASSERT(transferFunction.getData().segment(0).channel(0).nSamples() == nSamples);

            const auto& tf = transferFunction.getData();
            auto& sb = data;

            auto tfIt = tf.begin();
            for (auto sbIt = sb.begin() + currentSegment; sbIt != sb.end(); ++sbIt, ++tfIt)
            {
                ASSUME(output.multiplyAndAdd(*tfIt, *sbIt));
            }
            for (auto sbIt = sb.begin(); sbIt != sb.begin() + currentSegment; ++sbIt, ++tfIt)
            {
                ASSUME(output.multiplyAndAdd(*tfIt, *sbIt));
            }
        }

    private:
        size_t currentSegment{ 0 };
        SpectrumBufferData data;

        void moveIndex()
        {
            size_t numSegments = data.nSegments();
            currentSegment = (currentSegment + numSegments - 1) % numSegments;
        }
    };
}
