/*
  ==============================================================================

    FDCFOverlapAdd.hpp
    Created: 29 Jan 2024 6:40:02pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioTypes.hpp"
#include "../Tools/HannWindow.hpp"
#include "../Tools/RealFFT.hpp"
#include "../Utils/Assert.hpp"

namespace FDCF
{
    class OverlapAdd
    {
    public:
        void allocate(size_t nChannels, size_t blockSize)
        {
            fftOutput.allocate(RTC::Size()
                .with<Channels>(nChannels)
                .with<Samples>(2 * blockSize));
            overlappingOutput.allocate(RTC::Size()
                .with<Channels>(nChannels)
                .with<Samples>(2 * blockSize));

            fft.allocate(2 * blockSize);
        }

        void deallocate()
        {
            fftOutput.deallocate();
            overlappingOutput.deallocate();
            fft.deallocate();
        }

        void input(AudioSignalWithChannels::const_reference input, AudioSpectrumWithChannels::reference output)
        {
            size_t nChannels = fftOutput.nChannels();

            ASSERT(input.nChannels() == nChannels);
            ASSERT(input.channel(0).nSamples() == fft.signalLength() / 2);
            ASSERT(output.nChannels() == nChannels);
            ASSERT(output.channel(0).nSamples() == getSpectrumSize());

            for (size_t c = 0; c < nChannels; ++c)
            {
                fft.forward(input.channel(c), output.channel(c));
            }
        }

        void output(AudioSpectrumWithChannels::const_reference input, AudioSignalWithChannels::reference output)
        {
            size_t nChannels = fftOutput.nChannels();
            size_t blockSize = fft.signalLength() / 2;

            ASSERT(input.nChannels() == nChannels);
            ASSERT(input.channel(0).nSamples() == getSpectrumSize());
            ASSERT(output.nChannels() == nChannels);
            ASSERT(output.channel(0).nSamples() == blockSize);

            for (size_t i = 0; i < nChannels; ++i)
            {
                fft.inverse(input.channel(i), fftOutput.channel(i));
                ASSUME(overlappingOutput.channel(i).shiftAndAdd(fftOutput.channel(i), blockSize));
                ASSUME(output.channel(i).copy(overlappingOutput.channel(i).range(0, blockSize)));
            }
        }

        size_t getSpectrumSize() const { return fft.spectrumLength(); }

    private:
        AudioSignalWithChannels fftOutput;
        AudioSignalWithChannels overlappingOutput;

        RealFFT fft;
    };
}
