/*
  ==============================================================================

    AudioPlayer.hpp
    Created: 24 Oct 2023 5:15:12pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <optional>
#include <variant>

#include "AudioSource.hpp"
#include "AudioTypes.hpp"
#include "Utils/Assert.hpp"
#include "Utils/VariantHelpers.hpp"
#include "Utils/RealTimeContainers/RTCVector.hpp"
#include "Utils/RealTimeContainers/RTCUniquePointer.hpp"

class AudioPlayer
{
    using AudioSourceVector = RTC::Vector<RTC::UniquePointer<AudioSource>>;

public:
    struct Command
    {
        struct Volume { double volume; Volume(double volume) : volume(volume) {} };
        struct Position { size_t position; Position(size_t position) : position(position) {} };
        struct Source { std::optional<size_t> source; Source(std::optional<size_t> source) : source(source) {} };
        struct Bus { std::optional<size_t> bus; Bus(std::optional<size_t> bus) : bus(bus) {} };
        struct BusFade { std::optional<size_t> bus; double outTime; double pauseTime; double inTime; BusFade(std::optional<size_t> bus, double outTime, double pauseTime, double inTime) : bus(bus), outTime(outTime), pauseTime(pauseTime), inTime(inTime) {} };
        struct BusCrossfade { std::optional<size_t> bus; double time; BusCrossfade(std::optional<size_t> bus, double time) : bus(bus), time(time) {} };
        struct Play {};
        struct Pause {};
        struct Stop {};
        struct Loop { bool enable; Loop(bool enable) : enable(enable) {} };

        using Variant = std::variant<std::monostate, Volume, Position, Source, Bus, BusFade, BusCrossfade, Play, Pause, Stop, Loop>;

        Command() = default;
        Command(std::optional<size_t> iChannel, Variant command) : iChannel(iChannel), variant(command) {}

        std::optional<size_t> iChannel;
        Variant variant;
    };

    struct FadeRequest
    {
        enum class Type
        {
            Sequential,
            Concurrent,
        };

        std::optional<size_t> target;
        double outTime;
        double pauseTime;
        double inTime;
        Type type;

        FadeRequest(std::optional<size_t> target, double outTime, double pauseTime, double inTime, Type type) :
            target(target), outTime(outTime), pauseTime(pauseTime), inTime(inTime), type(type)
        {}
    };

    struct FadeState
    {
        enum class Type
        {
            In,
            Out,
            Pause,
            Cross,
            Done
        };

        std::optional<size_t> origin;
        std::optional<size_t> target;
        size_t position{ 0 };
        size_t fadeOutLength{ 0 };
        size_t pauseLength{ 0 };
        size_t fadeInLength{ 0 };
        Type type{ Type::Done };

        void reset()
        {
            origin = std::nullopt;
            target = std::nullopt;
            position = 0;
            fadeOutLength = 0;
            pauseLength = 0;
            fadeInLength = 0;
            type = Type::Done;
        }

        bool isActive() const { return type != Type::Done; }
    };

    struct State
    {
        std::optional<size_t> source;
        std::optional<size_t> bus;
        size_t position{ 0 };
        double volume{ 1 };
        bool isPlaying{ false };
        bool isLooping{ false };

        std::optional<FadeRequest> fadeRequest;

        void reset()
        {
            source = std::nullopt;
            bus = std::nullopt;
            position = 0;
            volume = 1;
            isPlaying = false;
            isLooping = false;

            fadeRequest = std::nullopt;
        }

        void applyCommand(const Command& command)
        {
            std::visit(VariantHelpers::overloaded{
                [](auto) {},
                [this](Command::Volume command) { volume = command.volume; },
                [this](Command::Position command) { position = command.position; },
                [this](Command::Source command)
                {
                    if (source != command.source)
                    {
                        source = command.source;
                        isPlaying = false;
                        position = 0;
                    }
                },
                [this](Command::Bus command) { bus = command.bus; },
                [this](Command::BusFade command)
                {
                    fadeRequest = FadeRequest(command.bus, command.outTime, command.pauseTime, command.inTime, FadeRequest::Type::Sequential);
                },
                [this](Command::BusCrossfade command)
                {
                    fadeRequest = FadeRequest(command.bus, command.time, 0, 0, FadeRequest::Type::Concurrent);
                },
                [this](Command::Play)
                {
                    if (source)
                    {
                        isPlaying = true;
                    }
                },
                [this](Command::Pause) { isPlaying = false; },
                [this](Command::Stop)
                {
                    isPlaying = false;
                    position = 0;
                },
                [this](Command::Loop command) { isLooping = command.enable; },
                }, command.variant);
        }
    };

    void allocate(size_t wantedChannels, unsigned wantedSampleRate, size_t wantedBlockSize)
    {
        sampleRate = wantedSampleRate;
        blockSize = wantedBlockSize;

        oldState.reset();
        newState.reset();
        fadeState.reset();

        readBuffer.allocate(RTC::Size()
            .with<Channels>(wantedChannels)
            .with<Samples>(wantedBlockSize));
    }

    void deallocate()
    {
        readBuffer.deallocate();
    }

    const State& read(const AudioSourceVector& sources, AudioSignalBuses& buses)
    {
        readFromSource(sources);

        checkForFadeRequest(buses);

        if (fadeState.isActive())
        {
            writeToBusFading(buses);
        }
        else
        {
            writeToBus(buses);
        }

        updateState(sources);

        return newState;
    }

    void applyCommand(const Command& command)
    {
        newState.applyCommand(command);
    }

    void notifySourceRemoved(size_t index)
    {
        if (newState.source)
        {
            if (*newState.source == index) { newState.source = std::nullopt; newState.isPlaying = false; }
            else if (*newState.source > index) { --*newState.source; }
        }

        if (oldState.source)
        {
            if (*oldState.source == index) { oldState.source = std::nullopt; oldState.isPlaying = false; }
            else if (*oldState.source > index) { --*oldState.source; }
        }
    }

    void notifyBusRemoved(size_t index, size_t numDynamicBuses = std::numeric_limits<size_t>::max())
    {
        if (newState.bus)
        {
            if (*newState.bus == index) { newState.bus = std::nullopt; }
            else if (*newState.bus > index && *newState.bus < numDynamicBuses) { --* newState.bus; }
        }

        if (oldState.bus)
        {
            if (*oldState.bus == index) { oldState.bus = std::nullopt; }
            else if (*oldState.bus > index && *oldState.bus < numDynamicBuses) { --*oldState.bus ; }
        }
    }

private:
    unsigned sampleRate;
    size_t blockSize;

    State oldState, newState;
    FadeState fadeState;
    AudioSignalWithChannels readBuffer;

    void checkForFadeRequest(const AudioSignalBuses& buses)
    {
        if (!fadeState.isActive() && newState.fadeRequest)
        {
            auto fadeRequest = *newState.fadeRequest;

            if (oldState.isPlaying &&
                newState.isPlaying &&
                oldState.bus != fadeRequest.target &&
                (isBusValid(oldState.bus, buses) ||
                isBusValid(fadeRequest.target, buses)))
            {
                size_t fadeOutLength = static_cast<size_t>(fadeRequest.outTime * sampleRate);
                size_t roundedFadeOutLength = std::max(fadeOutLength - (fadeOutLength % blockSize), blockSize);
                size_t pauseLength = static_cast<size_t>(fadeRequest.pauseTime * sampleRate);
                size_t roundedPauseLength = pauseLength - (pauseLength % blockSize);
                size_t fadeInLength = static_cast<size_t>(fadeRequest.inTime * sampleRate);
                size_t roundedFadeInLength = std::max(fadeInLength - (fadeInLength % blockSize), blockSize);

                fadeState.origin = oldState.bus;
                fadeState.target = fadeRequest.target;
                fadeState.position = 0;
                fadeState.fadeOutLength = roundedFadeOutLength;
                fadeState.pauseLength = roundedPauseLength;
                fadeState.fadeInLength = roundedFadeInLength;

                switch (fadeRequest.type)
                {
                case FadeRequest::Type::Concurrent:
                    if (isBusValid(fadeState.origin, buses))
                    {
                        if (isBusValid(fadeRequest.target, buses))
                        {
                            fadeState.type = FadeState::Type::Cross;
                        }
                        else
                        {
                            fadeState.type = FadeState::Type::Out;
                        }
                    }
                    else
                    {
                        fadeState.type = FadeState::Type::In;
                    }
                    break;

                case FadeRequest::Type::Sequential:
                    if (isBusValid(fadeState.origin, buses))
                    {
                        fadeState.type = FadeState::Type::Out;
                    }
                    else
                    {
                        fadeState.type = FadeState::Type::In;
                    }
                    break;

                default: ASSERT(false); break;
                }
            }

            newState.bus = fadeRequest.target;
            newState.fadeRequest = std::nullopt;
        }
    }

    void readFromSource(const AudioSourceVector& sources)
    {
        if (isSourceValid(oldState.source, sources) && oldState.isPlaying)
        {
            // Ensure that the player is stopped if the source is being changed
            ASSERT(!newState.isPlaying || newState.source == oldState.source);

            sources[*oldState.source]->read(oldState.position, oldState.isLooping, readBuffer);
        }
        else if (isSourceValid(newState.source, sources) && newState.isPlaying)
        {
            sources[*newState.source]->read(newState.position, newState.isLooping, readBuffer);
        }
    }

    void writeToBus(AudioSignalBuses& buses)
    {
        if (hasBusChanged() && !isStartingFromZero())
        {
            if (isBusValid(oldState.bus, buses) && oldState.isPlaying)
            {
                ASSUME(buses.bus(*oldState.bus).addFading(readBuffer, oldState.volume, 0));
            }

            if (isBusValid(newState.bus, buses) && newState.isPlaying)
            {
                ASSUME(buses.bus(*newState.bus).addFading(readBuffer, 0, newState.volume));
            }
        }
        else
        {
            if (isBusValid(newState.bus, buses) && newState.isPlaying)
            {
                ASSUME(buses.bus(*newState.bus).addFading(readBuffer, oldState.volume, newState.volume));
            }
        }
    }

    void writeToBusFading(AudioSignalBuses& buses)
    {
        ASSERT(fadeState.isActive());

        switch (fadeState.type)
        {
        case FadeState::Type::In:
        {
            ASSERT(isBusValid(fadeState.target, buses));

            double startPosition = static_cast<double>(fadeState.position) / fadeState.fadeInLength;
            double endPosition = startPosition + static_cast<double>(blockSize - 1) / fadeState.fadeInLength;
            double fadeInStartVolume = startPosition * oldState.volume;
            double fadeInEndVolume = endPosition * newState.volume;

            if (!newState.isPlaying)
            {
                ASSUME(buses.bus(*fadeState.target).addFading(readBuffer, fadeInStartVolume, 0));

                fadeState.type = FadeState::Type::Done;
            }
            else
            {
                ASSUME(buses.bus(*fadeState.target).addFading(readBuffer, fadeInStartVolume, fadeInEndVolume));

                fadeState.position += blockSize;

                if (fadeState.position >= fadeState.fadeInLength)
                {
                    fadeState.type = FadeState::Type::Done;
                }
            }
        }
        break;

        case FadeState::Type::Out:
        {
            ASSERT(isBusValid(fadeState.origin, buses));

            double startPosition = static_cast<double>(fadeState.position) / fadeState.fadeOutLength;
            double endPosition = startPosition + static_cast<double>(blockSize - 1) / fadeState.fadeOutLength;
            double fadeOutStartVolume = (1 - startPosition) * oldState.volume;
            double fadeOutEndVolume = (1 - endPosition) * newState.volume;

            if (!newState.isPlaying)
            {
                ASSUME(buses.bus(*fadeState.origin).addFading(readBuffer, fadeOutStartVolume, 0));

                fadeState.type = FadeState::Type::Done;
            }
            else
            {
                ASSUME(buses.bus(*fadeState.origin).addFading(readBuffer, fadeOutStartVolume, fadeOutEndVolume));

                fadeState.position += blockSize;

                if (fadeState.position >= fadeState.fadeOutLength)
                {
                    if (isBusValid(fadeState.target, buses) && newState.isPlaying)
                    {
                        fadeState.position = 0;

                        if (fadeState.pauseLength > 0)
                        {
                            fadeState.type = FadeState::Type::Pause;
                        }
                        else
                        {
                            fadeState.type = FadeState::Type::In;
                        }
                    }
                    else
                    {
                        fadeState.type = FadeState::Type::Done;
                    }
                }
            }
        }
        break;

        case FadeState::Type::Pause:
        {
            if (!newState.isPlaying)
            {
                fadeState.type = FadeState::Type::Done;
            }
            else
            {
                fadeState.position += blockSize;

                if (fadeState.position >= fadeState.pauseLength)
                {
                    fadeState.position = 0;
                    fadeState.type = FadeState::Type::In;
                }
            }
        }
        break;

        case FadeState::Type::Cross:
        {
            ASSERT(isBusValid(fadeState.origin, buses));
            ASSERT(isBusValid(fadeState.target, buses));

            double startPosition = static_cast<double>(fadeState.position) / fadeState.fadeOutLength;
            double endPosition = startPosition + static_cast<double>(blockSize - 1) / fadeState.fadeOutLength;
            double fadeOutStartVolume = (1 - startPosition) * oldState.volume;
            double fadeOutEndVolume = (1 - endPosition) * newState.volume;
            double fadeInStartVolume = startPosition * oldState.volume;
            double fadeInEndVolume = endPosition * newState.volume;

            if (!newState.isPlaying)
            {
                ASSUME(buses.bus(*fadeState.origin).addFading(readBuffer, fadeOutStartVolume, 0));
                ASSUME(buses.bus(*fadeState.target).addFading(readBuffer, fadeInStartVolume, 0));

                fadeState.type = FadeState::Type::Done;
            }
            else
            {
                ASSUME(buses.bus(*fadeState.origin).addFading(readBuffer, fadeOutStartVolume, fadeOutEndVolume));
                ASSUME(buses.bus(*fadeState.target).addFading(readBuffer, fadeInStartVolume, fadeInEndVolume));

                fadeState.position += blockSize;

                if (fadeState.position >= fadeState.fadeOutLength)
                {
                    fadeState.type = FadeState::Type::Done;
                }
            }
        }
        break;

        default: ASSERT(false); break;
        }
    }

    void updateState(const AudioSourceVector& sources)
    {
        if (newState.isPlaying &&
            fadeState.type != FadeState::Type::Pause &&
            isSourceValid(newState.source, sources))
        {
            newState.position += readBuffer.channelSize();

            if (auto sourceLength = sources[*newState.source]->getLength())
            {
                if (newState.position >= *sourceLength)
                {
                    if (newState.isLooping)
                    {
                        newState.position -= *sourceLength;
                    }
                    else
                    {
                        // Stop playback if file ended
                        newState.isPlaying = false;
                        newState.position = 0;
                    }
                }
            }
        }

        oldState = newState;
    }

    bool isSourceValid(std::optional<size_t>& iSource, const AudioSourceVector& sources) const
    {
        return iSource
            && iSource < sources.size();
    }

    bool isBusValid(std::optional<size_t>& iBus, const AudioSignalBuses& buses) const
    {
        return iBus
            && iBus < buses.nBuses();
    }

    bool hasBusChanged() const
    {
        return oldState.isPlaying != newState.isPlaying
            || oldState.bus != newState.bus;
    }

    bool isStartingFromZero() const
    {
        return !oldState.isPlaying
            && newState.isPlaying
            && newState.position == 0;
    }
};
