/*
  ==============================================================================

    AudioSourceBase.hpp
    Created: 24 Oct 2023 2:55:54pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <optional>

#include "AudioTypes.hpp"
#include "Utils/Assert.hpp"

class AudioSource
{
public:
    AudioSource() = default;
    AudioSource(const AudioSource&) = delete;
    AudioSource& operator=(const AudioSource&) = delete;

    AudioSource(unsigned sampleRate, AudioSignalWithChannels::const_reference signal)
    {
        ASSERT(signal.nChannels() != 0);
        ASSERT(signal.channel(0).nSamples() != 0);

        data.allocate(RTC::Size()
            .with<Channels>(signal.nChannels())
            .with<Samples>(signal.channel(0).nSamples()));

        ASSUME(data.copy(signal));

        requiredChannels = signal.nChannels();
        requiredSampleRate = sampleRate;
    }

    virtual ~AudioSource()
    {
        data.deallocate();
    }

    virtual size_t read(size_t position, bool looped, AudioSignalWithChannels::reference output)
    {
        ASSERT(output.nChannels() == data.nChannels());

        size_t nRequestedSamples = output.channelSize();

        if (position + nRequestedSamples <= data.channelSize())
        {
            auto dataIt = data.begin();
            for (auto& channel : output)
            {
                ASSUME(channel.copy(dataIt->range(position, nRequestedSamples)));
                ++dataIt;
            }
            return nRequestedSamples;
        }
        else
        {
            size_t limitedPosition = std::min(position, data.channelSize());
            size_t nSamplesToCopy = data.channelSize() - limitedPosition;
            size_t nRemainingSamples = nRequestedSamples - nSamplesToCopy;

            auto dataIt = data.begin();
            for (auto& channel : output)
            {
                ASSUME(channel.range(0, nSamplesToCopy).copy(dataIt->range(limitedPosition, nSamplesToCopy)));
                if (looped)
                {
                    ASSUME(channel.range(nSamplesToCopy, nRemainingSamples).copy(dataIt->range(0, nRemainingSamples)));
                }
                else
                {
                    channel.range(nSamplesToCopy, nRemainingSamples).setToZero();
                }
                ++dataIt;
            }
            return nSamplesToCopy;
        }
    }

    virtual std::optional<size_t> getLength() const { return data.channelSize(); }

    bool supports(size_t nChannels, unsigned sampleRate, size_t blockSize) const
    {
        return (!requiredChannels || requiredChannels == nChannels)
            && (!requiredSampleRate || requiredSampleRate == sampleRate)
            && (!requiredBlockSize || requiredBlockSize == blockSize);
    }

protected:
    std::optional<size_t> requiredChannels;
    std::optional<unsigned> requiredSampleRate;
    std::optional<size_t> requiredBlockSize;

private:
    AudioSignalWithChannels data;
};
