/*
  ==============================================================================

    VolumeFilter.hpp
    Created: 15 Jan 2024 5:33:21pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "AudioFilterBase.hpp"

class VolumeFilter : public AudioFilterBase
{
public:
    void run(AudioSignalWithChannels::reference signal, double volume)
    {
        signal.fade(previousVolume, volume);
        previousVolume = volume;
    }

private:
    double previousVolume{ 1 };
};