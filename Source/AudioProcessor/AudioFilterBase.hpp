/*
  ==============================================================================

    AudioFilterBase.hpp
    Created: 4 Nov 2023 2:37:34pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <optional>

#include "AudioTypes.hpp"

class AudioFilterBase
{
public:
    virtual ~AudioFilterBase() = default;

    bool supports(size_t inputChannels, size_t outputChannels, unsigned sampleRate, size_t blockSize) const
    {
        return (!requiredInputChannels || requiredInputChannels == inputChannels)
            && (!requiredOutputChannels || requiredOutputChannels == outputChannels)
            && (!requiredSampleRate || requiredSampleRate == sampleRate)
            && (!requiredBlockSize || requiredBlockSize == blockSize);
    }

protected:
    std::optional<size_t> requiredInputChannels;
    std::optional<size_t> requiredOutputChannels;
    std::optional<unsigned> requiredSampleRate;
    std::optional<size_t> requiredBlockSize;
};
