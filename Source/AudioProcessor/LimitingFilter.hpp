/*
  ==============================================================================

    LimitingVolumeFilter.hpp
    Created: 8 Nov 2023 1:17:31pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "AudioFilterBase.hpp"
#include "AudioTypes.hpp"
#include "Utils/Assert.hpp"

class LimitingFilter : public AudioFilterBase
{
public:
    static constexpr AudioValueType Threshold = AudioValueType{ 0.9 };
    static constexpr AudioValueType Knee = 1 - Threshold;

    struct State
    {
        AudioValueType peak{ 0 };
        AudioValueType compression{ 1 };
    };

    LimitingFilter(unsigned numInputChannels, unsigned sampleRate)
    {
        states.allocate(numInputChannels);
        decayFactor = static_cast<AudioValueType>(std::pow(0.1, 1.0 / sampleRate));

        requiredInputChannels = numInputChannels;
        requiredSampleRate = sampleRate;
    }

    ~LimitingFilter()
    {
        states.deallocate();
    }

    void run(AudioSignalWithChannels::reference signal)
    {
        ASSERT(signal.nChannels() == requiredInputChannels);

        auto stateIt = states.begin();
        for (auto& channel : signal)
        {
            auto& state = *stateIt++;

            for (auto& sample : channel)
            {
                AudioValueType abs = std::abs(sample);

                state.peak = std::max(abs, state.peak * decayFactor);
                state.compression = 1 - (1 - state.compression) * decayFactor;

                if (abs > Threshold)
                {
                    AudioValueType inverse = static_cast<AudioValueType>(std::pow(0.5, (abs - Threshold) / Knee));
                    AudioValueType limited = Threshold + Knee * (1 - inverse);

                    state.compression = std::min(limited / abs, state.compression);
                }

                sample *= state.compression;
            }
        }
    }

    const RTC::Array<State>& getStates() const { return states; }

private:
    RTC::Array<State> states;
    AudioValueType decayFactor;
};
