/*
  ==============================================================================

    AudioPlayerAudioProcessor.hpp
    Created: 24 Oct 2023 7:53:28pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioProcessorBase.hpp"
#include "../AudioPlayer.hpp"
#include "../AudioTypes.hpp"

#include "TestSources.hpp"

template<class Callback>
class AudioPlayerAudioProcessor : public Callback
{
    static_assert(std::is_base_of_v<AudioProcessorBase, Callback>);

public:
    AudioPlayerAudioProcessor(DebugOutput& debugOutput) : Callback(debugOutput) {}

    void process(const AudioSignalWithChannels&, AudioSignalWithChannels& output) override
    {
        for (auto& bus : buses)
        {
            bus.setToZero();
        }
        
        AudioPlayer::Command command;
        while (atomicCommandBuffer.pop(command))
        {
            if (!command.iChannel)
            {
                for (auto& player : audioPlayers)
                {
                    player.applyCommand(command);
                }
            }
            else if(command.iChannel < audioPlayers.size())
            {
                audioPlayers[*command.iChannel].applyCommand(command);
            }
        }

        for (auto& audioPlayer : audioPlayers)
        {
            audioPlayer.read(sources, buses);
        }

        for (size_t iPlayer = 0; iPlayer < NumPlayers; ++iPlayer)
        {
            audioPlayerStates[iPlayer] = audioPlayers[iPlayer].getState();
        }

        atomicStateBuffer.push(audioPlayerStates);

        assert(output.nChannels() >= ChannelsPerBus);

        for (const auto& bus : buses)
        {
            for (size_t iChannel = 0; iChannel < ChannelsPerBus; ++iChannel)
            {
                assert(output.channel(iChannel).size() == bus.channel(iChannel).size());
                
                output.channel(iChannel).add(bus.channel(iChannel));
            }
        }
    }

    void load(const AudioCallback::Settings& settings) override
    {
        for (auto& audioPlayer : audioPlayers)
        {
            audioPlayer.allocate(ChannelsPerBus, settings.bufferSize);
        }

        for (auto& bus : buses)
        {
            bus.allocate(ChannelsPerBus, settings.bufferSize);
        }

        sources.push_back(std::make_unique<ClickSource>(settings.sampleRate));
        sources.push_back(std::make_unique<NoiseSource>());
        sources.push_back(std::make_unique<SineSource>(settings.sampleRate));

        atomicCommandBuffer.push(AudioPlayer::Command(0, AudioPlayer::Command::Source(0)));
        atomicCommandBuffer.push(AudioPlayer::Command(0, AudioPlayer::Command::Bus(0)));
        atomicCommandBuffer.push(AudioPlayer::Command(1, AudioPlayer::Command::Source(1)));
        atomicCommandBuffer.push(AudioPlayer::Command(1, AudioPlayer::Command::Bus(0)));
        atomicCommandBuffer.push(AudioPlayer::Command(2, AudioPlayer::Command::Source(2)));
        atomicCommandBuffer.push(AudioPlayer::Command(2, AudioPlayer::Command::Bus(2)));
        atomicCommandBuffer.push(AudioPlayer::Command(3, AudioPlayer::Command::Source(3)));
        atomicCommandBuffer.push(AudioPlayer::Command(3, AudioPlayer::Command::Bus(3)));

        ASSUME(atomicCommandBuffer.push(AudioPlayer::Command(std::nullopt, AudioPlayer::Command::Play())));
    }

    void unload() override
    {
        for (auto& bus : buses)
        {
            bus.deallocate();
        }

        sources.clearWithDeallocation();
    }

    void errorCallback(const std::string&) override {}

private:
    static constexpr size_t NumPlayers = 4;
    static constexpr size_t MaxNumSources = 4;
    static constexpr size_t NumBuses = 4;
    static constexpr size_t ChannelsPerBus = 2;

    UniquePointerVector<AudioSource, MaxNumSources> sources;
    std::array<AudioSignalWithChannels, NumBuses> buses;

    std::array<AudioPlayer, NumPlayers> audioPlayers;
    std::array<AudioPlayer::State, NumPlayers> audioPlayerStates;

    AtomicRingBuffer<AudioPlayer::Command, 16> atomicCommandBuffer;
    AtomicRingBuffer<std::array<AudioPlayer::State, NumPlayers>> atomicStateBuffer;
};
