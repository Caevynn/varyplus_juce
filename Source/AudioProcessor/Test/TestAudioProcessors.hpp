/*
  ==============================================================================

    TestAudioProcessors.hpp
    Created: 5 Nov 2023 11:09:07pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "AudioPlayerAudioProcessor.hpp"
#include "BusyAudioProcessor.hpp"
#include "TestSourceAudioProcessors.hpp"
