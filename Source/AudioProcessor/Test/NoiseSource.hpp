/*
  ==============================================================================

    NoiseSource.hpp
    Created: 24 Oct 2023 3:05:20pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <random>
#include <algorithm>

#include "../AudioSource.hpp"

class NoiseSource : public AudioSource
{
    using Float = AudioValueType;

public:
    NoiseSource(Float standardDeviation = 0.125) :
        randomDevice(),
        randomGenerator(randomDevice()),
        normalDistribution(0, standardDeviation)
    {}

    size_t read(size_t /*position*/, bool /*looped*/, AudioSignalWithChannels::reference output) override
    {
        for (auto& sample : output.channel(0))
        {
            sample = std::clamp(normalDistribution(randomGenerator), Float{ -1 }, Float{ 1 });
        }

        for (size_t i = 1; i < output.nChannels(); ++i)
        {
            ASSUME(output.channel(i).copy(output.channel(0)));
        }

        return output.channelSize();
    }

    std::optional<size_t> getLength() const override { return std::nullopt; }

private:
    std::random_device randomDevice;
    std::mt19937 randomGenerator;
    std::normal_distribution<Float> normalDistribution;
};
