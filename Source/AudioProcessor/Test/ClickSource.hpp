/*
  ==============================================================================

    ClickSource.hpp
    Created: 24 Oct 2023 4:02:59pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <cmath>

#include "../AudioSource.hpp"
#include "../Tools/Sinc.hpp"

class ClickSource : public AudioSource
{
    using Float = AudioValueType;

public:
    ClickSource(unsigned sampleRate, Float frequency = 1, Float amplitude = 0.5)
    {
        createSinc(signal, sampleRate, frequency, amplitude);

        requiredSampleRate = sampleRate;
    }

    ~ClickSource()
    {
        signal.deallocate();
    }

    size_t read(size_t position, bool /*looped*/, AudioSignalWithChannels::reference output) override
    {
        size_t currentPosition = position % signal.channelSize();

        for (auto& sample : output.channel(0))
        {
            sample = signal.channel(0).sample(currentPosition);

            currentPosition = (currentPosition + 1) % signal.channelSize();
        }

        for(size_t i = 1; i < output.nChannels(); ++i)
        {
            ASSUME(output.channel(i).copy(output.channel(0)));
        }

        return output.channelSize();
    }

    std::optional<size_t> getLength() const override { return std::nullopt; }

    static void createSinc(AudioSignalWithChannels& signalWithChannels, unsigned sampleRate, Float frequency = 1, Float amplitude = 0.5)
    {
        size_t nSamples = static_cast<size_t>(static_cast<Float>(sampleRate) / frequency);

        AudioSignal sinc;
        Sinc::createWithAllocation(sinc, sampleRate, SincFrequency, 4);

        signalWithChannels.allocate(RTC::Size()
            .with<Channels>(1)
            .with<Samples>(nSamples));

        ASSUME(signalWithChannels.channel(0).range(0, sinc.nSamples()).addScaled(sinc, amplitude));

        sinc.deallocate();
    }

private:
    static constexpr double SincFrequency = 1000;

    AudioSignalWithChannels signal;
};
