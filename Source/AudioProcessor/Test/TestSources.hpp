/*
  ==============================================================================

    TestSources.hpp
    Created: 24 Oct 2023 5:15:44pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "ClickSource.hpp"
#include "NoiseSource.hpp"
#include "SineSource.hpp"
