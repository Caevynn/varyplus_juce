/*
  ==============================================================================

    BusyAudioProcessor.hpp
    Created: 24 Oct 2023 5:39:42pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioProcessorBase.hpp"
#include "../../Utils/Busy.hpp"

template<class Callback>
class BusyAudioProcessor : public Callback
{
    static_assert(std::is_base_of_v<AudioProcessorBase, Callback>);

public:
    BusyAudioProcessor(DebugOutput& debugOutput) : AudioDeviceCallback(debugOutput) {}

    void process(const AudioSignalWithChannels&, AudioSignalWithChannels&) override { Busy::ms(1); }
    void load(const AudioCallback::Settings&) override { Busy::s(1); }
    void unload() override {}
    void errorCallback(const std::string&) override {}
};
