/*
  ==============================================================================

    SineSource.hpp
    Created: 24 Oct 2023 3:23:45pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <cmath>

#include "../AudioSource.hpp"
#include "../Utils/Assert.hpp"

class SineSource : public AudioSource
{
    using Float = AudioValueType;

public:
    SineSource(unsigned sampleRate, Float frequency = 200, Float amplitude = 0.25) :
        step(frequency / sampleRate),
        amplitude(amplitude)
    {
        requiredSampleRate = sampleRate;
    }

    size_t read(size_t position, bool /*looped*/, AudioSignalWithChannels::reference output) override
    {
        double currentPosition = step * position;
        currentPosition -= static_cast<int>(currentPosition);

        for (auto& sample : output.channel(0))
        {
            sample = amplitude * static_cast<Float>(std::sin(2 * M_PI * currentPosition));

            currentPosition += step;
        }

        for (size_t i = 1; i < output.nChannels(); ++i)
        {
            ASSUME(output.channel(i).copy(output.channel(0)));
        }

        return output.channelSize();
    }

    std::optional<size_t> getLength() const override { return std::nullopt; }

private:
    Float step, amplitude;
};