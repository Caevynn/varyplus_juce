/*
  ==============================================================================

    TestSourceAudioProcessors.hpp
    Created: 24 Oct 2023 5:36:58pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "TestSources.hpp"
#include "../AudioProcessorBase.hpp"

template<class Callback>
class NoiseAudioProcessor : public Callback
{
    static_assert(std::is_base_of_v<AudioProcessorBase, Callback>);

public:
    NoiseAudioProcessor(DebugOutput& debugOutput) : AudioDeviceCallback(debugOutput) {}

    void process(const AudioSignalWithChannels&, AudioSignalWithChannels& output) override
    {
        assert(source);

        if (source)
        {
            source->read(0, output);
        }
    }

    void load(const AudioCallback::Settings&) override
    {
        source = std::make_unique<NoiseSource>();
    }

    void unload() override
    {
        source.reset();
    }

    void errorCallback(const std::string&) override {}

private:
    std::unique_ptr<NoiseSource> source;
};

template<class Callback>
class SineAudioProcessor : public Callback
{
    static_assert(std::is_base_of_v<AudioProcessorBase, Callback>);

public:
    SineAudioProcessor(DebugOutput& debugOutput) : AudioDeviceCallback(debugOutput) {}

    void process(const AudioSignalWithChannels&, AudioSignalWithChannels& output) override
    {
        source->read(0, output);
    }

    void load(const AudioCallback::Settings& settings) override
    {
        source = std::make_unique<SineSource>(settings.sampleRate);
    }

    void unload() override
    {
        source.reset();
    }

    void errorCallback(const std::string&) override {}

private:
    std::unique_ptr<SineSource> source;
};

template<class Callback>
class ClickAudioProcessor : public Callback
{
    static_assert(std::is_base_of_v<AudioProcessorBase, Callback>);

public:
    ClickAudioProcessor(DebugOutput& debugOutput) : AudioDeviceCallback(debugOutput) {}

    void process(const AudioSignalWithChannels&, AudioSignalWithChannels& output) override
    {
        source->read(0, output);
    }

    void load(const AudioCallback::Settings& settings) override
    {
        source = std::make_unique<ClickSource>(settings.sampleRate);
    }

    void unload() override
    {
        source.reset();
    }

    void errorCallback(const std::string&) override {}

private:
    std::unique_ptr<ClickSource> source;
};
