/*
  ==============================================================================

    OSCBase.hpp
    Created: 17 Nov 2023 3:52:41pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <string>
#include <vector>
#include <variant>
#include <functional>
#include <map>

namespace OSC
{
    class Message
    {
        using Value = std::variant<int, float, std::string>;

    public:
        Message(const std::string& address) : address(address) {}

        size_t numValues() const { return values.size(); }
        const std::string& getAddress() const { return address; }

        template<typename ValueType>
        void pushValue(ValueType value)
        {
            values.push_back(value);
        }

        template<typename ValueType, typename OtherValueType>
        [[nodiscard]] Message& withValue(OtherValueType value)
        {
            pushValue<ValueType>(static_cast<ValueType>(value));
            return *this;
        }

        template<typename ValueType>
        bool holdsValue(size_t index) const
        {
            return index < values.size() && std::holds_alternative<ValueType>(values[index]);
        }
        template<typename ValueType>
        ValueType getValue(size_t index) const
        {
            return std::get<ValueType>(values[index]);
        }

        template<typename ...ValueTypes>
        bool holdsValues() const
        {
            if constexpr (sizeof...(ValueTypes) == 0)
            {
                return numValues() == 0;
            }
            else
            {
                return checkValueTypes<ValueTypes...>(0);
            }
        }

    private:
        std::string address;
        std::vector<Value> values;

        template<typename ValueType, typename ...ValueTypes>
        bool checkValueTypes(size_t currentIndex) const
        {
            if (!holdsValue<ValueType>(currentIndex))
            {
                return false;
            }

            if constexpr (sizeof...(ValueTypes) == 0)
            {
                return numValues() == currentIndex + 1;
            }
            else
            {
                return checkValueTypes<ValueTypes...>(currentIndex + 1);
            }
        }
    };

    class ReaderBase
    {
    public:
        virtual ~ReaderBase() = default;

        virtual bool startReading(unsigned port) = 0;

        bool addHandler(const std::string& address, std::function<void(const Message&)> handler)
        {
            if (handlers.find(address) == handlers.end())
            {
                handlers[address] = handler;
                return true;
            }

            return false;
        }

    protected:
        bool handleMessage(const Message& message)
        {
            if (auto handler = handlers.find(message.getAddress()); handler != handlers.end())
            {
                handler->second(message);
                return true;
            }

            return false;
        }


    private:
        std::map<std::string, std::function<void(const Message&)>> handlers;
    };

    class WriterBase
    {
    public:
        virtual ~WriterBase() = default;

        virtual bool startWriting(const std::string& destinationAddress, unsigned destinationPort) = 0;

        virtual bool write(const Message& message) = 0;
    };
}
