/*
  ==============================================================================

    AudioFile.hpp
    Created: 6 Nov 2023 10:06:07am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <string>
#include <optional>
#include <vector>

#include "AudioTypes.hpp"
#include "AudioSource.hpp"

#include "Test/TestSources.hpp"

class AudioFile
{
public:
    class LoaderBase
    {
    public:
        virtual ~LoaderBase() = default;
        virtual AudioFile load(const std::string& filename, bool mono = false) = 0;
    };

    AudioFile(const std::string& filename) : filename(filename) {}

    AudioFile(AudioFile&&) = default;

    ~AudioFile()
    {
        signalWithChannels.deallocate();
    }

    std::unique_ptr<AudioSource> makeSource(unsigned wantedSampleRate)
    {
        if (filename == "click.testsource")
        {
            ClickSource::createSinc(signalWithChannels, wantedSampleRate);
            nChannels = signalWithChannels.nChannels();
            nSamples = signalWithChannels.channelSize();
            sampleRate = wantedSampleRate;
        }

        if (!isValid())
        {
            return std::unique_ptr<AudioSource>();
        }
        else if (isTest())
        {
            if (filename == "click.testsource") { return std::make_unique<ClickSource>(wantedSampleRate); }
            else if (filename == "noise.testsource") { return std::make_unique<NoiseSource>(); }
            else if (filename == "sine.testsource") { return std::make_unique<SineSource>(wantedSampleRate); }
            else { error = "Invalid test source"; return std::unique_ptr<AudioSource>(); }
        }
        else
        {
            return std::make_unique<AudioSource>(sampleRate, signalWithChannels);
        }
    }

    bool isValid() const { return !error; }
    bool isTest() const { return !error && nSamples == 0; }
    const std::string& getFilename() const { return filename; }
    const std::optional<std::string>& getErrorMsg() const { return error; }

    size_t getNumChannels() const { return nChannels; }
    size_t getNumSamples() const { return nSamples; }
    unsigned getSampleRate() const { return sampleRate; }

    const AudioSignalWithChannels& getSignalWithChannels() const { return signalWithChannels; }

private:
    friend class AudioFileLoader;

    AudioFile() = default;

    std::string filename;
    std::optional<std::string> error;

    size_t nChannels{ 0 }, nSamples{ 0 };
    unsigned sampleRate{ 0 };

    AudioSignalWithChannels signalWithChannels;
};
