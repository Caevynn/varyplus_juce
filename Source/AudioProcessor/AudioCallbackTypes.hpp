/*
  ==============================================================================

    AudioProcessorTypes.hpp
    Created: 7 Nov 2023 1:16:04pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <string>

#include "Utils/Time.hpp"

namespace AudioCallback
{
    enum class State
    {
        Stopped,
        Loading,
        Running,
        Unloading
    };

    class Usage
    {
    public:
        void setMeasurementFrequency(double measurementFrequency)
        {
            measurementInterval = 1.0 / measurementFrequency;
            decayFactor = 1 - std::pow(0.05, measurementInterval);
        }

        void addMeasurement(double measurement)
        {
            currentUsage += decayFactor * (measurement / measurementInterval - currentUsage);
        }

        double getCurrentUsage() const
        {
            return currentUsage;
        }

    private:
        double measurementInterval{ 0 };
        double decayFactor{ 0 };
        double currentUsage{ 0 };
    };

    class DebugOutput
    {
    public:
        virtual ~DebugOutput() = default;

        virtual void addError(const std::string& /*error*/) {}
        virtual void addLine(const std::string& /*line*/) {}

        virtual void resetMeasurement() {}
        virtual void addMeasurement(const Time::Measurement& /*measurement*/) {}
        virtual void invalidateLastMeasurement() {}
    };
};
