/*
  ==============================================================================

    AudioVector.hpp
    Created: 22 Oct 2023 12:39:32pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "Utils/RealTimeContainers/RTCArray.hpp"

template<typename ...Args>
class Channels : public RTC::InterfaceBase<Args...>
{
public:
    RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(channel, element)
    RTC_FUNCTION_ALIAS_CONST(nChannels, nElements)
    RTC_FUNCTION_ALIAS_CONST(channelSize, elementSize)
};

template<typename ...Args>
class Samples : public RTC::InterfaceBase<Args...>
{
public:
    RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(sample, element)
    RTC_FUNCTION_ALIAS_CONST(nSamples, nElements)
    RTC_FUNCTION_ALIAS_CONST(sampleSize, elementSize)
};

template<typename ...Args>
class Buses : public RTC::InterfaceBase<Args...>
{
public:
    RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(bus, element)
    RTC_FUNCTION_ALIAS_CONST(nBuses, nElements)
    RTC_FUNCTION_ALIAS_CONST(busSize, elementSize)
};

template<typename ...Args>
class Segments : public RTC::InterfaceBase<Args...>
{
public:
    RTC_FUNCTION_ALIAS_CONST_AND_NON_CONST(segment, element)
    RTC_FUNCTION_ALIAS_CONST(nSegments, nElements)
    RTC_FUNCTION_ALIAS_CONST(segmentSize, elementSize)
};

using AudioValueType = float;

using AudioSignal = RTC::Array<AudioValueType, Samples>;
using AudioSpectrum = RTC::Array<RTC::Complex<AudioValueType>, Samples>;
using AudioSignalWithChannels = RTC::Array<AudioValueType, Channels, Samples>;
using AudioSpectrumWithChannels = RTC::Array<RTC::Complex<AudioValueType>, Channels, Samples>;

using AudioSignalBuses = RTC::Array<AudioValueType, Buses, Channels, Samples>;
using SegmentedAudioSpectrumWithChannels = RTC::Array<RTC::Complex<AudioValueType>, Segments, Channels, Samples>;
