/*
  ==============================================================================

    HannWindow.hpp
    Created: 25 Oct 2023 10:32:40pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioTypes.hpp"

class HannWindow
{
public:
    static void createWithAllocation(AudioSignal& window, size_t length)
    {
        window.allocate(length);

        for (size_t i = 0; i < length; ++i)
        {
            double position = static_cast<double>(i) / length;
            window.sample(i) = static_cast<AudioValueType>(0.5 - 0.5 * std::cos(2 * M_PI * position));
        }
    }
};
