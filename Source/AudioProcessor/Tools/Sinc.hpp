/*
  ==============================================================================

    Sinc.hpp
    Created: 5 Nov 2023 11:14:06pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioTypes.hpp"

class Sinc
{
public:
    static void createWithAllocation(AudioSignal& sinc, unsigned sampleRate, double frequency, size_t lengthInPeriods)
    {
        size_t nSamples = static_cast<size_t>(lengthInPeriods * static_cast<double>(sampleRate) / frequency);

        sinc.allocate(nSamples);

        for (size_t iSample = 0; iSample < nSamples; ++iSample)
        {
            double sincPosition = static_cast<double>(iSample) - nSamples / 2;
            double sincTime = sincPosition / sampleRate;

            if (sincTime == 0)
            {
                sinc.sample(iSample) = 1;
            }
            else
            {
                double value = 2 * M_PI * frequency * sincTime;
                sinc.sample(iSample) = static_cast<AudioValueType>(std::sin(value) / value);
            }
        }
    }
};
