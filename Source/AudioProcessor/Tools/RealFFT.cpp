#include "RealFFT.hpp"

#include <cassert>

#include "../Utils/IsPowerOfTwo.hpp"

void RealFFT::allocate(size_t signalLength)
{
    assert(IsPowerOfTwo(signalLength));

    nFFT = signalLength / 2;

    fftData.allocate(nFFT);

    nStages = 1;
    for (size_t i = nFFT / 2; i > 1; ++nStages, i /= 2);

    table.twiddleFactor.allocate(nFFT);
    for (size_t iStage = 0; iStage < nStages; ++iStage)
    {
        size_t nButterflies = 1ull << iStage;

        auto twiddleReal = table.twiddleFactor.real().range(nButterflies, nButterflies);
        auto twiddleImag = table.twiddleFactor.imag().range(nButterflies, nButterflies);

        for (size_t iButterfly = 0; iButterfly < nButterflies; ++iButterfly)
        {
            double angle = M_PI * iButterfly / nButterflies;
            twiddleReal[iButterfly] = static_cast<Real>(std::cos(angle));
            twiddleImag[iButterfly] = static_cast<Real>(-std::sin(angle));
        }
    }

    table.cos.allocate(nFFT / 2);
    for (size_t i = 0; i < nFFT / 2; ++i)
    {
        size_t posReal = nFFT / 2 - 1 - i;
        size_t posImag = i + 1;

        table.cos.real()[i] = static_cast<Real>(std::cos(M_PI * posReal / nFFT));
        table.cos.imag()[i] = static_cast<Real>(std::cos(M_PI * posImag / nFFT));
    }

    table.swaps.allocate(nFFT / 2);
    for (size_t i = 0, j = 0; i < nFFT - 1; ++i)
    {
        if (i < j)
        {
            table.swaps.push_back({ static_cast<SwapIndex>(i), static_cast<SwapIndex>(j) });
        }

        size_t k = nFFT / 2;
        while (k <= j) { j -= k; k /= 2; }
        j += k;
    }
}

void RealFFT::deallocate()
{
    fftData.deallocate();
    table.twiddleFactor.deallocate();
    table.cos.deallocate();
    table.swaps.deallocate();
}

void RealFFT::forwardConverted(ConstSignal signal, Spectrum spectrum)
{
    signalToFFTData(signal);
    run();
    spectrumFromFFTData(spectrum);
}

void RealFFT::inverseConverted(ConstSpectrum spectrum, Signal signal)
{
    spectrumToFFTData(spectrum);
    run();
    signalFromFFTData(signal);
}

void RealFFT::signalToFFTData(ConstSignal& signal)
{
    size_t nValues = signal.size();

    assert(nValues <= 2 * nFFT);

    auto real = fftData.real();
    auto imag = fftData.imag();

    size_t i = 0;

    size_t nValuesBySimd = nValues - (nValues % Simd::Size);
    for (; i < nValuesBySimd / 2; i += Simd::Size)
    {
        Simd first, second;

        first.load(&signal[2 * i]);
        second.load(&signal[2 * i + Simd::Size]);

        auto [even, odd] = Simd::deinterleave(first, second);

        even.store(&real[i]);
        odd.store(&imag[i]);
    }

    size_t nValuesByPair = nValues - (nValues % 2);
    for (; i < nValuesByPair / 2; ++i)
    {
        real[i] = signal[2 * i];
        imag[i] = signal[2 * i + 1];
    }

    if (nValuesByPair != nValues)
    {
        real[i] = signal[2 * i];
        imag[i] = 0;
    }

    size_t nValuesCopied = nValues + (nValues % 2);
    real.range(nValuesCopied / 2, nFFT - nValuesCopied / 2).setToZero();
    imag.range(nValuesCopied / 2, nFFT - nValuesCopied / 2).setToZero();
}

void RealFFT::spectrumFromFFTData(Spectrum& spectrum)
{
    auto in = fftData.data();
    auto out = spectrum.data();
    auto cos = table.cos.data();

    const Simd point5(0.5);

    for (size_t i = 0; i < nFFT / 2; i += Simd::Size)
    {
        size_t j = nFFT - Simd::Size - i;

        SimdComplex ci, cj, cc;

        ci.load(&in[i + 1]);
        cj.load(&in[j]).reverse();
        cc.load(&cos[i]);

        SimdComplex cs = (ci + cj.conjugate()) * point5;
        SimdComplex cd = (cj - ci.conjugate()) * point5;
        SimdComplex cp = cc * cd.conjugate();

        (cs + cp).store(&out[i + 1]);
        (cs.conjugate() - cp.conjugate()).reverse().store(&out[j]);
    }

    out[0].real() = in[0].real() + in[0].imag();
    out[0].imag() = 0;
    out[nFFT].real() = in[0].real() - in[0].imag();
    out[nFFT].imag() = 0;
    out[nFFT / 2].real() = in[nFFT / 2].real();
    out[nFFT / 2].imag() = -in[nFFT / 2].imag();
}

void RealFFT::spectrumToFFTData(ConstSpectrum& spectrum)
{
    auto in = spectrum.data();
    auto out = fftData.data();
    auto cos = table.cos.data();

    const Simd point5(0.5);

    for (size_t i = 0; i < nFFT / 2; i += Simd::Size)
    {
        size_t j = nFFT - Simd::Size - i;

        SimdComplex ci, cj, cc;

        ci.load(&in[i + 1]);
        cj.load(&in[j]).reverse();
        cc.load(&cos[i]);

        SimdComplex cs = (ci + cj.conjugate()) * point5;
        SimdComplex cd = (ci - cj.conjugate()) * point5;
        SimdComplex cp = cc * cd.conjugate();

        (cp + cs.conjugate()).store(&out[i + 1]);
        (cs - cp.conjugate()).reverse().store(&out[j]);
    }

    out[0].real() = (in[0].real() + in[nFFT].real()) / 2;
    out[0].imag() = (in[0].real() - in[nFFT].real()) / 2;
    out[nFFT / 2].real() = in[nFFT / 2].real();
    out[nFFT / 2].imag() = -in[nFFT / 2].imag();
}

void RealFFT::signalFromFFTData(Signal& signal)
{
    assert(signal.size() == 2 * nFFT);

    const auto real = fftData.real();
    const auto imag = fftData.imag();

    const Simd norm(1 / static_cast<Real>(nFFT));

    for (size_t i = 0; i < nFFT; i += Simd::Size)
    {
        Simd even(&real[i]);
        Simd odd(&imag[i]);

        auto [first, second] = Simd::interleave(even, odd);

        first *= norm;
        second *= norm;

        first.store(&signal[2 * i]);
        second.store(&signal[2 * i + Simd::Size]);
    }
}

void RealFFT::run()
{
    auto data = fftData.data();
    auto twiddle = table.twiddleFactor.data();

    assert(fftData.size() == nFFT);

    // Reorder values according to bit reversal

    for (const auto& [a, b] : table.swaps)
    {
        std::swap(data[a].real(), data[b].real());
        std::swap(data[a].imag(), data[b].imag());
    }

    if (nStages > 2)
    {
        // Stages 1 - 3

        RTC::SimdComplex<float, 4> ct(&twiddle[4]);

        for (size_t iGroup = 0; iGroup < nFFT / 8; ++iGroup)
        {
            size_t i = 8 * iGroup;

            RTC::Complex<float> ci0(data[i + 0]);
            RTC::Complex<float> ci1(data[i + 1]);
            RTC::Complex<float> ci2(data[i + 2]);
            RTC::Complex<float> ci3(data[i + 3]);
            RTC::Complex<float> ci4(data[i + 4]);
            RTC::Complex<float> ci5(data[i + 5]);
            RTC::Complex<float> ci6(data[i + 6]);
            RTC::Complex<float> ci7(data[i + 7]);

            RTC::SimdComplex<float, 4> c0(
                (ci0 + ci1) + (ci2 + ci3),
                (ci0 - ci1) + (ci2 - ci3).rotateCW(),
                (ci0 + ci1) - (ci2 + ci3),
                (ci0 - ci1) - (ci2 - ci3).rotateCW()
            );

            RTC::SimdComplex<float, 4> c4(
                (ci4 + ci5) + (ci6 + ci7),
                (ci4 - ci5) + (ci6 - ci7).rotateCW(),
                (ci4 + ci5) - (ci6 + ci7),
                (ci4 - ci5) - (ci6 - ci7).rotateCW()
            );

            RTC::SimdComplex<float, 4> c4tmp = c4 * ct;

            (c0 + c4tmp).store(&data[i + 0]);
            (c0 - c4tmp).store(&data[i + 4]);
        }
    }

    size_t startStage = 3;

#if RTC_SIMD_SIZE == 4
    if (nStages % 2 == 0)
    {
        // Stage 4

        SimdComplex ct0(&twiddle[8]);
        SimdComplex ct1(&twiddle[12]);

        for (size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
        {
            size_t i = 16 * iGroup;

            SimdComplex c0(&data[i + 0 * 4]);
            SimdComplex c1(&data[i + 1 * 4]);
            SimdComplex c2(&data[i + 2 * 4]);
            SimdComplex c3(&data[i + 3 * 4]);

            SimdComplex c2tmp = c2 * ct0;
            SimdComplex c3tmp = c3 * ct1;

            (c0 + c2tmp).store(&data[i + 0 * 4]);
            (c1 + c3tmp).store(&data[i + 1 * 4]);
            (c0 - c2tmp).store(&data[i + 2 * 4]);
            (c1 - c3tmp).store(&data[i + 3 * 4]);
        }

        ++startStage;
    }
#elif RTC_SIMD_SIZE == 8
    if (nStages == 4)
    {
        // Stage 4

        SimdComplex ct(&twiddle[8]);

        for (size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
        {
            size_t i = 16 * iGroup;

            SimdComplex c0(&data[i + 0 * 8]);
            SimdComplex c1(&data[i + 1 * 8]);

            SimdComplex c1tmp = c1 * ct;

            (c0 + c1tmp).store(&data[i + 0 * 8]);
            (c0 - c1tmp).store(&data[i + 1 * 8]);
        }

        return;
    }

    if (nStages > 4)
    {
        // Stages 4 - 5

        SimdComplex ct0(&twiddle[8]);
        SimdComplex ct10(&twiddle[16]);
        SimdComplex ct11(&twiddle[24]);

        for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
        {
            size_t i = 32 * iGroup;

            SimdComplex c0(&data[i + 0 * 8]);
            SimdComplex c1(&data[i + 1 * 8]);
            SimdComplex c2(&data[i + 2 * 8]);
            SimdComplex c3(&data[i + 3 * 8]);

            SimdComplex c1tmp0 = c1 * ct0;
            SimdComplex c3tmp0 = c3 * ct0;
            SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
            SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;

            (c0 + c1tmp0 + c2tmp1).store(&data[i + 0 * 8]);
            (c0 - c1tmp0 + c3tmp1).store(&data[i + 1 * 8]);
            (c0 + c1tmp0 - c2tmp1).store(&data[i + 2 * 8]);
            (c0 - c1tmp0 - c3tmp1).store(&data[i + 3 * 8]);
        }

        startStage += 2;
    }

    if (nStages % 2 == 0)
    {
        // Stage 6

        for (size_t iGroup = 0; iGroup < nFFT / 64; ++iGroup)
        {
            for (size_t iButterfly = 0; iButterfly < 32; iButterfly += Simd::Size)
            {
                size_t i = 64 * iGroup + iButterfly;

                SimdComplex ct(&twiddle[32 + iButterfly]);

                SimdComplex c0(&data[i + 0 * 32]);
                SimdComplex c1(&data[i + 1 * 32]);

                SimdComplex c1tmp = c1 * ct;

                (c0 + c1tmp).store(&data[i + 0 * 32]);
                (c0 - c1tmp).store(&data[i + 1 * 32]);
            }
        }

        ++startStage;
    }
#elif RTC_SIMD_SIZE == 16
    if (nStages == 4)
    {
        // Stage 4

        RTC::SimdComplex<float, 8> ct(&twiddle[8]);

        for (size_t iGroup = 0; iGroup < nFFT / 16; ++iGroup)
        {
            size_t i = 16 * iGroup;

            RTC::SimdComplex<float, 8> c0(&data[i + 0 * 8]);
            RTC::SimdComplex<float, 8> c1(&data[i + 1 * 8]);

            RTC::SimdComplex<float, 8> c1tmp = c1 * ct;

            (c0 + c1tmp).store(&data[i + 0 * 8]);
            (c0 - c1tmp).store(&data[i + 1 * 8]);
        }

        return;
    }

    if (nStages > 4)
    {
        // Stages 4 - 5

        RTC::SimdComplex<float, 8> ct0(&twiddle[8]);
        RTC::SimdComplex<float, 16> ct1(&twiddle[16]);

        for (size_t iGroup = 0; iGroup < nFFT / 32; ++iGroup)
        {
            size_t i = 32 * iGroup;

            RTC::SimdComplex<float, 8> c0(&data[i + 0 * 8]);
            RTC::SimdComplex<float, 8> c1(&data[i + 1 * 8]);
            RTC::SimdComplex<float, 8> c2(&data[i + 2 * 8]);
            RTC::SimdComplex<float, 8> c3(&data[i + 3 * 8]);

            RTC::SimdComplex<float, 8> c1tmp0 = c1 * ct0;
            RTC::SimdComplex<float, 8> c3tmp0 = c3 * ct0;

            SimdComplex c01(c0 + c1tmp0, c0 - c1tmp0);
            SimdComplex c23(c2 + c3tmp0, c2 - c3tmp0);

            SimdComplex c23tmp1 = c23 * ct1;

            (c01 + c23tmp1).store(&data[i + 0 * 16]);
            (c01 - c23tmp1).store(&data[i + 1 * 16]);
        }

        startStage += 2;
    }

    if (nStages % 2 == 0)
    {
        // Stage 6

        SimdComplex ct0(&twiddle[32]);
        SimdComplex ct1(&twiddle[48]);

        for (size_t iGroup = 0; iGroup < nFFT / 64; ++iGroup)
        {
            size_t i = 64 * iGroup;

            SimdComplex c0(&data[i + 0 * 16]);
            SimdComplex c1(&data[i + 1 * 16]);
            SimdComplex c2(&data[i + 2 * 16]);
            SimdComplex c3(&data[i + 3 * 16]);

            SimdComplex c2tmp = c2 * ct0;
            SimdComplex c3tmp = c3 * ct1;

            (c0 + c2tmp).store(&data[i + 0 * 16]);
            (c1 + c3tmp).store(&data[i + 1 * 16]);
            (c0 - c2tmp).store(&data[i + 2 * 16]);
            (c1 - c3tmp).store(&data[i + 3 * 16]);
        }

        ++startStage;
    }
#endif

    // Remaining stages

    for (size_t iStage = startStage; iStage < nStages; iStage += 2)
    {
        size_t nButterflies = 1ull << iStage;

        assert(table.twiddleFactor.size() >= 4 * nButterflies);

        size_t nGroups = nFFT / (4 * nButterflies);

        for (size_t iGroup = 0; iGroup < nGroups; ++iGroup)
        {
            assert(nButterflies % Simd::Size == 0);

            for (size_t iButterfly = 0; iButterfly < nButterflies; iButterfly += Simd::Size)
            {
                size_t i = 4 * nButterflies * iGroup + iButterfly;

                SimdComplex ct0(&twiddle[iButterfly + 1 * nButterflies]);
                SimdComplex ct10(&twiddle[iButterfly + 2 * nButterflies]);
                SimdComplex ct11(&twiddle[iButterfly + 3 * nButterflies]);

                SimdComplex c0(&data[i + 0 * nButterflies]);
                SimdComplex c1(&data[i + 1 * nButterflies]);
                SimdComplex c2(&data[i + 2 * nButterflies]);
                SimdComplex c3(&data[i + 3 * nButterflies]);

                SimdComplex c1tmp0 = c1 * ct0;
                SimdComplex c3tmp0 = c3 * ct0;
                SimdComplex c2tmp1 = (c2 + c3tmp0) * ct10;
                SimdComplex c3tmp1 = (c2 - c3tmp0) * ct11;

                (c0 + c1tmp0 + c2tmp1).store(&data[i + 0 * nButterflies]);
                (c0 - c1tmp0 + c3tmp1).store(&data[i + 1 * nButterflies]);
                (c0 + c1tmp0 - c2tmp1).store(&data[i + 2 * nButterflies]);
                (c0 - c1tmp0 - c3tmp1).store(&data[i + 3 * nButterflies]);
            }
        }
    }
}
