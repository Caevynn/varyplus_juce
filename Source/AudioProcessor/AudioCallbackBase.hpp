/*
  ==============================================================================

    AudioCallback.hpp
    Created: 21 Oct 2023 12:10:37pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <string>

#include "AudioTypes.hpp"
#include "AudioCallbackTypes.hpp"

namespace AudioCallback
{
    class Base
    {
    public:
        State getState() const { return state; }
        bool isStopped() const { return getState() == State::Stopped; }
        bool isLoading() const { return getState() == State::Loading; }
        bool isRunning() const { return getState() == State::Running; }
        bool isUnloading() const { return getState() == State::Unloading; }

        double getUsage() const { return usage.getCurrentUsage(); }

        std::function<void(State)> onStateChange;
        std::function<void(const std::string&)> onError;

    protected:
        DebugOutput& debugOutput;
        State state{ State::Stopped };
        Usage usage;

        Base(DebugOutput& debugOutput) : debugOutput(debugOutput) {}

        Base(const Base&) = delete;
        Base& operator=(const Base&) = delete;
        virtual ~Base() = default;

        // To be overloaded by implementation
        virtual unsigned getNumInputChannels() const = 0;
        virtual unsigned getNumOutputChannels() const = 0;
        virtual unsigned getSampleRate() const = 0;
        virtual unsigned getBlockSize() const = 0;

        // To be overloaded by processor
        virtual void load() = 0;
        virtual void process(const AudioSignalWithChannels& input, AudioSignalWithChannels& output) = 0;
        virtual void unload() = 0;
        virtual void timer() = 0;
    };
}
