#include "MainComponent.hpp"

#include "Utils/FilenameHelper.hpp"

MainComponent::MainComponent() :
    tabbedComponent(juce::TabbedButtonBar::Orientation::TabsAtTop),
    audioProcessor(debugOutput),
    mainEditor(audioProcessor.getMainInterface()),
    fileEditor(audioProcessor.getMainInterface()),
    audioDeviceManager(audioProcessor, debugOutput),
    audioDeviceEditor(audioDeviceManager, debugOutput)
{
    audioDeviceManager.setMode(AudioDevice::Mode::OutputOnly);
    audioDeviceManager.setSampleRateFilter(AudioDevice::SampleRateFilter::AllowOnlyPowerOfTwo);

    audioProcessor.onStateChange = [this](AudioCallback::State state)
    {
        switch (state)
        {
        case AudioCallback::State::Stopped:
            stateLabel.setLabelText("Device is stopped");
            stateLabel.setBackgroundColour(juce::Colours::darkred);
            break;

        case AudioCallback::State::Loading:
            break;

        case AudioCallback::State::Running:
            stateLabel.setLabelText("Device is running");
            stateLabel.setBackgroundColour(juce::Colours::darkgreen);
            break;

        case AudioCallback::State::Unloading:
            break;
        }
    };

    audioProcessor.onError = [this](const std::string& error)
    {
        debugOutput.addError(error);
        juce::AlertWindow::showAsync(juce::MessageBoxOptions().withButton("Ok").withMessage(error).withIconType(juce::MessageBoxIconType::WarningIcon), nullptr);
    };

    auto backgroundColour = getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId);
    tabbedComponent.addTab("Device manager", backgroundColour, &audioDeviceEditor, false);
    tabbedComponent.addTab("Audio processor", backgroundColour, &mainEditor, false);
    tabbedComponent.addTab("Loaded files", backgroundColour, &fileEditor, false);
    tabbedComponent.addTab("Debug output", backgroundColour, &debugOutput, false);
    addAndMakeVisible(tabbedComponent);

    stateLabel.setLabelText("Device is not running");
    stateLabel.setBackgroundColour(juce::Colours::darkred);
    addAndMakeVisible(stateLabel);

    shrimpButton.setButtonText("Shrimp");
    shrimpButton.onClick = [this]()
        {
            audioProcessor.setInputBusChannels(1);
            isShrimpVikktorSelected = true;
            shrimpNotVikktor = true;
            if (onTitleChange) { onTitleChange("Shrimp"); }
            resized();
        };
    addAndMakeVisible(shrimpButton);

    vikktorButton.setButtonText("VIKKtor");
    vikktorButton.onClick = [this]()
        {
            audioProcessor.setInputBusChannels(24);
            isShrimpVikktorSelected = true;
            shrimpNotVikktor = false;
            if (onTitleChange) { onTitleChange("VIKKtor"); }
            resized();
        };
    addAndMakeVisible(vikktorButton);

    juce::Font::setDefaultMinimumHorizontalScaleFactor(1.0f);

#if RTC_SIMD_SIZE == 16
    debugOutput.addLine("Using AVX512 instruction set.");
#elif RTC_SIMD_SIZE == 8
    debugOutput.addLine("Using AVX instruction set.");
#elif RTC_SIMD_SIZE == 4
    debugOutput.addLine("Using SSE2 instruction set.");
#endif

    setSize(600, 400);

    startTimer(10);
}

void MainComponent::paint(juce::Graphics& g)
{
    g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));
}

void MainComponent::resized()
{
    auto area = getLocalBounds();

    shrimpButton.setVisible(!isShrimpVikktorSelected);
    vikktorButton.setVisible(!isShrimpVikktorSelected);
    stateLabel.setVisible(isShrimpVikktorSelected);
    tabbedComponent.setVisible(isShrimpVikktorSelected);

    if (!isShrimpVikktorSelected)
    {
        area.removeFromLeft(area.getWidth() / 3);
        area.removeFromRight(area.getWidth() / 2);
        auto upperArea = area.removeFromTop(area.getWidth());
        auto lowerArea = area;

        shrimpButton.setBounds(upperArea.removeFromBottom(30).reduced(3));
        vikktorButton.setBounds(lowerArea.removeFromTop(30).reduced(3));
    }
    else
    {
        stateLabel.setBounds(area.removeFromBottom(24));
        tabbedComponent.setBounds(area.reduced(5));
    }
}

bool MainComponent::isInterestedInFileDrag(const juce::StringArray& files)
{
    for (const auto& file : files)
    {
        auto appendix = FilenameHelper::appendix(file.toStdString());

        if (appendix != "sofa" &&
            appendix != "sofa24" &&
            appendix != "wav" &&
            appendix != "mp3" &&
            appendix != "testfilter" &&
            appendix != "testsource" &&
            appendix != "testmasterfilter")
        {
            return false;
        }
    }

    return true;
}

void MainComponent::filesDropped(const juce::StringArray& files, int, int)
{
    for (const auto& file : files)
    {
        auto filename = file.toStdString();
        auto appendix = FilenameHelper::appendix(filename);

        if (appendix == "sofa")
        {
            if (SofaFileLoader::isSimpleFreeFieldHRIR(filename))
            {
                audioProcessor.getMainInterface().loadFilterFile(filename);
            }
            else if (SofaFileLoader::isSimpleHeadphoneIR(filename))
            {
                audioProcessor.getMainInterface().loadMasterFilterFile(filename);
            }
        }
        else if (appendix == "sofa24" || appendix == "testfilter")
        {
            audioProcessor.getMainInterface().loadFilterFile(filename);
        }
        else if (appendix == "testmasterfilter")
        {
            audioProcessor.getMainInterface().loadMasterFilterFile(filename);
        }
        else
        {
            audioProcessor.getMainInterface().loadSourceFile(filename);
        }
    }
}

void MainComponent::timerCallback()
{
    stateLabel.setMeterValue(audioProcessor.getUsage());
}
