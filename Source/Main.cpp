#include <JuceHeader.h>

#include "MainComponent.hpp"

#include "../SemanticVersion/SemanticVersion.hpp"

#ifdef _WIN32
#include <Windows.h>

void raiseWindow(void* windowHandle)
{
    // This function may look a little over the top, but it all seems necessary...

    HWND ownWindow = static_cast<HWND>(windowHandle);

    DWORD ownThread = GetCurrentThreadId();
    DWORD forgroundThread = GetWindowThreadProcessId(GetForegroundWindow(), NULL);

    AttachThreadInput(forgroundThread, ownThread, TRUE);

    SetWindowPos(ownWindow, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
    SetWindowPos(ownWindow, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOMOVE);
    SetForegroundWindow(ownWindow);
    SetFocus(ownWindow);
    SetActiveWindow(ownWindow);
    ShowWindow(ownWindow, SW_RESTORE);

    AttachThreadInput(forgroundThread, ownThread, FALSE);
}
#else
void raiseWindow([[maybe_unused]] void* windowHandle)
{

}
#endif

class ShrimpApplication  : public juce::JUCEApplication
{
public:
    ShrimpApplication() {}

    const juce::String getApplicationName() override       { return ProjectInfo::projectName; }
    const juce::String getApplicationVersion() override    { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed() override             { return false; }

    void initialise ([[maybe_unused]] const juce::String& commandLine) override
    {
        mainWindow.reset(new MainWindow);
        
        raiseWindow(mainWindow->getWindowHandle());
    }

    void shutdown() override
    {
        mainWindow = nullptr;
    }

    void systemRequestedQuit() override
    {
        quit();
    }

    void anotherInstanceStarted ([[maybe_unused]] const juce::String& commandLine) override
    {
        raiseWindow(mainWindow->getWindowHandle());

        auto options = juce::MessageBoxOptions()
            .withIconType(juce::MessageBoxIconType::WarningIcon)
            .withTitle("Error")
            .withMessage("Shrimp is already running")
            .withButton("Ok");

        juce::AlertWindow::showAsync(options, [this](int) { mainWindow->setAlwaysOnTop(false); });
    }

    class MainWindow    : public juce::DocumentWindow
    {
    public:
        MainWindow()
            : DocumentWindow("Please select...",
                             juce::Desktop::getInstance().getDefaultLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId),
                             DocumentWindow::allButtons)
        {
            setUsingNativeTitleBar(true);

            MainComponent* mainComponent = new MainComponent();
            mainComponent->onTitleChange = [this](const std::string& newTitle)
                {
                    setName(newTitle + " - v" + SemanticVersion.data());
                };

            setContentOwned(mainComponent, true);

#if JUCE_IOS || JUCE_ANDROID
            setFullScreen (true);
#else
            setResizable(true, true);
            centreWithSize(getWidth(), getHeight());
#endif

            setVisible(true);
        }

        void closeButtonPressed() override
        {
            JUCEApplication::getInstance()->systemRequestedQuit();
        }

    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
    };

private:
    std::unique_ptr<MainWindow> mainWindow;
};

//==============================================================================
// This macro generates the main() routine that launches the app.
START_JUCE_APPLICATION (ShrimpApplication)
