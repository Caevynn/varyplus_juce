#include "SofaFileLoader.hpp"

#include <algorithm>

#include <SOFASimpleFreeFieldHRIR.h>
#include <SOFASimpleHeadphoneIR.h>
#include <SOFAHelper.h>
#include <SOFAExceptions.h>

#include "../Utils/FilenameHelper.hpp"

class SofaConventionReader
{
public:
    SofaConventionReader(const std::string& filename)
    {
        // Disable exceptions logging
        const bool exceptionState = sofa::Exception::IsLoggedToCerr();
        sofa::Exception::LogToCerr(false);

        try
        {
            const sofa::File file(filename);
            convention = file.GetSOFAConventions();
        }
        catch(...)
        {
            convention = "";
        }

        // Restore exceptions logging
        sofa::Exception::LogToCerr(exceptionState);
    }

    bool isSimpleFreeFieldHRIR() const { return convention == "SimpleFreeFieldHRIR"; }
    bool isSimpleHeadphoneIR() const { return convention == "SimpleHeadphoneIR"; }

    const std::string& getName() const { return convention; }

private:
    std::string convention;
};

bool SofaFileLoader::isSimpleFreeFieldHRIR(const std::string& filename)
{
    SofaConventionReader convention(filename);
    return convention.isSimpleFreeFieldHRIR();
}

bool SofaFileLoader::isSimpleHeadphoneIR(const std::string& filename)
{
    SofaConventionReader convention(filename);
    return convention.isSimpleHeadphoneIR();
}

FDCF::File SofaFileLoader::loadFDCF(const std::string& filename)
{
    std::string appendix = FilenameHelper::appendix(filename);

    if (appendix == "testmasterfilter")
    {
        return FDCF::File(FilenameHelper::wihtoutPath(filename));
    }
    else if (appendix == "sofa")
    {
        SofaConventionReader convention(filename);

        if (convention.isSimpleHeadphoneIR())
        {
            return loadSimpleHeadphoneIR(filename);
        }
        else
        {
            FDCF::File file(filename);
            file.error = "Sofa convention not supported: " + convention.getName();
            return file;
        }
    }
    else
    {
        FDCF::File file(filename);
        file.error = "Format not supported: " + appendix;
        return file;
    }
}

HRTF::File SofaFileLoader::loadHRTF(const std::string& filename)
{
    std::string appendix = FilenameHelper::appendix(filename);

    if (appendix == "testfilter")
    {
        return HRTF::File(FilenameHelper::wihtoutPath(filename));
    }
    else if (appendix == "sofa24")
    {
        return load24(filename);
    }
    else if (appendix == "sofa")
    {
        SofaConventionReader convention(filename);

        if (convention.isSimpleFreeFieldHRIR())
        {
            return loadSimpleFreeFieldHRIR(filename);
        }
        else
        {
            HRTF::File file(filename);
            file.error = "Sofa convention not supported: " + convention.getName();
            return file;
        }
    }
    else
    {
        HRTF::File file(filename);
        file.error = "Format not supported: " + appendix;
        return file;
    }
}

FDCF::File SofaFileLoader::loadSimpleHeadphoneIR(const std::string& filename)
{
    FDCF::File file(filename);

    try
    {
        const sofa::SimpleHeadphoneIR sofaFile(filename);

        if (!sofaFile.IsValid())
        {
            file.error = "Opening file failed";
            return file;
        }

        if (file.nChannels = sofaFile.GetNumReceivers(); file.nChannels == 0)
        {
            file.error = "Number of channels: 0";
            return file;
        }

        if (file.nSamples = sofaFile.GetNumDataSamples(); file.nSamples == 0)
        {
            file.error = "Number of samples: 0";
            return file;
        }

        size_t nMeasurements = sofaFile.GetNumMeasurements();
        if (nMeasurements == 0)
        {
            file.error = "Number of measurements: 0";
            return file;
        }

        if (double tmp; sofaFile.GetSamplingRate(tmp))
        {
            if (file.sampleRate = static_cast<unsigned>(tmp); file.sampleRate == 0)
            {
                file.error = "Invalid sample rate: 0";
                return file;
            }
        }
        else
        {
            file.error = "Failed to read sample rate";
            return file;
        }

        std::vector<double> data;

        if (!sofaFile.GetDataIR(data) || data.size() != nMeasurements * file.nChannels * file.nSamples)
        {
            file.error = "Invalid data size: " + std::to_string(data.size()) + " (expected: " + std::to_string(nMeasurements * file.nChannels * file.nSamples) + ")";
            return file;
        }

        file.impulseResponse.allocate(RTC::Size()
            .with<Channels>(file.nChannels)
            .with<Samples>(file.nSamples));

        size_t firstMeasurementSize = file.nChannels * file.nSamples;

        auto channel = file.impulseResponse.begin();
        for (auto it = data.begin(); it != data.begin() + firstMeasurementSize;)
        {
            // Copy with explicit type conversion
            std::transform(it, it + file.nSamples, channel->begin(),
                [](const auto& x) { return static_cast<AudioValueType>(x); });

            it += file.nSamples;
            ++channel;
        }
    }
    catch (const std::exception& e)
    {
        file.error = e.what();
        return file;
    }

    return file;
}

HRTF::File SofaFileLoader::load24(const std::string& filename)
{
    std::string filenameBase = FilenameHelper::withoutAppendix(filename);

    HRTF::File file(filename);

    std::string firstFilename = filenameBase + "1.sofa";
    HRTF::File firstFile = loadSimpleFreeFieldHRIR(firstFilename);

    file.nEmitters = 24;
    file.nReceivers = firstFile.getNumReceivers();
    file.nSamples = firstFile.getNumSamples();
    file.nMeasurements = firstFile.getNumMeasurements();
    file.sampleRate = firstFile.getSampleRate();

    std::vector<HRTF::ImpulseResponseData> data;
    data.resize(file.getNumMeasurements());

    std::vector<HRTF::ListenerOrientation> orientations;
    orientations.resize(file.getNumMeasurements());

    for (size_t index = 0; index < file.getNumMeasurements(); ++index)
    {
        const auto& impulseResponse = firstFile.getImpulseResponses()[index];

        orientations[index] = impulseResponse.getListenerOrientation();

        data[index].allocate(RTC::Size()
            .with<HRTF::Receivers>(file.getNumReceivers())
            .with<HRTF::Emitters>(file.getNumEmitters())
            .with<Samples>(file.getNumSamples()));

        for (size_t iReceiver = 0; iReceiver < file.getNumReceivers(); ++iReceiver)
        {
            ASSUME(data[index].receiver(iReceiver).emitter(0).copy(impulseResponse.getData().receiver(iReceiver).emitter(0)));
        }
    }

    for (size_t i = 1; i < 24; ++i)
    {
        std::string currentFilename = filenameBase + std::to_string(i + 1) + ".sofa";

        HRTF::File currentFile = loadSimpleFreeFieldHRIR(currentFilename);

        if (!currentFile.isValid())
        {
            for (auto& measurement : data) { measurement.deallocate(); }
            file.error = FilenameHelper::wihtoutPath(currentFilename) + ": " + *currentFile.getErrorMsg();
            return file;
        }

        if (currentFile.getNumMeasurements() != file.getNumMeasurements())
        {
            for (auto& measurement : data) { measurement.deallocate(); }
            file.error = FilenameHelper::wihtoutPath(currentFilename) + ": Number of measurements doesn't match";
            return file;
        }

        if (currentFile.getNumReceivers() != file.getNumReceivers())
        {
            for (auto& measurement : data) { measurement.deallocate(); }
            file.error = FilenameHelper::wihtoutPath(currentFilename) + ": Number of channels doesn't match";
            return file;
        }

        if (currentFile.getNumSamples() != file.getNumSamples())
        {
            for (auto& measurement : data) { measurement.deallocate(); }
            file.error = FilenameHelper::wihtoutPath(currentFilename) + ": Number of samples doesn't match";
            return file;
        }

        if (currentFile.getSampleRate() != file.getSampleRate())
        {
            for (auto& measurement : data) { measurement.deallocate(); }
            file.error = FilenameHelper::wihtoutPath(currentFilename) + ": Sample rate doesn't match";
            return file;
        }

        for (size_t index = 0; index < file.getNumMeasurements(); ++index)
        {
            const auto& impulseResponse = currentFile.getImpulseResponses()[index];

            double distance = orientations[index].distance(impulseResponse.getListenerOrientation());

            if (distance > 1e-3)
            {
                for (auto& measurement : data) { measurement.deallocate(); }
                file.error = FilenameHelper::wihtoutPath(currentFilename) + ": Listener orientations don't match";
                return file;
            }

            for (size_t iReceiver = 0; iReceiver < file.getNumReceivers(); ++iReceiver)
            {
                ASSUME(data[index].receiver(iReceiver).emitter(i).copy(impulseResponse.getData().receiver(iReceiver).emitter(0)));
            }
        }
    }

    for (size_t i = 0; i < file.getNumMeasurements(); ++i)
    {
        file.impulseResponses.emplace_back(std::move(data[i]), orientations[i]);
    }

    return file;
}

HRTF::File SofaFileLoader::loadSimpleFreeFieldHRIR(const std::string& filename)
{
    HRTF::File file(filename);

    file.nEmitters = 1;

    try
    {
        const sofa::SimpleFreeFieldHRIR sofaFile(filename);

        if (!sofaFile.IsValid())
        {
            file.error = "Opening file failed";
            return file;
        }

        if (file.nReceivers = sofaFile.GetNumReceivers(); file.nReceivers == 0)
        {
            file.error = "Number of channels: 0";
            return file;
        }

        if (file.nSamples = sofaFile.GetNumDataSamples(); file.nSamples == 0)
        {
            file.error = "Number of samples: 0";
            return file;
        }

        if (file.nMeasurements = sofaFile.GetNumMeasurements(); file.nMeasurements == 0)
        {
            file.error = "Number of measurements: 0";
            return file;
        }

        if (double tmp; sofaFile.GetSamplingRate(tmp))
        {
            if (file.sampleRate = static_cast<unsigned>(tmp); file.sampleRate == 0)
            {
                file.error = "Invalid sample rate: 0";
                return file;
            }
        }
        else
        {
            file.error = "Failed to read sample rate";
            return file;
        }

        std::vector<HRTF::ListenerOrientation> listenerOrientations;

        if (file.nMeasurements == 1)
        {
            listenerOrientations.push_back(HRTF::ListenerOrientation(0, 0));
        }
        else
        {
            size_t nExpectedValues = file.nMeasurements * HRTF::ListenerOrientation::NumDimensions;

            if (std::vector<double> positions; sofaFile.GetSourcePosition(positions) && positions.size() == nExpectedValues)
            {
                for (auto it = positions.begin(); it != positions.end();)
                {
                    double azimuth = *it++;
                    double elevation = *it++;
                    [[maybe_unused]] double radius = *it++;

                    listenerOrientations.push_back(HRTF::ListenerOrientation(-azimuth, -elevation));
                }
            }
            else
            {
                file.error = "Number of listener positions: " + std::to_string(listenerOrientations.size()) + " (expected: " + std::to_string(file.nMeasurements) + ")";
                return file;
            }
        }

        std::vector<double> data;

        if (!sofaFile.GetDataIR(data) || data.size() != file.nMeasurements * file.nReceivers * file.nSamples)
        {
            file.error = "Invalid data size: " + std::to_string(data.size()) + " (expected: " + std::to_string(file.nMeasurements * file.nReceivers * file.nSamples) + ")";
            return file;
        }

        file.impulseResponses.reserve(file.nMeasurements);

        auto listenerOrientationIt = listenerOrientations.begin();
        for (auto it = data.begin(); it != data.end();)
        {
            HRTF::ImpulseResponseData measurement;

            measurement.allocate(RTC::Size()
                .with<HRTF::Receivers>(file.nReceivers)
                .with<HRTF::Emitters>(1)
                .with<Samples>(file.nSamples));

            for (auto& receiver : measurement)
            {
                auto emitter = receiver.emitter(0);

                // Copy with explicit type conversion
                std::transform(it, it + file.nSamples, emitter.begin(),
                    [](const auto& x) { return static_cast<AudioValueType>(x); });

                it += file.nSamples;
            }

            file.impulseResponses.emplace_back(std::move(measurement), *listenerOrientationIt++);
        }
    }
    catch (const std::exception& e)
    {
        file.error = e.what();
        return file;
    }

    return file;
}
