#pragma once

#include "../HRTF/File.hpp"
#include "../AudioProcessor/FrequencyDomainConvolution/FDCFFile.hpp"

class SofaFileLoader :

    public FDCF::File::LoaderBase,
    public HRTF::File::LoaderBase
{
public:
    FDCF::File loadFDCF(const std::string& filename) override;
    HRTF::File loadHRTF(const std::string& filename) override;

    static bool isSimpleFreeFieldHRIR(const std::string& filename);
    static bool isSimpleHeadphoneIR(const std::string& filename);

private:
    FDCF::File loadSimpleHeadphoneIR(const std::string& filename);

    HRTF::File load24(const std::string& filename);
    HRTF::File loadSimpleFreeFieldHRIR(const std::string& filename);
};
