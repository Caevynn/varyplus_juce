/*
  ==============================================================================

    FilenameHelper.hpp
    Created: 6 Nov 2023 12:07:23pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <string>
#include <algorithm>

class FilenameHelper
{
public:
    static std::string wihtoutPath(const std::string& filename)
    {
        size_t pos = filename.find_last_of("/\\");

        if (pos == std::string::npos)
        {
            return filename;
        }

        return filename.substr(pos + 1);
    }

    static std::string withoutAppendix(const std::string& filename)
    {
        size_t pos = filename.find_last_of(".");

        if (pos == std::string::npos)
        {
            return filename;
        }

        return filename.substr(0, pos);
    }

    static std::string appendix(const std::string& filename)
    {
        size_t pos = filename.find_last_of(".");

        if (pos == std::string::npos)
        {
            return "";
        }

        std::string appendix = filename.substr(pos + 1);

        // Convert to lower case
        std::transform(appendix.begin(), appendix.end(), appendix.begin(), [](unsigned char c)
            {
                return static_cast<unsigned char>(std::tolower(c));
            });

        return appendix;
    }
};
