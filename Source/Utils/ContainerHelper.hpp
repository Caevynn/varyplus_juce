/*
  ==============================================================================

    ContainerHelper.hpp
    Created: 18 Oct 2023 1:44:22pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <optional>

class ContainerHelper
{
public:
    template<typename Container, typename Element>
    static Element findClosest(const Container& container, const Element& element)
    {
        static_assert(std::is_arithmetic_v<Element>);

        if (container.size() == 0)
        {
            return element;
        }

        Element closestDiff = diff(static_cast<Element>(container.front()), element);
        Element closestElement = container.front();

        for (const auto& currentElement : container)
        {
            Element currentDiff = diff(static_cast<Element>(currentElement), element);
            if (currentDiff < closestDiff)
            {
                closestDiff = currentDiff;
                closestElement = currentElement;
            }
        }
        return closestElement;
    }

    template<typename Container, typename Element>
    static bool contains(const Container& container, const Element& element)
    {
        for (const auto& containedElement : container)
        {
            if (containedElement == element)
            {
                return true;
            }
        }

        return false;
    }

    template<typename Element, typename Container>
    static std::vector<Element> toVector(const Container& container, std::function<bool(Element)> validator = nullptr)
    {
        std::vector<Element> vector;

        for (const auto& element : container)
        {
            if (!validator || validator(static_cast<Element>(element)))
            {
                vector.push_back(static_cast<Element>(element));
            }
        }

        return vector;
    }

private:
    template<typename Element>
    static Element diff(const Element& a, const Element& b)
    {
        static_assert(std::is_arithmetic_v<Element>);

        return a > b ? a - b : b - a;
    }
};
