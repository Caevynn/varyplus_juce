/*
  ==============================================================================

    Decibel.hpp
    Created: 8 Nov 2023 4:54:20pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <cmath>

class Decibel
{
public:
    static double fromLinear(double linearValue)
    {
        double dBValue = 20 * std::log10(linearValue);
        return dBValue;
    }

    static double toLinear(double dBValue)
    {
        double linearValue = std::pow(10, dBValue / 20);
        return linearValue;
    }
};
