/*
  ==============================================================================

    LogRange.hpp
    Created: 8 Nov 2023 4:54:40pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <cmath>

class LogTaper
{
public:
    static double toLinear(double logValue, double linearRange = 1.0)
    {
        double normalizedLinearValue = a * std::exp(b * logValue) - a;
        return normalizedLinearValue * linearRange;
    }

    static double fromLinear(double linearValue, double linearRange = 1.0)
    {
        double normalizedLinearValue = linearValue / linearRange;
        return std::log((normalizedLinearValue + a) / a) / b;
    }

private:
    static constexpr double a{ 0.031623 };
    static constexpr double b{ 3.485 };

    // a = 1 / 10^(dbRange / 20)
    // b = std::log(1 / a + 1);
    // e.g.: a = 0.0.031623, b = 3.485 @ 30 dB
};
