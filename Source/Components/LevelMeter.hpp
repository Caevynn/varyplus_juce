/*
  ==============================================================================

    LevelMeter.hpp
    Created: 9 Nov 2023 12:07:05pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../Utils/LogTaper.hpp"

class LevelMeter : public juce::Component
{
public:
    void showCompression(bool enabled)
    {
        isCompressionEnabled = enabled;
    }

    void setValue(double peak, double compression = 1)
    {
        peakValue = peak;
        compressionValue = compression;
        isEnabled = true;

        repaint();
    }

    void setEnabled(bool enabled)
    {
        isEnabled = enabled;

        repaint();
    }

private:
    bool isEnabled{ false };
    bool isCompressionEnabled{ false };
    double peakValue{ 0 };
    double compressionValue{ 1 };

    void paint(juce::Graphics& g) override
    {
        auto peakArea = getLocalBounds();

        double zeroPosition = LogTaper::fromLinear(1, 2);
        int zeroHeight = static_cast<int>(zeroPosition * getHeight());

        if (!isEnabled)
        {
            g.setColour(juce::Colours::black.withAlpha(0.5f));

            if (isCompressionEnabled)
            {
                auto compressionArea = peakArea.removeFromRight(getWidth() / 3);
                compressionArea.removeFromLeft(1);

                g.fillRect(compressionArea.removeFromBottom(zeroHeight));
                compressionArea.removeFromBottom(1);
                g.fillRect(compressionArea);
            }

            g.fillRect(peakArea.removeFromBottom(zeroHeight));
            peakArea.removeFromBottom(1);
            g.fillRect(peakArea);

            return;
        }

        if (isCompressionEnabled)
        {
            auto compressionArea = peakArea.removeFromRight(getWidth() / 3);
            compressionArea.removeFromLeft(1);

            double compressionPosition = LogTaper::fromLinear(compressionValue, 2);
            int compressionHeight = static_cast<int>(compressionPosition * getHeight());

            g.setColour(juce::Colours::black);
            g.fillRect(compressionArea.removeFromBottom(compressionHeight));

            g.setColour(juce::Colours::darkred);
            g.fillRect(compressionArea.removeFromBottom(zeroHeight - compressionHeight));

            compressionArea.removeFromBottom(1);

            g.setColour(juce::Colours::black);
            g.fillRect(compressionArea);
        }

        double peakPosition = std::min(LogTaper::fromLinear(peakValue, 2), 1.0);
        int peakHeight = static_cast<int>(peakPosition * getHeight());

        if (peakHeight > 0)
        {
            double thresholdPosition = LogTaper::fromLinear(LimitingFilter::Threshold, 2);
            int thresholdHeight = static_cast<int>(thresholdPosition * getHeight());

            if (peakHeight > zeroHeight)
            {
                g.setColour(juce::Colours::darkgreen);
                g.fillRect(peakArea.removeFromBottom(thresholdHeight));

                g.setColour(juce::Colours::darkorange);
                g.fillRect(peakArea.removeFromBottom(zeroHeight - thresholdHeight));

                peakArea.removeFromBottom(1);

                g.setColour(juce::Colours::darkred);
                g.fillRect(peakArea.removeFromBottom(peakHeight - zeroHeight - 1));

                g.setColour(juce::Colours::black);
                g.fillRect(peakArea);
            }
            else if (peakHeight > thresholdHeight)
            {
                g.setColour(juce::Colours::darkgreen);
                g.fillRect(peakArea.removeFromBottom(thresholdHeight));

                g.setColour(juce::Colours::darkorange);
                g.fillRect(peakArea.removeFromBottom(peakHeight - thresholdHeight));

                g.setColour(juce::Colours::black);
                g.fillRect(peakArea.removeFromBottom(zeroHeight - peakHeight));

                peakArea.removeFromBottom(1);

                g.setColour(juce::Colours::black);
                g.fillRect(peakArea);
            }
            else
            {
                g.setColour(juce::Colours::darkgreen);
                g.fillRect(peakArea.removeFromBottom(peakHeight));

                g.setColour(juce::Colours::black);
                g.fillRect(peakArea.removeFromBottom(zeroHeight - peakHeight));

                peakArea.removeFromBottom(1);

                g.setColour(juce::Colours::black);
                g.fillRect(peakArea);
            }
        }
        else
        {
            g.setColour(juce::Colours::black);
            g.fillRect(peakArea.removeFromBottom(zeroHeight));

            peakArea.removeFromBottom(1);

            g.setColour(juce::Colours::black);
            g.fillRect(peakArea);
        }
    }
};
