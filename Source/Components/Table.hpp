/*
  ==============================================================================

    Table.hpp
    Created: 15 Nov 2023 11:03:26am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include <optional>

class TableHeader : public juce::TableHeaderComponent
{
private:
    // Disable resizing of columns
    void mouseDrag(const juce::MouseEvent&) override {}
    juce::MouseCursor getMouseCursor() override { return juce::MouseCursor(); }
};

class Table :
    private juce::TableListBoxModel,
    public juce::TableListBox
{
public:
    Table()
    {
        setModel(this);
        setHeader(std::make_unique<TableHeader>());
    }

    int getNumRows() override
    {
        return static_cast<int>(rows.size());
    }

    void paintRowBackground(juce::Graphics& g, int rowNumber, int /*width*/, int /*height*/, bool rowIsSelected) override
    {
        if (rowIsSelected)
        {
            g.fillAll(juce::Colours::lightblue);
        }
        else
        {
            if (rowNumber % 2 == 0)
            {
                g.fillAll(getLookAndFeel().findColour(juce::ListBox::backgroundColourId));
            }
            else
            {
                auto backgroundColour = getLookAndFeel().findColour(juce::ListBox::backgroundColourId);
                auto textColour = getLookAndFeel().findColour(juce::ListBox::textColourId);

                g.fillAll(backgroundColour.interpolatedWith(textColour, 0.03f));
            }
        }
    }

    void paintCell(juce::Graphics& g, int rowNumber, int columnId, int width, int height, bool rowIsSelected) override
    {
        if (rowNumber < static_cast<int>(rows.size()))
        {
            if (isRowActive[rowNumber])
            {
                if (rowIsSelected)
                {
                    g.setColour(juce::Colours::darkblue);
                }
                else
                {
                    g.setColour(getLookAndFeel().findColour(juce::ListBox::textColourId));
                }
            }
            else
            {
                g.setColour(juce::Colours::lightgrey.interpolatedWith(juce::Colours::grey, 0.5f));
            }

            auto& columns = rows[rowNumber];

            size_t colNumber = static_cast<size_t>(columnId - 1);
            if (colNumber < columns.size())
            {
                auto& cell = columns[colNumber];

                g.drawText(cell, 2, 0, width - 4, height, juce::Justification::centredLeft);
            }
        }
    }

    bool addColumn(const std::string& heading, std::optional<unsigned> columnWidth = std::nullopt)
    {
        if (rows.empty())
        {
            columnWidths.push_back(columnWidth);
            getHeader().addColumn(heading, getHeader().getNumColumns(true) + 1, 50);
            return true;
        }

        return false;
    }

    bool addRow(const std::vector<std::string>& row)
    {
        if (static_cast<int>(row.size()) == getHeader().getNumColumns(true))
        {
            rows.push_back(row);
            isRowActive.push_back(true);
            updateContent();
            return true;
        }

        return false;
    }

    void removeAllRows()
    {
        rows.clear();
        isRowActive.clear();
        updateContent();
    }

    void setRowActive(size_t index, bool active)
    {
        if (index < isRowActive.size())
        {
            if (isRowActive[index] != active)
            {
                isRowActive[index] = active;
                repaint();
            }
        }
    }

    std::function<void(size_t)> onDelete;

private:
    std::vector<std::optional<unsigned>> columnWidths;
    std::vector<std::vector<std::string>> rows;
    std::vector<bool> isRowActive;

    void backgroundClicked(const juce::MouseEvent&) override
    {
        deselectAllRows();
    }

    void deleteKeyPressed(int currentSelectedRow) override
    {
        if (currentSelectedRow >= 0 && currentSelectedRow < static_cast<int>(rows.size()))
        {
            if (onDelete) { onDelete(static_cast<size_t>(currentSelectedRow)); }
        }
    }

    void resized() override
    {
        juce::TableListBox::resized();

        unsigned usableWidth = getWidth();

        if (getVerticalScrollBar().isVisible())
        {
            usableWidth -= getVerticalScrollBar().getWidth();
        }

        unsigned numFlexibleColumns{ 0 };
        unsigned usedWidth{ 0 };

        for (const auto& width : columnWidths)
        {
            if (width)
            {
                usedWidth += *width;
            }
            else
            {
                ++numFlexibleColumns;
            }
        }

        int columnId{ 1 };
        for (const auto& width : columnWidths)
        {
            if (width)
            {
                getHeader().setColumnWidth(columnId, *width);
            }
            else
            {
                unsigned flexibleWidth = (usableWidth - usedWidth) / numFlexibleColumns;
                usedWidth += flexibleWidth;
                --numFlexibleColumns;

                getHeader().setColumnWidth(columnId, flexibleWidth);
            }

            ++columnId;
        }
    }
};
