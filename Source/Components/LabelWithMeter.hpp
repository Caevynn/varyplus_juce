/*
  ==============================================================================

    LabelWithMeter.hpp
    Created: 15 Nov 2023 2:50:18pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "Meter.hpp"

class LabelWithMeter : public juce::Component
{
public:
    LabelWithMeter()
    {
        addAndMakeVisible(label);
        addAndMakeVisible(meter);
    }

    void setLabelText(const std::string& text)
    {
        label.setText(text, juce::dontSendNotification);
    }

    void setMeterValue(double value)
    {
        meter.setValue(value);
    }

    void setBackgroundColour(juce::Colour newBackgroundColour)
    {
        backgroundColour = newBackgroundColour;
        repaint();
    }

private:
    static constexpr int maxMeterSize = 150;

    juce::Label label;
    Meter meter;

    juce::Colour backgroundColour;

    void paint(juce::Graphics& g) override
    {
        g.fillAll(backgroundColour);
    }

    void resized() override
    {
        auto area = getLocalBounds();

        meter.setBounds(area.removeFromRight(std::min(maxMeterSize, getWidth() / 2)).reduced(10, 5));
        label.setBounds(area);
    }
};
