/*
  ==============================================================================

    LabeledSlider.hpp
    Created: 8 Nov 2023 3:42:12pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "Slider.hpp"
#include "../Utils/Decibel.hpp"
#include "../Utils/LogTaper.hpp"

class LevelSlider :
    public juce::Component
{
public:
    LevelSlider()
    {
        label.setEditable(false, true);
        label.setJustificationType(juce::Justification::centred);
        label.onTextChange = [this]()
        {
            auto text = label.getText();

            if (std::isdigit(text[0]) || (text[0] == '-' && std::isdigit(text[1])))
            {
                double newValue = Decibel::toLinear(text.getDoubleValue());

                if (slider.isAutomaticValueChange())
                {
                    setValue(newValue);
                }

                if (onChange) { onChange(newValue); }
            }
            else
            {
                updateLabel();
            }
        };
        addAndMakeVisible(label);

        slider.setRange(0, 1);
        slider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, true, 0, 0);
        slider.onValueChange = [this](double newValue)
        {
            if (slider.isAutomaticValueChange())
            {
                updateLabel();
            }

            if (onChange) { onChange(LogTaper::toLinear(newValue, 2)); }
        };
        slider.onRightClick = [this]()
        {
            if (slider.isAutomaticValueChange())
            {
                setValue(1);
            }

            if (onChange) { onChange(1); }
        };
        addAndMakeVisible(slider);

        setValue(1);
    }

    void disableAutomaticValueChange()
    {
        slider.disableAutomaticValueChange();
    }

    void setValue(double value)
    {
        double newSliderValue = LogTaper::fromLinear(value, 2);

        if (std::abs(newSliderValue - slider.getValue()) > 1e-6)
        {
            slider.setValue(newSliderValue, juce::dontSendNotification);
            updateLabel();
        }
    }

    std::function<void(double)> onChange;

private:
    juce::Label label;
    Slider slider;

    void updateLabel()
    {
        double linearValue = LogTaper::toLinear(slider.getValue(), 2);

        if (linearValue < 1e-6)
        {
            label.setText(juce::String::fromUTF8(u8"-\u221E dB"), juce::dontSendNotification);
        }
        else
        {
            double dBValue = Decibel::fromLinear(linearValue);
            double abs = std::abs(dBValue);
            int natural = static_cast<int>(abs);
            int cent = static_cast<int>((abs - natural) * 100 + 0.5);
            if (cent >= 100)
            {
                natural += 1;
                cent -= 100;
            }
            std::string sign = dBValue < 0 && !(natural == 0 && cent == 0) ? "-" : "";
            std::string optionalZero = cent < 10 ? "0" : "";
            label.setText(sign + std::to_string(natural) + "." + optionalZero + std::to_string(cent) + " dB", juce::dontSendNotification);
        }
    }

    void resized() override
    {
        auto area = getLocalBounds();

        label.setBounds(area.removeFromLeft(getWidth() / 3));
        slider.setBounds(area);
    }
};
