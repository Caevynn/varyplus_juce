/*
  ==============================================================================

    PlotWithStatistics.hpp
    Created: 20 Oct 2023 2:06:59pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "Plot.hpp"

template<typename ValueType = float>
class PlotWithStatistics : public juce::Component, private juce::Timer
{
public:
    class Statistics
    {
    public:
        void reset()
        {
            min = 0;
            max = 0;
            sum = 0;
            count = 0;
        }

        void push(ValueType value)
        {
            if (count > 0)
            {
                min = std::min(min, value);
                max = std::max(max, value);
            }
            else
            {
                min = value;
                max = value;
            }

            sum += value;
            ++count;
        }

        ValueType getMin() const { return min; }
        ValueType getMax() const { return max; }
        ValueType getAvg() const { return sum / std::max(count, 1u); }

    private:
        ValueType min{ 0 }, max{ 0 }, sum{ 0 };
        unsigned count{ 0 };
    };

    PlotWithStatistics(const std::string& title)
    {
        nameLabel.setText(title, juce::NotificationType::dontSendNotification);
        minLabel.setText("Min: -", juce::NotificationType::dontSendNotification);
        maxLabel.setText("Max: -", juce::NotificationType::dontSendNotification);
        avgLabel.setText("Avg: -", juce::NotificationType::dontSendNotification);
        posLabel.setText("Pos: - ", juce::NotificationType::dontSendNotification);

        addAndMakeVisible(plot);
        addAndMakeVisible(nameLabel);
        addAndMakeVisible(minLabel);
        addAndMakeVisible(maxLabel);
        addAndMakeVisible(avgLabel);
        addAndMakeVisible(posLabel);

        plot.onMouseMove = [this](std::optional<ValueType> value)
        {
            if (value)
            {
                posLabel.setText("Pos: " + std::to_string(static_cast<int>(*value)), juce::NotificationType::dontSendNotification);
            }
            else
            {
                posLabel.setText("Pos: - ", juce::NotificationType::dontSendNotification);
            }
        };

        plot.onMouseDown = [this]()
        {
            statistics.reset();
        };

        startTimerHz(25);
    }

    void reset()
    {
        plot.reset();
        statistics.reset();
    }

    void push(ValueType value)
    {
        plot.push(value);
        statistics.push(value);
    }

private:
    Plot<ValueType> plot;

    Statistics statistics;

    juce::Label nameLabel, minLabel, maxLabel, avgLabel, posLabel;

    void resized() override
    {
        auto area = getLocalBounds();

        auto labelArea = area.removeFromBottom(30);

        plot.setBounds(area.reduced(3));
        nameLabel.setBounds(labelArea.removeFromLeft(getWidth() / 5).reduced(3));
        minLabel.setBounds(labelArea.removeFromLeft(getWidth() / 5).reduced(3));
        maxLabel.setBounds(labelArea.removeFromLeft(getWidth() / 5).reduced(3));
        avgLabel.setBounds(labelArea.removeFromLeft(getWidth() / 5).reduced(3));
        posLabel.setBounds(labelArea.reduced(3));
    }

    void timerCallback() override
    {
        using Range = typename Plot<ValueType>::Range;
        using Marker = typename Plot<ValueType>::Marker;

        plot.setRange(Range(0, statistics.getMax() * ValueType(1.1)));
        plot.setMarkers(
            Marker(statistics.getAvg(), juce::Colours::red),
            Marker(statistics.getMin(), juce::Colours::blue),
            Marker(statistics.getMax(), juce::Colours::blue));
        plot.repaint();

        minLabel.setText("Min: " + std::to_string(static_cast<int>(statistics.getMin())), juce::NotificationType::dontSendNotification);
        maxLabel.setText("Max: " + std::to_string(static_cast<int>(statistics.getMax())), juce::NotificationType::dontSendNotification);
        avgLabel.setText("Avg: " + std::to_string(static_cast<int>(statistics.getAvg())), juce::NotificationType::dontSendNotification);
    }
};
