/*
  ==============================================================================

    PlaybackSlider.hpp
    Created: 9 Nov 2023 8:45:22pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "Slider.hpp"

class PlaybackSlider :
    public juce::Component
{
public:
    PlaybackSlider()
    {
        label.setJustificationType(juce::Justification::centred);
        addAndMakeVisible(label);

        slider.setRange(0, 1);
        slider.disableAutomaticValueChange();
        slider.setValue(0);
        slider.setTextBoxStyle(juce::Slider::TextEntryBoxPosition::NoTextBox, true, 0, 0);
        slider.onValueChange = [this](double newValue)
        {
            if (onChange) { onChange(static_cast<size_t>(newValue * *playbackLength)); }
        };
        addAndMakeVisible(slider);

        updateLabel();
    }

    void setValues(size_t position, std::optional<size_t> length, unsigned sampleRate)
    {
        label.setEnabled(length.operator bool());
        slider.setEnabled(length.operator bool());

        if (length)
        {
            slider.setValue(static_cast<double>(position) / *length, juce::dontSendNotification);
        }
        else
        {
            if (playbackLength)
            {
                slider.setValue(0, juce::dontSendNotification);
            }
        }

        playbackLength = length;
        playbackSampleRate = sampleRate;

        updateLabel();
    }

    std::function<void(size_t)> onChange;

private:
    juce::Label label;
    Slider slider;

    std::optional<size_t> playbackLength;
    unsigned playbackSampleRate;

    void resized() override
    {
        auto area = getLocalBounds();

        label.setBounds(area.removeFromLeft(getWidth() / 3));
        slider.setBounds(area);
    }

    void updateLabel()
    {
        if (playbackLength)
        {
            double length_s = static_cast<double>(*playbackLength) / playbackSampleRate;
            double position_s = slider.getValue() * length_s;

            label.setText(timeToString(position_s) + " / " + timeToString(length_s), juce::dontSendNotification);
        }
        else
        {
            label.setText("--:-- / --:--", juce::dontSendNotification);
        }
    }

    std::string timeToString(double time_s)
    {
        int minutes = static_cast<int>(time_s / 60);
        int seconds = static_cast<int>(time_s - 60 * minutes);
        std::string optionalZero = seconds < 10 ? "0" : "";
        return std::to_string(minutes) + ":" + optionalZero + std::to_string(seconds);
    }
};