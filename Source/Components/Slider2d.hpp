/*
  ==============================================================================

    Slider2d.hpp
    Created: 13 Nov 2023 12:50:10pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class Slider2d : public juce::Component
{
public:
    void setValue(double x, double y)
    {
        setPosition(static_cast<float>(x), static_cast<float>(y), false);
    }

    void disableAutomaticValueChange() { automaticValueChange = false; }

    std::function<void(double, double)> onValueChange;

private:
    bool automaticValueChange{ true };

    juce::Slider slider;
    juce::Point<float> position{ 0, 0 };

    void resized() override
    {
        slider.setBounds(getLocalBounds());
    }

    void paint(juce::Graphics& g) override
    {
        int thumbSize = getLookAndFeel().getSliderThumbRadius(slider);
        auto area = getLocalBounds().reduced(thumbSize / 2);

        juce::Point<float> center{ static_cast<float>(area.getCentreX()), static_cast<float>(area.getCentreY()) };
        juce::Rectangle<float> trackRectangle(static_cast<float>(area.getWidth()), static_cast<float>(area.getHeight()));

        g.setColour(slider.findColour(juce::Slider::trackColourId));
        g.fillRoundedRectangle(trackRectangle.withCentre(center), static_cast<float>(thumbSize / 4));

        juce::Rectangle<float> knobRectangle(static_cast<float>(thumbSize), static_cast<float>(thumbSize));
        juce::Point<float> knobPosition{ center.x + position.x * area.getWidth() / 2, center.y + position.y * area.getHeight() / 2 };

        g.setColour(slider.findColour(juce::Slider::thumbColourId));
        g.fillEllipse(knobRectangle.withCentre(knobPosition));
    }

    void mouseDown(const juce::MouseEvent& event) override
    {
        if (event.mods.isRightButtonDown())
        {
            setPosition(0, 0, true);
        }
        else
        {
            mouseDrag(event);
        }
    }

    void mouseDrag(const juce::MouseEvent& event) override
    {
        if (event.mods.isLeftButtonDown())
        {
            int thumbSize = getLookAndFeel().getSliderThumbRadius(slider);
            auto area = getLocalBounds().reduced(thumbSize / 2);

            int x = std::clamp(event.getPosition().x, area.getX(), area.getRight() - 1) - area.getX();
            int y = std::clamp(event.getPosition().y, area.getY(), area.getBottom() - 1) - area.getY();

            float relativeX = static_cast<float>(x - area.getWidth() / 2) / area.getWidth() * 2;
            float relativeY = static_cast<float>(y - area.getHeight() / 2) / area.getHeight() * 2;
         
            setPosition(relativeX, relativeY, true);
        }
    }

    void setPosition(float x, float y, bool notify)
    {
        if (notify)
        {
            if (onValueChange) { onValueChange(x, y); }
        }

        if(automaticValueChange || !notify)
        {
            position = { x, y };
            repaint();
        }
    }
};
