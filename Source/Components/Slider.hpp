/*
  ==============================================================================

    Slider.hpp
    Created: 13 Nov 2023 12:22:41pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class Slider : public juce::Slider
{
public:
    Slider()
    {
        juce::Slider::onValueChange = [this]()
        {
            if (automaticValueChange)
            {
                if (onValueChange && isEnabled()) { onValueChange(getValue()); }
            }
        };
    }

    void disableAutomaticValueChange() { automaticValueChange = false; }
    bool isAutomaticValueChange() const { return automaticValueChange; }

    std::function<void(double)> onValueChange;
    std::function<void()> onRightClick;

private:
    bool automaticValueChange{ true };

    void mouseDown(const juce::MouseEvent& event) override
    {
        if (event.mods.isLeftButtonDown())
        {
            if (automaticValueChange)
            {
                juce::Slider::mouseDown(event);
            }
            else
            {
                mouseDrag(event);
            }
        }
        else
        {
            if (onRightClick && isEnabled()) { onRightClick(); }
        }
    }

    void mouseDrag(const juce::MouseEvent& event) override
    {
        if (event.mods.isLeftButtonDown())
        {
            if (automaticValueChange)
            {
                juce::Slider::mouseDrag(event);
            }
            else
            {
                auto area = getLookAndFeel().getSliderLayout(*this).sliderBounds;

                double xPosition = static_cast<double>(event.getPosition().x - area.getX());
                double relativeXPosition = std::clamp(xPosition / area.getWidth(), 0.0, 1.0);

                if (onValueChange && isEnabled()) { onValueChange(relativeXPosition); }
            }
        }
    }

    void mouseWheelMove(const juce::MouseEvent& event, const juce::MouseWheelDetails& wheel) override
    {
        if (automaticValueChange)
        {
            juce::Slider::mouseWheelMove(event, wheel);
        }
        else
        {
            double move = wheel.deltaY > 0 ? 0.1 : -0.1;
            double relativeXPosition = std::clamp(getValue() + move, 0.0, 1.0);

            if (onValueChange && isEnabled()) { onValueChange(relativeXPosition); }
        }
    }
};
