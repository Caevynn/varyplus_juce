/*
  ==============================================================================

    LabeledCombo.hpp
    Created: 18 Oct 2023 5:21:47pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "ComboBox.hpp"

class LabeledCombo : public juce::Component
{
public:
    LabeledCombo()
    {
        addAndMakeVisible(label);
        addAndMakeVisible(combo);

        combo.onChange = [this](int newId) { if (onChange) { onChange(static_cast<unsigned>(newId) - 1); } };
    }

    void setLabelText(const std::string& text) { label.setText(text, juce::dontSendNotification); }
    void setEnabled(bool shouldBeEnabled) { label.setEnabled(shouldBeEnabled);  combo.setEnabled(shouldBeEnabled); }

    void clear() { combo.clear(juce::dontSendNotification); }
    void addItem(const std::string& text, unsigned ID) { combo.addItem(text, ID + 1); }
    unsigned getNumItems() const { return combo.getNumItems(); }

    void disableAutomaticSelectionChange() { combo.disableAutomaticSelectionChange(); }
    unsigned getSelectedId() const { return combo.getSelectedId() - 1; }
    void setSelectedId(unsigned ID) { combo.setSelectedId(static_cast<int>(ID + 1), juce::dontSendNotification); }

    void addListener(juce::ComboBox::Listener* listener) { combo.addListener(listener); }
    void removeListener(juce::ComboBox::Listener* listener) { combo.removeListener(listener); }

    void addSeparator() { combo.addSeparator(); }
    void addSectionHeading(const std::string& text) { combo.addSectionHeading(text); }

    void hidePopup() { combo.hidePopup(); }

    bool isPointerOf(juce::ComboBox* pointer) { return pointer == &combo; }

    std::function<void(unsigned)> onChange;

private:
    juce::Label label;
    ComboBox combo;

    void resized() override
    {
        auto area = getLocalBounds();
        label.setBounds(area.removeFromLeft(getWidth() / 3));
        combo.setBounds(area);
    }
};
