/*
  ==============================================================================

    NumericLabel.hpp
    Created: 26 Jan 2024 11:12:27pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class NumericLabel : public juce::Label
{
public:
    NumericLabel()
    {
        setEditable(false, true);
        setJustificationType(juce::Justification::centred);
        onTextChange = [this]()
            {
                auto text = getText();

                if (std::isdigit(text[0]) || (text[0] == '-' && std::isdigit(text[1])))
                {
                    currentValue = text.getDoubleValue();

                    if (onChange) { onChange(currentValue); }
                }

                setValue(currentValue);
            };

        setColour(juce::Label::backgroundColourId, getLookAndFeel().findColour(juce::ComboBox::backgroundColourId));
        setColour(juce::Label::outlineColourId, getLookAndFeel().findColour(juce::ComboBox::outlineColourId));

        setValue(0);
    }

    void setUnit(const std::string& wantedUnit)
    {
        unit = " " + wantedUnit;
    }

    double getValue() const { return currentValue; }

    void setValue(double newValue)
    {
        currentValue = newValue;

        double abs = std::abs(currentValue);
        int natural = static_cast<int>(abs);
        int cent = static_cast<int>((abs - natural) * 100 + 0.5);
        if (cent >= 100)
        {
            natural += 1;
            cent -= 100;
        }

        std::string sign = currentValue < 0 && !(natural == 0 && cent == 0) ? "-" : "";
        std::string optionalZero = cent < 10 ? "0" : "";
        setText(sign + std::to_string(natural) + "." + optionalZero + std::to_string(cent) + unit, juce::dontSendNotification);
    }

    std::function<void(double)> onChange;

private:
    double currentValue{ 0 };
    std::string unit;
};
