/*
  ==============================================================================

    Plot.hpp
    Created: 20 Oct 2023 2:06:49pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

template<typename ValueType = float, size_t BufferSize = 256>
class Plot : public juce::Component
{
public:
    struct Range
    {
        ValueType min, max;

        Range(ValueType min, ValueType max) : min(min), max(max) {}
    };

    struct Marker
    {
        ValueType value;
        juce::Colour colour;

        Marker(ValueType value, juce::Colour colour = juce::Colours::black) : value(value), colour(colour) {}
    };

    Plot() : data(BufferSize, 0), range(0, 0) {}

    void reset()
    {
        std::fill(data.begin(), data.end(), ValueType(0));
        markers.clear();
    }

    void push(ValueType value)
    {
        currentIndex = (currentIndex + 1) % data.size();
        data[currentIndex] = value;
    }

    void push(const std::vector<ValueType>& values)
    {
        for (const auto& value : values)
        {
            push(value);
        }
    }

    void setMarkers(const std::vector<Marker>& wantedMarkers)
    {
        markers = wantedMarkers;
    }

    template<typename... Args>
    void setMarkers(Args&&... args)
    {
        markers.clear();
        (markers.push_back(std::forward<Args>(args)), ...);
    }

    void setRange(Range wantedRange)
    {
        range = wantedRange;

        updateMousePosition();
    }

    std::function<void(std::optional<ValueType>)> onMouseMove;
    std::function<void()> onMouseDown;

private:
    size_t currentIndex{ 0 };
    std::vector<ValueType> data;
    Range range;
    std::vector<Marker> markers;
    bool isMouseCaptured{ false };

    ValueType getValue(size_t position) const
    {
        return data[(currentIndex + position + 1) % data.size()];
    }

    void updateMousePosition() const
    {
        if (onMouseMove && isMouseCaptured)
        {
            float mouseX = static_cast<float>(getMouseXYRelative().getX());
            float mouseY = static_cast<float>(getMouseXYRelative().getY());
            float width = static_cast<float>(getWidth());
            float height = static_cast<float>(getHeight());

            if (mouseX >= 0 && mouseX <= width && mouseY >= 0 && mouseY <= height)
            {
                float relativeY = (height - mouseY) / height;
                onMouseMove(relativeY * (range.max - range.min) + range.min);
            }
        }
    }

    void paint(juce::Graphics& g) override
    {
        g.fillAll(juce::Colours::white);

        if (range.max == range.min)
        {
            // Nothing to paint
            return;
        }

        float height = static_cast<float>(getHeight());
        float width = static_cast<float>(getWidth());

        float horizontalScale = width / data.size();
        float verticalScale = height / static_cast<float>(range.max - range.min);

        juce::Path dataPath;
        float x0 = 0;
        float y0 = verticalScale * static_cast<float>(getValue(0) - range.min);
        dataPath.startNewSubPath(x0, height - y0);
        for (size_t position = 1; position < data.size(); ++position)
        {
            float x = horizontalScale * position;
            float y = verticalScale * static_cast<float>(getValue(position) - range.min);
            dataPath.lineTo(x, height - y);
        }
        g.setColour(juce::Colours::black);
        g.strokePath(dataPath, juce::PathStrokeType(1));

        for (const auto& marker : markers)
        {
            juce::Path markerPath;
            float y = verticalScale * static_cast<float>(marker.value - range.min);
            markerPath.startNewSubPath(0, height - y);
            markerPath.lineTo(width, height - y);
            g.setColour(marker.colour);
            g.strokePath(markerPath, juce::PathStrokeType(1));
        }
    }

    void mouseMove(const juce::MouseEvent&) override
    {
        isMouseCaptured = true;

        updateMousePosition();
    }

    void mouseExit(const juce::MouseEvent&) override
    {
        isMouseCaptured = false;

        if (onMouseMove) { onMouseMove(std::nullopt); }
    }

    void mouseDown(const juce::MouseEvent&) override
    {
        if (onMouseDown) { onMouseDown(); }
    }
};
