/*
  ==============================================================================

    WindowWithContent.hpp
    Created: 19 Oct 2023 1:58:34pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

template<typename Content>
class WindowWithContent : public juce::DocumentWindow
{
public:
    template<typename ...Args>
    WindowWithContent(const std::string& title, int width, int height, bool resizable, Args&& ...args) :
        juce::DocumentWindow(title, juce::Colours::white, juce::DocumentWindow::TitleBarButtons::allButtons),
        content(std::forward<Args>(args)...)
    {
        content.setSize(width, height);
        setUsingNativeTitleBar(true);
        setContentNonOwned(&content, true);
        centreWithSize(width, height);
        setResizable(resizable, false);

        if (!resizable)
        {
            setTitleBarButtonsRequired(TitleBarButtons::closeButton, false);
        }
    }

    void setTitle(const std::string& newTitle)
    {
        setName(newTitle);
    }

    void show()
    {
        setVisible(true);
        toFront(false);
    }

    void hide()
    {
        setVisible(false);
    }

    Content& getContent() { return content; }
    const Content& getContent() const { return content; }

private:
    Content content;

    void closeButtonPressed() override
    {
        hide();
    }
};
