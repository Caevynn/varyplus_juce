/*
  ==============================================================================

    ComboBox.hpp
    Created: 13 Nov 2023 12:22:49pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class ComboBox : public juce::ComboBox
{
public:
    ComboBox()
    {
        juce::ComboBox::onChange = [this]()
        {
            if (automaticSelectionChange)
            {
                if (onChange) { onChange(getSelectedId()); }
            }
        };
    }

    void disableAutomaticSelectionChange() { automaticSelectionChange = false; }

    std::function<void(int)> onChange;

private:
    bool automaticSelectionChange{ true };

    void mouseDown(const juce::MouseEvent& event) override
    {
        if (automaticSelectionChange)
        {
            juce::ComboBox::mouseDown(event);
        }
        else
        {
            for (juce::PopupMenu::MenuItemIterator iterator(*getRootMenu(), true); iterator.next();)
            {
                auto& item = iterator.getItem();
                if (item.itemID != 0) { item.isTicked = (item.itemID == getSelectedId()); }
            }

            getRootMenu()->showMenuAsync(juce::PopupMenu::Options().withTargetComponent(this).withMinimumWidth(getWidth()).withInitiallySelectedItem(getSelectedId()),
                [this](int resultID)
                {
                    if (resultID > 0 && resultID != getSelectedId())
                    {
                        if (onChange) { onChange(resultID); }
                    }
                });
        }
    }
};
