/*
  ==============================================================================

    Meter.hpp
    Created: 15 Nov 2023 2:50:07pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class Meter : public juce::Component
{
public:
    void setValue(double newValue)
    {
        value = newValue;
        repaint();
    }

private:
    double value;

    void paint(juce::Graphics& g) override
    {
        g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));

        auto area = getLocalBounds().reduced(3);

        int barWidth = static_cast<int>(std::min(value, 1.0) * area.getWidth());

        auto lowerArea = area.removeFromLeft(area.getWidth() / 2);
        auto upperArea = area;

        if (value <= 0.5)
        {
            g.setGradientFill(juce::ColourGradient::horizontal(juce::Colours::green, juce::Colours::orange, lowerArea));
            g.fillRect(lowerArea.removeFromLeft(barWidth));
        }
        else
        {
            g.setGradientFill(juce::ColourGradient::horizontal(juce::Colours::green, juce::Colours::orange, lowerArea));
            g.fillRect(lowerArea);

            g.setGradientFill(juce::ColourGradient::horizontal(juce::Colours::orange, juce::Colours::red, upperArea));
            g.fillRect(upperArea.removeFromLeft(barWidth - lowerArea.getWidth()));
        }
    }
};
