/*
  ==============================================================================

    TabbedComponent.hpp
    Created: 15 Nov 2023 1:41:13pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class TabBarButton : public juce::TabBarButton
{
public:
    TabBarButton(const juce::String& name, juce::TabbedButtonBar& owner) :
        juce::TabBarButton(name, owner)
    {}

    int getBestTabLength(int) override
    {
        int totalWidth = getTabbedButtonBar().getWidth();
        int numTabs = getTabbedButtonBar().getNumTabs();
        int tabWidth = totalWidth / numTabs;

        if (getIndex() == numTabs - 1)
        {
            return totalWidth - (numTabs - 1) * tabWidth;
        }
        else
        {
            return tabWidth;
        }
    }
};

class TabbedComponent : public juce::TabbedComponent
{
public:
    TabbedComponent(juce::TabbedButtonBar::Orientation orientation) :
        juce::TabbedComponent(orientation)
    {}

protected:
    juce::TabBarButton* createTabButton(const juce::String& tabName, int /*tabIndex*/) override
    {
        return new TabBarButton(tabName, getTabbedButtonBar());
    }
};
