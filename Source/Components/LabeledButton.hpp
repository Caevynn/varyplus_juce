/*
  ==============================================================================

    LabeledButton.hpp
    Created: 18 Oct 2023 5:21:56pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class LabeledButton : public juce::Component
{
public:
    LabeledButton()
    {
        addAndMakeVisible(label);
        addAndMakeVisible(button);

        button.onClick = [this]() { if (onClick) { onClick(); } };
    }

    void setLabelText(const std::string& text) { label.setText(text, juce::NotificationType::dontSendNotification); }
    void setButtonText(const std::string& text) { button.setButtonText(text); }
    void setEnabled(bool shouldBeEnabled) { label.setEnabled(shouldBeEnabled);  button.setEnabled(shouldBeEnabled); }

    std::function<void()> onClick;

private:
    juce::Label label;
    juce::TextButton button;

    void resized() override
    {
        auto area = getLocalBounds();
        label.setBounds(area.removeFromLeft(getWidth() / 3));
        button.setBounds(area);
    }
};
