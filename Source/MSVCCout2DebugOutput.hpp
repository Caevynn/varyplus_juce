/*
  ==============================================================================

    MSVCCout2DebugOutput.hpp
    Created: 3 Nov 2023 5:06:14pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#ifdef _MSC_VER

#include <iostream>

#ifndef NOMINMAX
#   define NOMINMAX
#endif
#include <Windows.h>

class MSVCCout2DebugOutput
{
    class DebugStreambuf : public std::streambuf
    {
    public:
        virtual int_type overflow(int_type c = EOF) {
            if (c != EOF) {
                TCHAR buf[] = { static_cast<TCHAR>(c), '\0' };
                OutputDebugString(buf);
            }
            return c;
        }
    };

    DebugStreambuf dbgstream;
    std::streambuf* default_stream;

public:
    MSVCCout2DebugOutput()
    {
        default_stream = std::cout.rdbuf(&dbgstream);
    }

    ~MSVCCout2DebugOutput()
    {
        std::cout.rdbuf(default_stream);
    }
};
#else
class MSVCCout2DebugOutput {};
#endif
