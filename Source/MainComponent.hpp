#pragma once

#include <JuceHeader.h>

#include "AudioDevice/AudioDeviceManager.hpp"
#include "AudioDevice/AudioDeviceEditor.hpp"

#include "Shrimp/Processor/MainProcessor.hpp"
#include "Shrimp/Editor/MainEditor.hpp"
#include "Shrimp/Editor/FileEditor.hpp"

#include "DebugOutput/TextAndPlotDebugOutput.hpp"

#include "AudioDevice/AudioDeviceCallback.hpp"
#include "OSCImplementation.hpp"
#include "AudioFileLoader/AudioFileLoader.hpp"
#include "SofaFileLoader/SofaFileLoader.hpp"

#include "Components/TabbedComponent.hpp"
#include "Components/LabelWithMeter.hpp"

#include "MSVCCout2DebugOutput.hpp"

class MainComponent  :
    public juce::Component,
    public juce::FileDragAndDropTarget,
    private juce::Timer
{
public:
    MainComponent();

    std::string getMainTitle()
    {
        if (audioProcessor.getMainInterface().getNumInputBusChannels() == 24)
        {
            return "VIKKtor";
        }
        else
        {
            return "Shrimp";
        }
    }

    std::function<void(const std::string&)> onTitleChange;

private:
    // Construct first to allow debug output via cout in constructors
    MSVCCout2DebugOutput msvcCout2DebugOutput;

    TabbedComponent tabbedComponent;
    LabelWithMeter stateLabel;

    TextAndPlotDebugOutput debugOutput;

    Shrimp::MainProcessor<AudioDeviceCallback, OSCImplementation, AudioFileLoader, SofaFileLoader> audioProcessor;
    Shrimp::MainEditor mainEditor;
    Shrimp::FileEditor fileEditor;

    AudioDeviceManager audioDeviceManager;
    AudioDeviceEditor audioDeviceEditor;

    bool isShrimpVikktorSelected{ false };
    bool shrimpNotVikktor{ false };
    juce::TextButton shrimpButton;
    juce::TextButton vikktorButton;

    // juce::Component overrides
    void paint(juce::Graphics&) override;
    void resized() override;

    // juce::FileDragAndDropTarget overrides
    bool isInterestedInFileDrag(const juce::StringArray& files) override;
    void filesDropped(const juce::StringArray& files, int x, int y) override;

    // juce::Timer overrides
    void timerCallback() override;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
