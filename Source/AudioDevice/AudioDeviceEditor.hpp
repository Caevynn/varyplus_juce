/*
  ==============================================================================

    DeviceManagerEditor.hpp
    Created: 17 Oct 2023 1:08:22pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "AudioDeviceManager.hpp"

#include "../Components/LabeledButton.hpp"
#include "../Components/LabeledCombo.hpp"

class AudioDeviceEditor : public juce::Component
{
public:
    AudioDeviceEditor(AudioDeviceManager& deviceManager, DebugOutput& debugOutput) :
        debugOutput(debugOutput),
        deviceManager(deviceManager)
    {
        deviceBox.setLabelText("Device");
        deviceBox.onChange = [this](unsigned)
        {
            if (auto selectedDevice = getSelectedDevice())
            {
                if (this->deviceManager.selectDevice(*selectedDevice))
                {
                    updateControls();
                    updateState();
                }

                if (!this->deviceManager.getActiveDevice().isAvailable())
                {
                    std::string message = "Device " + selectedDevice->composedName() + " is not available.";
                    juce::AlertWindow::showAsync(juce::MessageBoxOptions().withButton("Ok").withMessage(message), nullptr);
                }
            }
        };
        addAndMakeVisible(deviceBox);

        inputChannelsBox.setLabelText("Input channels");
        inputChannelsBox.onChange = [this](unsigned newId) { this->deviceManager.getActiveDevice().setInputChannels(newId); };
        addAndMakeVisible(inputChannelsBox);

        outputChannelsBox.setLabelText("Output channels");
        outputChannelsBox.onChange = [this](unsigned newId) { this->deviceManager.getActiveDevice().setOutputChannels(newId); };
        addAndMakeVisible(outputChannelsBox);

        sampleRateBox.setLabelText("Sample rate");
        sampleRateBox.onChange = [this](unsigned newId) { this->deviceManager.getActiveDevice().setSampleRate(newId); };
        addAndMakeVisible(sampleRateBox);

        bufferSizeBox.setLabelText("Buffer size");
        bufferSizeBox.onChange = [this](unsigned newId) { this->deviceManager.getActiveDevice().setBufferSize(newId); };
        addAndMakeVisible(bufferSizeBox);

        advancedButton.setLabelText("Advanced");
        advancedButton.setButtonText("Open driver dialog...");
        advancedButton.onClick = [this]() { this->deviceManager.getActiveDevice().showControlPanel(); };
        addAndMakeVisible(advancedButton);

        startStopButton.onClick = [this]()
        {
            if (this->deviceManager.getActiveDevice().isPlaying())
            {
                this->deviceManager.stop();

                updateDevices();
                updateControls();
                updateState();
            }
            else
            {
                if (auto error = this->deviceManager.start())
                {
                    this->debugOutput.addLine("Failed to start playback - " + *error);
                }
                
                updateState();
            }
        };
        addAndMakeVisible(startStopButton);

        this->deviceManager.changeCallback = [this]()
        {
            auto oldDevice = getSelectedDevice();

            // Hide popups as they will not be updated when active
            deviceBox.hidePopup();
            inputChannelsBox.hidePopup();
            outputChannelsBox.hidePopup();
            sampleRateBox.hidePopup();
            bufferSizeBox.hidePopup();

            updateDevices();
            updateControls();
            updateState();

            auto newDevice = getSelectedDevice();

            if (oldDevice && (!newDevice || *oldDevice != *newDevice))
            {
                std::string message = "Device " + oldDevice->composedName() + " has been disconnected.";
                juce::AlertWindow::showAsync(juce::MessageBoxOptions().withButton("Ok").withMessage(message), nullptr);
            }
        };

        updateDevices();
        updateControls();
        updateState();
    }

private:
    DebugOutput& debugOutput;
    AudioDeviceManager& deviceManager;

    std::vector<AudioDevice::Name> listedDevices;

    LabeledCombo deviceBox;
    LabeledCombo inputChannelsBox;
    LabeledCombo outputChannelsBox;
    LabeledCombo sampleRateBox;
    LabeledCombo bufferSizeBox;
    LabeledButton advancedButton;

    juce::TextButton startStopButton;

    std::optional<AudioDevice::Name> getSelectedDevice() const
    {
        if (deviceBox.getSelectedId() < listedDevices.size())
        {
            return listedDevices[deviceBox.getSelectedId()];
        }

        return std::nullopt;
    }

    void updateDevices()
    {
        deviceBox.clear();
        listedDevices.clear();

        auto selectedDevice = deviceManager.getActiveDevice().getName();

        for (const auto& typeName : deviceManager.getAvailableTypes())
        {
            deviceBox.addSectionHeading(typeName);

            for (const auto& deviceName : deviceManager.getAvailableDevicesForType(typeName))
            {
                unsigned id = static_cast<unsigned>(listedDevices.size());

                deviceBox.addItem(deviceName, id);

                if (selectedDevice && selectedDevice->typeName == typeName && selectedDevice->deviceName == deviceName)
                {
                    deviceBox.setSelectedId(id);
                }

                listedDevices.push_back({ typeName, deviceName });
            }

            deviceBox.addSeparator();
        }
    }

    void updateControls()
    {
        inputChannelsBox.clear();
        outputChannelsBox.clear();
        sampleRateBox.clear();
        bufferSizeBox.clear();

        const auto& activeDevice = deviceManager.getActiveDevice();

        if (activeDevice.isAvailable())
        {
            for (unsigned i = 0; i <= activeDevice.getAvailableInputChannels(); ++i)
            {
                inputChannelsBox.addItem(std::to_string(i), i);
            }
            inputChannelsBox.setSelectedId(activeDevice.getInputChannels());

            for (unsigned i = 0; i <= activeDevice.getAvailableOutputChannels(); ++i)
            {
                outputChannelsBox.addItem(std::to_string(i), i);
            }
            outputChannelsBox.setSelectedId(activeDevice.getOutputChannels());

            for (const auto& sampleRate : activeDevice.getAvailableSampleRates())
            {
                sampleRateBox.addItem(std::to_string(sampleRate), sampleRate);
            }
            sampleRateBox.setSelectedId(activeDevice.getSampleRate());

            for (const auto& bufferSize : activeDevice.getAvailableBufferSizes())
            {
                bufferSizeBox.addItem(std::to_string(bufferSize), bufferSize);
            }
            bufferSizeBox.setSelectedId(activeDevice.getBufferSize());
        }
    }

    void updateState()
    {
        const auto& activeDevice = deviceManager.getActiveDevice();

        bool isAvailable = activeDevice.isAvailable();
        bool isPlaying = activeDevice.isPlaying();
        bool hasInputs = activeDevice.hasInputs();
        bool hasOutputs = activeDevice.hasOutputs();
        bool hasControlPanel = activeDevice.hasControlPanel();

        deviceBox.setEnabled(!isPlaying);
        inputChannelsBox.setEnabled(!isPlaying && isAvailable && hasInputs);
        outputChannelsBox.setEnabled(!isPlaying && isAvailable && hasOutputs);
        sampleRateBox.setEnabled(!isPlaying && isAvailable);
        bufferSizeBox.setEnabled(!isPlaying && isAvailable);
        advancedButton.setEnabled(!isPlaying && isAvailable && hasControlPanel);
        startStopButton.setEnabled(isPlaying || isAvailable);

        startStopButton.setButtonText(isPlaying ? "Stop" : "Start");
    }

    void paint(juce::Graphics& g) override
    {
        g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));
    }

    void resized() override
    {
        auto area = getLocalBounds();

        deviceBox.setBounds(area.removeFromTop(30).reduced(3));
        inputChannelsBox.setBounds(area.removeFromTop(30).reduced(3));
        outputChannelsBox.setBounds(area.removeFromTop(30).reduced(3));
        sampleRateBox.setBounds(area.removeFromTop(30).reduced(3));
        bufferSizeBox.setBounds(area.removeFromTop(30).reduced(3));
        advancedButton.setBounds(area.removeFromTop(30).reduced(3));

        startStopButton.setBounds(area.removeFromBottom(30).reduced(3));
    }
};
