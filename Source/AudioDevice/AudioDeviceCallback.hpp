/*
  ==============================================================================

    AudioDeviceCallback.hpp
    Created: 19 Oct 2023 2:04:17pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "AudioDevice.hpp"
#include "../AudioProcessor/AudioCallbackBase.hpp"
#include "../AudioProcessor/Utils/RealTimeContainers/RTCAtomicRingBuffer.hpp"
#include "../AudioProcessor/Utils/Time.hpp"
#include "../AudioProcessor/Utils/Assert.hpp"

class AudioDeviceCallback :
    public juce::AudioIODeviceCallback,
    public AudioCallback::Base,
    private juce::Timer
{
protected:
    AudioDeviceCallback(AudioCallback::DebugOutput& debugOutput) :
        AudioCallback::Base(debugOutput)
    {
        usage.setMeasurementFrequency(TimerFrequency);

        startTimerHz(TimerFrequency);
    }

    virtual ~AudioDeviceCallback() = default;

    unsigned getNumInputChannels() const override { return currentNumInputChannels; }
    unsigned getNumOutputChannels() const override { return currentNumOutputChannels; }
    unsigned getSampleRate() const override { return currentSampleRate; }
    unsigned getBlockSize() const override { return currentBlockSize; }

private:
    static constexpr unsigned TimerFrequency{ 25 };

    RTC::AtomicRingBuffer<Time::Measurement, 256> measurementBuffer;

    AudioSignalWithChannels inputData;
    AudioSignalWithChannels outputData;

    unsigned currentNumInputChannels{ 0 };
    unsigned currentNumOutputChannels{ 0 };
    unsigned currentSampleRate{ 0 };
    unsigned currentBlockSize{ 0 };

    void updateState(AudioCallback::State newState, juce::AudioIODevice* devicePtr = nullptr)
    {
        if (newState != state)
        {
            state = newState;

            switch (state)
            {
            case AudioCallback::State::Stopped:
                debugOutput.addLine("Playback stopped");
                break;

            case AudioCallback::State::Loading:
                debugOutput.addLine("Starting playback...");
                break;

            case AudioCallback::State::Running:
                ASSERT(devicePtr != nullptr);
                debugOutput.addLine(
                    "Playback started (device: " + AudioDevice::Name(*devicePtr).composedName() +
                    ", input channels: " + std::to_string(getNumInputChannels()) +
                    ", output channels: " + std::to_string(getNumOutputChannels()) +
                    ", sample rate: " + std::to_string(getSampleRate()) +
                    ", block size: " + std::to_string(getBlockSize()) + ")");
                debugOutput.resetMeasurement();
                break;

            case AudioCallback::State::Unloading:
                debugOutput.addLine("Stopping playback...");
                break;
            }

            if (onStateChange)
            {
                onStateChange(state);
            }
        }
    }

    void audioDeviceAboutToStart(juce::AudioIODevice* device) override
    {
        updateState(AudioCallback::State::Loading);

        currentNumInputChannels = device->getActiveInputChannels().countNumberOfSetBits();
        currentNumOutputChannels = device->getActiveOutputChannels().countNumberOfSetBits();
        currentSampleRate = static_cast<unsigned>(device->getCurrentSampleRate());
        currentBlockSize = device->getCurrentBufferSizeSamples();

        measurementBuffer.clearWithDeallocation();
        usage.setMeasurementFrequency(static_cast<double>(currentSampleRate) / currentBlockSize);

        inputData.allocate(RTC::Size()
            .with<Channels>(currentNumInputChannels)
            .with<Samples>(currentBlockSize));
        outputData.allocate(RTC::Size()
            .with<Channels>(currentNumOutputChannels)
            .with<Samples>(currentBlockSize));

        load();

        updateState(AudioCallback::State::Running, device);
    }

    void audioDeviceIOCallbackWithContext(
        const float* const* inputChannelData,
        [[maybe_unused]] int numInputDataChannels,
        float* const* outputChannelData,
        [[maybe_unused]] int numOutputDataChannels,
        [[maybe_unused]] int numSamples,
        const juce::AudioIODeviceCallbackContext& /*context*/) override
    {
        ASSERT(numInputDataChannels == static_cast<int>(currentNumInputChannels));
        ASSERT(numOutputDataChannels == static_cast<int>(currentNumOutputChannels));
        ASSERT(numSamples == static_cast<int>(currentBlockSize));

        Time::Measurement measurement;
        measurement.setStart();

        for (auto& channel : inputData)
        {
            const auto* samplePtr = *inputChannelData++;

            if constexpr (std::is_same_v<AudioValueType, float>)
            {
                std::memcpy(&channel.sample(0), samplePtr, channel.nSamples() * sizeof(float));
            }
            else
            {
                for (auto& sample : channel)
                {
                    sample = static_cast<AudioValueType>(*samplePtr++);
                }
            }
        }

        outputData.setToZero();

        process(inputData, outputData);

        for (auto& channel : outputData)
        {
            auto* samplePtr = *outputChannelData++;

            if constexpr (std::is_same_v<AudioValueType, float>)
            {
                std::memcpy(samplePtr, &channel.sample(0), channel.nSamples() * sizeof(float));
            }
            else
            {
                for (auto& sample : channel)
                {
                    *samplePtr++ = static_cast<float>(sample);
                }
            }
        }

        measurement.setEnd();
        measurementBuffer.push(measurement);
    }

    void audioDeviceStopped() override
    {
        updateState(AudioCallback::State::Unloading);

        unload();

        inputData.deallocate();
        outputData.deallocate();

        usage.setMeasurementFrequency(TimerFrequency);

        updateState(AudioCallback::State::Stopped);
    }

    void audioDeviceError(const juce::String& errorMessage) override
    {
        debugOutput.addError("Playback error: " + errorMessage.toStdString());

        if (onError)
        {
            onError(errorMessage.toStdString());
        }
    }

    void timerCallback() override
    {
        if (isRunning())
        {
            for (Time::Measurement measurement; measurementBuffer.pop(measurement);)
            {
                debugOutput.addMeasurement(measurement);
                usage.addMeasurement(measurement.getDuration());
            }

            if (auto overflowCount = measurementBuffer.getResetOverflowCount(); overflowCount > 0)
            {
                debugOutput.addError("Measurement buffer overflows: " + std::to_string(overflowCount));
                debugOutput.invalidateLastMeasurement();
            }
        }
        else
        {
            usage.addMeasurement(0);
        }

        timer();
    }
};
