/*
  ==============================================================================

    DeviceManager.h
    Created: 17 Oct 2023 10:38:47am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include <vector>
#include <string>

#include "AudioDevice.hpp"
#include "AudioDeviceCallback.hpp"

#include "../DebugOutput/DebugOutput.hpp"
#include "../Utils/ContainerHelper.hpp"

class AudioDeviceManager : public juce::AudioIODeviceType::Listener
{
public:

    AudioDeviceManager(AudioDeviceCallback& callback, DebugOutput& debugOutput) :
        callback(callback),
        debugOutput(debugOutput)
    {
        if (auto asio = juce::AudioIODeviceType::createAudioIODeviceType_ASIO())
        {
            types.push_back(std::unique_ptr<juce::AudioIODeviceType>(asio));
        }

        if(auto wasapi = juce::AudioIODeviceType::createAudioIODeviceType_WASAPI(juce::WASAPIDeviceMode::shared))
        {
            types.push_back(std::unique_ptr<juce::AudioIODeviceType>(wasapi));
        }

        if(auto directSound = juce::AudioIODeviceType::createAudioIODeviceType_DirectSound())
        {
            types.push_back(std::unique_ptr<juce::AudioIODeviceType>(directSound));
        }

        if (auto jack = juce::AudioIODeviceType::createAudioIODeviceType_JACK())
        {
            types.push_back(std::unique_ptr<juce::AudioIODeviceType>(jack));
        }

        if (auto alsa = juce::AudioIODeviceType::createAudioIODeviceType_ALSA())
        {
            types.push_back(std::unique_ptr<juce::AudioIODeviceType>(alsa));
        }

        if (auto coreAudio = juce::AudioIODeviceType::createAudioIODeviceType_CoreAudio())
        {
            types.push_back(std::unique_ptr<juce::AudioIODeviceType>(coreAudio));
        }

        selectDefaultDevice();

        for (auto& type : types)
        {
            type->addListener(this);
        }
    }

    ~AudioDeviceManager()
    {
        for (auto& type : types)
        {
            type->removeListener(this);
        }
    }

    std::optional<std::string> start()
    {
        return activeDevice.start(&callback);
    }

    void stop()
    {
        activeDevice.stop();
    }

    std::vector<std::string> getAvailableTypes() const
    {
        std::vector<std::string> typeNames;

        for (const auto& type : types)
        {
            typeNames.push_back(type->getTypeName().toStdString());
        }

        return typeNames;
    }

    std::vector<std::string> getAvailableDevicesForType(const std::string& typeName)
    {
        std::vector<std::string> deviceNames;

        if (auto type = getTypePointer(typeName))
        {
            type->scanForDevices();

            for (const auto& deviceName : type->getDeviceNames())
            {
                deviceNames.push_back(deviceName.toStdString());
            }
        }

        return deviceNames;
    }

    std::vector<AudioDevice::Name> getAvailableDevices()
    {
        std::vector<AudioDevice::Name> availableDevices;

        for (const auto& typeName : getAvailableTypes())
        {
            for (const auto& deviceName : getAvailableDevicesForType(typeName))
            {
                availableDevices.push_back({ typeName, deviceName });
            }
        }

        return availableDevices;
    }

    bool selectDevice(const AudioDevice::Name& device)
    {
        if (!activeDevice.isPlaying())
        {
            if (auto type = getTypePointer(device.typeName))
            {
                activeDevice.unload();

                if (auto createdDevice = type->createDevice(device.deviceName, device.deviceName))
                {
                    activeDevice.load(createdDevice);

                    return true;
                }
            }
        }

        return false;
    }

    bool selectDefaultDevice()
    {
        if (!activeDevice.isPlaying())
        {
            for (const auto& typeName : getAvailableTypes())
            {
                auto deviceNames = getAvailableDevicesForType(typeName);

                if (!deviceNames.empty())
                {
                    selectDevice(AudioDevice::Name(typeName, deviceNames[0]));

                    return true;
                }
            }

            activeDevice.unload();
        }

        return false;
    }

    void setMode(AudioDevice::Mode mode) { activeDevice.setMode(mode); }
    void setSampleRateFilter(AudioDevice::SampleRateFilter filter) { activeDevice.setSampleRateFilter(filter); }

    AudioDevice& getActiveDevice() { return activeDevice; }
    const AudioDevice& getActiveDevice() const { return activeDevice; }

    std::function<void()> changeCallback;

private:
    static constexpr unsigned DefaultSampleRate = 44100;
    static constexpr unsigned DefaultBufferSize = 256;

    AudioDeviceCallback& callback;

    DebugOutput& debugOutput;

    std::vector<std::unique_ptr<juce::AudioIODeviceType>> types;

    AudioDevice activeDevice;

    juce::AudioIODeviceType* getTypePointer(const std::string& typeName)
    {
        for (auto& type : types)
        {
            if (type->getTypeName().compare(typeName) == 0)
            {
                return type.get();
            }
        }

        return nullptr;
    }

    void notifyChange()
    {
        if (changeCallback)
        {
            changeCallback();
        }
    }

    void audioDeviceListChanged() override
    {
        debugOutput.addLine("Devices have changed");

        auto activeDeviceName = activeDevice.getName();
        bool isActiveDeviceStillAvailable = activeDeviceName && ContainerHelper::contains(getAvailableDevices(), *activeDeviceName);

        if (!activeDevice.isPlaying())
        {
            debugOutput.addLine("Updating devices");

            if (isActiveDeviceStillAvailable)
            {
                selectDevice(*activeDeviceName);
                notifyChange();
            }
            else
            {
                selectDefaultDevice();
                notifyChange();
            }
        }
        else
        {
            if (!isActiveDeviceStillAvailable)
            {
                debugOutput.addError(activeDeviceName->composedName() + ": Device has been disconnected");

                activeDevice.stop();
                selectDefaultDevice();
                notifyChange();
            }
            else if (auto error = activeDevice.getLastError())
            {
                debugOutput.addError(activeDeviceName->composedName() + ": " + *error);

                activeDevice.stop();
                selectDevice(*activeDeviceName);
                notifyChange();
            }
        }
    }
};
