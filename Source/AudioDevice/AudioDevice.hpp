/*
  ==============================================================================

    AudioDevice.hpp
    Created: 19 Oct 2023 10:06:37am
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

#include "../Utils/ContainerHelper.hpp"
#include "../AudioProcessor/Utils/IsPowerOfTwo.hpp"

class AudioDevice
{
public:
    struct Name
    {
        std::string typeName;
        std::string deviceName;

        Name(const std::string& typeName, const std::string& deviceName)
            : typeName(typeName), deviceName(deviceName)
        {}

        Name(const juce::AudioIODevice& device) :
            typeName(device.getTypeName().toStdString()), deviceName(device.getName().toStdString())
        {}

        std::string composedName() const
        {
            return deviceName + " (" + typeName + ")";
        }

        bool operator==(const Name& other) const
        {
            return typeName == other.typeName && deviceName == other.deviceName;
        }

        bool operator!=(const Name& other) const
        {
            return !(*this == other);
        }
    };

    struct Settings
    {
        unsigned nInputChannels{ 0 };
        unsigned nOutputChannels{ 0 };

        unsigned sampleRate{ 0 };
        unsigned bufferSize{ 0 };

        Settings(unsigned nInputChannels, unsigned nOutputChannels, unsigned sampleRate, unsigned bufferSize)
            : nInputChannels(nInputChannels), nOutputChannels(nOutputChannels), sampleRate(sampleRate), bufferSize(bufferSize)
        {}
    };

    enum class Mode
    {
        InputOnly,
        OutputOnly,
        Duplex
    };

    enum class SampleRateFilter
    {
        AllowAll,
        AllowOnlyPowerOfTwo
    };

    AudioDevice() :
        settings(DefaultInputChannels, DefaultOutputChannels, DefaultSampleRate, DefaultBufferSize)
    {}

    ~AudioDevice()
    {
        stop();
        unload();
    }

    bool load(juce::AudioIODevice * device)
    {
        if (!isPlaying())
        {
            if (activeDevice && activeDevice->isOpen())
            {
                activeDevice->close();
            }

            activeDevice = std::unique_ptr<juce::AudioIODevice>(device);

            if (isAvailable())
            {
                settings.nInputChannels = hasInputs() ? std::min(settings.nInputChannels, getAvailableInputChannels()) : 0;
                settings.nOutputChannels = hasOutputs() ? std::min(settings.nOutputChannels, getAvailableOutputChannels()) : 0;
                settings.sampleRate = ContainerHelper::findClosest(getAvailableSampleRates(), settings.sampleRate);
                settings.bufferSize = ContainerHelper::findClosest(getAvailableBufferSizes(), settings.bufferSize);
            }

            return true;
        }

        return false;
    }

    void unload()
    {
        if (activeDevice && activeDevice->isOpen())
        {
            activeDevice->close();
        }

        activeDevice.reset();
    }

    std::optional<std::string> start(juce::AudioIODeviceCallback* callback)
    {
        if (isLoaded() && isAvailable() && !isPlaying())
        {
            if (activeDevice->isOpen())
            {
                activeDevice->close();
            }

            juce::BigInteger inputChannels, outputChannels;
            inputChannels.setRange(0, settings.nInputChannels, true);
            outputChannels.setRange(0, settings.nOutputChannels, true);

            auto error = activeDevice->open(inputChannels, outputChannels, settings.sampleRate, settings.bufferSize);

            if(error.isNotEmpty())
            {
                return "Failed to open device (" + error.toStdString() + ")";
            }

            if (static_cast<unsigned>(activeDevice->getCurrentSampleRate()) != settings.sampleRate)
            {
                return "Failed to set sample rate (current: " + std::to_string(activeDevice->getCurrentSampleRate()) + ", requested: " + std::to_string(settings.sampleRate) + ")";
            }

            if (static_cast<unsigned>(activeDevice->getCurrentBufferSizeSamples()) != settings.bufferSize)
            {
                return "Failed to set buffer size (current: " + std::to_string(activeDevice->getCurrentBufferSizeSamples()) + ", requested: " + std::to_string(settings.bufferSize) + ")";
            }

            activeDevice->start(callback);
        }

        return std::nullopt;
    }

    void stop()
    {
        if (isPlaying())
        {
            activeDevice->stop();
        }
    }

    bool isLoaded() const
    {
        return activeDevice.operator bool();
    }

    bool isLoaded(const Name& name) const
    {
        return getName() ? name == *getName() : false;
    }

    bool isAvailable() const
    {
        bool inputChannelsAvailable = (!hasInputs() || getAvailableInputChannels() > 0);
        bool outputChannelsAvailable = (!hasOutputs() || getAvailableOutputChannels() > 0);
        bool sampleRateAvailable = getAvailableSampleRates().size() > 0;
        bool bufferSizeAvailable = getAvailableBufferSizes().size() > 0;

        return inputChannelsAvailable && outputChannelsAvailable && sampleRateAvailable && bufferSizeAvailable;
    }

    bool isPlaying() const
    {
        return activeDevice && activeDevice->isPlaying();
    }

    std::optional<Name> getName() const
    {
        if (activeDevice)
        {
            return Name(*activeDevice);
        }

        return std::nullopt;
    }

    unsigned getAvailableInputChannels() const
    {
        return activeDevice ? activeDevice->getInputChannelNames().size() : 0;
    }

    unsigned getAvailableOutputChannels() const
    {
        return activeDevice ? activeDevice->getOutputChannelNames().size() : 0;
    }

    std::vector<unsigned> getAvailableSampleRates() const
    {
        if (activeDevice)
        {
            return ContainerHelper::toVector<unsigned>(activeDevice->getAvailableSampleRates());
        }

        return std::vector<unsigned>();
    }

    std::vector<unsigned> getAvailableBufferSizes() const
    {
        if (activeDevice)
        {
            std::function<bool(unsigned)> filter;

            switch (sampleRateFilter)
            {
            case SampleRateFilter::AllowOnlyPowerOfTwo:
                filter = IsPowerOfTwo;
                break;

            default:
                break;
            }

            return ContainerHelper::toVector<unsigned>(activeDevice->getAvailableBufferSizes(), filter);
        }

        return std::vector<unsigned>();
    }

    bool setInputChannels(unsigned nInputChannels)
    {
        if (isAvailable() && nInputChannels <= getAvailableInputChannels())
        {
            settings.nInputChannels = nInputChannels;
            return true;
        }

        return false;
    }

    bool setOutputChannels(unsigned nOutputChannels)
    {
        if (isAvailable() && nOutputChannels <= getAvailableOutputChannels())
        {
            settings.nOutputChannels = nOutputChannels;
            return true;
        }

        return false;
    }

    bool setSampleRate(unsigned sampleRate)
    {
        if (isAvailable() && ContainerHelper::contains(getAvailableSampleRates(), sampleRate))
        {
            settings.sampleRate = sampleRate;
            return true;
        }

        return false;
    }

    bool setBufferSize(unsigned bufferSize)
    {
        if (isAvailable() && ContainerHelper::contains(getAvailableBufferSizes(), bufferSize))
        {
            settings.bufferSize = bufferSize;
            return true;
        }

        return false;
    }

    std::optional<std::string> getLastError() const
    {
        if (activeDevice)
        {
            auto lastError = activeDevice->getLastError().toStdString();

            if (!lastError.empty())
            {
                return lastError;
            }
        }

        return std::nullopt;
    }

    unsigned getInputChannels() const { return settings.nInputChannels; }
    unsigned getOutputChannels() const { return settings.nOutputChannels; }
    unsigned getSampleRate() const { return settings.sampleRate; }
    unsigned getBufferSize() const { return settings.bufferSize; }

    bool hasInputs() const { return mode != Mode::OutputOnly; }
    bool hasOutputs() const { return mode != Mode::InputOnly; }

    bool hasControlPanel() const { return activeDevice && activeDevice->hasControlPanel(); }
    void showControlPanel() { if (hasControlPanel()) { activeDevice->showControlPanel(); } }

    void setMode(Mode wanted) { mode = wanted; }
    bool isMode(Mode expected) const { return mode == expected; }
    Mode getMode() const { return mode; }

    void setSampleRateFilter(SampleRateFilter wanted) { sampleRateFilter = wanted; }
    bool isSampleRateFilter(SampleRateFilter expected) const { return sampleRateFilter == expected; }
    SampleRateFilter getSampleRateFilter() const { return sampleRateFilter; }

private:
    static constexpr unsigned DefaultInputChannels = 2;
    static constexpr unsigned DefaultOutputChannels = 2;
    static constexpr unsigned DefaultSampleRate = 44100;
    static constexpr unsigned DefaultBufferSize = 256;

    std::unique_ptr<juce::AudioIODevice> activeDevice;

    Settings settings;

    Mode mode{ Mode::OutputOnly };
    SampleRateFilter sampleRateFilter{ SampleRateFilter::AllowOnlyPowerOfTwo };
};
