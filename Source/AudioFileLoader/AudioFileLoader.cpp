/*
  ==============================================================================

    AudioFileLoader.cpp
    Created: 4 Nov 2023 3:12:43pm
    Author:  Kevin

  ==============================================================================
*/

#include "AudioFileLoader.hpp"

#include <JuceHeader.h>

#include "../Utils/FilenameHelper.hpp"

AudioFile AudioFileLoader::load(const std::string& filename, bool mono)
{
    if (FilenameHelper::appendix(filename) == "testsource")
    {
        return AudioFile(FilenameHelper::wihtoutPath(filename));
    }

    AudioFile file(filename);

    juce::AudioFormatManager manager;
    manager.registerBasicFormats();

    juce::File juceFile(filename);

    if (!juceFile.existsAsFile())
    {
        file.error = "Can't open file " + filename;
        return file;
    }

    auto reader = std::unique_ptr<juce::AudioFormatReader>(manager.createReaderFor(juceFile));

    if (!reader)
    {
        file.error = "Failed to read file " + filename;
        return file;
    }
    
    if (file.sampleRate = static_cast<unsigned>(reader->sampleRate); file.sampleRate == 0)
    {
        file.error = "Sample rate: " + std::to_string(file.sampleRate);
        return file;
    }

    if (file.nChannels = reader->numChannels; file.nChannels == 0)
    {
        file.error = "Number of channels: " + std::to_string(file.nChannels);
        return file;
    }

    if (file.nSamples = reader->lengthInSamples; file.nSamples == 0)
    {
        file.error = "Number of samples: " + std::to_string(file.nSamples);
        return file;
    }

    juce::AudioBuffer<AudioValueType> buffer;
    buffer.setSize(static_cast<int>(file.nChannels), static_cast<int>(file.nSamples));

    if (!reader->read(&buffer, 0, static_cast<int>(file.nSamples), 0, true, true))
    {
        file.error = "Failed to read data";
        return file;
    }

    if (mono)
    {
        file.signalWithChannels.allocate(RTC::Size()
            .with<Channels>(1)
            .with<Samples>(file.nSamples));

        const auto* const* channelPtr = buffer.getArrayOfReadPointers();
        for (size_t iChannel = 0; iChannel < file.nChannels; ++iChannel)
        {
            const auto* samplePtr = *channelPtr++;
            for (auto& sample : file.signalWithChannels.channel(0))
            {
                sample += *samplePtr++ / file.nChannels;
            }
        }

        file.nChannels = 1;
    }
    else
    {
        file.signalWithChannels.allocate(RTC::Size()
            .with<Channels>(file.nChannels)
            .with<Samples>(file.nSamples));

        const auto* const* channelPtr = buffer.getArrayOfReadPointers();
        for (auto& channel : file.signalWithChannels)
        {
            const auto* samplePtr = *channelPtr++;
            for (auto& sample : channel)
            {
                sample = *samplePtr++;
            }
        }
    }

    return file;
}