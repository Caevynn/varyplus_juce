/*
  ==============================================================================

    AudioFileLoader.h
    Created: 4 Nov 2023 3:12:43pm
    Author:  Kevin

  ==============================================================================
*/

#pragma once

#include "../AudioProcessor/AudioFile.hpp"

class AudioFileLoader : public AudioFile::LoaderBase
{
public:
    AudioFile load(const std::string& filename, bool mono = false) override;
};
