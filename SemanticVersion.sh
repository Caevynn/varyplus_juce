#! /bin/bash

NUMBER_PATTERN='^[0-9]+$'
SEMVER_PATTERN='^v[0-9]+\.[0-9]+\.[0-9]+$'

function look_for_valid_tag()
{
	LATEST_VERSION=v0.0.0
	VALID_TAG_FOUND=false
	
	ALL_TAGS=`git tag --list v[0-9]*.[0-9]*.[0-9]* --sort=-version:refname --merged`
	
	for tag in $ALL_TAGS
	do
		if [[ $tag =~ $SEMVER_PATTERN ]]
		then
			LATEST_VERSION=$tag
			VALID_TAG_FOUND=true
			return
		fi
	done
}

function parse_commit_messages()
{
	if [[ "$VALID_TAG_FOUND" == true ]]
	then
		COMMIT_MESSAGES=`git log --oneline --reverse ${LATEST_VERSION}..HEAD | awk '{print $2}'`
	else
		COMMIT_MESSAGES=`git log --oneline --reverse | awk '{print $2}'`
	fi
	
	MAJOR=`echo $LATEST_VERSION | sed s/v// | awk -F. '{print $1}'`
	MINOR=`echo $LATEST_VERSION | sed s/v// | awk -F. '{print $2}'`
	PATCH=`echo $LATEST_VERSION | sed s/v// | awk -F. '{print $3}'`
		
	for commit_message in $COMMIT_MESSAGES
	do
		if [[ "$commit_message" == "BREAKING" ]]
		then
			((MAJOR++))
			MINOR=0
			PATCH=0
		elif [[ "$commit_message" == "feat:" ]]
		then
			((MINOR++))
			PATCH=0
		elif [[ "$commit_message" == "fix:" ]]
		then
			((PATCH++))
		fi
	done
	
	NEW_VERSION=v$MAJOR.$MINOR.$PATCH
}

function check_branch_name()
{
	BRANCH=`git rev-parse --abbrev-ref HEAD`
	
	if [[ "$BRANCH" != master ]]
	then
		NEW_VERSION=${NEW_VERSION}_$BRANCH
	fi
}

function write_header_file()
{
	mkdir -p SemanticVersion
	
	HEADER_FILE=SemanticVersion/SemanticVersion.hpp
	
	echo "#pragma once" > $HEADER_FILE
	echo "" >> $HEADER_FILE
	echo "#include <string_view>" >> $HEADER_FILE
	echo "" >> $HEADER_FILE
	echo "static constexpr std::string_view SemanticVersion = \"${NEW_VERSION}\";" >> $HEADER_FILE
	echo "" >> $HEADER_FILE
}

function write_tag()
{
	if [[ $NEW_VERSION != $LATEST_VERSION ]]
	then
		git tag $NEW_VERSION --force
		git push origin $NEW_VERSION --force
	fi
}

look_for_valid_tag
parse_commit_messages
check_branch_name
write_header_file
write_tag

